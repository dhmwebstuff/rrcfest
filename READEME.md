# Reggae Roots Culture Festival

Based off [Divi](https://www.elegantthemes.com/gallery/divi/) theme older version.
Custom post Types added for custom website
  
# Install

  - zip rrcfest_them and import as normal theme
  - zip all plugin folder individually and install each plugin
 
# Editing
### General
- **image url needs to be entered for image to show. Select image in feature image and copy the url to the image url **
 #### Front page
- Use the Home Boxes
-- **DO NOT CHANGE THE TITLE OF HOME BOXES**
-- **CHANGE THE BOXES IN THE 'TEXT' (NOT THE VISUAL) EDITOR**

#### Artists
- **Festival Year is important for correct artists to show up**
- **Performance time will need to be changed to show start and end time i.e. 7pm-pm**
- images need to be resized to 1440 x 1080 for best results

#### Sponsors
- images need to be equal in width and height i.e. 150 x 150

#### Tickets
- for ticket price only need to add dollar amount. the $ is automatically added.

#### Media
- youtube link can be directly added to conted for videos

