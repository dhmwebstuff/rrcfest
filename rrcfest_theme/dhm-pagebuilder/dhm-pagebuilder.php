<?php

define( 'ET_BUILDER_THEME', true );
function dhm_setup_builder() {
	define( 'ET_BUILDER_DIR', get_template_directory() . '/includes/builder/' );
	define( 'ET_BUILDER_URI', get_template_directory_uri() . '/includes/builder' );
	define( 'ET_BUILDER_LAYOUT_POST_TYPE', 'dhm_pb_layout' );

	$theme_version = dhm_get_theme_version();
	define( 'ET_BUILDER_VERSION', $theme_version );

	load_theme_textdomain( 'dhm_builder', ET_BUILDER_DIR . 'languages' );
	require ET_BUILDER_DIR . 'framework.php';

	dhm_pb_register_posttypes();
}
add_action( 'init', 'dhm_setup_builder' );