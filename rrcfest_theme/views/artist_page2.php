<?php
/*
Template Name: Artists Rrcfest Page 2
*/

get_header();

$is_page_builder_used = dhm_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div class="main-content">

    <div class="container">
        <h1 class="main_title"><?php the_title(); ?></h1>
        <div class="artistContainer"></div>            
</div>
</div> <!-- #main-content -->
<script>
    (function($){
        $( document ).ready(function(){
           
                $.get("../wp-json/wp/v2/rrcf_artists", function(data, status){
                    var artistData = data;
                    var artistLink = [];
                    var artistID;
                    _.each(artistData, function( value_artistData, index_artistData){

                        var artistTitle = value_artistData['title']['rendered'];
                        var artistImage = value_artistData['rrcf_artists_meta']['imageURL'];
                        var artistLinks = value_artistData['rrcf_artists_meta']['artistLinks'];
                        var artistFacebook = value_artistData['rrcf_artists_meta']['artistLinksFb'];
                        var artistTwitter = value_artistData['rrcf_artists_meta']['artistLinksTw'];
                        var artistInstagram = value_artistData['rrcf_artists_meta']['artistLinksInsta'];
                        var artistYoutube = value_artistData['rrcf_artists_meta']['artistLinksYt'];
                        var performanceTime = value_artistData['rrcf_artists_meta']['performanceTime'];

                        var artistLinkObject = $('<li class="artistSite"><a href="" class="artistSite">Website</a> | </li>');
                        var artistFacebookObject = $('<li class="artistFB"><a href="" class="artistFB">Facebook</a> | </li>');
                        var artistTwitterObject = $('<li class="artistTW"><a href="" class="artistTW">Twitter</a> | </li>');
                        var artistInstagramObject = $('<li class="artistIN"><a href="" class="artistIN">Instagram</a> | </li>');
                        var artistYoutubeObject = $('<li class="artistYT"><a href="" class="artistYT">Youtube</a></li>');


                        var artistSmallReturnObject = $('<div class="artistBlockSmall"><div class="artistImageSmall"></div><div class="artistTitleContainer displayTable"><div class="artistTitleSmall"></div><div class="moreInfo"><span class="artistCircle">P</span></div></div>');
                        var artistLargeReturnObject = $('<div class="artistBlockLarge displayNone"><div class="close"><span class="artistClose">Q</span></div><div class="artistLargeContainer displayTable"><div class="artistImageLarge"></div><div class="artistInfoContainer"><div class="artistTitleLarge"></div><div class="artistLinks"><ul class="artistList"></ul></div><div class="performing"><h2>Performing</h2><div class="pTime"></div></div></div></div></div>');
                        
                        
                        $('.artistContainer').append(artistSmallReturnObject);

                        // append links to dom
                        artistLargeReturnObject.find('.artistList').append(artistLinkObject);
                        artistLargeReturnObject.find('.artistList').append(artistFacebookObject);
                        artistLargeReturnObject.find('.artistList').append(artistTwitterObject);
                        artistLargeReturnObject.find('.artistList').append(artistInstagramObject);
                        artistLargeReturnObject.find('.artistList').append(artistYoutubeObject);

                        // remove links if blank
                        if (artistLinks == "") {
                           artistLargeReturnObject.find('.artistSite').remove();
                        };
                        if (artistFacebook == "") {
                           artistLargeReturnObject.find('.artistFB').remove();
                        };
                        if (artistTwitter == "") {
                           artistLargeReturnObject.find('.artistTW').remove();
                        };
                        if (artistInstagram == "") {
                           artistLargeReturnObject.find('.artistIN').remove();
                        };

                        if (artistYoutube == "") {
                           artistLargeReturnObject.find('.artistYT').remove();
                        };

                        // set links
                        artistLinkObject.find('.artistSite').attr('href', 'http://' + artistLinks);
                        artistFacebookObject.find('.artistFB').attr('href', 'http://' + artistFacebook);
                        artistTwitterObject.find('.artistTW').attr('href', 'http://' + artistTwitter);
                        artistInstagramObject.find('.artistIN').attr('href', 'http://' +artistInstagram);
                        artistYoutubeObject.find('.artistYT').attr('href', 'http://' + artistYoutube);

                        // backstretch style
                        // artistSmallReturnObject.find('.artistImageSmall').css({'height': '100px'});
                        // artistLargeReturnObject.find('.artistImageLarge').css({'height': '50px'});
                        $('.artistContainer').append(artistLargeReturnObject);

                        artistSmallReturnObject.find('.artistTitleSmall').append(artistTitle);
                        artistSmallReturnObject.find('.artistImageSmall').backstretch(artistImage)
                        
                        
                        artistLargeReturnObject.find('.artistTitleLarge').append(artistTitle);
                        artistLargeReturnObject.find('.moreInfo').append('');
                        artistLargeReturnObject.find('.artistImageLarge').backstretch(artistImage);
                        artistLargeReturnObject.find('.pTime').append(performanceTime);

                        $('.artistBlockSmall').each(function(value, index){
                            $(this).off('click');
                            $(this).on('click', function(){
                               console.log("clicked")
                            })
                        })
                        
                    })
                })

                
            

           

            
            
        //     $.ajax({
        //       url: "../wp-json/wp/v2/rrcf_artists",
        //       // context: document.body
        //   }).done(function( artistData ) {
        //       _.each(artistData, function( value_artistData, index_artistData){
        //         // console.log(value_artistData);
        //         // set info to variables
        //         var artistImage = value_artistData.rrcf_artists_meta.imageURL;
        //         var artistTitle = value_artistData.title['rendered'];
        //         var artistLinks = value_artistData.rrcf_artists_meta.artistLinks;
        //         var performanceTime = value_artistData.rrcf_artists_meta.performanceTime;

        //         // create object for artist 
        //         var artistReturnObject = $('<div class="artistBlockSmall"><div class="artistImageSmall"></div><div class="artistTitleSmall"></div><div class="moreInfo"></div></div><div class="artistBlockLarge displayNone"><div class="artistImageLarge"></div><div class="artistTitleLarge"><div class="artistLinks"></div><div class="performing"><h2>Performing</h2><div class="pTime"></div></div></div>');

        //         $('.artistContainer').append(artistReturnObject);
        //         // console.log(artistTitle);
        //         artistReturnObject.addClass('artistID' + value_artistData.id );
        //         $('.artistTitleSmall').append(artistTitle);
                    


               
        //       })
        //   });
        })
    })(jQuery);
    </script>
    <script>

    </script>

<?php get_footer(); ?>