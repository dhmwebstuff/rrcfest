<div class="wrap">
	<h2>Website edit FAQ</h2>
	<ul>
		<li>
			<h3>Sitewide</h3>
			<ul>
				<li>Header: Contact Dirty Hands Media</li>
				<li>Footer: Contact Dirty Hands Media</li>
			</ul>
		</li>
		<li>
			<h3>Home</h3>
			<ul>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/edit.php?post_type=rrcf_homeboxes">Edit Homepage</a> The Home Page is divided into three portions
				</li>
			</ul>
		</li>
		<li>
			<h3>About</h3>
			<ul>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/post.php?post=48&action=edit">Edit About Page</a>
				</li>
			</ul>
		</li>
		<li>
			<h3>Lineup</h3>
			<ul>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/post.php?post=653&action=edit">Change Day 1 Date</a>
					<span><strong>*IMPORTANT*</strong></span>
					<span>Do not include "th, nd, or rd" with the date</span>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/post.php?post=655&action=edit">Change Day 2 Date</a>
					<span><strong>*IMPORTANT*</strong></span>
					<span>Do not include "th, nd, or rd" with the date</span>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/edit.php?post_type=rrcf_artists">Add/Edit Artists</a>
				</li>
				<li>
					<p><strong>*IMPORTANT*</strong></p>
					<p>Artist Images need to be resized to 1440px x 1080px. Use <a href="https://www.gimp.org/">GIMP</a> to resize the images</p>
					<p><strong>*IMPORTANT*<br/>Make sure to copy this link</strong> <img src="http://rrcfest.com/wp-content/uploads/2016/04/artist-ss.jpg" alt="" style="width: 100%; height: auto; display: block;"></p>
					<p><strong>*IMPORTANT*<br/>To this area</strong> <img src="http://rrcfest.com/wp-content/uploads/2016/04/artist-img-url-ss.jpg" alt="" style="width: 100%; height: auto; display: block;"></p>
				</li>
			</ul>
		</li>
		<li>
			<h3>Cultural Experiences</h3>
			<ul>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/post.php?post=647&action=edit">Edit Cultural Experiences Page</a>
				</li>
			</ul>
		</li>
		<li>
			<h3>Media</h3>
			<ul>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/post.php?post=65&action=edit">Edit Media Page</a>
				</li>
			</ul>
		</li>
		<li>
			<h3>Tickets</h3>
			<ul>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/edit.php?post_type=rrcf_tickets">Add/Edit Tickets</a>
				</li>
			</ul>
		</li>
		<li>
			<h3>Sponsors</h3>
			<ul>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/edit.php?post_type=rrcf_sponsors">Add/Edit Sponsors</a>
				</li>
				<li>
					<p><strong>*IMPORTANT*</strong></p>
					<p>Sponsor Images need to be resized to 150px x 150px. Use <a href="https://www.gimp.org/">GIMP</a> to resize the images</p>
					<p><strong>*IMPORTANT*<br/>Make sure to copy this link</strong> <img src="http://rrcfest.com/wp-content/uploads/2016/05/sponsor-ss1.png" alt="" style="width: 100%; height: auto; display: block;"></p>
					<p><strong>*IMPORTANT*<br/>To this area</strong> <img src="http://rrcfest.com/wp-content/uploads/2016/05/sponsor-ss2.png" alt="" style="width: 100%; height: auto; display: block;"></p>
				</li>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/post.php?post=172&action=edit">Edit Sponsorship Fact Sheet Link</a>
				</li>
			</ul>
		</li>
		<li>
			<h3>FAQ</h3>

			<ul>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/post.php?post=63&action=edit">Edit FAQ Page</a>
				</li>
			</ul>
		</li>
		<li>
			<h3>Contact</h3>

			<ul>
				<li>
					<a href="<?php echo home_url(); ?>/wp-admin/post.php?post=69&action=edit">Edit Vendor Application Link</a>
				</li>
			</ul>
		</li>
	</ul>
</div>