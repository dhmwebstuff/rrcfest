<?php
/**
 * Template Name: RRCFest Artists v2
 *
 * @package rrcfest
 * 
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="content">
				<?php get_template_part( 'views/output_rrcf_artists' ); ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
	<script type="text/template" id="artists-template">
         <% _.each(artists, function(faq) { %>
         	
        <div><%= faq['attributes']['title']['rendered'] %></div>
        <div> <%= faq['attributes']['content']['rendered'] %></div>
    <% }); %>
    </script>

    <script type="text/javascript">
    (function($){
    	$( document ).ready(function() {
 
    		var Artists = Backbone.Collection.extend({
    			// url:'../wp-json/posts?type=tiff_collec_faq'
    			// url: '../wp-json/wp/v2/posts?type'
    			url: '../wp-json/wp/v2/rrcf_artists'
			
    		});
    		var ArtistsList = Backbone.View.extend({
    			el: '.content',
    			render: function() {
    				var that = this;
    				artistsReturnObject = new Artists();
    				artistsReturnObject.fetch({
    					success: function() {
    						
    						artists = artistsReturnObject["models"];
    						console.log(artists)
    						var template = _.template($('#artists-template').html(), artists);
    						that.$el.html(template);
    						
    					}
    				})
    			}
    		});



    		var Router = Backbone.Router.extend({
    			routes: {
    				"": "lineup"
    			}
    		});

    		var artistsList = new ArtistsList();
    		var router = new Router();
    		router.on('route:lineup', function() {
    			artistsList.render();
    		});
    		Backbone.history.start();
    	});
	console.log(Artists.url);
    })(jQuery);
    </script>

<?php get_footer(); ?>