<?php
/*
Template Name: Artists Rrcfest Page 5
*/

get_header();

$is_page_builder_used = dhm_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div id="main-content">
<div class="overlay displayNone"></div>
    <div class="container">

        <h1 class="main_title"><?php the_title(); ?></h1>
        <div class="lineup_instructions">Click on artist to see performance time</div>
        <div class="artistSmallContainer">
            <div class="day-1">
                <h2 class="first-day"></h2>
            </div>
            <div style="clear: both;"></div>
            <div class="day-2">
                <h2 class="second-day"></h2>
            </div>
        </div>
        <div class="artistListLarge"></div>
        <div style="clear: both;"></div>
    </div>
</div> <!-- #main-content -->
<script src="../wp-content/themes/rrcfest_theme/js/artists.js"></script>


<?php get_footer(); ?>



