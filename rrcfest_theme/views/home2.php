<?php
/*
Template Name: Home 2 Rrcfest
*/

get_header();

$is_page_builder_used = dhm_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div id="main-content">
<div class="overlay displayNone"></div>
    <div class="container">
        <div class="displayTable">
            <div class="displayTableCell">
                <div class="home_boxes_container row"></div>
            </div>
        </div>
    </div>

</div> 
<script>
(function($){
    $( document ).ready(function(){
        
        $.get("../wp-json/wp/v2/rrcf_homeboxes", function(data, status){
            var home_box_data = data;

            _.each(home_box_data, function(value_home_box_data, index_home_box_data){
                console.log(value_home_box_data);
                var home_box_content = value_home_box_data.content.rendered;
                var returnObject = $("<div class='col-md-4 " + "hb_id_" + value_home_box_data.id + "'" + "/>" )
                $('.home_boxes_container').append(returnObject);
                returnObject.append(home_box_content);
                returnObject.addClass(value_home_box_data.slug);
                

            }) 
        })
    })
})(jQuery);
</script>


<?php get_footer(); ?>



