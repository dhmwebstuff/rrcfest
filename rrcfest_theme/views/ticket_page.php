<?php
/*
Template Name: Tickets Rrcfest Page 
*/

get_header();

$is_page_builder_used = dhm_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div id="main-content">

    <div class="container">
        <h1 class="main_title"><?php the_title(); ?></h1>
        <div class="the_content"></div>
       
      <div class="ticket_container row"></div>
</div>
</div> <!-- #main-content -->
<script>
    (function($){
        $( document ).ready(function(){
            $.get("../wp-json/wp/v2/rrcf_tickets", function(data, status){
                var ticketData = data;
                
                _.each(ticketData, function( value_ticketData, index_ticketData){
                    var that = this;
                    console.log(value_ticketData);

                   
                    // fix external link
                    var externalURL = function(urlRequest){
                        return (urlRequest.indexOf("http:") == -1 && urlRequest.indexOf("https:") == -1) ? "http://" + urlRequest : urlRequest;
                    }
                    var ticketTitle = value_ticketData.title.rendered;
                    var ticketPrice = value_ticketData.rrcf_tickets_meta.ticketPrice;
                    var ticketLink = externalURL(value_ticketData.rrcf_tickets_meta.ticketLinks);
                    var moreTicketInfo = value_ticketData.content.rendered;

                    var ticketReturnObject = $('<div class="ticketItem displayTable col-md-4 col-sm-12"><div class="ticket_block"><div class="titleBlock displayTable"><div class="ticketTitle displayTableCell"></div></div><div class="priceBlock displayTable"><div class="ticketPriceSpan displayTableCell">$&nbsp;<span class="ticketPrice"></span></div></div><div class="linkBlock displayTable"><div class="ticketLink displayTableCell"><a target="_blank"><div>buy now</div></a></div></div><div class="infoBlock displayTable"><div class="moreTicketInfo"></div></div></div></div>');

                    ticketReturnObject.find('.ticketTitle').append(ticketTitle);
                    ticketReturnObject.find('.ticketPrice').append(ticketPrice);
                    ticketReturnObject.find('.ticketLink a').attr('href',ticketLink);

                    $('.ticket_container').append(ticketReturnObject);
                })
            })
            // get page content

            $.get("../wp-json/wp/v2/pages/52", function(data, status){
                var ticketPageData = data;

                console.log(ticketPageData);

                var pageContent = $('<div class="content_block">' + _.unescape(ticketPageData.content.rendered) + '</div>'); 
                // var sponsorButton = $('<div class="vendor_button">Become A Sponsor</div>');
                $('.the_content').html(pageContent);
                // $('.page_title').append(sponsorButton);
                // console.log(sponsorButton)


            })
        })

    })(jQuery);
    </script>
    <script>

    </script>

<?php get_footer(); ?>

