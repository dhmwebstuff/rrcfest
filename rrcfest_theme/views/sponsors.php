<?php
/*
Template Name: Sponsors Rrcfest
*/

get_header();

$is_page_builder_used = dhm_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div id="main-content">
<div class="overlay displayNone"></div>
    <div class="container">
        <h1 class="main_title"><?php the_title(); ?></h1>
        
        <div class="titleSponsorsContainer row"></div>
        <div class="sponsorsContainer row"></div>

        <div class="sponsor_info row"></div>
        
    </div>
</div> <!-- #main-content -->

<script>
    (function($){
        $( document ).ready(function(){
            
                $.get("../wp-json/wp/v2/rrcf_sponsors/?filter&per_page=100", function(data, status){
                    var sponsorsData = data;

                    

                    _.each(sponsorsData, function( value_sponsorsData, index_sponsorsData){
                        

                        // set variables
                        var sponsor_name = _.unescape(value_sponsorsData.title.rendered);
                        var sponsor_image = value_sponsorsData.rrcf_sponsors_meta.sponsorImageURL;
                        var sponsor_url = value_sponsorsData.rrcf_sponsors_meta.sponsorLink;
                        var sponsor_title = (value_sponsorsData.rrcf_sponsors_meta.sponsors_sponsor_title == 'on') ? true : false;
                        // console.log(sponsor_title);

                        if (sponsor_title) {
                            var titleSponsorReturnObject = $('<div class="sponsor_item title_sponsor col-md-12"><h3>We Would Like To Recognize Our</h3><h3>Title Sponsor</h3><a href="" class="sponosor_link" target="_blank"><div class="sponsor_image"><img class="img-responsive" /></div><div class="sponsor_name"></div></a></div>');
                            titleSponsorReturnObject.find('img').attr('src', sponsor_image );
                            titleSponsorReturnObject.find('.sponosor_link').attr('href', sponsor_url );
                            titleSponsorReturnObject.find('.sponsor_name').append(sponsor_name)
                            // console.log(sponsorReturnObject.find('.sponsor_name').size());

                            $('.titleSponsorsContainer').append(titleSponsorReturnObject);
                        } else {
                            var sponsorReturnObject = $('<div class="sponsor_item col-md-2"><a href="" class="sponosor_link" target="_blank"><div class="sponsor_image"><img class="img-responsive" /></div><div class="sponsor_name"></div></a></div>');
                            sponsorReturnObject.find('img').attr('src', sponsor_image );
                            sponsorReturnObject.find('.sponosor_link').attr('href', sponsor_url );
                            sponsorReturnObject.find('.sponsor_name').append(sponsor_name)
                            $('.sponsorsContainer').append(sponsorReturnObject);

                            // sponsorReturnObject.after($('.'))
                        };
                        
                        
                        // if ($('.sponsorsContainer').find('.sponsor_item').size() < 4) {
                        //     $('.sponsor_item').eq(0).addClass(' col-md-offset-2');
                        // } else {
                        //     $('.sponsor_item').eq(0).addClass(' col-md-offset-1');
                        // };

                        if (sponsor_url == "") {
                            sponsorReturnObject.find('a').css({'cursor': 'default'});
                            sponsorReturnObject.find('a').off();
                            sponsorReturnObject.find('a').on('click', function(e){
                                e.preventDefault();
                            });
                        };




                       

                    })

                })
                
                // get page content

                $.get("../wp-json/wp/v2/pages/172", function(data, status){
                    var sponsorsPageData = data;

                    console.log(sponsorsPageData);
                    var pageContent = $('<span class="page_title col-md-6 col-md-offset-3">' + _.unescape(sponsorsPageData.content.rendered) + '</span>'); 
                    var sponsorButton = $('<div class="vendor_button">Become A Sponsor</div>');
                    $('.sponsor_info').html(pageContent);
                    // $('.page_title').append(sponsorButton);
                    // console.log(sponsorButton)


                })


        })
    })(jQuery);
    </script>

<?php get_footer(); ?>



