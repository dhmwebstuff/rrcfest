<?php
/*
Template Name: Contact Rrcfest
*/

get_header();

$is_page_builder_used = dhm_pb_is_pagebuilder_used( get_the_ID() ); ?>
<link rel="stylesheet" href="/wp-content/themes/rrcfest_theme/css/contact.less.css">
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->

<script src="//cdnjs.cloudflare.com/ajax/libs/validate.js/0.9.0/validate.min.js"></script>
<!-- <script src="app.js"></script> -->



<div id="main-content">
   <div class="container">
     <h1 class="main_title"><?php the_title(); ?></h1>
     <div class="row">
         <div class="form_container col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3">
            <h2 class="form_header">Fill out the below for inquiries about the Roots Reggae Culture Festival</h2>
            <div class="ezForm">
                <div class="msgStatus"></div>
                <form id="jsForm" class="formBlock" action="/wp-content/themes/rrcfest_theme/views/sendform.php">
                    <div class="nameBlock form-group">
                        <label for="name">Name: </label>
                        <input type="text" id="name" name="name" class="form-control" required/>
                    </div>
                    <div class="emailBlock form-group">
                        <label for="email">Email: </label>
                        <input type="email" id="email" name="email" class="form-control" required/>
                    </div>
                    <div class="phoneBlock form-group">
                        <label for="phone">Phone: </label>
                        <input type="tel" id="phone" name="phone" class="form-control" placeholder="555-555-5555" required/>
                    </div>
                    <div class="insterestBlock form-group">
                        <label for="interest">Interest: </label>
                        <select id="interest" name="interest" class="form-control">
                            <option value="Artist Performance-Booking">Artist performance/Booking</option>
                            <option value="Sponsorship-Investment">Sponsorship/Investment</option>
                            <option value="Vending">Vending</option><option value="Employment">Employment</option><option value="General">General</option>
                        </select>
                    </div>
                    <div class="msgBlock form-group">
                        <label for="message">Message: </label>
                        <textarea name="msg" id="msg" class="form-control" rows="3" required></textarea>
                    </div>
                    <div class="btnBlock">
                        <button type="submit" class="send btn btn-success btn-block btn-lg">Send</button>
                    </div>
                </form>
            </div>
            <div class="vender_info"></div>

     </div>

    </div>
    
</div>
    <!-- #main-content -->
<script>
  
// send form
var $ = jQuery;
$(function() {
        var form = $('#jsForm');

          // get status msg div
          var formMessages = $('.msgStatus');

          // rest of code

          $(form).submit(function(e){
            e.preventDefault();

            // Serialize the form data.
            var formData = $(form).serialize();

            // submit form via ajax
            $.ajax({
                type: 'POST',
                url: $(form).attr('action'),
                data: formData
            }).done(function(response) {
                // Make sure that the formMessages div has the 'success' class.
                $(formMessages).removeClass('alert alert-danger');
                $(formMessages).addClass('alert alert-success');

                // Set the message text.
                $(formMessages).text(response);

                // Clear the form.
                $('#name').val('');
                $('#email').val('');
                $('#msg').val('');
                $('#phone').val('');


                // fade out success message
                setTimeout(function() {
                  $('.msgStatus').fadeOut('slow');
                }, 3000 );

            }).fail(function(data) {
                // Make sure that the formMessages div has the 'error' class.
                $(formMessages).removeClass('alert alert-success');
                $(formMessages).addClass('alert alert-danger');


                // Set the message text.
                if (data.responseText !== '') {
                    $(formMessages).text(data.responseText);
                } else {
                    $(formMessages).text('Oops! An error occured and your message could not be sent.');
                }
            });
          })
        
        // get page content

        $.get("../wp-json/wp/v2/pages/69", function(data, status){
            var contacePageData = data;

            // console.log(contacePageData);
            var pageContent = $('<div>' + _.unescape(contacePageData.content.rendered) + '</div>'); 
            // var sponsorButton = $('<div class="vendor_button">Become A Sponsor</div>');
            $('.vender_info').html(pageContent);
            // $('.page_title').append(sponsorButton);
            // console.log(sponsorButton)


        })
    });
</script>

<?php get_footer(); ?>


