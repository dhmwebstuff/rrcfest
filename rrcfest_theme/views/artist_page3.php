<?php
/*
Template Name: Artists Rrcfest Page 3
*/

get_header();

$is_page_builder_used = dhm_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div class="main-content">

    <div class="container">
        <h1 class="main_title"><?php the_title(); ?></h1>
       
</div>
</div> <!-- #main-content -->
<script>
    (function($){
        $( document ).ready(function(){
           
                $.get("../wp-json/wp/v2/rrcf_artists", function(data, status){
                    var artistData = data;
                    var artistLink = [];
                    var artistID;
                    _.each(artistData, function( value_artistData, index_artistData){
                        console.log(value_artistData)

                        var artistReturnObject = $('<div class="artistList"><div class="artistContainer"><a href="" class="artistModal"><div class="imageContainer"></div></a><div class="infoContainer"><div class="artistName"><a href="" class="artistModal"><span class="artistTitle"></span></a><div class="moreInfo"><span class="artistCircle">P</span></div></div></div></div></div>');
                        artistReturnObject.data('artist_data', value_artistData);
                        var artistImage = value_artistData.rrcf_artists_meta.imageURL;
                        var artistTitle = value_artistData.title.rendered;

                       

                        artistReturnObject.find('img').addClass('artistImage');
                        artistReturnObject.addClass("artistItem");
                        artistReturnObject.addClass("artist" + value_artistData.id);



                        artistReturnObject.find("a").attr('href', "#" + "artist" + value_artistData.id)
                        artistReturnObject.find(".modalContent").attr('id', "artist" + value_artistData.id)
                        artistReturnObject.find('.artistTitle').append(artistTitle);

                        $('.main-content').find('.container').append(artistReturnObject);
                        artistReturnObject.find('.imageContainer').backstretch(artistImage);
                        
                    })

                    $('.artistModal').on('click', function(e){
                        e.preventDefault();
                        var this_artists_data = $(this).parents('.artistList').data('artist_data');

                        var artistMoreInfoReturnObject = $('<div class="artistPopup modalContent"><div class="modalArtistContainer"><div class="imageContainerDetails"></div><div class="artistInfoContainer"><div class="artistTitle"><div class="artistNameDetails"></div></div><h3 class="artistLinksHeader">LINKS</h3><div class="artistLinks"><ul class="artistListLinks"></ul></div><div class="performing"><h2>Performing</h2><div class="pTime"></div></div><div class="artistBio"></div></div></div></div>');

                        var artistLinks = this_artists_data.rrcf_artists_meta.artistLinks;
                        var artistFacebook = this_artists_data.rrcf_artists_meta.artistLinksFb;
                        var artistTwitter = this_artists_data.rrcf_artists_meta.artistLinksTw;
                        var artistInstagram = this_artists_data.rrcf_artists_meta.artistLinksInsta;
                        var artistYoutube = this_artists_data.rrcf_artists_meta.artistLinksYt;
                        var performanceTime = this_artists_data.rrcf_artists_meta.performanceTime;

                        var artistLinkObject = $('<li class="artistSite"><a href="" class="artistSite">Website</a> &nbsp; </li>');
                        var artistFacebookObject = $('<li class="artistFB"><a href="" class="artistFB">Facebook</a> &nbsp; </li>');
                        var artistTwitterObject = $('<li class="artistTW"><a href="" class="artistTW">Twitter</a> &nbsp; </li>');
                        var artistInstagramObject = $('<li class="artistIN"><a href="" class="artistIN">Instagram</a> &nbsp; </li>');
                        var artistYoutubeObject = $('<li class="artistYT"><a href="" class="artistYT">Youtube</a></li>');

                        artistMoreInfoReturnObject.find('.artistNameDetails').append(this_artists_data.title.rendered);

                        // append links to dom
                        artistMoreInfoReturnObject.find('.artistListLinks').append(artistLinkObject);
                        artistMoreInfoReturnObject.find('.artistListLinks').append(artistFacebookObject);
                        artistMoreInfoReturnObject.find('.artistListLinks').append(artistTwitterObject);
                        artistMoreInfoReturnObject.find('.artistListLinks').append(artistInstagramObject);
                        artistMoreInfoReturnObject.find('.artistListLinks').append(artistYoutubeObject);
                        artistMoreInfoReturnObject.find('.pTime').append(performanceTime);

                         // remove links if blank
                        if (artistLinks == "") {
                           artistMoreInfoReturnObject.find('.artistSite').remove();
                        };
                        if (artistFacebook == "") {
                           artistMoreInfoReturnObject.find('.artistFB').remove();
                        };
                        if (artistTwitter == "") {
                           artistMoreInfoReturnObject.find('.artistTW').remove();
                        };
                        if (artistInstagram == "") {
                           artistMoreInfoReturnObject.find('.artistIN').remove();
                        };

                        if (artistYoutube == "") {
                           artistMoreInfoReturnObject.find('.artistYT').remove();
                        };

                        // set links
                        artistLinkObject.find('.artistSite').attr('href', 'http://' + artistLinks);
                        artistFacebookObject.find('.artistFB').attr('href', 'http://' + artistFacebook);
                        artistTwitterObject.find('.artistTW').attr('href', 'http://' + artistTwitter);
                        artistInstagramObject.find('.artistIN').attr('href', 'http://' +artistInstagram);
                        artistYoutubeObject.find('.artistYT').attr('href', 'http://' + artistYoutube);

                        $.magnificPopup.open({
                            mainClass: "modal_artist_bio",
                            items: {
                                src: artistMoreInfoReturnObject,
                                type: 'inline'
                            },
                            callbacks: {
                                open: function(){
                                    artistMoreInfoReturnObject.find('.imageContainerDetails').backstretch(this_artists_data.rrcf_artists_meta.imageURL)
                                }
                            }
                        });

                    })
                })
        })
    })(jQuery);
    </script>
    <script>

    </script>

<?php get_footer(); ?>

