<?php
/*
Template Name: Artists Rrcfest Page
*/

get_header();

$is_page_builder_used = dhm_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div class="main-content">


<script type="text/template" id="artists-template">
		<% var imageID; %>
         <% _.each(artists, function(artist) { %>
         	<% imageID = artist['attributes']['rrcf_artists_meta']['imageURL'] %>
         	<% console.log(imageID) %>
         	<div class="artistBlockSmall"> 
         		<div class="artistImageSmall"><img src=""/></div>	
         		<div class="artistTitle"><%= artist['attributes']['title']['rendered'] %></div>
         	</div>
         	<% var $ = jQuery; %>
         	<% var imageDiv = $('.artistImageSmall'); %>
         	<% jQuery('.artistImageSmall').find('img').attr('src', imageID); %>
         	
         	<% $('.artistImageSmall').append('<p>dasdf</p>'); %>
         	
    	<% }); %>
         
</script>


    <script type="text/javascript">
    (function($){
    	$( document ).ready(function() {
 			var imageURL;
    		var Artists = Backbone.Collection.extend({
    			// url:'../wp-json/posts?type=tiff_collec_faq'
    			// url: '../wp-json/wp/v2/posts?type'
    			url: '../wp-json/wp/v2/rrcf_artists'
			
    		});


    		var ArtistsList = Backbone.View.extend({
    			el: '.main-content',
    			render: function() {
    				var that = this;
    				artistsReturnObject = new Artists();
    			
    				artistsReturnObject.fetch({
    					success: function() {
    						
    						artists = artistsReturnObject["models"];
    						console.log(artists)
    						var template = _.template($('#artists-template').html(), artists);
    						that.$el.html(template);
    						
    					}
    				})
    				
    			}
    		});


    		var Router = Backbone.Router.extend({
    			routes: {
    				"": "lineup"
    			}
    		});

    		var artistsList = new ArtistsList();
    		
    		var router = new Router();
    		router.on('route:lineup', function() {
    			artistsList.render();
    			
    		});
    		// console.log('mediareturn', mediaReturn);
    		Backbone.history.start();
    	});
	
    })(jQuery);
    </script>
    <script>

    </script>

			
</div> <!-- #main-content -->

<?php get_footer(); ?>