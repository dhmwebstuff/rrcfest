<?php
/*
Template Name: Artists Rrcfest Page 4
*/

get_header();

$is_page_builder_used = dhm_pb_is_pagebuilder_used( get_the_ID() ); ?>

<div id="main-content">
<div class="overlay displayNone"></div>
    <div class="container">

        <h1 class="main_title"><?php the_title(); ?></h1>
        <div class="artistSmallContainer"></div>
        <div class="artistListLarge"></div>
        <div style="clear: both;"></div>
    </div>
</div> <!-- #main-content -->

<script>
    (function($){
        $( document ).ready(function(){
                
                $.get("../wp-json/wp/v2/rrcf_artists/?filter&per_page=100", function(data, status){
                   
                    var artistData = data;
                    var artistLink = [];
                    var artistID;
                    _.each(artistData, function( value_artistData, index_artistData){
                        var currentYear = new Date().getFullYear();
                        var festivalYear = value_artistData.rrcf_artists_meta.festivalYear;

                        if (festivalYear == currentYear ) {
                            var artistReturnObject = $('<div class="artistContainerSmall col-md-6"><a href="" class="artist-expand-link"><div class="imageContainer"></div></a><div class="infoContainer"><div class="artistName"><a href="" class="artist-expand-link"><span class="artistTitle"></span><div class="moreInfo"><span class="artistCircle">P</span></div></a></div></div></div>');
                            artistReturnObject.data('artist_data', value_artistData);
                            var artistImage = (value_artistData.rrcf_artists_meta.imageURL == "") ? "http://162.243.80.171/wp-content/uploads/2016/03/rrcf-logo-lg.png" : value_artistData.rrcf_artists_meta.imageURL;
                            var artistTitle = value_artistData.title.rendered;
                            var artistImageObject = $('<img class="img-responsive" src="' + artistImage + '"/>')

                           

                            artistReturnObject.find('img').addClass('artistImage');
                            artistReturnObject.addClass("artistItem");
                            artistReturnObject.addClass("artist" + value_artistData.id);
                            artistReturnObject.find("a").addClass("artist" + value_artistData.id);
                            artistReturnObject.find("a").attr('data-artistID', value_artistData.id )



                            artistReturnObject.find("a").attr('href', "#" + "artist" + value_artistData.id)
                            
                            artistReturnObject.find('.artistTitle').append(artistTitle);

                            $('#main-content').find('.artistSmallContainer').append(artistReturnObject);
                           
                            artistReturnObject.find('.imageContainer').append($('<img class="img-responsive" src="' + artistImage + '"/>'));
                           
                            var this_artists_data = $(this).parents('.artistList').data('artist_data');

                            var artistMoreInfoReturnObject = $('<div class="artist-expand displayNone col-xs-12 col-sm-12"><div class="largeArtistContainer"><div class="artistImage"></div><div class="artistInfoContainer"><div class="artistTitle"><div class="artistNameDetails"></div></div><h3 class="artistLinksHeader">LINKS</h3><div class="artistLinks"><ul class="artistListLinks"></ul></div><div class="performing"><h2>Performing</h2><div class="pTime"></div></div><div class="artistBio"></div><div class="close"><span class="dhm-pb-icon">Q</span></div></div></div></div>');
                            
                            artistMoreInfoReturnObject.addClass('artist_' + value_artistData.id)
                            artistMoreInfoReturnObject.attr('data-artistID', value_artistData.id)

                            var artistLinks = value_artistData.rrcf_artists_meta.artistLinks;
                            var artistFacebook = (value_artistData.rrcf_artists_meta.artistLinksFb == "") ? 'www.facebook.com/rrcfest' : value_artistData.rrcf_artists_meta.artistLinksFb ;
                            var artistTwitter = value_artistData.rrcf_artists_meta.artistLinksTw;
                            var artistInstagram = value_artistData.rrcf_artists_meta.artistLinksInsta;
                            var artistYoutube = value_artistData.rrcf_artists_meta.artistLinksYt;
                            var performanceTime = (value_artistData.rrcf_artists_meta.performanceTime == '') ? 'TBD' : value_artistData.rrcf_artists_meta.performanceTime;

                            var artistLinkObject = $('<li class="artistSite"><a href="" class="artistSite" target="_blank">Website</a> &nbsp; </li>');
                            var artistFacebookObject = $('<li class="artistFB"><a href="" class="artistFB" target="_blank">Facebook</a> &nbsp; </li>');
                            var artistTwitterObject = $('<li class="artistTW"><a href="" class="artistTW" target="_blank">Twitter</a> &nbsp; </li>');
                            var artistInstagramObject = $('<li class="artistIN"><a href="" class="artistIN" target="_blank">Instagram</a> &nbsp; </li>');
                            var artistYoutubeObject = $('<li class="artistYT"><a href="" class="artistYT" target="_blank">Youtube</a></li>');

                            artistMoreInfoReturnObject.find('.artistNameDetails').append(value_artistData.title.rendered);

                            // append links to dom
                            artistMoreInfoReturnObject.find('.artistListLinks').append(artistLinkObject);
                            artistMoreInfoReturnObject.find('.artistListLinks').append(artistFacebookObject);
                            artistMoreInfoReturnObject.find('.artistListLinks').append(artistTwitterObject);
                            artistMoreInfoReturnObject.find('.artistListLinks').append(artistInstagramObject);
                            artistMoreInfoReturnObject.find('.artistListLinks').append(artistYoutubeObject);
                            artistMoreInfoReturnObject.find('.pTime').append(performanceTime);

                             // remove links if blank
                            if (artistLinks == "") {
                               artistMoreInfoReturnObject.find('.artistSite').remove();
                            };
                            if (artistFacebook == "") {
                               artistMoreInfoReturnObject.find('.artistFB').remove();
                            };
                            if (artistTwitter == "") {
                               artistMoreInfoReturnObject.find('.artistTW').remove();
                            };
                            if (artistInstagram == "") {
                               artistMoreInfoReturnObject.find('.artistIN').remove();
                            };

                            if (artistYoutube == "") {
                               artistMoreInfoReturnObject.find('.artistYT').remove();
                            };
                            // remove artist header if no links provided
                            if (artistLinks == "" && artistFacebook == "" && artistTwitter == "" && artistInstagram == "" && artistYoutube == "" ) {
                              artistMoreInfoReturnObject.find('.artistLinksHeader').remove();  
                            };

                            // set links
                            artistLinkObject.find('.artistSite').attr('href', 'http://' + artistLinks);
                            artistFacebookObject.find('.artistFB').attr('href', 'http://' + artistFacebook);
                            artistTwitterObject.find('.artistTW').attr('href', 'http://' + artistTwitter);
                            artistInstagramObject.find('.artistIN').attr('href', 'http://' +artistInstagram);
                            artistYoutubeObject.find('.artistYT').attr('href', 'http://' + artistYoutube);

                            $('.artistListLarge').append(artistMoreInfoReturnObject);

                            artistMoreInfoReturnObject.find('.artistImage').append(artistImageObject);

                            var element_to_scroll_to;

                            $('.artist' + value_artistData.id).find('a').off();
                            $('.artist' + value_artistData.id).find('a').on('click', function(e){
                                var element_to_scroll_to;
                                
                                 var animateSomething = function(params){
                                    $(params.target).animate(params.animation, params.time, params.callback)
                                }
                                e.preventDefault();
                                $('.overlay').removeClass('displayNone');
                                var this_artist_object = $(this)
                                var this_link_artistid = this_artist_object.attr('data-artistid');
                                var artistExpandReturnObject = $('.artist_' + this_link_artistid );
                                var this_artist_object_position = $(this).offset();
                                
                                artistExpandReturnObject.removeClass('displayNone');
                                artistExpandReturnObject.scrollTop = this_artist_object_position;

                                element_to_scroll_to = this_artist_object;


                                $('html, body').animate({
                                    scrollTop: element_to_scroll_to.offset()
                                });
                                animateSomething({
                                    target: this_artist_object,
                                    animation: {
                                        opacity: 1,
                                    },
                                    time: 200,
                                })


                                artistExpandReturnObject.find('.close').off();
                                artistExpandReturnObject.find('.close').on('click', function(e){

                                    var headerHeight = $('#main-header').outerHeight();
                                    var additionalOffset = 50;

                                    var headerWithAdditionalOffset = headerHeight + additionalOffset;

                                    $('.overlay').addClass('displayNone');
                                    artistExpandReturnObject.addClass('displayNone');
                                    $('html, body').animate({
                                        scrollTop: element_to_scroll_to.offset().top - headerWithAdditionalOffset
                                    }, 'slow');
                                    animateSomething({
                                        target: this_artist_object,
                                        animation: {
                                            opacity: 1,
                                        },
                                        time: 500,
                                    })
                                });
                                    

                            }) 
                        }


                }) // each

            })

        })
    })(jQuery);
    </script>

<?php get_footer(); ?>



