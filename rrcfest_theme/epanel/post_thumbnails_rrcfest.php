<?php
add_theme_support( 'post-thumbnails' );

global $dhm_theme_image_sizes;

$dhm_theme_image_sizes = array(
	'400x250'  => 'dhm-pb-post-main-image',
	'1080x675' => 'dhm-pb-post-main-image-fullwidth',
	'400x284'   => 'dhm-pb-portfolio-image',
	'1080x9999' => 'dhm-pb-portfolio-image-single',
);

$dhm_theme_image_sizes = apply_filters( 'dhm_theme_image_sizes', $dhm_theme_image_sizes );
$crop = apply_filters( 'dhm_post_thumbnails_crop', true );

if ( is_array( $dhm_theme_image_sizes ) ){
	foreach ( $dhm_theme_image_sizes as $image_size_dimensions => $image_size_name ){
		$dimensions = explode( 'x', $image_size_dimensions );

		if ( in_array( $image_size_name, array( 'dhm-pb-portfolio-image-single' ) ) )
			$crop = false;

		add_image_size( $image_size_name, $dimensions[0], $dimensions[1], $crop );

		$crop = apply_filters( 'dhm_post_thumbnails_crop', true );
	}
}