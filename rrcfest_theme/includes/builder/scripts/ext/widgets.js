( function($) {

	$( document ).ready( function() {
		$( '.widget-liquid-right' ).append( dhm_pb_options.widget_info );

		var $create_box = $( '#dhm_pb_widget_area_create' ),
			$widget_name_input = $create_box.find( '#dhm_pb_new_widget_area_name' ),
			$dhm_pb_sidebars = $( 'div[id^=dhm_pb_widget_area_]' );

		$create_box.find( '.dhm_pb_create_widget_area' ).click( function( event ) {
			var $this_el = $(this);

			event.preventDefault();

			if ( $widget_name_input.val() === '' ) return;

			$.ajax( {
				type: "POST",
				url: dhm_pb_options.ajaxurl,
				data:
				{
					action : 'dhm_pb_add_widget_area',
					dhm_load_nonce : dhm_pb_options.dhm_load_nonce,
					dhm_widget_area_name : $widget_name_input.val()
				},
				success: function( data ){
					$this_el.siblings( '.dhm_pb_widget_area_result' ).hide().html( data ).slideToggle();
				}
			} );
		} );

		$dhm_pb_sidebars.each( function() {
			if ( $(this).is( '#dhm_pb_widget_area_create' ) || $(this).closest( '.inactive-sidebar' ).length ) return true;

			$(this).closest('.widgets-holder-wrap').find('.sidebar-name h3').before( '<a href="#" class="dhm_pb_widget_area_remove">' + dhm_pb_options.delete_string + '</a>' );

			$( '.dhm_pb_widget_area_remove' ).click( function( event ) {
				var $this_el = $(this);

				event.preventDefault();

				$.ajax( {
					type: "POST",
					url: dhm_pb_options.ajaxurl,
					data:
					{
						action : 'dhm_pb_remove_widget_area',
						dhm_load_nonce : dhm_pb_options.dhm_load_nonce,
						dhm_widget_area_name : $this_el.closest( '.widgets-holder-wrap' ).find( 'div[id^=dhm_pb_widget_area_]' ).attr( 'id' )
					},
					success: function( data ){
						$( '#' + data ).closest( '.widgets-holder-wrap' ).remove();
					}
				} );
			} );
		} );
	} );

} )(jQuery);