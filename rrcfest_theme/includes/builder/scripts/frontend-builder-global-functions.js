(function($){
	window.dhm_pb_smooth_scroll = function( $target, $top_section, speed, easing ) {
		var $window_width = $( window ).width();

		if ( $( 'body' ).hasClass( 'dhm_fixed_nav' ) && $window_width > 980 ) {
			$menu_offset = $( '#top-header' ).outerHeight() + $( '#main-header' ).outerHeight() - 1;
		} else {
			$menu_offset = -1;
		}

		if ( $ ('#wpadminbar').length && $window_width > 600 ) {
			$menu_offset += $( '#wpadminbar' ).outerHeight();
		}

		//fix sidenav scroll to top
		if ( $top_section ) {
			$scroll_position = 0;
		} else {
			$scroll_position = $target.offset().top - $menu_offset;
		}

		// set swing (animate's scrollTop default) as default value
		if( typeof easing === 'undefined' ){
			easing = 'swing';
		}

		$( 'html, body' ).animate( { scrollTop :  $scroll_position }, speed, easing );
	}

	window.dhm_fix_video_wmode = function( video_wrapper ) {
		$( video_wrapper ).each( function() {
			if ( $(this).find( 'iframe' ).length ) {
				var $this_el = $(this).find( 'iframe' ),
					src_attr = $this_el.attr('src'),
					wmode_character = src_attr.indexOf( '?' ) == -1 ? '?' : '&amp;',
					this_src = src_attr + wmode_character + 'wmode=opaque';

				$this_el.attr('src', this_src);
			}
		} );
	}

	window.dhm_pb_form_placeholders_init = function( $form ) {
		$form.find('input:text, textarea').each(function(index,domEle){
			var $dhm_current_input = jQuery(domEle),
				$dhm_comment_label = $dhm_current_input.siblings('label'),
				dhm_comment_label_value = $dhm_current_input.siblings('label').text();
			if ( $dhm_comment_label.length ) {
				$dhm_comment_label.hide();
				if ( $dhm_current_input.siblings('span.required') ) {
					dhm_comment_label_value += $dhm_current_input.siblings('span.required').text();
					$dhm_current_input.siblings('span.required').hide();
				}
				$dhm_current_input.val(dhm_comment_label_value);
			}
		}).bind('focus',function(){
			var dhm_label_text = jQuery(this).siblings('label').text();
			if ( jQuery(this).siblings('span.required').length ) dhm_label_text += jQuery(this).siblings('span.required').text();
			if (jQuery(this).val() === dhm_label_text) jQuery(this).val("");
		}).bind('blur',function(){
			var dhm_label_text = jQuery(this).siblings('label').text();
			if ( jQuery(this).siblings('span.required').length ) dhm_label_text += jQuery(this).siblings('span.required').text();
			if (jQuery(this).val() === "") jQuery(this).val( dhm_label_text );
		});
	}

	window.dhm_duplicate_menu = function( menu, append_to, menu_id, menu_class ){
		append_to.each( function() {
			var $this_menu = $(this),
				$cloned_nav;

			menu.clone().attr('id',menu_id).removeClass().attr('class',menu_class).appendTo( $this_menu );
			$cloned_nav = $this_menu.find('> ul');
			$cloned_nav.find('.menu_slide').remove();
			$cloned_nav.find('li:first').addClass('dhm_first_mobile_item');

			$cloned_nav.find( 'a' ).on( 'click', function(){
				$this_menu.trigger( 'click' );
			} );

			$this_menu.on( 'click', function(){
				if ( $(this).hasClass('closed') ){
					$(this).removeClass( 'closed' ).addClass( 'opened' );
					$cloned_nav.slideDown( 500 );
				} else {
					$(this).removeClass( 'opened' ).addClass( 'closed' );
					$cloned_nav.slideUp( 500 );
				}
				return false;
			} );

			$this_menu.on( 'click', 'a', function(event){
				event.stopPropagation();
			} );
		} );

		$('#mobile_menu .centered-inline-logo-wrap').remove();
	}

	// remove placeholder text before form submission
	window.dhm_pb_remove_placeholder_text = function( $form ) {
		$form.find('input:text, textarea').each(function(index,domEle){
			var $dhm_current_input = jQuery(domEle),
				$dhm_label = $dhm_current_input.siblings('label'),
				dhm_label_value = $dhm_current_input.siblings('label').text();

			if ( $dhm_label.length && $dhm_label.is(':hidden') ) {
				if ( $dhm_label.text() == $dhm_current_input.val() )
					$dhm_current_input.val( '' );
			}
		});
	}

	window.dhm_fix_fullscreen_section = function() {
		var $dhm_window = $(window);

		$( 'section.dhm_pb_fullscreen' ).each( function(){
			var $this_section = $( this );

			$.proxy( dhm_calc_fullscreen_section, $this_section )();

			$dhm_window.on( 'resize', $.proxy( dhm_calc_fullscreen_section, $this_section ) );
		});
	}
})(jQuery)