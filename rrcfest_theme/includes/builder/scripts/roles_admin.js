(function($){
	$( document ).ready( function() {
		var $container             = $( '.dhm_pb_roles_options_container' ),
			$yes_no_button_wrapper = $container.find( '.dhm_pb_yes_no_button_wrapper' ),
			$yes_no_button         = $container.find( '.dhm_pb_yes_no_button' ),
			$yes_no_select         = $container.find( 'select' ),
			$body                  = $( 'body' );

		$yes_no_button_wrapper.each( function() {
			var $this_el = $( this ),
				$this_switcher = $this_el.find( '.dhm_pb_yes_no_button' ),
				selected_value = $this_el.find( 'select' ).val();

			if ( 'on' === selected_value ) {
				$this_switcher.removeClass( 'dhm_pb_off_state' );
				$this_switcher.addClass( 'dhm_pb_on_state' );
			} else {
				$this_switcher.removeClass( 'dhm_pb_on_state' );
				$this_switcher.addClass( 'dhm_pb_off_state' );
			}
		});

		$yes_no_button.click( function() {
			var $this_el = $( this ),
				$this_select = $this_el.closest( '.dhm_pb_yes_no_button_wrapper' ).find( 'select' );

			if ( $this_el.hasClass( 'dhm_pb_off_state') ) {
				$this_el.removeClass( 'dhm_pb_off_state' );
				$this_el.addClass( 'dhm_pb_on_state' );
				$this_select.val( 'on' );
			} else {
				$this_el.removeClass( 'dhm_pb_on_state' );
				$this_el.addClass( 'dhm_pb_off_state' );
				$this_select.val( 'off' );
			}

			$this_select.trigger( 'change' );
		});

		$yes_no_select.change( function() {
			var $this_el = $( this ),
				$this_switcher = $this_el.closest( '.dhm_pb_yes_no_button_wrapper' ).find( '.dhm_pb_yes_no_button' ),
				new_value = $this_el.val();

			if ( 'on' === new_value ) {
				$this_switcher.removeClass( 'dhm_pb_off_state' );
				$this_switcher.addClass( 'dhm_pb_on_state' );
			} else {
				$this_switcher.removeClass( 'dhm_pb_on_state' );
				$this_switcher.addClass( 'dhm_pb_off_state' );
			}

		});

		$( '.dhm-pb-layout-buttons:not(.dhm-pb-layout-buttons-reset)' ).click( function() {
			var $clicked_tab = $( this ),
				open_tab = $clicked_tab.data( 'open_tab' );

			$( '.dhm_pb_roles_options_container.active-container' ).css( { 'display' : 'block', 'opacity' : 1 } ).stop( true, true ).animate( { opacity : 0 }, 300, function() {
				var $this_container = $( this );
				$this_container.css( 'display', 'none' );
				$this_container.removeClass( 'active-container' );
				$( '.' + open_tab ).addClass( 'active-container' ).css( { 'display' : 'block', 'opacity' : 0 } ).stop( true, true ).animate( { opacity : 1 }, 300 );
			});

			$( '.dhm-pb-layout-buttons' ).removeClass( 'dhm_pb_roles_active_menu' );

			$clicked_tab.addClass( 'dhm_pb_roles_active_menu' );
		});

		$( '#dhm_pb_save_roles' ).click( function() {
			var $all_options = $( '.dhm_pb_roles_container_all' ).find( 'form' ),
				all_options_array = {},
				options_combined = '';

			$all_options.each( function() {
				var this_form = $( this ),
					form_id = this_form.data( 'role_id' ),
					form_settings = this_form.serialize();

				all_options_array[form_id] = form_settings;
			});

			options_combined = JSON.stringify( all_options_array );

			$.ajax({
				type: 'POST',
				url: dhm_pb_roles_options.ajaxurl,
				dataType: 'json',
				data: {
					action : 'dhm_pb_save_role_settings',
					dhm_pb_options_all : options_combined,
					dhm_pb_save_roles_nonce : dhm_pb_roles_options.dhm_roles_nonce
				},
				beforeSend: function ( xhr ){
					$( '#dhm_pb_loading_animation' ).removeClass( 'dhm_pb_hide_loading' );
					$( '#dhm_pb_success_animation' ).removeClass( 'dhm_pb_active_success' );
					$( '#dhm_pb_loading_animation' ).show();
				},
				success: function( data ){
					$( '#dhm_pb_loading_animation' ).addClass( 'dhm_pb_hide_loading' );
					$( '#dhm_pb_success_animation' ).addClass( 'dhm_pb_active_success' ).show();

					setTimeout( function(){
						$( '#dhm_pb_success_animation' ).fadeToggle();
						$( '#dhm_pb_loading_animation' ).fadeToggle();
					}, 1000 );
				}
			});

			return false;
		} );

		$( '.dhm_pb_toggle_all' ).click( function() {
			var $options_section = $( this ).closest( '.dhm_pb_roles_section_container' ),
				$toggles = $options_section.find( '.dhm-pb-main-setting' ),
				on_buttons_count = 0,
				off_buttons_count = 0;

			$toggles.each( function() {
				if ( 'on' === $( this ).val() ) {
					on_buttons_count++;
				} else {
					off_buttons_count++;
				}
			});

			if ( on_buttons_count >= off_buttons_count ) {
				$toggles.val( 'off' );
			} else {
				$toggles.val( 'on' );
			}

			$toggles.change();
		});

		$( '.dhm-pb-layout-buttons-reset' ).click( function() {
			var $confirm_modal =
				"<div class='dhm_pb_modal_overlay' data-action='reset_roles'>\
					<div class='dhm_pb_prompt_modal'>\
					<h3>" + dhm_pb_roles_options.modal_title + "</h3>\
					<p>" + dhm_pb_roles_options.modal_message + "</p>\
						<a href='#' class='dhm_pb_prompt_dont_proceed dhm-pb-modal-close'>\
							<span>" + dhm_pb_roles_options.modal_no + "<span>\
						</span></span></a>\
						<div class='dhm_pb_prompt_buttons'>\
							<a href='#' class='dhm_pb_prompt_proceed'>" + dhm_pb_roles_options.modal_yes + "</a>\
						</div>\
					</div>\
				</div>";

			$( 'body' ).append( $confirm_modal );

			return false;
		});

		$( 'body' ).on( 'click', '.dhm-pb-modal-close', function() {
			$( this ).closest( '.dhm_pb_modal_overlay' ).remove();
		});

		$( 'body' ).on( 'click', '.dhm_pb_prompt_proceed', function() {
			var $all_toggles = $( '.dhm-pb-main-setting' );

			$all_toggles.val( 'on' );
			$all_toggles.change();

			$( this ).closest( '.dhm_pb_modal_overlay' ).remove();
		});

		$body.append( '<div id="dhm_pb_loading_animation"></div>' );
		$body.append( '<div id="dhm_pb_success_animation"></div>' );

		$( '#dhm_pb_loading_animation' ).hide();
		$( '#dhm_pb_success_animation' ).hide();
	});
})(jQuery)