<ul class="dhm-social-icons">

<?php if ( 'on' === dhm_get_option( 'rrcfest_show_facebook_icon', 'on' ) ) : ?>
	<li class="dhm-social-icon dhm-social-facebook">
		<a href="<?php echo esc_url( dhm_get_option( 'rrcfest_facebook_url', '#' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Facebook', 'Rrcfest' ); ?></span>
		</a>
	</li>
<?php endif; ?>
<?php if ( 'on' === dhm_get_option( 'rrcfest_show_twitter_icon', 'on' ) ) : ?>
	<li class="dhm-social-icon dhm-social-twitter">
		<a href="<?php echo esc_url( dhm_get_option( 'rrcfest_twitter_url', '#' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Twitter', 'Rrcfest' ); ?></span>
		</a>
	</li>
<?php endif; ?>
<?php if ( 'on' === dhm_get_option( 'rrcfest_show_google_icon', 'on' ) ) : ?>
	<li class="dhm-social-icon dhm-social-google-plus">
		<a href="<?php echo esc_url( dhm_get_option( 'rrcfest_google_url', '#' ) ); ?>" class="icon">
			<span><?php esc_html_e( 'Google', 'Rrcfest' ); ?></span>
		</a>
	</li>
<?php endif; ?>
<?php if ( 'on' === dhm_get_option( 'rrcfest_show_rss_icon', 'on' ) ) : ?>
<?php
	$et_rss_url = '' !== dhm_get_option( 'rrcfest_rss_url' )
		? dhm_get_option( 'rrcfest_rss_url' )
		: get_bloginfo( 'rss2_url' );
?>
	<li class="dhm-social-icon dhm-social-rss">
		<a href="<?php echo esc_url( $et_rss_url ); ?>" class="icon">
			<span><?php esc_html_e( 'RSS', 'Rrcfest' ); ?></span>
		</a>
	</li>
<?php endif; ?>

</ul>