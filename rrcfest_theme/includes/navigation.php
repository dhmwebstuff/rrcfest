<div class="pagination clearfix">
	<div class="alignleft"><?php next_posts_link(esc_html__('&laquo; Older Entries','Rrcfest')); ?></div>
	<div class="alignright"><?php previous_posts_link(esc_html__('Next Entries &raquo;', 'Rrcfest')); ?></div>
</div>