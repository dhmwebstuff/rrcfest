<?php
function dhm_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Sidebar', 'Rrcfest' ),
		'id' => 'sidebar-1',
		'before_widget' => '<div id="%1$s" class="dhm_pb_widget %2$s">',
		'after_widget' => '</div> <!-- end .dhm_pb_widget -->',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area', 'Rrcfest' ) . ' #1',
		'id' => 'sidebar-2',
		'before_widget' => '<div id="%1$s" class="fwidget dhm_pb_widget %2$s">',
		'after_widget' => '</div> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area', 'Rrcfest' ) . ' #2',
		'id' => 'sidebar-3',
		'before_widget' => '<div id="%1$s" class="fwidget dhm_pb_widget %2$s">',
		'after_widget' => '</div> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area', 'Rrcfest' ) . ' #3',
		'id' => 'sidebar-4',
		'before_widget' => '<div id="%1$s" class="fwidget dhm_pb_widget %2$s">',
		'after_widget' => '</div> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );

	register_sidebar( array(
		'name' => __( 'Footer Area', 'Rrcfest' ) . ' #4',
		'id' => 'sidebar-5',
		'before_widget' => '<div id="%1$s" class="fwidget dhm_pb_widget %2$s">',
		'after_widget' => '</div> <!-- end .fwidget -->',
		'before_title' => '<h4 class="title">',
		'after_title' => '</h4>',
	) );
}
add_action( 'widgets_init', 'dhm_widgets_init' );