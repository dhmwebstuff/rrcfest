<?php if ( 'on' == dhm_get_option( 'rrcfest_back_to_top', 'false' ) ) : ?>

	<span class="dhm_pb_scroll_top dhm-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="dhm-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #dhm-footer-nav -->

			<?php endif; ?>

				<div id="footer-bottom">
					<div class="container clearfix">
				<?php
					if ( false !== dhm_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}
				?>

						<p id="footer-info"><strong>&copy; <?php echo date("Y"); ?> Roots Reggae Culture Festival</strong></p>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #dhm-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->
	<script>

	var th = jQuery('#top-header').height(); 
	var hh = jQuery('#main-header').height(); 
	var fh = jQuery('#main-footer').height(); 
	var wh = jQuery(window).height(); 
	var ch = wh - (th + hh + fh); 
	jQuery('#main-content').css('min-height', ch);
	// if (jQuery('.artistContainerSmall').size() < 1) {
	// 	setTimeout(function(){jQuery('.main-footer').removeClass('displayNone')}, 500);
	// };
	if(typeof pageID !== "undefined"){
		setTimeout(function(){jQuery('.main-footer').removeClass('displayNone')}, 1000);

	}
	</script>
	</script>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-76995116-1', 'auto');
	ga('send', 'pageview');

	</script>
	<?php wp_footer(); ?>
</body>
</html>