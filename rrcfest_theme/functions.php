<?php

if ( ! isset( $content_width ) ) $content_width = 1080;

function dhm_setup_theme() {
	global $themename, $shortname, $dhm_store_options_in_one_row, $default_colorscheme;
	$themename = 'Rrcfest';
	$shortname = 'rrcfest';
	$dhm_store_options_in_one_row = true;

	$default_colorscheme = "Default";

	$template_directory = get_template_directory();

	require_once( $template_directory . '/epanel/custom_functions.php' );

	require_once( $template_directory . '/includes/functions/choices.php' );

	require_once( $template_directory . '/includes/functions/sanitization.php' );

	require_once( $template_directory . '/includes/functions/comments.php' );

	require_once( $template_directory . '/includes/functions/sidebars.php' );

	load_theme_textdomain( 'Rrcfest', $template_directory . '/lang' );

	require_once( $template_directory . '/epanel/core_functions.php' );

	require_once( $template_directory . '/epanel/post_thumbnails_rrcfest.php' );

	include( $template_directory . '/includes/widgets.php' );

	register_nav_menus( array(
		'primary-menu'   => __( 'Primary Menu', 'Rrcfest' ),
		'secondary-menu' => __( 'Secondary Menu', 'Rrcfest' ),
		'footer-menu'    => __( 'Footer Menu', 'Rrcfest' ),
	) );

	// don't display the empty title bar if the widget title is not set
	remove_filter( 'widget_title', 'dhm_widget_force_title' );

	remove_filter( 'body_class', 'dhm_add_fullwidth_body_class' );

	add_action( 'wp_enqueue_scripts', 'dhm_add_responsive_shortcodes_css', 11 );

	// Declare theme supports
	add_theme_support( 'title-tag' );

	add_theme_support( 'post-formats', array(
		'video', 'audio', 'quote', 'gallery', 'link'
	) );

	add_theme_support( 'woocommerce' );

	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );

	remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
	add_action( 'woocommerce_before_main_content', 'dhm_rrcfest_output_content_wrapper', 10 );

	remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );
	add_action( 'woocommerce_after_main_content', 'dhm_rrcfest_output_content_wrapper_end', 10 );

	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

	// deactivate page templates and custom import functions
	remove_action( 'init', 'dhm_activate_features' );

	remove_action('admin_menu', 'dhm_add_epanel');

	// Load editor styling
	add_editor_style( 'css/editor-style.css' );
}
add_action( 'after_setup_theme', 'dhm_setup_theme' );

function dhm_theme_epanel_reminder(){
	global $shortname, $themename;

	$documentation_url         = 'http://www.dirtyhandsmedia.com';
	$documentation_option_name = $shortname . '_2_4_documentation_message';

	if ( false === dhm_get_option( $shortname . '_logo' ) && false === dhm_get_option( $documentation_option_name ) ) {
		$message = sprintf(
			__( 'Welcome to Rrcfest! Before rrcfestng in to your new theme, please visit the <a style="color: #fff; font-weight: bold;" href="%1$s" target="_blank">Rrcfest Documentation</a> page for access to dozens of in-depth tutorials.', $themename ),
			esc_url( $documentation_url )
		);

		printf(
			'<div class="notice is-dismissible" style="background-color: #6C2EB9; color: #fff; border-left: none;">
				<p>%1$s</p>
			</div>',
			$message
		);

		dhm_update_option( $documentation_option_name, 'triggered' );
	}
}
add_action( 'admin_init', 'dhm_theme_epanel_reminder' );

if ( ! function_exists( 'dhm_rrcfest_fonts_url' ) ) :
function dhm_rrcfest_fonts_url() {
	$fonts_url = '';

	/* Translators: If there are characters in your language that are not
	 * supported by Open Sans, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$open_sans = _x( 'on', 'Open Sans font: on or off', 'Rrcfest' );

	if ( 'off' !== $open_sans ) {
		$font_families = array();

		if ( 'off' !== $open_sans )
			$font_families[] = 'Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800';

		$protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
			'family' => implode( '%7C', $font_families ),
			'subset' => 'latin,latin-ext',
		);
		$fonts_url = add_query_arg( $query_args, "$protocol://fonts.googleapis.com/css" );
	}

	return $fonts_url;
}
endif;

function dhm_rrcfest_load_fonts() {
	$fonts_url = dhm_rrcfest_fonts_url();
	if ( ! empty( $fonts_url ) )
		wp_enqueue_style( 'rrcfest-fonts', esc_url_raw( $fonts_url ), array(), null );
}
add_action( 'wp_enqueue_scripts', 'dhm_rrcfest_load_fonts' );

function dhm_add_home_link( $args ) {
	// add Home link to the custom menu WP-Admin page
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'dhm_add_home_link' );

function dhm_rrcfest_load_scripts_styles(){
	global $wp_styles;

	$template_dir = get_template_directory_uri();

	$theme_version = dhm_get_theme_version();

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	wp_enqueue_script( 'rrcfest-custom-script', $template_dir . '/js/custom.js', array( 'jquery' ), $theme_version, true );

	if ( 'on' === dhm_get_option( 'rrcfest_smooth_scroll', false ) ) {
		wp_enqueue_script( 'smooth-scroll', $template_dir . '/js/smoothscroll.js', array( 'jquery' ), $theme_version, true );
	}

	$dhm_gf_enqueue_fonts = array();
	$dhm_gf_heading_font = sanitize_text_field( dhm_get_option( 'heading_font', 'none' ) );
	$dhm_gf_body_font = sanitize_text_field( dhm_get_option( 'body_font', 'none' ) );
	$dhm_gf_button_font = sanitize_text_field( dhm_get_option( 'all_buttons_font', 'none' ) );
	$dhm_gf_primary_nav_font = sanitize_text_field( dhm_get_option( 'primary_nav_font', 'none' ) );
	$dhm_gf_secondary_nav_font = sanitize_text_field( dhm_get_option( 'secondary_nav_font', 'none' ) );

	$site_domain = get_locale();
	$dhm_one_font_languages = dhm_get_one_font_languages();

	if ( 'none' != $dhm_gf_heading_font ) $dhm_gf_enqueue_fonts[] = $dhm_gf_heading_font;
	if ( 'none' != $dhm_gf_body_font ) $dhm_gf_enqueue_fonts[] = $dhm_gf_body_font;
	if ( 'none' != $dhm_gf_button_font ) $dhm_gf_enqueue_fonts[] = $dhm_gf_button_font;
	if ( 'none' != $dhm_gf_primary_nav_font ) $dhm_gf_enqueue_fonts[] = $dhm_gf_primary_nav_font;
	if ( 'none' != $dhm_gf_secondary_nav_font ) $dhm_gf_enqueue_fonts[] = $dhm_gf_secondary_nav_font;

	if ( isset( $dhm_one_font_languages[$site_domain] ) ) {
		$dhm_gf_font_name_slug = strtolower( str_replace( ' ', '-', $dhm_one_font_languages[$site_domain]['language_name'] ) );
		wp_enqueue_style( 'dhm-gf-' . $dhm_gf_font_name_slug, $dhm_one_font_languages[$site_domain]['google_font_url'], array(), null );
	} else if ( ! empty( $dhm_gf_enqueue_fonts ) ) {
		foreach ( $dhm_gf_enqueue_fonts as $single_font ) {
			dhm_builder_enqueue_font( $single_font );
		}
	}

	/*
	 * Loads the main stylesheet.
	 */
	wp_enqueue_style( 'rrcfest-style', get_stylesheet_uri(), array(), $theme_version );
}
add_action( 'wp_enqueue_scripts', 'dhm_rrcfest_load_scripts_styles' );

function dhm_add_mobile_navigation(){
	printf(
		'<div id="dhm_mobile_nav_menu">
			<div class="mobile_nav closed">
				<span class="select_page">%1$s</span>
				<span class="mobile_menu_bar"></span>
			</div>
		</div>',
		esc_html__( 'Select Page', 'Rrcfest' )
	);
}
add_action( 'dhm_header_top', 'dhm_add_mobile_navigation' );

if ( ! function_exists( '_wp_render_title_tag' ) ) :
/**
 * Manually add <title> tag in head for WordPress 4.1 below for backward compatibility
 * Title tag is automatically added for WordPress 4.1 above via theme support
 * @return void
 */
	function dhm_add_title_tag_back_compat() { ?>
		<title><?php wp_title( '-', true, 'right' ); ?></title>
<?php
	}
	add_action( 'wp_head', 'dhm_add_title_tag_back_compat' );
endif;

function dhm_add_viewport_meta(){
	echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />';
}
add_action( 'wp_head', 'dhm_add_viewport_meta' );

function dhm_remove_additional_stylesheet( $stylesheet ){
	global $default_colorscheme;
	return $default_colorscheme;
}
add_filter( 'dhm_get_additional_color_scheme', 'dhm_remove_additional_stylesheet' );

if ( ! function_exists( 'dhm_list_pings' ) ) :
function dhm_list_pings($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li id="comment-<?php comment_ID(); ?>"><?php comment_author_link(); ?> - <?php comment_excerpt(); ?>
<?php }
endif;

if ( ! function_exists( 'dhm_get_theme_version' ) ) :
function dhm_get_theme_version() {
	$theme_info = wp_get_theme();

	if ( is_child_theme() ) {
		$theme_info = wp_get_theme( $theme_info->parent_theme );
	}

	$theme_version = $theme_info->display( 'Version' );

	return $theme_version;
}
endif;

function dhm_add_post_meta_box() {
	// Add Page settings meta box only if it's not disabled for current user
	if ( dhm_pb_is_allowed( 'page_options' ) ) {
		add_meta_box( 'dhm_settings_meta_box', __( 'Rrcfest Page Settings', 'Rrcfest' ), 'dhm_single_settings_meta_box', 'page', 'side', 'high' );
	}
	add_meta_box( 'dhm_settings_meta_box', __( 'Rrcfest Post Settings', 'Rrcfest' ), 'dhm_single_settings_meta_box', 'post', 'side', 'high' );
	add_meta_box( 'dhm_settings_meta_box', __( 'Rrcfest Product Settings', 'Rrcfest' ), 'dhm_single_settings_meta_box', 'product', 'side', 'high' );
	add_meta_box( 'dhm_settings_meta_box', __( 'Rrcfest Project Settings', 'Rrcfest' ), 'dhm_single_settings_meta_box', 'project', 'side', 'high' );
}
add_action( 'add_meta_boxes', 'dhm_add_post_meta_box' );

if ( ! function_exists( 'dhm_pb_portfolio_meta_box' ) ) :
function dhm_pb_portfolio_meta_box() { ?>
	<div class="dhm_project_meta">
		<strong class="dhm_project_meta_title"><?php echo esc_html__( 'Skills', 'Rrcfest' ); ?></strong>
		<p><?php echo get_the_term_list( get_the_ID(), 'project_tag', '', ', ' ); ?></p>

		<strong class="dhm_project_meta_title"><?php echo esc_html__( 'Posted on', 'Rrcfest' ); ?></strong>
		<p><?php echo get_the_date(); ?></p>
	</div>
<?php }
endif;

if ( ! function_exists( 'dhm_single_settings_meta_box' ) ) :
function dhm_single_settings_meta_box( $post ) {
	$post_id = get_the_ID();

	wp_nonce_field( basename( __FILE__ ), 'dhm_settings_nonce' );

	$page_layout = get_post_meta( $post_id, '_dhm_pb_page_layout', true );

	$side_nav = get_post_meta( $post_id, '_dhm_pb_side_nav', true );

	$post_hide_nav = get_post_meta( $post_id, '_dhm_pb_post_hide_nav', true ) && 'off' === get_post_meta( $post_id, '_dhm_pb_post_hide_nav', true ) ? 'default' : get_post_meta( $post_id, '_dhm_pb_post_hide_nav', true );

	$show_title = get_post_meta( $post_id, '_dhm_pb_show_title', true );

	$page_layouts = array(
		'dhm_right_sidebar'   => __( 'Right Sidebar', 'Rrcfest' ),
		'dhm_left_sidebar'    => __( 'Left Sidebar', 'Rrcfest' ),
		'dhm_full_width_page' => __( 'Full Width', 'Rrcfest' ),
	);

	$layouts = array(
		'light' => __( 'Light', 'Rrcfest' ),
		'dark'  => __( 'Dark', 'Rrcfest' ),
	);
	$post_bg_color  = ( $bg_color = get_post_meta( $post_id, '_dhm_post_bg_color', true ) ) && '' !== $bg_color
		? $bg_color
		: '#ffffff';
	$post_use_bg_color = get_post_meta( $post_id, '_dhm_post_use_bg_color', true )
		? true
		: false;
	$post_bg_layout = ( $layout = get_post_meta( $post_id, '_dhm_post_bg_layout', true ) ) && '' !== $layout
		? $layout
		: 'light'; ?>

	<p class="dhm_pb_page_settings dhm_pb_page_layout_settings">
		<label for="dhm_pb_page_layout" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Page Layout', 'Rrcfest' ); ?>: </label>

		<select id="dhm_pb_page_layout" name="dhm_pb_page_layout">
		<?php
		foreach ( $page_layouts as $layout_value => $layout_name ) {
			printf( '<option value="%2$s"%3$s>%1$s</option>',
				esc_html( $layout_name ),
				esc_attr( $layout_value ),
				selected( $layout_value, $page_layout, false )
			);
		} ?>
		</select>
	</p>
	<p class="dhm_pb_page_settings dhm_pb_side_nav_settings" style="display: none;">
		<label for="dhm_pb_side_nav" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Dot Navigation', 'Rrcfest' ); ?>: </label>

		<select id="dhm_pb_side_nav" name="dhm_pb_side_nav">
			<option value="off" <?php selected( 'off', $side_nav ); ?>><?php esc_html_e( 'Off', 'Rrcfest' ); ?></option>
			<option value="on" <?php selected( 'on', $side_nav ); ?>><?php esc_html_e( 'On', 'Rrcfest' ); ?></option>
		</select>
	</p>
	<p class="dhm_pb_page_settings">
		<label for="dhm_pb_post_hide_nav" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Hide Nav Before Scroll', 'Rrcfest' ); ?>: </label>

		<select id="dhm_pb_post_hide_nav" name="dhm_pb_post_hide_nav">
			<option value="default" <?php selected( 'default', $post_hide_nav ); ?>><?php esc_html_e( 'Default', 'Rrcfest' ); ?></option>
			<option value="no" <?php selected( 'no', $post_hide_nav ); ?>><?php esc_html_e( 'Off', 'Rrcfest' ); ?></option>
			<option value="on" <?php selected( 'on', $post_hide_nav ); ?>><?php esc_html_e( 'On', 'Rrcfest' ); ?></option>
		</select>
	</p>

<?php if ( 'post' === $post->post_type ) : ?>
	<p class="dhm_pb_page_settings dhm_pb_single_title" style="display: none;">
		<label for="dhm_single_title" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Post Title', 'Rrcfest' ); ?>: </label>

		<select id="dhm_single_title" name="dhm_single_title">
			<option value="on" <?php selected( 'on', $show_title ); ?>><?php esc_html_e( 'Show', 'Rrcfest' ); ?></option>
			<option value="off" <?php selected( 'off', $show_title ); ?>><?php esc_html_e( 'Hide', 'Rrcfest' ); ?></option>
		</select>
	</p>

	<p class="dhm_rrcfest_quote_settings dhm_rrcfest_audio_settings dhm_rrcfest_link_settings dhm_rrcfest_format_setting dhm_pb_page_settings">
		<label for="dhm_post_use_bg_color" style="display: block; font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Use Background Color', 'Rrcfest' ); ?></label>
		<input name="dhm_post_use_bg_color" type="checkbox" id="dhm_post_use_bg_color" <?php checked( $post_use_bg_color ); ?> />
	</p>

	<p class="dhm_post_bg_color_setting dhm_rrcfest_format_setting dhm_pb_page_settings">
		<input id="dhm_post_bg_color" name="dhm_post_bg_color" class="color-picker-hex" type="text" maxlength="7" placeholder="<?php esc_attr_e( 'Hex Value', 'Rrcfest' ); ?>" value="<?php echo esc_attr( $post_bg_color ); ?>" data-default-color="#ffffff" />
	</p>

	<p class="dhm_rrcfest_quote_settings dhm_rrcfest_audio_settings dhm_rrcfest_link_settings dhm_rrcfest_format_setting">
		<label for="dhm_post_bg_layout" style="font-weight: bold; margin-bottom: 5px;"><?php esc_html_e( 'Text Color', 'Rrcfest' ); ?>: </label>
		<select id="dhm_post_bg_layout" name="dhm_post_bg_layout">
	<?php
		foreach ( $layouts as $layout_name => $layout_title )
			printf( '<option value="%s"%s>%s</option>',
				esc_attr( $layout_name ),
				selected( $layout_name, $post_bg_layout, false ),
				esc_html( $layout_title )
			);
	?>
		</select>
	</p>
<?php endif;

}
endif;

function dhm_rrcfest_post_settings_save_details( $post_id, $post ){
	global $pagenow;

	if ( 'post.php' != $pagenow ) return $post_id;

	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return $post_id;

	$post_type = get_post_type_object( $post->post_type );
	if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) )
		return $post_id;

	if ( ! isset( $_POST['dhm_settings_nonce'] ) || ! wp_verify_nonce( $_POST['dhm_settings_nonce'], basename( __FILE__ ) ) )
		return $post_id;

	if ( isset( $_POST['dhm_post_use_bg_color'] ) )
		update_post_meta( $post_id, '_dhm_post_use_bg_color', true );
	else
		delete_post_meta( $post_id, '_dhm_post_use_bg_color' );

	if ( isset( $_POST['dhm_post_bg_color'] ) )
		update_post_meta( $post_id, '_dhm_post_bg_color', sanitize_text_field( $_POST['dhm_post_bg_color'] ) );
	else
		delete_post_meta( $post_id, '_dhm_post_bg_color' );

	if ( isset( $_POST['dhm_post_bg_layout'] ) )
		update_post_meta( $post_id, '_dhm_post_bg_layout', sanitize_text_field( $_POST['dhm_post_bg_layout'] ) );
	else
		delete_post_meta( $post_id, '_dhm_post_bg_layout' );

	if ( isset( $_POST['dhm_single_title'] ) )
		update_post_meta( $post_id, '_dhm_pb_show_title', sanitize_text_field( $_POST['dhm_single_title'] ) );
	else
		delete_post_meta( $post_id, '_dhm_pb_show_title' );

	if ( isset( $_POST['dhm_pb_post_hide_nav'] ) )
		update_post_meta( $post_id, '_dhm_pb_post_hide_nav', sanitize_text_field( $_POST['dhm_pb_post_hide_nav'] ) );
	else
		delete_post_meta( $post_id, '_dhm_pb_post_hide_nav' );

	if ( isset( $_POST['dhm_pb_page_layout'] ) ) {
		update_post_meta( $post_id, '_dhm_pb_page_layout', sanitize_text_field( $_POST['dhm_pb_page_layout'] ) );
	} else {
		delete_post_meta( $post_id, '_dhm_pb_page_layout' );
	}

	if ( isset( $_POST['dhm_pb_side_nav'] ) ) {
		update_post_meta( $post_id, '_dhm_pb_side_nav', sanitize_text_field( $_POST['dhm_pb_side_nav'] ) );
	} else {
		delete_post_meta( $post_id, '_dhm_pb_side_nav' );
	}

}
add_action( 'save_post', 'dhm_rrcfest_post_settings_save_details', 10, 2 );

function dhm_get_one_font_languages() {
	$one_font_languages = array(
		'he_IL' => array(
			'language_name'   => 'Hebrew',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/alefhebrew.css',
			'font_family'     => "'Alef Hebrew', serif",
		),
		'ja' => array(
			'language_name'   => 'Japanese',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/notosansjapanese.css',
			'font_family'     => "'Noto Sans Japanese', serif",
		),
		'ko_KR' => array(
			'language_name'   => 'Korean',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/hanna.css',
			'font_family'     => "'Hanna', serif",
		),
		'ar' => array(
			'language_name'   => 'Arabic',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/lateef.css',
			'font_family'     => "'Lateef', serif",
		),
		'th' => array(
			'language_name'   => 'Thai',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/notosansthai.css',
			'font_family'     => "'Noto Sans Thai', serif",
		),
		'ms_MY' => array(
			'language_name'   => 'Malay',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/notosansmalayalam.css',
			'font_family'     => "'Noto Sans Malayalam', serif",
		),
		'zh_CN' => array(
			'language_name'   => 'Chinese',
			'google_font_url' => '//fonts.googleapis.com/earlyaccess/cwtexfangsong.css',
			'font_family'     => "'cwTeXFangSong', serif",
		),
	);

	return $one_font_languages;
}

function dhm_rrcfest_customize_register( $wp_customize ) {
	$wp_customize->remove_section( 'title_tagline' );
	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'colors' );
	$wp_customize->register_control_type( 'ET_Rrcfest_Customize_Color_Alpha_Control' );

	wp_register_script( 'wp-color-picker-alpha', get_template_directory_uri() . '/includes/builder/scripts/ext/wp-color-picker-alpha.min.js', array( 'jquery', 'wp-color-picker' ) );

	$option_set_name           = 'dhm_customizer_option_set';
	$option_set_allowed_values = apply_filters( 'dhm_customizer_option_set_allowed_values', array( 'module', 'theme' ) );

	$customizer_option_set = '';

	/**
	 * Set a transient,
	 * if 'dhm_customizer_option_set' query parameter is set to one of the allowed values
	 */
	if ( isset( $_GET[ $option_set_name ] ) && in_array( $_GET[ $option_set_name ], $option_set_allowed_values ) ) {
		$customizer_option_set = $_GET[ $option_set_name ];

		set_transient( 'dhm_rrcfest_customizer_option_set', $customizer_option_set, DAY_IN_SECONDS );
	}

	if ( '' === $customizer_option_set && ( $dhm_customizer_option_set_value = get_transient( 'dhm_rrcfest_customizer_option_set' ) ) ) {
		$customizer_option_set = $dhm_customizer_option_set_value;
	}

	dhm_builder_init_global_settings();

	if ( isset( $customizer_option_set ) && 'module' === $customizer_option_set ) {
		// display wp error screen if module customizer disabled for current user
		if ( ! dhm_pb_is_allowed( 'module_customizer' ) ) {
			wp_die( __( "you don't have sufficient permissions to access this page", 'Rrcfest' ) );
		}

		$removed_default_sections = array( 'nav', 'static_front_page' );
		foreach ( $removed_default_sections as $default_section ) {
			$wp_customize->remove_section( $default_section );
		}

		dhm_rrcfest_customizer_module_settings( $wp_customize );
	} else {
		// display wp error screen if theme customizer disabled for current user
		if ( ! dhm_pb_is_allowed( 'theme_customizer' ) ) {
			wp_die( __( "you don't have sufficient permissions to access this page", 'Rrcfest' ) );
		}

		dhm_rrcfest_customizer_theme_settings( $wp_customize );
	}
}
add_action( 'customize_register', 'dhm_rrcfest_customize_register' );

if ( ! function_exists( 'dhm_rrcfest_customizer_theme_settings' ) ) :
function dhm_rrcfest_customizer_theme_settings( $wp_customize ) {
	$site_domain = get_locale();

	$google_fonts = dhm_builder_get_google_fonts();

	$dhm_domain_fonts = array(
		'ru_RU' => 'cyrillic',
		'uk' => 'cyrillic',
		'bg_BG' => 'cyrillic',
		'vi' => 'vietnamese',
		'el' => 'greek',
	);

	$dhm_one_font_languages = dhm_get_one_font_languages();

	$font_choices = array();
	$font_choices['none'] = array(
		'label' => 'Default Theme Font'
	);

	foreach ( $google_fonts as $google_font_name => $google_font_properties ) {
		if ( '' !== $site_domain && isset( $dhm_domain_fonts[$site_domain] ) && false === strpos( $google_font_properties['character_set'], $dhm_domain_fonts[$site_domain] ) ) {
			continue;
		}
		$font_choices[ $google_font_name ] = array(
			'label' => $google_font_name,
			'data'  => array(
				'parent_font'    => isset( $google_font_properties['parent_font'] ) ? $google_font_properties['parent_font'] : '',
				'parent_styles'  => isset( $google_font_properties['parent_font'] ) && isset( $google_fonts[$google_font_properties['parent_font']]['styles'] ) ? $google_fonts[$google_font_properties['parent_font']]['styles'] : $google_font_properties['styles'],
				'current_styles' => isset( $google_font_properties['parent_font'] ) && isset( $google_fonts[$google_font_properties['parent_font']]['styles'] ) && isset( $google_font_properties['styles'] ) ? $google_font_properties['styles'] : '',
				'parent_subset'  => isset( $google_font_properties['parent_font'] ) && isset( $google_fonts[$google_font_properties['parent_font']]['character_set'] ) ? $google_fonts[$google_font_properties['parent_font']]['character_set'] : ''
			)
		);
	}

	$wp_customize->add_panel( 'dhm_rrcfest_general_settings' , array(
		'title'		=> __( 'General Settings', 'Rrcfest' ),
		'priority'	=> 1,
	) );

	$wp_customize->add_section( 'title_tagline', array(
		'title'    => __( 'Site Identity', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_general_settings',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_general_layout' , array(
		'title'		=> __( 'Layout Settings', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_general_settings',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_general_typography' , array(
		'title'		=> __( 'Typography', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_general_settings',
	) );

	$wp_customize->add_panel( 'dhm_rrcfest_mobile' , array(
		'title'		=> __( 'Mobile Styles', 'Rrcfest' ),
		'priority' => 6,
	) );

	$wp_customize->add_section( 'dhm_rrcfest_mobile_tablet' , array(
		'title'		=> __( 'Tablet', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_mobile',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_mobile_phone' , array(
		'title'		=> __( 'Phone', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_mobile',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_mobile_menu' , array(
		'title'		=> __( 'Mobile Menu', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_mobile',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_general_background' , array(
		'title'		=> __( 'Background', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_general_settings',
	) );

	$wp_customize->add_panel( 'dhm_rrcfest_header_panel', array(
	    'title' => __( 'Header & Navigation', 'Rrcfest' ),
	    'priority' => 2,
	) );

	$wp_customize->add_section( 'dhm_rrcfest_header_layout' , array(
		'title'		=> __( 'Header Format', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_header_panel',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_header_primary' , array(
		'title'		=> __( 'Primary Menu Bar', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_header_panel',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_header_secondary' , array(
		'title'		=> __( 'Secondary Menu Bar', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_header_panel',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_header_fixed' , array(
		'title'		=> __( 'Fixed Navigation Settings', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_header_panel',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_header_information' , array(
		'title'		=> __( 'Header Elements', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_header_panel',
	) );

	$wp_customize->add_panel( 'dhm_rrcfest_footer_panel' , array(
		'title'		=> __( 'Footer', 'Rrcfest' ),
		'priority'	=> 3,
	) );

	$wp_customize->add_section( 'dhm_rrcfest_footer_layout' , array(
		'title'		=> __( 'Layout', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_footer_panel',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_footer_widgets' , array(
		'title'		=> __( 'Widgets', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_footer_panel',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_footer_elements' , array(
		'title'		=> __( 'Footer Elements', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_footer_panel',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_footer_menu' , array(
		'title'		=> __( 'Footer Menu', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_footer_panel',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_bottom_bar' , array(
		'title'		=> __( 'Bottom Bar', 'Rrcfest' ),
		'panel' => 'dhm_rrcfest_footer_panel',
	) );

	$wp_customize->add_section( 'dhm_color_schemes' , array(
		'title'       => __( 'Color Schemes', 'Rrcfest' ),
		'priority'    => 7,
		'description' => __( 'Note: Color settings set above should be applied to the Default color scheme.', 'Rrcfest' ),
	) );

	$wp_customize->add_panel( 'dhm_rrcfest_buttons_settings' , array(
		'title'		=> __( 'Buttons', 'Rrcfest' ),
		'priority'	=> 4,
	) );

	$wp_customize->add_section( 'dhm_rrcfest_buttons' , array(
		'title'       => __( 'Buttons Style', 'Rrcfest' ),
		'panel'       => 'dhm_rrcfest_buttons_settings',
	) );

	$wp_customize->add_section( 'dhm_rrcfest_buttons_hover' , array(
		'title'       => __( 'Buttons Hover Style', 'Rrcfest' ),
		'panel'       => 'dhm_rrcfest_buttons_settings',
	) );

	$wp_customize->add_panel( 'dhm_rrcfest_blog_settings' , array(
		'title'		=> __( 'Blog', 'Rrcfest' ),
		'priority'	=> 5,
	) );

	$wp_customize->add_section( 'dhm_rrcfest_blog_post' , array(
		'title'       => __( 'Post', 'Rrcfest' ),
		'panel'       => 'dhm_rrcfest_blog_settings',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[post_meta_font_size]', array(
		'default'       => '14',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->get_setting( 'blogname' )->transport        = 'postMessage';

	$wp_customize->get_setting( 'blogdescription' )->transport = 'postMessage';

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[post_meta_font_size]', array(
		'label'	      => __( 'Meta Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_blog_post',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 32,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[post_meta_height]', array(
		'default'       => '1',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_float_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[post_meta_height]', array(
		'label'	      => __( 'Meta Line Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_blog_post',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => .8,
			'max'  => 3,
			'step' => .1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[post_meta_spacing]', array(
		'default'       => '0',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[post_meta_spacing]', array(
		'label'	      => __( 'Meta Letter Spacing', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_blog_post',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => -2,
			'max'  => 10,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[post_meta_style]', array(
		'default'       => '',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[post_meta_style]', array(
		'label'	      => __( 'Meta Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_blog_post',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[post_header_font_size]', array(
		'default'       => '30',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[post_header_font_size]', array(
		'label'	      => __( 'Header Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_blog_post',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 72,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[post_header_height]', array(
		'default'       => '1',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_float_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[post_header_height]', array(
		'label'	      => __( 'Header Line Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_blog_post',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0.8,
			'max'  => 3,
			'step' => 0.1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[post_header_spacing]', array(
		'default'       => '0',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_int_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[post_header_spacing]', array(
		'label'	      => __( 'Header Letter Spacing', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_blog_post',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => -2,
			'max'  => 10,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[post_header_style]', array(
		'default'       => '',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[post_header_style]', array(
		'label'	      => __( 'Header Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_blog_post',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[boxed_layout]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[boxed_layout]', array(
		'label'		=> __( 'Enable Boxed Layout', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_layout',
		'type'      => 'checkbox',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[content_width]', array(
		'default'       => '1080',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[content_width]', array(
		'label'	      => __( 'Website Content Width', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_layout',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 960,
			'max'  => 1920,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[gutter_width]', array(
		'default'       => '3',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[gutter_width]', array(
		'label'	      => __( 'Website Gutter Width', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_layout',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 1,
			'max'  => 4,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[use_sidebar_width]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[use_sidebar_width]', array(
		'label'		=> __( 'Use Custom Sidebar Width', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_layout',
		'type'      => 'checkbox',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[sidebar_width]', array(
		'default'       => '21',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[sidebar_width]', array(
		'label'	      => __( 'Sidebar Width', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_layout',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 19,
			'max'  => 33,
			'step' => 1,
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[section_padding]', array(
		'default'       => '4',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[section_padding]', array(
		'label'	      => __( 'Section Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_layout',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 10,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[phone_section_height]', array(
		'default'       => dhm_get_option( 'tablet_section_height', '50' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[phone_section_height]', array(
		'label'	      => __( 'Section Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_mobile_phone',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 150,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[tablet_section_height]', array(
		'default'       => '50',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[tablet_section_height]', array(
		'label'	      => __( 'Section Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_mobile_tablet',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 150,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[row_padding]', array(
		'default'       => '2',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[row_padding]', array(
		'label'	      => __( 'Row Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_layout',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 10,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[phone_row_height]', array(
		'default'       => dhm_get_option( 'tablet_row_height', '30' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[phone_row_height]', array(
		'label'	      => __( 'Row Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_mobile_phone',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 150,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[tablet_row_height]', array(
		'default'       => '30',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[tablet_row_height]', array(
		'label'	      => __( 'Row Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_mobile_tablet',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 150,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[cover_background]', array(
		'default'       => 'on',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[cover_background]', array(
		'label'		=> __( 'Stretch Background Image', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_background',
		'type'      => 'checkbox',
	) );

	if ( ! is_null( $wp_customize->get_setting( 'background_color' ) ) ) {
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color', array(
			'label'		=> __( 'Background Color', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_general_background',
		) ) );
	}

	if ( ! is_null( $wp_customize->get_setting( 'background_image' ) ) ) {
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background_image', array(
			'label'		=> __( 'Background Image', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_general_background',
		) ) );
	}

	$wp_customize->add_control( 'background_repeat', array(
		'label'		=> __( 'Background Repeat', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_background',
		'type'      => 'radio',
		'choices'    => array(
				'no-repeat'  => __( 'No Repeat', 'Rrcfest' ),
				'repeat'     => __( 'Tile', 'Rrcfest' ),
				'repeat-x'   => __( 'Tile Horizontally', 'Rrcfest' ),
				'repeat-y'   => __( 'Tile Vertically', 'Rrcfest' ),
			),
	) );

	$wp_customize->add_control( 'background_position_x', array(
		'label'		=> __( 'Background Position', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_background',
		'type'      => 'radio',
		'choices'    => array(
				'left'       => __( 'Left', 'Rrcfest' ),
				'center'     => __( 'Center', 'Rrcfest' ),
				'right'      => __( 'Right', 'Rrcfest' ),
			),
	) );

	$wp_customize->add_control( 'background_attachment', array(
		'label'		=> __( 'Background Position', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_background',
		'type'      => 'radio',
		'choices'    => array(
				'scroll'     => __( 'Scroll', 'Rrcfest' ),
				'fixed'      => __( 'Fixed', 'Rrcfest' ),
			),
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[body_font_size]', array(
		'default'       => '14',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[body_font_size]', array(
		'label'	      => __( 'Body Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_typography',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 32,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[body_font_height]', array(
		'default'       => '1.7',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_float_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[body_font_height]', array(
		'label'	      => __( 'Body Line Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_typography',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0.8,
			'max'  => 3,
			'step' => 0.1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[phone_body_font_size]', array(
		'default'       => dhm_get_option( 'tablet_body_font_size', '14' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[phone_body_font_size]', array(
		'label'	      => __( 'Body Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_mobile_phone',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 32,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[tablet_body_font_size]', array(
		'default'       => dhm_get_option( 'body_font_size', '14' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[tablet_body_font_size]', array(
		'label'	      => __( 'Body Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_mobile_tablet',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 32,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[body_header_size]', array(
		'default'       => '30',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[body_header_size]', array(
		'label'	      => __( 'Header Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_typography',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 22,
			'max'  => 72,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[body_header_spacing]', array(
		'default'       => '0',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_int_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[body_header_spacing]', array(
		'label'	      => __( 'Header Letter Spacing', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_typography',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => -2,
			'max'  => 10,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[body_header_height]', array(
		'default'       => '1',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_float_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[body_header_height]', array(
		'label'	      => __( 'Header Line Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_typography',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0.8,
			'max'  => 3,
			'step' => 0.1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[body_header_style]', array(
		'default'       => '',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[body_header_style]', array(
		'label'	      => __( 'Header Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_general_typography',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[phone_header_font_size]', array(
		'default'       => dhm_get_option( 'tablet_header_font_size', '30' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[phone_header_font_size]', array(
		'label'	      => __( 'Header Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_mobile_phone',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 22,
			'max'  => 72,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[tablet_header_font_size]', array(
		'default'       => dhm_get_option( 'body_header_size', '30' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[tablet_header_font_size]', array(
		'label'	      => __( 'Header Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_mobile_tablet',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 22,
			'max'  => 72,
			'step' => 1
		),
	) ) );

	if ( ! isset( $dhm_one_font_languages[$site_domain] ) ) {
		$wp_customize->add_setting( 'dhm_rrcfest[heading_font]', array(
			'default'		=> 'none',
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'dhm_sanitize_font_choices',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Select_Option ( $wp_customize, 'dhm_rrcfest[heading_font]', array(
			'label'		=> __( 'Header Font', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_general_typography',
			'settings'	=> 'dhm_rrcfest[heading_font]',
			'type'		=> 'select',
			'choices'	=> $font_choices,
		) ) );

		$wp_customize->add_setting( 'dhm_rrcfest[body_font]', array(
			'default'		=> 'none',
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'dhm_sanitize_font_choices',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Select_Option ( $wp_customize, 'dhm_rrcfest[body_font]', array(
			'label'		=> __( 'Body Font', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_general_typography',
			'settings'	=> 'dhm_rrcfest[body_font]',
			'type'		=> 'select',
			'choices'	=> $font_choices
		) ) );
	}

	$wp_customize->add_setting( 'dhm_rrcfest[link_color]', array(
		'default'	=> dhm_get_option( 'accent_color', '#2ea3f2' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[link_color]', array(
		'label'		=> __( 'Body Link Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_typography',
		'settings'	=> 'dhm_rrcfest[link_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[font_color]', array(
		'default'		=> '#666666',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[font_color]', array(
		'label'		=> __( 'Body Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_typography',
		'settings'	=> 'dhm_rrcfest[font_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[header_color]', array(
		'default'		=> '#666666',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[header_color]', array(
		'label'		=> __( 'Header Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_typography',
		'settings'	=> 'dhm_rrcfest[header_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[accent_color]', array(
		'default'		=> '#2ea3f2',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[accent_color]', array(
		'label'		=> __( 'Theme Accent Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_general_layout',
		'settings'	=> 'dhm_rrcfest[accent_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[color_schemes]', array(
		'default'		=> 'none',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_color_scheme',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[color_schemes]', array(
		'label'		=> __( 'Color Schemes', 'Rrcfest' ),
		'section'	=> 'dhm_color_schemes',
		'settings'	=> 'dhm_rrcfest[color_schemes]',
		'type'		=> 'select',
		'choices'	=> dhm_rrcfest_color_scheme_choices(),
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[header_style]', array(
		'default'       => 'left',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_header_style',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[header_style]', array(
		'label'		=> __( 'Header Style', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_layout',
		'type'      => 'select',
		'choices'	=> dhm_rrcfest_header_style_choices(),
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[vertical_nav]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[vertical_nav]', array(
		'label'		=> __( 'Enable Vertical Navigation', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_layout',
		'type'      => 'checkbox',
	) );

	if ( 'on' === dhm_get_option( 'rrcfest_fixed_nav', 'on' ) ) {

		$wp_customize->add_setting( 'dhm_rrcfest[hide_nav]', array(
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'wp_validate_boolean',
		) );

		$wp_customize->add_control( 'dhm_rrcfest[hide_nav]', array(
			'label'		=> __( 'Hide Navigation Until Scroll', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_header_layout',
			'type'      => 'checkbox',
		) );

	} // 'on' === dhm_get_option( 'rrcfest_fixed_nav', 'on' )

	$wp_customize->add_setting( 'dhm_rrcfest[show_header_social_icons]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[show_header_social_icons]', array(
		'label'		=> __( 'Show Social Icons', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_information',
		'type'      => 'checkbox',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[show_search_icon]', array(
		'default'       => 'on',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[show_search_icon]', array(
		'label'		=> __( 'Show Search Icon', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_information',
		'type'      => 'checkbox',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[nav_fullwidth]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[nav_fullwidth]', array(
		'label'		=> __( 'Make Full Width', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_primary',
		'type'      => 'checkbox',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[hide_primary_logo]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[hide_primary_logo]', array(
		'label'		=> __( 'Hide Logo Image', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_primary',
		'type'      => 'checkbox',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[menu_height]', array(
		'default'       => '66',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[menu_height]', array(
		'label'	      => __( 'Menu Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_header_primary',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 30,
			'max'  => 300,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[logo_height]', array(
		'default'       => '54',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[logo_height]', array(
		'label'	      => __( 'Logo Max Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_header_primary',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 30,
			'max'  => 100,
			'step' => 1,
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_font_size]', array(
		'default'       => '14',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[primary_nav_font_size]', array(
		'label'	      => __( 'Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_header_primary',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 12,
			'max'  => 24,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_font_spacing]', array(
		'default'       => '0',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_int_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[primary_nav_font_spacing]', array(
		'label'	      => __( 'Letter Spacing', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_header_primary',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => -1,
			'max'  => 8,
			'step' => 1
		),
	) ) );

	if ( ! isset( $dhm_one_font_languages[$site_domain] ) ) {
		$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_font]', array(
			'default'		=> 'none',
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'dhm_sanitize_font_choices',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Select_Option ( $wp_customize, 'dhm_rrcfest[primary_nav_font]', array(
			'label'		=> __( 'Font', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_header_primary',
			'settings'	=> 'dhm_rrcfest[primary_nav_font]',
			'type'		=> 'select',
			'choices'	=> $font_choices
		) ) );
	}

	$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_font_style]', array(
		'default'       => '',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[primary_nav_font_style]', array(
		'label'	      => __( 'Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_header_primary',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_font_size]', array(
		'default'       => '12',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_fullwidth]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[secondary_nav_fullwidth]', array(
		'label'		=> __( 'Make Full Width', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_secondary',
		'type'      => 'checkbox',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[secondary_nav_font_size]', array(
		'label'	      => __( 'Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_header_secondary',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 12,
			'max'  => 20,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_font_spacing]', array(
		'default'       => '0',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_int_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[secondary_nav_font_spacing]', array(
		'label'	      => __( 'Letter Spacing', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_header_secondary',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => -1,
			'max'  => 8,
			'step' => 1
		),
	) ) );

	if ( ! isset( $dhm_one_font_languages[$site_domain] ) ) {
		$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_font]', array(
			'default'		=> 'none',
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'dhm_sanitize_font_choices',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Select_Option ( $wp_customize, 'dhm_rrcfest[secondary_nav_font]', array(
			'label'		=> __( 'Font', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_header_secondary',
			'settings'	=> 'dhm_rrcfest[secondary_nav_font]',
			'type'		=> 'select',
			'choices'	=> $font_choices
		) ) );
	}

	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_font_style]', array(
		'default'       => '',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[secondary_nav_font_style]', array(
		'label'	      => __( 'Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_header_secondary',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[menu_link]', array(
		'default'		=> 'rgba(0,0,0,0.6)',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[menu_link]', array(
		'label'		=> __( 'Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_primary',
		'settings'	=> 'dhm_rrcfest[menu_link]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[hide_mobile_logo]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[hide_mobile_logo]', array(
		'label'		=> __( 'Hide Logo Image', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_mobile_menu',
		'type'      => 'checkbox',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[mobile_menu_link]', array(
		'default'		=> dhm_get_option( 'menu_link', 'rgba(0,0,0,0.6)' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[mobile_menu_link]', array(
		'label'		=> __( 'Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_mobile_menu',
		'settings'	=> 'dhm_rrcfest[mobile_menu_link]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[menu_link_active]', array(
		'default'		=> dhm_get_option( 'accent_color', '#2ea3f2' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[menu_link_active]', array(
		'label'		=> __( 'Active Link Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_primary',
		'settings'	=> 'dhm_rrcfest[menu_link_active]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_bg]', array(
		'default'		=> '#ffffff',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[primary_nav_bg]', array(
		'label'		=> __( 'Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_primary',
		'settings'	=> 'dhm_rrcfest[primary_nav_bg]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_dropdown_bg]', array(
		'default'		=> dhm_get_option( 'primary_nav_bg', '#ffffff' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[primary_nav_dropdown_bg]', array(
		'label'		=> __( 'Dropdown Menu Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_primary',
		'settings'	=> 'dhm_rrcfest[primary_nav_dropdown_bg]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_dropdown_line_color]', array(
		'default'		=> dhm_get_option( 'accent_color', '#2ea3f2' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[primary_nav_dropdown_line_color]', array(
		'label'		=> __( 'Dropdown Menu Line Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_primary',
		'settings'	=> 'dhm_rrcfest[primary_nav_dropdown_line_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_dropdown_link_color]', array(
		'default'		=> dhm_get_option( 'menu_link', 'rgba(0,0,0,0.7)' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[primary_nav_dropdown_link_color]', array(
		'label'		=> __( 'Dropdown Menu Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_primary',
		'settings'	=> 'dhm_rrcfest[primary_nav_dropdown_link_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_dropdown_animation]', array(
		'default'       => 'fade',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_dropdown_animation',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[primary_nav_dropdown_animation]', array(
		'label'		=> __( 'Dropdown Menu Animation', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_primary',
		'type'      => 'select',
		'choices'	=> dhm_rrcfest_dropdown_animation_choices(),
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[mobile_primary_nav_bg]', array(
		'default'		=> dhm_get_option( 'primary_nav_bg', '#ffffff' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[mobile_primary_nav_bg]', array(
		'label'		=> __( 'Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_mobile_menu',
		'settings'	=> 'dhm_rrcfest[mobile_primary_nav_bg]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_bg]', array(
		'default'		=> dhm_get_option( 'accent_color', '#2ea3f2' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[secondary_nav_bg]', array(
		'label'		=> __( 'Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_secondary',
		'settings'	=> 'dhm_rrcfest[secondary_nav_bg]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_text_color_new]', array(
		'default'		=> '#ffffff',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[secondary_nav_text_color_new]', array(
		'label'		=> __( 'Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_secondary',
		'settings'	=> 'dhm_rrcfest[secondary_nav_text_color_new]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_dropdown_bg]', array(
		'default'		=> dhm_get_option( 'secondary_nav_bg', dhm_get_option( 'accent_color', '#2ea3f2' ) ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[secondary_nav_dropdown_bg]', array(
		'label'		=> __( 'Dropdown Menu Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_secondary',
		'settings'	=> 'dhm_rrcfest[secondary_nav_dropdown_bg]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_dropdown_link_color]', array(
		'default'		=> dhm_get_option( 'secondary_nav_text_color_new', '#ffffff' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[secondary_nav_dropdown_link_color]', array(
		'label'		=> __( 'Dropdown Menu Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_secondary',
		'settings'	=> 'dhm_rrcfest[secondary_nav_dropdown_link_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_dropdown_animation]', array(
		'default'       => 'fade',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_dropdown_animation',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[secondary_nav_dropdown_animation]', array(
		'label'		=> __( 'Dropdown Menu Animation', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_secondary',
		'type'      => 'select',
		'choices'	=> dhm_rrcfest_dropdown_animation_choices(),
	) );

	// Setting with no control kept for backwards compatbility
	$wp_customize->add_setting( 'dhm_rrcfest[primary_nav_text_color]', array(
		'default'       => 'dark',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	// Setting with no control kept for backwards compatbility
	$wp_customize->add_setting( 'dhm_rrcfest[secondary_nav_text_color]', array(
		'default'       => 'light',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	if ( 'on' === dhm_get_option( 'rrcfest_fixed_nav', 'on' ) ) {
		$wp_customize->add_setting( 'dhm_rrcfest[hide_fixed_logo]', array(
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'wp_validate_boolean',
		) );

		$wp_customize->add_control( 'dhm_rrcfest[hide_fixed_logo]', array(
			'label'		=> __( 'Hide Logo Image', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_header_fixed',
			'type'      => 'checkbox',
		) );

		$wp_customize->add_setting( 'dhm_rrcfest[minimized_menu_height]', array(
			'default'       => '40',
			'type'          => 'option',
			'capability'    => 'edit_theme_options',
			'transport'     => 'postMessage',
			'sanitize_callback' => 'absint',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[minimized_menu_height]', array(
			'label'	      => __( 'Fixed Menu Height', 'Rrcfest' ),
			'section'     => 'dhm_rrcfest_header_fixed',
			'type'        => 'range',
			'input_attrs' => array(
				'min'  => 30,
				'max'  => 300,
				'step' => 1
			),
		) ) );

		$wp_customize->add_setting( 'dhm_rrcfest[fixed_primary_nav_font_size]', array(
			'default'       => dhm_get_option( 'primary_nav_font_size', '14' ),
			'type'          => 'option',
			'capability'    => 'edit_theme_options',
			'transport'     => 'postMessage',
			'sanitize_callback' => 'absint',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[fixed_primary_nav_font_size]', array(
			'label'	      => __( 'Text Size', 'Rrcfest' ),
			'section'     => 'dhm_rrcfest_header_fixed',
			'type'        => 'range',
			'input_attrs' => array(
				'min'  => 12,
				'max'  => 24,
				'step' => 1
			),
		) ) );

		$wp_customize->add_setting( 'dhm_rrcfest[fixed_primary_nav_bg]', array(
			'default'		=> dhm_get_option( 'primary_nav_bg', '#ffffff' ),
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'dhm_sanitize_alpha_color',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[fixed_primary_nav_bg]', array(
			'label'		=> __( 'Primary Menu Background Color', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_header_fixed',
			'settings'	=> 'dhm_rrcfest[fixed_primary_nav_bg]',
		) ) );

		$wp_customize->add_setting( 'dhm_rrcfest[fixed_secondary_nav_bg]', array(
			'default'		=> dhm_get_option( 'secondary_nav_bg', '#2ea3f2' ),
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'dhm_sanitize_alpha_color',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[fixed_secondary_nav_bg]', array(
			'label'		=> __( 'Secondary Menu Background Color', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_header_fixed',
			'settings'	=> 'dhm_rrcfest[fixed_secondary_nav_bg]',
		) ) );

		$wp_customize->add_setting( 'dhm_rrcfest[fixed_menu_link]', array(
			'default'       => dhm_get_option( 'menu_link', 'rgba(0,0,0,0.6)' ),
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'dhm_sanitize_alpha_color',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[fixed_menu_link]', array(
			'label'		=> __( 'Primary Menu Link Color', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_header_fixed',
			'settings'	=> 'dhm_rrcfest[fixed_menu_link]',
		) ) );

		$wp_customize->add_setting( 'dhm_rrcfest[fixed_secondary_menu_link]', array(
			'default'       => dhm_get_option( 'secondary_nav_text_color_new', '#ffffff' ),
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage'
		) );

		$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[fixed_secondary_menu_link]', array(
			'label'		=> __( 'Secondary Menu Link Color', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_header_fixed',
			'settings'	=> 'dhm_rrcfest[fixed_secondary_menu_link]',
		) ) );

		$wp_customize->add_setting( 'dhm_rrcfest[fixed_menu_link_active]', array(
			'default'       => dhm_get_option( 'menu_link_active', '#2ea3f2' ),
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'dhm_sanitize_alpha_color',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[fixed_menu_link_active]', array(
			'label'		=> __( 'Active Primary Menu Link Color', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_header_fixed',
			'settings'	=> 'dhm_rrcfest[fixed_menu_link_active]',
		) ) );
	}

	$wp_customize->add_setting( 'dhm_rrcfest[phone_number]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_html_input_text',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[phone_number]', array(
		'label'		=> __( 'Phone Number', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_information',
		'type'      => 'text',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[header_email]', array(
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_email',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[header_email]', array(
		'label'		=> __( 'Email', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_header_information',
		'type'      => 'text',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[show_footer_social_icons]', array(
		'default'       => 'on',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'wp_validate_boolean',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[show_footer_social_icons]', array(
		'label'		=> __( 'Show Social Icons', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_elements',
		'type'      => 'checkbox',
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_columns]', array(
		'default'       => '4',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_footer_column',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[footer_columns]', array(
		'label'		=> __( 'Column Layout', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_layout',
		'settings'	=> 'dhm_rrcfest[footer_columns]',
		'type'		=> 'select',
		'choices'	=> dhm_rrcfest_footer_column_choices(),
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_bg]', array(
		'default'		=> '#222222',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[footer_bg]', array(
		'label'		=> __( 'Footer Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_layout',
		'settings'	=> 'dhm_rrcfest[footer_bg]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[widget_header_font_size]', array(
		'default'       => dhm_get_option( 'body_header_size' * .6, '18' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[widget_header_font_size]', array(
		'label'	      => __( 'Header Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_footer_widgets',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 72,
			'step' => 1,
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[widget_header_font_style]', array(
		'default'       => dhm_get_option( 'widget_header_font_style', '' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[widget_header_font_style]', array(
		'label'	      => __( 'Header Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_footer_widgets',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[widget_body_font_size]', array(
		'default'       => dhm_get_option( 'body_font_size', '14' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[widget_body_font_size]', array(
		'label'	      => __( 'Body/Link Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_footer_widgets',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 32,
			'step' => 1,
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[widget_body_line_height]', array(
		'default'       => '1.7',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_float_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[widget_body_line_height]', array(
		'label'	      => __( 'Body/Link Line Height', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_footer_widgets',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0.8,
			'max'  => 3,
			'step' => 0.1,
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[widget_body_font_style]', array(
		'default'       => dhm_get_option( 'footer_widget_body_font_style', '' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[widget_body_font_style]', array(
		'label'	      => __( 'Body Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_footer_widgets',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_widget_text_color]', array(
		'default'		=> '#fff',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[footer_widget_text_color]', array(
		'label'		=> __( 'Widget Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_widgets',
		'settings'	=> 'dhm_rrcfest[footer_widget_text_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_widget_link_color]', array(
		'default'		=> '#fff',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[footer_widget_link_color]', array(
		'label'		=> __( 'Widget Link Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_widgets',
		'settings'	=> 'dhm_rrcfest[footer_widget_link_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_widget_header_color]', array(
		'default'		=> dhm_get_option( 'accent_color', '#2ea3f2' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[footer_widget_header_color]', array(
		'label'		=> __( 'Widget Header Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_widgets',
		'settings'	=> 'dhm_rrcfest[footer_widget_header_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_widget_bullet_color]', array(
		'default'		=> dhm_get_option( 'accent_color', '#2ea3f2' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'sanitize_hex_color',
	) );

	$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[footer_widget_bullet_color]', array(
		'label'		=> __( 'Widget Bullet Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_widgets',
		'settings'	=> 'dhm_rrcfest[footer_widget_bullet_color]',
	) ) );

	/* Footer Menu */
	$wp_customize->add_setting( 'dhm_rrcfest[footer_menu_background_color]', array(
		'default'		=> 'rgba(255,255,255,0.05)',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[footer_menu_background_color]', array(
		'label'		=> __( 'Footer Menu Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_menu',
		'settings'	=> 'dhm_rrcfest[footer_menu_background_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_menu_text_color]', array(
		'default'		=> '#bbbbbb',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[footer_menu_text_color]', array(
		'label'		=> __( 'Footer Menu Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_menu',
		'settings'	=> 'dhm_rrcfest[footer_menu_text_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_menu_active_link_color]', array(
		'default'		=> dhm_get_option( 'accent_color', '#2ea3f2' ),
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[footer_menu_active_link_color]', array(
		'label'		=> __( 'Footer Menu Active Link Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_footer_menu',
		'settings'	=> 'dhm_rrcfest[footer_menu_active_link_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_menu_letter_spacing]', array(
		'default'       => '0',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[footer_menu_letter_spacing]', array(
		'label'	      => __( 'Letter Spacing', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_footer_menu',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 20,
			'step' => 1,
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_menu_font_style]', array(
		'default'       => dhm_get_option( 'footer_footer_menu_font_style', '' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[footer_menu_font_style]', array(
		'label'	      => __( 'Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_footer_menu',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[footer_menu_font_size]', array(
		'default'       => '14',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[footer_menu_font_size]', array(
		'label'	      => __( 'Font Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_footer_menu',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 32,
			'step' => 1,
		),
	) ) );

	/* Bottom Bar */
	$wp_customize->add_setting( 'dhm_rrcfest[bottom_bar_background_color]', array(
		'default'		=> 'rgba(0,0,0,0.32)',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[bottom_bar_background_color]', array(
		'label'		=> __( 'Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_bottom_bar',
		'settings'	=> 'dhm_rrcfest[bottom_bar_background_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[bottom_bar_text_color]', array(
		'default'		=> '#666666',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[bottom_bar_text_color]', array(
		'label'		=> __( 'Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_bottom_bar',
		'settings'	=> 'dhm_rrcfest[bottom_bar_text_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[bottom_bar_font_style]', array(
		'default'       => dhm_get_option( 'footer_bottom_bar_font_style', '' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[bottom_bar_font_style]', array(
		'label'	      => __( 'Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_bottom_bar',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[bottom_bar_font_size]', array(
		'default'       => '14',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[bottom_bar_font_size]', array(
		'label'	      => __( 'Font Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_bottom_bar',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 32,
			'step' => 1,
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[bottom_bar_social_icon_size]', array(
		'default'       => '24',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[bottom_bar_social_icon_size]', array(
		'label'	      => __( 'Social Icon Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_bottom_bar',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 10,
			'max'  => 32,
			'step' => 1,
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[bottom_bar_social_icon_color]', array(
		'default'		=> '#666666',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[bottom_bar_social_icon_color]', array(
		'label'		=> __( 'Social Icon Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_bottom_bar',
		'settings'	=> 'dhm_rrcfest[bottom_bar_social_icon_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_font_size]', array(
		'default'       => ET_Global_Settings::get_value( 'all_buttons_font_size', 'default' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[all_buttons_font_size]', array(
		'label'	      => __( 'Text Size', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_buttons',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 12,
			'max'  => 30,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_text_color]', array(
		'default'		=> '#ffffff',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[all_buttons_text_color]', array(
		'label'		=> __( 'Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons',
		'settings'	=> 'dhm_rrcfest[all_buttons_text_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_bg_color]', array(
		'default'		=> 'rgba(0,0,0,0)',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[all_buttons_bg_color]', array(
		'label'		=> __( 'Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons',
		'settings'	=> 'dhm_rrcfest[all_buttons_bg_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_border_width]', array(
		'default'       => ET_Global_Settings::get_value( 'all_buttons_border_width', 'default' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[all_buttons_border_width]', array(
		'label'	      => __( 'Border Width', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_buttons',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 10,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_border_color]', array(
		'default'		=> '#ffffff',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[all_buttons_border_color]', array(
		'label'		=> __( 'Border Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons',
		'settings'	=> 'dhm_rrcfest[all_buttons_border_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_border_radius]', array(
		'default'       => ET_Global_Settings::get_value( 'all_buttons_border_radius', 'default' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[all_buttons_border_radius]', array(
		'label'	      => __( 'Border Radius', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_buttons',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 50,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_spacing]', array(
		'default'       => ET_Global_Settings::get_value( 'all_buttons_spacing', 'default' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_int_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[all_buttons_spacing]', array(
		'label'	      => __( 'Letter Spacing', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_buttons',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => -2,
			'max'  => 10,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_font_style]', array(
		'default'       => '',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_style',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[all_buttons_font_style]', array(
		'label'	      => __( 'Button Font Style', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_buttons',
		'type'        => 'font_style',
		'choices'     => dhm_rrcfest_font_style_choices(),
	) ) );

	if ( ! isset( $dhm_one_font_languages[$site_domain] ) ) {
		$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_font]', array(
			'default'		=> 'none',
			'type'			=> 'option',
			'capability'	=> 'edit_theme_options',
			'transport'		=> 'postMessage',
			'sanitize_callback' => 'dhm_sanitize_font_choices',
		) );

		$wp_customize->add_control( new ET_Rrcfest_Select_Option ( $wp_customize, 'dhm_rrcfest[all_buttons_font]', array(
			'label'		=> __( 'Buttons Font', 'Rrcfest' ),
			'section'	=> 'dhm_rrcfest_buttons',
			'settings'	=> 'dhm_rrcfest[all_buttons_font]',
			'type'		=> 'select',
			'choices'	=> $font_choices
		) ) );
	}

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_icon]', array(
		'default'       => 'yes',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_yes_no',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[all_buttons_icon]', array(
		'label'		=> __( 'Add Button Icon', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons',
		'type'      => 'select',
		'choices'	=> dhm_rrcfest_yes_no_choices(),
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_selected_icon]', array(
		'default'       => '5',
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_font_icon',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Icon_Picker_Option ( $wp_customize, 'dhm_rrcfest[all_buttons_selected_icon]', array(
		'label'	      => __( 'Select Icon', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_buttons',
		'type'        => 'icon_picker',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_icon_color]', array(
		'default'		=> '#ffffff',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[all_buttons_icon_color]', array(
		'label'		=> __( 'Icon Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons',
		'settings'	=> 'dhm_rrcfest[all_buttons_icon_color]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_icon_placement]', array(
		'default'       => 'right',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_left_right',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[all_buttons_icon_placement]', array(
		'label'		=> __( 'Icon Placement', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons',
		'type'      => 'select',
		'choices'	=> dhm_rrcfest_left_right_choices(),
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_icon_hover]', array(
		'default'       => 'yes',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_yes_no',
	) );

	$wp_customize->add_control( 'dhm_rrcfest[all_buttons_icon_hover]', array(
		'label'		=> __( 'Only Show Icon on Hover', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons',
		'type'      => 'select',
		'choices'	=> dhm_rrcfest_yes_no_choices(),
	) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_text_color_hover]', array(
		'default'		=> '#ffffff',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[all_buttons_text_color_hover]', array(
		'label'		=> __( 'Text Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons_hover',
		'settings'	=> 'dhm_rrcfest[all_buttons_text_color_hover]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_bg_color_hover]', array(
		'default'		=> 'rgba(255,255,255,0.2)',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[all_buttons_bg_color_hover]', array(
		'label'		=> __( 'Background Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons_hover',
		'settings'	=> 'dhm_rrcfest[all_buttons_bg_color_hover]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_border_color_hover]', array(
		'default'		=> 'rgba(0,0,0,0)',
		'type'			=> 'option',
		'capability'	=> 'edit_theme_options',
		'transport'		=> 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_alpha_color',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[all_buttons_border_color_hover]', array(
		'label'		=> __( 'Border Color', 'Rrcfest' ),
		'section'	=> 'dhm_rrcfest_buttons_hover',
		'settings'	=> 'dhm_rrcfest[all_buttons_border_color_hover]',
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_border_radius_hover]', array(
		'default'       => ET_Global_Settings::get_value( 'all_buttons_border_radius_hover', 'default' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'absint'
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[all_buttons_border_radius_hover]', array(
		'label'	      => __( 'Border Radius', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_buttons_hover',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => 0,
			'max'  => 50,
			'step' => 1
		),
	) ) );

	$wp_customize->add_setting( 'dhm_rrcfest[all_buttons_spacing_hover]', array(
		'default'       => ET_Global_Settings::get_value( 'all_buttons_spacing_hover', 'default' ),
		'type'          => 'option',
		'capability'    => 'edit_theme_options',
		'transport'     => 'postMessage',
		'sanitize_callback' => 'dhm_sanitize_int_number',
	) );

	$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[all_buttons_spacing_hover]', array(
		'label'	      => __( 'Letter Spacing', 'Rrcfest' ),
		'section'     => 'dhm_rrcfest_buttons_hover',
		'type'        => 'range',
		'input_attrs' => array(
			'min'  => -2,
			'max'  => 10,
			'step' => 1
		),
	) ) );
}
endif;

if ( ! function_exists( 'dhm_rrcfest_customizer_module_settings' ) ) :
function dhm_rrcfest_customizer_module_settings( $wp_customize ) {
		/* Section: Image */
		$wp_customize->add_section( 'dhm_pagebuilder_image', array(
		    'priority'       => 10,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Image', 'Rrcfest' ),
		    'description'    => __( 'Image Module Settings', 'Rrcfest' ),
		) );

			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_image-animation]', array(
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_image_animation',
			) );

			$wp_customize->add_control( 'dhm_rrcfest[dhm_pb_image-animation]', array(
				'label'		=> __( 'Animation', 'Rrcfest' ),
				'description' => __( 'This controls default direction of the lazy-loading animation.', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_image',
				'type'      => 'select',
				'choices'	=> dhm_rrcfest_image_animation_choices(),
			) );

		/* Section: Gallery */
		$wp_customize->add_section( 'dhm_pagebuilder_gallery', array(
		    'priority'       => 20,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Gallery', 'Rrcfest' ),
		) );

			// Zoom Icon Color
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_gallery-zoom_icon_color]', array(
				'default'		=> ET_Global_Settings::get_value( 'dhm_pb_gallery-zoom_icon_color', 'default' ), // default color should be theme's accent color
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'sanitize_hex_color',
			) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[dhm_pb_gallery-zoom_icon_color]', array(
				'label'		=> __( 'Zoom Icon Color', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_gallery',
				'settings'	=> 'dhm_rrcfest[dhm_pb_gallery-zoom_icon_color]',
			) ) );

			// Hover Overlay Color
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_gallery-hover_overlay_color]', array(
				'default'		=> ET_Global_Settings::get_value( 'dhm_pb_gallery-hover_overlay_color', 'default' ),
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_alpha_color',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[dhm_pb_gallery-hover_overlay_color]', array(
				'label'		=> __( 'Hover Overlay Color', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_gallery',
				'settings'	=> 'dhm_rrcfest[dhm_pb_gallery-hover_overlay_color]',
			) ) );

			// Title Font Size: Range 10px - 72px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_gallery-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_gallery-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_gallery-title_font_size]', array(
				'label'	      => __( 'Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_gallery',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Title Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_gallery-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_gallery-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_gallery-title_font_style]', array(
				'label'	      => __( 'Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_gallery',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// caption font size Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_gallery-caption_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_gallery-caption_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_gallery-caption_font_size]', array(
				'label'	      => __( 'Caption Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_gallery',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// caption font style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_gallery-caption_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_gallery-caption_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_gallery-caption_font_style]', array(
				'label'	      => __( 'Caption Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_gallery',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Blurb */
		$wp_customize->add_section( 'dhm_pagebuilder_blurb', array(
		    'priority'       => 30,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Blurb', 'Rrcfest' ),
		) );

			// Header Font Size: Range 10px - 72px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_blurb-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_blurb-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_blurb-header_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_blurb',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

		/* Section: Tabs */
		$wp_customize->add_section( 'dhm_pagebuilder_tabs', array(
		    'priority'       => 40,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Tabs', 'Rrcfest' ),
		) );

			// Tab Title Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_tabs-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_tabs-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_tabs-title_font_size]', array(
				'label'	      => __( 'Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_tabs',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Tab Title Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_tabs-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_tabs-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_tabs-title_font_style]', array(
				'label'	      => __( 'Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_tabs',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Padding: Range 0 - 50px
			/* If padding is 20px then the content padding is 20px and the tab padding is: { padding: 10px(50%) 20px; }	*/
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_tabs-padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_tabs-padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_tabs-padding]', array(
				'label'	      => __( 'Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_tabs',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 50,
					'step' => 1,
				),
			) ) );

		/* Section: Slider */
		$wp_customize->add_section( 'dhm_pagebuilder_slider', array(
		    'priority'       => 50,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Slider', 'Rrcfest' ),
		    // 'description'    => '',
		) );

			// Slider Padding: Top/Bottom Only
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_slider-padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_slider-padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_slider-padding]', array(
				'label'	      => __( 'Top & Bottom Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_slider',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 5,
					'max'  => 50,
					'step' => 1,
				),
			) ) );

			// Header Font size: Range 10px - 72px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_slider-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_slider-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_slider-header_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_slider',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Header Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_slider-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_slider-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_slider-header_font_style]', array(
				'label'	      => __( 'Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_slider',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Content Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_slider-body_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_slider-body_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_slider-body_font_size]', array(
				'label'	      => __( 'Content Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_slider',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Content Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_slider-body_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_slider-body_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_slider-body_font_style]', array(
				'label'	      => __( 'Content Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_slider',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Testimonial */
		$wp_customize->add_section( 'dhm_pagebuilder_testimonial', array(
		    'priority'       => 60,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Testimonial', 'Rrcfest' ),
		) );

			// Author Name Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_testimonial-author_name_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_testimonial-author_name_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_testimonial-author_name_font_style]', array(
				'label'	      => __( 'Name Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_testimonial',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Author Details Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_testimonial-author_details_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_testimonial-author_details_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_testimonial-author_details_font_style]', array(
				'label'	      => __( 'Details Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_testimonial',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Portrait Border Radius
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_testimonial-portrait_border_radius]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_testimonial-portrait_border_radius', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_testimonial-portrait_border_radius]', array(
				'label'	      => __( 'Portrait Border Radius', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_testimonial',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
			) ) );

			// Portrait Width
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_testimonial-portrait_width]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_testimonial-portrait_width', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_testimonial-portrait_width]', array(
				'label'	      => __( 'Image Width', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_testimonial',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 200,
					'step' => 1,
				),
			) ) );

			// Portrait Height
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_testimonial-portrait_height]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_testimonial-portrait_height', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_testimonial-portrait_height]', array(
				'label'	      => __( 'Image Height', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_testimonial',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 200,
					'step' => 1,
				),
			) ) );

		/* Section: Pricing Table */
		$wp_customize->add_section( 'dhm_pagebuilder_pricing_table', array(
		    'priority'       => 70,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Pricing Table', 'Rrcfest' ),
		) );

			// Header Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_pricing_tables-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_pricing_tables-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_pricing_tables-header_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_pricing_table',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Header Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_pricing_tables-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_pricing_tables-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_pricing_tables-header_font_style]', array(
				'label'	      => __( 'Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_pricing_table',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Subhead Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_pricing_tables-subheader_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_pricing_tables-subheader_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_pricing_tables-subheader_font_size]', array(
				'label'	      => __( 'Subheader Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_pricing_table',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Subhead Font Style:  B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_pricing_tables-subheader_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_pricing_tables-subheader_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_pricing_tables-subheader_font_style]', array(
				'label'	      => __( 'Subheader Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_pricing_table',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Price font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_pricing_tables-price_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_pricing_tables-price_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_pricing_tables-price_font_size]', array(
				'label'	      => __( 'Price Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_pricing_table',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 100,
					'step' => 1,
				),
			) ) );

			// Price font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_pricing_tables-price_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_pricing_tables-price_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_pricing_tables-price_font_style]', array(
				'label'	      => __( 'Pricing Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_pricing_table',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Call To Action */
		$wp_customize->add_section( 'dhm_pagebuilder_call_to_action', array(
		    'priority'       => 80,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Call To Action', 'Rrcfest' ),
		) );

			// Header font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_cta-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_cta-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_cta-header_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_call_to_action',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Header Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_cta-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_cta-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_cta-header_font_style]', array(
				'label'	      => __( 'Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_call_to_action',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Padding: Range 0px - 200px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_cta-custom_padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_cta-custom_padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_cta-custom_padding]', array(
				'label'	      => __( 'Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_call_to_action',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 200,
					'step' => 1,
				),
			) ) );

		/* Section: Audio */
		$wp_customize->add_section( 'dhm_pagebuilder_audio', array(
		    'priority'       => 90,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Audio', 'Rrcfest' ),
		) );

			// Header Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_audio-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_audio-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_audio-title_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_audio',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Header Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_audio-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_audio-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_audio-title_font_style]', array(
				'label'	      => __( 'Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_audio',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Subhead Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_audio-caption_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_audio-caption_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_audio-caption_font_size]', array(
				'label'	      => __( 'Subheader Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_audio',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Subhead Font Style:  B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_audio-caption_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_audio-caption_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_audio-caption_font_style]', array(
				'label'	      => __( 'Subheader Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_audio',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Email Optin */
		$wp_customize->add_section( 'dhm_pagebuilder_subscribe', array(
		    'priority'       => 100,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Email Optin', 'Rrcfest' ),
		) );

			// Header font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_signup-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_signup-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_signup-header_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_subscribe',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Header Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_signup-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_signup-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_signup-header_font_style]', array(
				'label'	      => __( 'Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_subscribe',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Padding: Range 0px - 200px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_signup-padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_signup-padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_signup-padding]', array(
				'label'	      => __( 'Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_subscribe',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 200,
					'step' => 1,
				),
			) ) );

		/* Section: Login */
		$wp_customize->add_section( 'dhm_pagebuilder_login', array(
		    'priority'       => 110,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Login', 'Rrcfest' ),
		    // 'description'    => '',
		) );

			// Header font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_login-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_login-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_login-header_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_login',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Header Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_login-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_login-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_login-header_font_style]', array(
				'label'	      => __( 'Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_login',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Padding: Range 0px - 200px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_login-custom_padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_login-custom_padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_login-custom_padding]', array(
				'label'	      => __( 'Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_login',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 200,
					'step' => 1,
				),
			) ) );

		/* Section: Portfolio */
		$wp_customize->add_section( 'dhm_pagebuilder_portfolio', array(
		    'priority'       => 120,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Portfolio', 'Rrcfest' ),
		) );

			// Zoom Icon Color
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_portfolio-zoom_icon_color]', array(
				'default'		=> ET_Global_Settings::get_value( 'dhm_pb_portfolio-zoom_icon_color', 'default' ),
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'sanitize_hex_color',
			) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[dhm_pb_portfolio-zoom_icon_color]', array(
				'label'		=> __( 'Zoom Icon Color', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_portfolio',
				'settings'	=> 'dhm_rrcfest[dhm_pb_portfolio-zoom_icon_color]',
			) ) );

			// Hover Overlay Color
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_portfolio-hover_overlay_color]', array(
				'default'		=> ET_Global_Settings::get_value( 'dhm_pb_portfolio-hover_overlay_color', 'default' ),
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_alpha_color',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[dhm_pb_portfolio-hover_overlay_color]', array(
				'label'		=> __( 'Hover Overlay Color', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_portfolio',
				'settings'	=> 'dhm_rrcfest[dhm_pb_portfolio-hover_overlay_color]',
			) ) );

			// Title Font Size: Range 10px - 72px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_portfolio-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_portfolio-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_portfolio-title_font_size]', array(
				'label'	      => __( 'Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_portfolio',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Title Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_portfolio-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_portfolio-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_portfolio-title_font_style]', array(
				'label'	      => __( 'Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_portfolio',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Category font size Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_portfolio-caption_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_portfolio-caption_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_portfolio-caption_font_size]', array(
				'label'	      => __( 'Caption Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_portfolio',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Category Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_portfolio-caption_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_portfolio-caption_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_portfolio-caption_font_style]', array(
				'label'	      => __( 'Caption Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_portfolio',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Filterable Portfolio */
		$wp_customize->add_section( 'dhm_pagebuilder_filterable_portfolio', array(
		    'priority'       => 130,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Filterable Portfolio', 'Rrcfest' ),
		) );

			// Zoom Icon Color
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_filterable_portfolio-zoom_icon_color]', array(
				'default'		=> ET_Global_Settings::get_value( 'dhm_pb_filterable_portfolio-zoom_icon_color', 'default' ),
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'sanitize_hex_color',
			) );

			$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'dhm_rrcfest[dhm_pb_filterable_portfolio-zoom_icon_color]', array(
				'label'		=> __( 'Zoom Icon Color', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_filterable_portfolio',
				'settings'	=> 'dhm_rrcfest[dhm_pb_filterable_portfolio-zoom_icon_color]',
			) ) );

			// Hover Overlay Color
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_filterable_portfolio-hover_overlay_color]', array(
				'default'		=> ET_Global_Settings::get_value( 'dhm_pb_filterable_portfolio-hover_overlay_color', 'default' ),
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_alpha_color',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Customize_Color_Alpha_Control( $wp_customize, 'dhm_rrcfest[dhm_pb_filterable_portfolio-hover_overlay_color]', array(
				'label'		=> __( 'Hover Overlay Color', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_filterable_portfolio',
				'settings'	=> 'dhm_rrcfest[dhm_pb_filterable_portfolio-hover_overlay_color]',
			) ) );

			// Title Font Size: Range 10px - 72px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_filterable_portfolio-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_filterable_portfolio-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_filterable_portfolio-title_font_size]', array(
				'label'	      => __( 'Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_filterable_portfolio',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Title Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_filterable_portfolio-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_filterable_portfolio-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_filterable_portfolio-title_font_style]', array(
				'label'	      => __( 'Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_filterable_portfolio',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Category font size Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_filterable_portfolio-caption_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_filterable_portfolio-caption_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_filterable_portfolio-caption_font_size]', array(
				'label'	      => __( 'Caption Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_filterable_portfolio',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Category Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_filterable_portfolio-caption_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_filterable_portfolio-caption_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_filterable_portfolio-caption_font_style]', array(
				'label'	      => __( 'Caption Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_filterable_portfolio',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Filters Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_filterable_portfolio-filter_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_filterable_portfolio-filter_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_filterable_portfolio-filter_font_size]', array(
				'label'	      => __( 'Filters Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_filterable_portfolio',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Filters Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_filterable_portfolio-filter_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_filterable_portfolio-filter_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_filterable_portfolio-filter_font_style]', array(
				'label'	      => __( 'Filters Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_filterable_portfolio',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Bar Counter */
		$wp_customize->add_section( 'dhm_pagebuilder_bar_counter', array(
		    'priority'       => 140,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Bar Counter', 'Rrcfest' ),
		) );

			// Label Font Size
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_counters-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_counters-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_counters-title_font_size]', array(
				'label'	      => __( 'Label Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_bar_counter',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Labels Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_counters-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_counters-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_counters-title_font_style]', array(
				'label'	      => __( 'Label Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_bar_counter',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Percent Font Size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_counters-percent_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_counters-percent_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_counters-percent_font_size]', array(
				'label'	      => __( 'Percent Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_bar_counter',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Percent Font Style: : B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_counters-percent_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_counters-percent_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_counters-percent_font_style]', array(
				'label'	      => __( 'Percent Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_bar_counter',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Bar Padding: Range 0px - 30px (top and bottom padding only)
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_counters-padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_counters-padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_counters-padding]', array(
				'label'	      => __( 'Bar Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_bar_counter',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 50,
					'step' => 1,
				),
			) ) );

			// Bar Border Radius
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_counters-border_radius]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_counters-border_radius', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_counters-border_radius]', array(
				'label'	      => __( 'Bar Border Radius', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_bar_counter',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 80,
					'step' => 1,
				),
			) ) );

		/* Section: Circle Counter */
		$wp_customize->add_section( 'dhm_pagebuilder_circle_counter', array(
		    'priority'       => 150,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Circle Counter', 'Rrcfest' ),
		) );
			// Number Font Size
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_circle_counter-number_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_circle_counter-number_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_circle_counter-number_font_size]', array(
				'label'	      => __( 'Number Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_circle_counter',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Number Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_circle_counter-number_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_circle_counter-number_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_circle_counter-number_font_style]', array(
				'label'	      => __( 'Number Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_circle_counter',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Title Font Size: Range 10px - 72px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_circle_counter-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_circle_counter-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_circle_counter-title_font_size]', array(
				'label'	      => __( 'Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_circle_counter',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Title Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_circle_counter-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_circle_counter-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_circle_counter-title_font_style]', array(
				'label'	      => __( 'Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_circle_counter',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Number Counter */
		$wp_customize->add_section( 'dhm_pagebuilder_number_counter', array(
		    'priority'       => 160,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Number Counter', 'Rrcfest' ),
		) );

			// Number Font Size
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_number_counter-number_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_number_counter-number_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_number_counter-number_font_size]', array(
				'label'	      => __( 'Number Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_number_counter',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Number Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_number_counter-number_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_number_counter-number_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_number_counter-number_font_style]', array(
				'label'	      => __( 'Number Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_number_counter',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Title Font Size: Range 10px - 72px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_number_counter-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_number_counter-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_number_counter-title_font_size]', array(
				'label'	      => __( 'Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_number_counter',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Title Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_number_counter-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_number_counter-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_number_counter-title_font_style]', array(
				'label'	      => __( 'Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_number_counter',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Accordion */
		$wp_customize->add_section( 'dhm_pagebuilder_accordion', array(
		    'priority'       => 170,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Accordion', 'Rrcfest' ),
		) );
			// Title Font Size
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_accordion-toggle_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_accordion-toggle_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_accordion-toggle_font_size]', array(
				'label'	      => __( 'Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_accordion',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Accordion Title Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_accordion-toggle_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_accordion-toggle_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_accordion-toggle_font_style]', array(
				'label'	      => __( 'Opened Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_accordion',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Inactive Accordion Title Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_accordion-inactive_toggle_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_accordion-inactive_title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_accordion-inactive_toggle_font_style]', array(
				'label'	      => __( 'Closed Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_accordion',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Toggle Accordion Icon Font Size
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_accordion-toggle_icon_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_accordion-toggle_icon_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_accordion-toggle_icon_size]', array(
				'label'	      => __( 'Toggle Icon Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_accordion',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 16,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Padding: Range 0 - 50px
			/* Padding effects each individual Accordion */
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_accordion-custom_padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_accordion-custom_padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_accordion-custom_padding]', array(
				'label'	      => __( 'Toggle Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_accordion',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 50,
					'step' => 1,
				),
			) ) );

		/* Section: Toggle */
		$wp_customize->add_section( 'dhm_pagebuilder_toggle', array(
		    'priority'       => 180,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Toggle', 'Rrcfest' ),
		) );

			// Title Font Size
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_toggle-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_toggle-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_toggle-title_font_size]', array(
				'label'	      => __( 'Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_toggle',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Toggle Title Font Style
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_toggle-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_toggle-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_toggle-title_font_style]', array(
				'label'	      => __( 'Opened Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_toggle',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Inactive Toggle Title Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_toggle-inactive_title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_toggle-inactive_title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_toggle-inactive_title_font_style]', array(
				'label'	      => __( 'Closed Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_toggle',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Open& Close Icon Font Size
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_toggle-toggle_icon_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_toggle-toggle_icon_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_toggle-toggle_icon_size]', array(
				'label'	      => __( 'Toggle Icon Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_toggle',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 16,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Padding: Range 0 - 50px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_toggle-custom_padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_toggle-custom_padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_toggle-custom_padding]', array(
				'label'	      => __( 'Toggle Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_toggle',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 50,
					'step' => 1,
				),
			) ) );

		/* Section: Contact Form */
		$wp_customize->add_section( 'dhm_pagebuilder_contact_form', array(
		    'priority'       => 190,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Contact Form', 'Rrcfest' ),
		) );

			// Header Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_contact_form-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_contact_form-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_contact_form-title_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_contact_form',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Header Font Style:  B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_contact_form-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_contact_form-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_contact_form-title_font_style]', array(
				'label'	      => __( 'Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_contact_form',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Input Field Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_contact_form-form_field_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_contact_form-form_field_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_contact_form-form_field_font_size]', array(
				'label'	      => __( 'Input Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_contact_form',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Input Field Font Style:  B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_contact_form-form_field_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_contact_form-form_field_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_contact_form-form_field_font_style]', array(
				'label'	      => __( 'Input Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_contact_form',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Input Field Padding: Range 0 - 50px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_contact_form-padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_contact_form-padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_contact_form-padding]', array(
				'label'	      => __( 'Input Field Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_contact_form',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 50,
					'step' => 1,
				),
			) ) );

			// Captcha Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_contact_form-captcha_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_contact_form-captcha_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_contact_form-captcha_font_size]', array(
				'label'	      => __( 'Captcha Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_contact_form',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Captcha Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_contact_form-captcha_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_contact_form-captcha_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_contact_form-captcha_font_style]', array(
				'label'	      => __( 'Captcha Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_contact_form',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Sidebar */
		$wp_customize->add_section( 'dhm_pagebuilder_sidebar', array(
		    'priority'       => 200,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Sidebar', 'Rrcfest' ),
		) );

			// Header Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_sidebar-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_sidebar-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_sidebar-header_font_size]', array(
				'label'	      => __( 'Widget Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_sidebar',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Header font style
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_sidebar-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_sidebar-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_sidebar-header_font_style]', array(
				'label'	      => __( 'Widget Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_sidebar',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Show/hide Vertical Divider
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_sidebar-remove_border]', array(
				'default'		=> ET_Global_Settings::get_checkbox_value( 'dhm_pb_sidebar-remove_border', 'default' ),
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'wp_validate_boolean',
			) );

			$wp_customize->add_control( 'dhm_rrcfest[dhm_pb_sidebar-remove_border]', array(
				'label'		=> __( 'Remove Vertical Divider', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_sidebar',
				'type'      => 'checkbox',
			) );

		/* Section: Divider */
		$wp_customize->add_section( 'dhm_pagebuilder_divider', array(
		    'priority'       => 200,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Divider', 'Rrcfest' ),
		) );

			// Show/hide Divider
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_divider-show_divider]', array(
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'wp_validate_boolean',
			) );

			$wp_customize->add_control( 'dhm_rrcfest[dhm_pb_divider-show_divider]', array(
				'label'		=> __( 'Show Divider', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_divider',
				'type'      => 'checkbox',
			) );

			// Divider Style
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_divider-divider_style]', array(
				'default'		=> ET_Global_Settings::get_value( 'dhm_pb_divider-divider_style', 'default' ),
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_divider_style',
			) );

			$wp_customize->add_control( 'dhm_rrcfest[dhm_pb_divider-divider_style]', array(
				'label'		=> __( 'Divider Style', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_divider',
				'settings'	=> 'dhm_rrcfest[dhm_pb_divider-divider_style]',
				'type'		=> 'select',
				'choices'	=> dhm_rrcfest_divider_style_choices(),
			) );

			// Divider Weight
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_divider-divider_weight]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_divider-divider_weight', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_divider-divider_weight]', array(
				'label'	      => __( 'Divider Weight', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_divider',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
			) ) );

			// Divider Height
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_divider-height]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_divider-height', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_divider-height]', array(
				'label'	      => __( 'Divider Height', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_divider',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 0,
					'max'  => 100,
					'step' => 1,
				),
			) ) );

			// Divider Position
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_divider-divider_position]', array(
				'default'		=> ET_Global_Settings::get_value( 'dhm_pb_divider-divider_position', 'default' ),
				'type'			=> 'option',
				'capability'	=> 'edit_theme_options',
				'transport'		=> 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_divider_position',
			) );

			$wp_customize->add_control( 'dhm_rrcfest[dhm_pb_divider-divider_position]', array(
				'label'		=> __( 'Divider Position', 'Rrcfest' ),
				'section'	=> 'dhm_pagebuilder_divider',
				'settings'	=> 'dhm_rrcfest[dhm_pb_divider-divider_position]',
				'type'		=> 'select',
				'choices'	=> dhm_rrcfest_divider_position_choices(),
			) );

		/* Section: Person */
		$wp_customize->add_section( 'dhm_pagebuilder_person', array(
		    'priority'       => 210,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Person', 'Rrcfest' ),
		) );

			// Header Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_team_member-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_team_member-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_team_member-header_font_size]', array(
				'label'	      => __( 'Name Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_person',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Header font style
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_team_member-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_team_member-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_team_member-header_font_style]', array(
				'label'	      => __( 'Name Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_person',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Subhead Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_team_member-subheader_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_team_member-subheader_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_team_member-subheader_font_size]', array(
				'label'	      => __( 'Subheader Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_person',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Subhead Font Style:  B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_team_member-subheader_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_team_member-subheader_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_team_member-subheader_font_style]', array(
				'label'	      => __( 'Subheader Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_person',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Network Icons size: Range 16px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_team_member-social_network_icon_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_team_member-social_network_icon_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_team_member-social_network_icon_size]', array(
				'label'	      => __( 'Social Network Icon Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_person',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 16,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

		/* Section: Blog */
		$wp_customize->add_section( 'dhm_pagebuilder_blog', array(
		    'priority'       => 220,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Blog', 'Rrcfest' ),
		) );

			// Post Title Font Size
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_blog-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_blog-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_blog-header_font_size]', array(
				'label'	      => __( 'Post Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_blog',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Post Title Font Style
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_blog-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_blog-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_blog-header_font_style]', array(
				'label'	      => __( 'Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_blog',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Meta Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_blog-meta_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_blog-meta_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_blog-meta_font_size]', array(
				'label'	      => __( 'Meta Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_blog',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Meta Field Font Style:  B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_blog-meta_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_blog-meta_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_blog-meta_font_style]', array(
				'label'	      => __( 'Meta Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_blog',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Blog Grid */
		$wp_customize->add_section( 'dhm_pagebuilder_masonry_blog', array(
		    'priority'       => 230,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Blog Grid', 'Rrcfest' ),
		) );

			// Post Title Font Size
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_blog_masonry-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_blog_masonry-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_blog_masonry-header_font_size]', array(
				'label'	      => __( 'Post Title Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_masonry_blog',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Post Title Font Style
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_blog_masonry-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_blog_masonry-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_blog_masonry-header_font_style]', array(
				'label'	      => __( 'Title Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_masonry_blog',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Meta Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_blog_masonry-meta_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_blog_masonry-meta_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_blog_masonry-meta_font_size]', array(
				'label'	      => __( 'Meta Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_masonry_blog',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Meta Field Font Style:  B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_blog_masonry-meta_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_blog_masonry-meta_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_blog_masonry-meta_font_style]', array(
				'label'	      => __( 'Meta Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_masonry_blog',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Shop */
		$wp_customize->add_section( 'dhm_pagebuilder_shop', array(
		    'priority'       => 240,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Shop', 'Rrcfest' ),
		) );

			// Product Name Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_shop-title_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_shop-title_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_shop-title_font_size]', array(
				'label'	      => __( 'Product Name Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_shop',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Product Name Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_shop-title_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_shop-title_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_shop-title_font_style]', array(
				'label'	      => __( 'Product Name Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_shop',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Sale Badge Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_shop-sale_badge_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_shop-sale_badge_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_shop-sale_badge_font_size]', array(
				'label'	      => __( 'Sale Badge Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_shop',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Sale Badge Font Style:  B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_shop-sale_badge_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_shop-sale_badge_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_shop-sale_badge_font_style]', array(
				'label'	      => __( 'Sale Badge Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_shop',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Price Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_shop-price_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_shop-price_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_shop-price_font_size]', array(
				'label'	      => __( 'Price Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_shop',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Price Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_shop-price_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_shop-price_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_shop-price_font_style]', array(
				'label'	      => __( 'Price Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_shop',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Sale Price Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_shop-sale_price_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_shop-sale_price_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_shop-sale_price_font_size]', array(
				'label'	      => __( 'Sale Price Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_shop',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Sale Price Font Style: B / I / TT / U/
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_shop-sale_price_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_shop-sale_price_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_shop-sale_price_font_style]', array(
				'label'	      => __( 'Sale Price Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_shop',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Countdown */
		$wp_customize->add_section( 'dhm_pagebuilder_countdown', array(
		    'priority'       => 250,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Countdown', 'Rrcfest' ),
		) );

			// Header Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_countdown_timer-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_countdown_timer-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_countdown_timer-header_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_countdown',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Header Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_countdown_timer-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_countdown_timer-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_countdown_timer-header_font_style]', array(
				'label'	      => __( 'Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_countdown',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Social Follow */
		$wp_customize->add_section( 'dhm_pagebuilder_social_follow', array(
		    'priority'       => 250,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Social Follow', 'Rrcfest' ),
		) );

			// Follow Button Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_social_media_follow-icon_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_social_media_follow-icon_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_social_media_follow-icon_size]', array(
				'label'	      => __( 'Follow Font & Icon Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_social_follow',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Follow Button Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_social_media_follow-button_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_social_media_follow-button_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_social_media_follow-button_font_style]', array(
				'label'	      => __( 'Button Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_social_follow',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

		/* Section: Fullwidth Slider */
		$wp_customize->add_section( 'dhm_pagebuilder_fullwidth_slider', array(
		    'priority'       => 270,
		    'capability'     => 'edit_theme_options',
		    'title'          => __( 'Fullwidth Slider', 'Rrcfest' ),
		) );

			// Slider Padding: Top/Bottom Only
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_fullwidth_slider-padding]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_fullwidth_slider-padding', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_fullwidth_slider-padding]', array(
				'label'	      => __( 'Top & Bottom Padding', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_fullwidth_slider',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 5,
					'max'  => 50,
					'step' => 1,
				),
			) ) );

			// Header Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_fullwidth_slider-header_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_fullwidth_slider-header_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_fullwidth_slider-header_font_size]', array(
				'label'	      => __( 'Header Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_fullwidth_slider',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 72,
					'step' => 1,
				),
			) ) );

			// Header Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_fullwidth_slider-header_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_fullwidth_slider-header_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_fullwidth_slider-header_font_style]', array(
				'label'	      => __( 'Header Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_fullwidth_slider',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );

			// Content Font size: Range 10px - 32px
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_fullwidth_slider-body_font_size]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_fullwidth_slider-body_font_size', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'absint',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Range_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_fullwidth_slider-body_font_size]', array(
				'label'	      => __( 'Content Font Size', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_fullwidth_slider',
				'type'        => 'range',
				'input_attrs' => array(
					'min'  => 10,
					'max'  => 32,
					'step' => 1,
				),
			) ) );

			// Content Font Style: B / I / TT / U
			$wp_customize->add_setting( 'dhm_rrcfest[dhm_pb_fullwidth_slider-body_font_style]', array(
				'default'       => ET_Global_Settings::get_value( 'dhm_pb_fullwidth_slider-body_font_style', 'default' ),
				'type'          => 'option',
				'capability'    => 'edit_theme_options',
				'transport'     => 'postMessage',
				'sanitize_callback' => 'dhm_sanitize_font_style',
			) );

			$wp_customize->add_control( new ET_Rrcfest_Font_Style_Option ( $wp_customize, 'dhm_rrcfest[dhm_pb_fullwidth_slider-body_font_style]', array(
				'label'	      => __( 'Content Font Style', 'Rrcfest' ),
				'section'     => 'dhm_pagebuilder_fullwidth_slider',
				'type'        => 'font_style',
				'choices'     => dhm_rrcfest_font_style_choices(),
			) ) );
}
endif;

/**
 * Add action hook to the footer in customizer preview.
 */
function dhm_customizer_preview_footer_action() {
	if ( is_customize_preview() ) {
		do_action( 'dhm_customizer_footer_preview' );
	}
}
add_action( 'wp_footer', 'dhm_customizer_preview_footer_action' );

/**
 * Add container with social icons to the footer in customizer preview.
 * Used to get the icons and append them into the header when user enables the header social icons in customizer.
 */
function dhm_load_social_icons() {
	echo '<div class="dhm_customizer_social_icons" style="display:none;">';
		get_template_part( 'includes/social_icons', 'header' );
	echo '</div>';
}
add_action( 'dhm_customizer_footer_preview', 'dhm_load_social_icons' );

function dhm_rrcfest_customize_preview_js() {
	$theme_version = dhm_get_theme_version();
	wp_enqueue_script( 'rrcfest-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), $theme_version, true );
}
add_action( 'customize_preview_init', 'dhm_rrcfest_customize_preview_js' );

function dhm_rrcfest_customize_preview_css() {
	$theme_version = dhm_get_theme_version();

	wp_enqueue_style( 'rrcfest-customizer-controls-styles', get_template_directory_uri() . '/css/theme-customizer-controls-styles.css', array(), $theme_version );
	wp_enqueue_script( 'rrcfest-customizer-controls-js', get_template_directory_uri() . '/js/theme-customizer-controls.js', array( 'jquery' ), $theme_version, true );
}
add_action( 'customize_controls_enqueue_scripts', 'dhm_rrcfest_customize_preview_css' );

/**
 * Add custom customizer control
 * Check for WP_Customizer_Control existence before adding custom control because WP_Customize_Control is loaded on customizer page only
 *
 * @see _wp_customize_include()
 */
if ( class_exists( 'WP_Customize_Control' ) ) {

	/**
	 * Font style control for Customizer
	 */
	class ET_Rrcfest_Font_Style_Option extends WP_Customize_Control {
		public $type = 'font_style';
		public function render_content() {
			?>
			<label>
				<?php if ( ! empty( $this->label ) ) : ?>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php endif;
				if ( ! empty( $this->description ) ) : ?>
					<span class="description customize-control-description"><?php echo $this->description; ?></span>
				<?php endif; ?>
			</label>
			<?php $current_values = explode('|', $this->value() );
			if ( empty( $this->choices ) )
				return;
			foreach ( $this->choices as $value => $label ) :
				$checked_class = in_array( $value, $current_values ) ? ' dhm_font_style_checked' : '';
				?>
					<span class="dhm_font_style dhm_font_value_<?php echo $value; echo $checked_class; ?>">
						<input type="checkbox" class="dhm_font_style_checkbox" value="<?php echo esc_attr( $value ); ?>" <?php checked( in_array( $value, $current_values ) ); ?> />
					</span>
				<?php
			endforeach;
			?>
			<input type="hidden" class="dhm_font_styles" <?php $this->input_attrs(); ?> value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
			<?php
		}
	}

	/**
	 * Icon picker control for Customizer
	 */
	class ET_Rrcfest_Icon_Picker_Option extends WP_Customize_Control {
		public $type = 'icon_picker';

		public function render_content() {

		?>
		<label>
			<?php if ( ! empty( $this->label ) ) : ?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<?php endif;
			dhm_pb_font_icon_list(); ?>
			<input type="hidden" class="dhm_selected_icon" <?php $this->input_attrs(); ?> value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> />
		</label>
		<?php
		}
	}

	/**
	 * Range-based sliding value picker for Customizer
	 */
	class ET_Rrcfest_Range_Option extends WP_Customize_Control {
		public $type = 'range';

		public function render_content() {
		?>
		<label>
			<?php if ( ! empty( $this->label ) ) : ?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<?php endif;
			if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo $this->description; ?></span>
			<?php endif; ?>
			<input type="<?php echo esc_attr( $this->type ); ?>" <?php $this->input_attrs(); ?> value="<?php echo esc_attr( $this->value() ); ?>" <?php $this->link(); ?> data-reset_value="<?php echo esc_attr( $this->setting->default ); ?>" />
			<input type="number" <?php $this->input_attrs(); ?> class="dhm-pb-range-input" value="<?php echo esc_attr( $this->value() ); ?>" />
			<span class="dhm_rrcfest_reset_slider"></span>
		</label>
		<?php
		}
	}

	/**
	 * Custom Select option which supports data attributes for the <option> tags
	 */
	class ET_Rrcfest_Select_Option extends WP_Customize_Control {
		public $type = 'select';

		public function render_content() {
		?>
		<label>
			<?php if ( ! empty( $this->label ) ) : ?>
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<?php endif;
			if ( ! empty( $this->description ) ) : ?>
				<span class="description customize-control-description"><?php echo $this->description; ?></span>
			<?php endif; ?>

			<select <?php $this->link(); ?>>
				<?php
				foreach ( $this->choices as $value => $attributes ) {
					$data_output = '';

					if ( ! empty( $attributes['data'] ) ) {
						foreach( $attributes['data'] as $data_name => $data_value ) {
							if ( '' !== $data_value ) {
								$data_output .= sprintf( ' data-%1$s="%2$s"',
									esc_attr( $data_name ),
									esc_attr( $data_value )
								);
							}
						}
					}

					echo '<option value="' . esc_attr( $value ) . '"' . selected( $this->value(), $value, false ) . $data_output . '>' . $attributes['label'] . '</option>';
				}
				?>
			</select>
		</label>
		<?php
		}
	}

	/**
	 * Color picker with alpha color support for Customizer
	 */
	class ET_Rrcfest_Customize_Color_Alpha_Control extends WP_Customize_Control {
		public $type = 'dhm_coloralpha';

		public $statuses;

		public function __construct( $manager, $id, $args = array() ) {
			$this->statuses = array( '' => __( 'Default', 'Rrcfest' ) );
			parent::__construct( $manager, $id, $args );
		}

		public function enqueue() {
			wp_enqueue_script( 'wp-color-picker-alpha' );
			wp_enqueue_style( 'wp-color-picker' );
		}

		public function to_json() {
			parent::to_json();
			$this->json['statuses'] = $this->statuses;
			$this->json['defaultValue'] = $this->setting->default;
		}

		public function render_content() {}

		public function content_template() {
			?>
			<# var defaultValue = '';
			if ( data.defaultValue ) {
				if ( '#' !== data.defaultValue.substring( 0, 1 ) && 'rgba' !== data.defaultValue.substring( 0, 4 ) ) {
					defaultValue = '#' + data.defaultValue;
				} else {
					defaultValue = data.defaultValue;
				}
				defaultValue = ' data-default-color=' + defaultValue; // Quotes added automatically.
			} #>
			<label>
				<# if ( data.label ) { #>
					<span class="customize-control-title">{{{ data.label }}}</span>
				<# } #>
				<# if ( data.description ) { #>
					<span class="description customize-control-description">{{{ data.description }}}</span>
				<# } #>
				<div class="customize-control-content">
					<input class="color-picker-hex" data-alpha="true" type="text" maxlength="30" placeholder="<?php esc_attr_e( 'Hex Value', 'Rrcfest' ); ?>" {{ defaultValue }} />
				</div>
			</label>
			<?php
		}
	}

}

function dhm_rrcfest_add_customizer_css(){ ?>
	<?php
		// Detect legacy settings
		$detect_legacy_secondary_nav_color = dhm_get_option( 'secondary_nav_text_color', 'Light' );
		$detect_legacy_primary_nav_color = dhm_get_option( 'primary_nav_text_color', 'Dark' );

		if ( $detect_legacy_primary_nav_color == 'Light' ) {
			$legacy_primary_nav_color = '#ffffff';
		} else {
			$legacy_primary_nav_color = 'rgba(0,0,0,0.6)';
		}

		if ( $detect_legacy_secondary_nav_color == 'Light' ) {
			$legacy_secondary_nav_color = '#ffffff';
		} else {
			$legacy_secondary_nav_color = 'rgba(0,0,0,0.7)';
		}

		$body_font_size = absint( dhm_get_option( 'body_font_size', '14' ) );
		$body_font_height = floatval( dhm_get_option( 'body_font_height', '1.7' ) );
		$body_header_size = absint( dhm_get_option( 'body_header_size', '30' ) );
		$body_header_style = dhm_get_option( 'body_header_style', '', '', true );
		$body_header_spacing = intval( dhm_get_option( 'body_header_spacing', '0' ) );
		$body_header_height = floatval( dhm_get_option( 'body_header_height', '1' ) );
		$body_font_color = dhm_get_option( 'font_color', '#666666' );
		$body_header_color = dhm_get_option( 'header_color', '#666666' );

		$accent_color = dhm_get_option( 'accent_color', '#2ea3f2' );
		$link_color = dhm_get_option( 'link_color', $accent_color );

		$content_width = absint( dhm_get_option( 'content_width', '1080' ) );
		$large_content_width = intval ( $content_width * 1.25 );
		$use_sidebar_width = dhm_get_option( 'use_sidebar_width', false );
		$sidebar_width = intval( dhm_get_option( 'sidebar_width', 21 ) );
		$section_padding = absint( dhm_get_option( 'section_padding', '4' ) );
		$row_padding = absint( dhm_get_option( 'row_padding', '2' ) );

		$tablet_header_font_size = absint( dhm_get_option( 'tablet_header_font_size', $body_header_size ) );
		$tablet_body_font_size = absint( dhm_get_option( 'tablet_body_font_size', $body_font_size ) );
		$tablet_section_height = absint( dhm_get_option( 'tablet_section_height', '50' ) );
		$tablet_row_height = absint( dhm_get_option( 'tablet_row_height', '30' ) );

		$phone_header_font_size = absint( dhm_get_option( 'phone_header_font_size', $tablet_header_font_size ) );
		$phone_body_font_size = absint( dhm_get_option( 'phone_body_font_size', $tablet_body_font_size ) );
		$phone_section_height = absint( dhm_get_option( 'phone_section_height', $tablet_section_height ) );
		$phone_row_height = absint( dhm_get_option( 'phone_row_height', $tablet_row_height ) );

		$header_style = dhm_get_option( 'header_style', 'left' );
		$menu_height = absint( dhm_get_option( 'menu_height', '66' ) );
		$logo_height = absint( dhm_get_option( 'logo_height', '54' ) );
		$menu_link = dhm_get_option( 'menu_link', $legacy_primary_nav_color );
		$menu_link_active = dhm_get_option( 'menu_link_active', '#2ea3f2' );
		$vertical_nav = dhm_get_option( 'vertical_nav', false );

		$hide_primary_logo = dhm_get_option( 'hide_primary_logo', 'false' );
		$hide_fixed_logo = dhm_get_option( 'hide_fixed_logo', 'false' );

		$primary_nav_font_size = absint( dhm_get_option( 'primary_nav_font_size', '14' ) );
		$primary_nav_font_spacing = intval( dhm_get_option( 'primary_nav_font_spacing', '0' ) );
		$primary_nav_bg = dhm_get_option( 'primary_nav_bg', '#ffffff' );
		$primary_nav_font_style = dhm_get_option( 'primary_nav_font_style', '', '', true );
		$primary_nav_dropdown_bg = dhm_get_option( 'primary_nav_dropdown_bg', $primary_nav_bg );
		$primary_nav_dropdown_link_color = dhm_get_option( 'primary_nav_dropdown_link_color', $menu_link );
		$primary_nav_dropdown_line_color = dhm_get_option( 'primary_nav_dropdown_line_color', $accent_color );

		$mobile_menu_link = dhm_get_option( 'mobile_menu_link', $menu_link );
		$mobile_primary_nav_bg = dhm_get_option( 'mobile_primary_nav_bg', $primary_nav_bg );

		$secondary_nav_font_size = absint( dhm_get_option( 'secondary_nav_font_size', '12' ) );
		$secondary_nav_font_spacing = intval( dhm_get_option( 'secondary_nav_font_spacing', '0' ) );
		$secondary_nav_font_style = dhm_get_option( 'secondary_nav_font_style', '', '', true );
		$secondary_nav_text_color_new = dhm_get_option( 'secondary_nav_text_color_new', $legacy_secondary_nav_color );
		$secondary_nav_bg = dhm_get_option( 'secondary_nav_bg', dhm_get_option( 'accent_color', '#2ea3f2' ) );
		$secondary_nav_dropdown_bg = dhm_get_option( 'secondary_nav_dropdown_bg', $secondary_nav_bg );
		$secondary_nav_dropdown_link_color = dhm_get_option( 'secondary_nav_dropdown_link_color', $secondary_nav_text_color_new );

		$fixed_primary_nav_font_size = absint( dhm_get_option( 'fixed_primary_nav_font_size', $primary_nav_font_size ) );
		$fixed_primary_nav_bg = dhm_get_option( 'fixed_primary_nav_bg', $primary_nav_bg );
		$fixed_secondary_nav_bg = dhm_get_option( 'fixed_secondary_nav_bg', $secondary_nav_bg );
		$fixed_menu_height = absint( dhm_get_option( 'minimized_menu_height', '40' ) );
		$fixed_menu_link = dhm_get_option( 'fixed_menu_link', $menu_link );
		$fixed_menu_link_active = dhm_get_option( 'fixed_menu_link_active', $menu_link_active );
		$fixed_secondary_menu_link = dhm_get_option( 'fixed_secondary_menu_link', $secondary_nav_text_color_new );

		$footer_bg = dhm_get_option( 'footer_bg', '#222222' );
		$footer_widget_link_color = dhm_get_option( 'footer_widget_link_color', '#fff' );
		$footer_widget_text_color = dhm_get_option( 'footer_widget_text_color', '#fff' );
		$footer_widget_header_color = dhm_get_option( 'footer_widget_header_color', $accent_color );
		$footer_widget_bullet_color = dhm_get_option( 'footer_widget_bullet_color', $accent_color );

		$widget_header_font_size = intval( dhm_get_option( 'widget_header_font_size', intval( dhm_get_option( 'body_header_size' * .6, '18' ) ) ) );
		$widget_body_font_size = absint( dhm_get_option( 'widget_body_font_size', $body_font_size ) );
		$widget_body_line_height = floatval( dhm_get_option( 'widget_body_line_height', '1.7' ) );

		$button_text_size = absint( dhm_get_option( 'all_buttons_font_size', '20' ) );
		$button_text_color = dhm_get_option( 'all_buttons_text_color', '#ffffff' );
		$button_bg_color = dhm_get_option( 'all_buttons_bg_color', 'rgba(0,0,0,0)' );
		$button_border_width = absint( dhm_get_option( 'all_buttons_border_width', '2' ) );
		$button_border_color = dhm_get_option( 'all_buttons_border_color', '#ffffff' );
		$button_border_radius = absint( dhm_get_option( 'all_buttons_border_radius', '3' ) );
		$button_text_style = dhm_get_option( 'all_buttons_font_style', '', '', true );
		$button_icon = dhm_get_option( 'all_buttons_selected_icon', '5' );
		$button_spacing = intval( dhm_get_option( 'all_buttons_spacing', '0' ) );
		$button_icon_color = dhm_get_option( 'all_buttons_icon_color', '#ffffff' );
		$button_text_color_hover = dhm_get_option( 'all_buttons_text_color_hover', '#ffffff' );
		$button_bg_color_hover = dhm_get_option( 'all_buttons_bg_color_hover', 'rgba(255,255,255,0.2)' );
		$button_border_color_hover = dhm_get_option( 'all_buttons_border_color_hover', 'rgba(0,0,0,0)' );
		$button_border_radius_hover = absint( dhm_get_option( 'all_buttons_border_radius_hover', '3' ) );
		$button_spacing_hover = intval( dhm_get_option( 'all_buttons_spacing_hover', '0' ) );
		$button_icon_size = 1.6 * intval( $button_text_size );
	?>
	<style id="theme-customizer-css">
		<?php if ( 14 !== $body_font_size ) { ?>
			@media only screen and ( min-width: 767px ) {
				body, .dhm_pb_column_1_2 .dhm_quote_content blockquote cite, .dhm_pb_column_1_2 .dhm_link_content a.dhm_link_main_url, .dhm_pb_column_1_3 .dhm_quote_content blockquote cite, .dhm_pb_column_3_8 .dhm_quote_content blockquote cite, .dhm_pb_column_1_4 .dhm_quote_content blockquote cite, .dhm_pb_blog_grid .dhm_quote_content blockquote cite, .dhm_pb_column_1_3 .dhm_link_content a.dhm_link_main_url, .dhm_pb_column_3_8 .dhm_link_content a.dhm_link_main_url, .dhm_pb_column_1_4 .dhm_link_content a.dhm_link_main_url, .dhm_pb_blog_grid .dhm_link_content a.dhm_link_main_url, body .dhm_pb_bg_layout_light .dhm_pb_post p,  body .dhm_pb_bg_layout_dark .dhm_pb_post p { font-size: <?php echo esc_html( $body_font_size ); ?>px; }
				.dhm_pb_slide_content, .dhm_pb_best_value { font-size: <?php echo esc_html( intval( $body_font_size * 1.14 ) ); ?>px; }
			}
		<?php } ?>
		<?php if ( '#666666' !== $body_font_color) { ?>
			body { color: <?php echo esc_html( $body_font_color ); ?>; }
		<?php } ?>
		<?php if ( '#666666' !== $body_header_color ) { ?>
				h1, h2, h3, h4, h5, h6 { color: <?php echo esc_html( $body_header_color ); ?>; }
			<?php } ?>
		<?php if ( 1.7 !== $body_font_height ) { ?>
			body { line-height: <?php echo esc_html( $body_font_height ); ?>em; }
		<?php } ?>
		<?php if ( $accent_color !== '#2ea3f2' ) { ?>
			.woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button, .woocommerce-message, .woocommerce-error, .woocommerce-info { background: <?php echo esc_html( $accent_color ); ?> !important; }
			#dhm_search_icon:hover, .mobile_menu_bar:before, .dhm-social-icon a:hover, .dhm_pb_sum, .dhm_pb_pricing li a, .dhm_pb_pricing_table_button, .dhm_overlay:before, .entry-summary p.price ins, .woocommerce div.product span.price, .woocommerce-page div.product span.price, .woocommerce #content div.product span.price, .woocommerce-page #content div.product span.price, .woocommerce div.product p.price, .woocommerce-page div.product p.price, .woocommerce #content div.product p.price, .woocommerce-page #content div.product p.price, .dhm_pb_member_social_links a:hover, .woocommerce .star-rating span:before, .woocommerce-page .star-rating span:before, .dhm_pb_widget li a:hover, .dhm_pb_filterable_portfolio .dhm_pb_portfolio_filters li a.active, .dhm_pb_filterable_portfolio .dhm_pb_portofolio_pagination ul li a.active, .dhm_pb_gallery .dhm_pb_gallery_pagination ul li a.active, .wp-pagenavi span.current, .wp-pagenavi a:hover, .nav-single a, .posted_in a { color: <?php echo esc_html( $accent_color ); ?>; }
			.dhm_pb_contact_submit, .dhm_password_protected_form .dhm_submit_button, .dhm_pb_bg_layout_light .dhm_pb_newsletter_button, .comment-reply-link, .form-submit input, .dhm_pb_bg_layout_light .dhm_pb_promo_button, .dhm_pb_bg_layout_light .dhm_pb_more_button, .woocommerce a.button.alt, .woocommerce-page a.button.alt, .woocommerce button.button.alt, .woocommerce-page button.button.alt, .woocommerce input.button.alt, .woocommerce-page input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce-page #respond input#submit.alt, .woocommerce #content input.button.alt, .woocommerce-page #content input.button.alt, .woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button { color: <?php echo esc_html( $accent_color ); ?>; }
			.footer-widget h4 { color: <?php echo esc_html( $accent_color ); ?>; }
			.dhm-search-form, .nav li ul, .dhm_mobile_menu, .footer-widget li:before, .dhm_pb_pricing li:before, blockquote { border-color: <?php echo esc_html( $accent_color ); ?>; }
			.dhm_pb_counter_amount, .dhm_pb_featured_table .dhm_pb_pricing_heading, .dhm_quote_content, .dhm_link_content, .dhm_audio_content { background-color: <?php echo esc_html( $accent_color ); ?>; }
		<?php } ?>
		<?php if ( 1080 !== $content_width ) { ?>
			.container, .dhm_pb_row, .dhm_pb_slider .dhm_pb_container, .dhm_pb_fullwidth_section .dhm_pb_title_container, .dhm_pb_fullwidth_section .dhm_pb_title_featured_container, .dhm_pb_fullwidth_header:not(.dhm_pb_fullscreen) .dhm_pb_fullwidth_header_container { max-width: <?php echo esc_html( $content_width ); ?>px; }
			.dhm_boxed_layout #page-container, .dhm_fixed_nav.dhm_boxed_layout #page-container #top-header, .dhm_fixed_nav.dhm_boxed_layout #page-container #main-header, .dhm_boxed_layout #page-container .container, .dhm_boxed_layout #page-container .dhm_pb_row { max-width: <?php echo esc_html( intval( dhm_get_option( 'content_width', '1080' ) ) + 160 ); ?>px; }
		<?php } ?>
		<?php if ( $link_color !== '#2ea3f2' ) { ?>
			a { color: <?php echo esc_html( $link_color ); ?>; }
		<?php } ?>
		<?php if ( $primary_nav_bg !== '#ffffff' ) { ?>
			#main-header, #main-header .nav li ul, .dhm-search-form, #main-header .dhm_mobile_menu { background-color: <?php echo esc_html( $primary_nav_bg ); ?>; }
		<?php } ?>
		<?php if ( $primary_nav_dropdown_bg !== $primary_nav_bg ) { ?>
			#main-header .nav li ul { background-color: <?php echo esc_html( $primary_nav_dropdown_bg ); ?>; }
		<?php } ?>
		<?php if ( $primary_nav_dropdown_line_color !== $accent_color ) { ?>
			.nav li ul { border-color: <?php echo esc_html( $primary_nav_dropdown_line_color ); ?>; }
		<?php } ?>
		<?php if ( $secondary_nav_bg !== '#2ea3f2' ) { ?>
			#top-header, #dhm-secondary-nav li ul { background-color: <?php echo esc_html( $secondary_nav_bg ); ?>; }
		<?php } ?>
		<?php if ( $secondary_nav_dropdown_bg !== $secondary_nav_bg ) { ?>
			#dhm-secondary-nav li ul { background-color: <?php echo esc_html( $secondary_nav_dropdown_bg ); ?>; }
		<?php } ?>
		<?php if ( $secondary_nav_text_color_new !== '#ffffff' ) { ?>
		#top-header, #top-header a { color: <?php echo esc_html( $secondary_nav_text_color_new ); ?>; }
		<?php } ?>
		<?php if ( $secondary_nav_dropdown_link_color !== $secondary_nav_text_color_new ) { ?>
			#dhm-secondary-nav li ul a { color: <?php echo esc_html( $secondary_nav_dropdown_link_color ); ?>; }
		<?php } ?>
		<?php if ( $menu_link !== 'rgba(0,0,0,0.6)' ) { ?>
			.dhm_header_style_centered .mobile_nav .select_page, .dhm_header_style_split .mobile_nav .select_page, .dhm_nav_text_color_light #top-menu > li > a, .dhm_nav_text_color_dark #top-menu > li > a, #top-menu a, .dhm_mobile_menu li a, .dhm_nav_text_color_light .dhm_mobile_menu li a, .dhm_nav_text_color_dark .dhm_mobile_menu li a, #dhm_search_icon:before, .dhm_search_form_container input, span.dhm_close_search_field:after, #dhm-top-navigation .dhm-cart-info, .mobile_menu_bar:before { color: <?php echo esc_html( $menu_link ); ?>; }
			.dhm_search_form_container input::-moz-placeholder { color: <?php echo esc_html( $menu_link ); ?>; }
			.dhm_search_form_container input::-webkit-input-placeholder { color: <?php echo esc_html( $menu_link ); ?>; }
			.dhm_search_form_container input:-ms-input-placeholder { color: <?php echo esc_html( $menu_link ); ?>; }
		<?php } ?>
		<?php if ( $primary_nav_dropdown_link_color !== $menu_link ) { ?>
			#main-header .nav li ul a { color: <?php echo esc_html( $primary_nav_dropdown_link_color ); ?>; }
		<?php } ?>
		<?php if ( 12 !== $secondary_nav_font_size || '' !== $secondary_nav_font_style || 0 !== $secondary_nav_font_spacing ) { ?>
			#top-header, #top-header a, #dhm-secondary-nav li li a, #top-header .dhm-social-icon a:before {
				<?php if ( 12 !== $secondary_nav_font_size ) { ?>
					font-size: <?php echo esc_html( $secondary_nav_font_size ); ?>px;
				<?php } ?>
				<?php if ( '' !== $secondary_nav_font_style ) { ?>
					<?php echo esc_html( dhm_pb_print_font_style( $secondary_nav_font_style ) ); ?>
				<?php } ?>
				<?php if ( 0 !== $secondary_nav_font_spacing ) { ?>
					letter-spacing: <?php echo esc_html( $secondary_nav_font_spacing ); ?>px;
				<?php } ?>
			}
		<?php } ?>
		<?php if ( 14 !== $primary_nav_font_size ) { ?>
			#top-menu li a { font-size: <?php echo esc_html( $primary_nav_font_size ); ?>px; }
			body.dhm_vertical_nav .container.dhm_search_form_container .dhm-search-form input { font-size: <?php echo esc_html( $primary_nav_font_size ); ?>px !important; }
		<?php } ?>

		<?php if ( 0 !== $primary_nav_font_spacing || '' !== $primary_nav_font_style ) { ?>
			#top-menu li a, .dhm_search_form_container input {
				<?php if ( '' !== $primary_nav_font_style ) { ?>
					<?php echo esc_html( dhm_pb_print_font_style( $primary_nav_font_style ) ); ?>
				<?php } ?>
				<?php if ( 0 !== $primary_nav_font_spacing ) { ?>
					letter-spacing: <?php echo esc_html( $primary_nav_font_spacing ); ?>px;
				<?php } ?>
			}

			.dhm_search_form_container input::-moz-placeholder {
				<?php if ( '' !== $primary_nav_font_style ) { ?>
					<?php echo esc_html( dhm_pb_print_font_style( $primary_nav_font_style ) ); ?>
				<?php } ?>
				<?php if ( 0 !== $primary_nav_font_spacing ) { ?>
					letter-spacing: <?php echo esc_html( $primary_nav_font_spacing ); ?>px;
				<?php } ?>
			}
			.dhm_search_form_container input::-webkit-input-placeholder {
				<?php if ( '' !== $primary_nav_font_style ) { ?>
					<?php echo esc_html( dhm_pb_print_font_style( $primary_nav_font_style ) ); ?>
				<?php } ?>
				<?php if ( 0 !== $primary_nav_font_spacing ) { ?>
					letter-spacing: <?php echo esc_html( $primary_nav_font_spacing ); ?>px;
				<?php } ?>
			}
			.dhm_search_form_container input:-ms-input-placeholder {
				<?php if ( '' !== $primary_nav_font_style ) { ?>
					<?php echo esc_html( dhm_pb_print_font_style( $primary_nav_font_style ) ); ?>
				<?php } ?>
				<?php if ( 0 !== $primary_nav_font_spacing ) { ?>
					letter-spacing: <?php echo esc_html( $primary_nav_font_spacing ); ?>px;
				<?php } ?>
			}
		<?php } ?>

		<?php if ( $menu_link_active !== '#2ea3f2' ) { ?>
			#top-menu li.current-menu-ancestor > a, #top-menu li.current-menu-item > a,
			.dhm_color_scheme_red #top-menu li.current-menu-ancestor > a, .dhm_color_scheme_red #top-menu li.current-menu-item > a,
			.dhm_color_scheme_pink #top-menu li.current-menu-ancestor > a, .dhm_color_scheme_pink #top-menu li.current-menu-item > a,
			.dhm_color_scheme_orange #top-menu li.current-menu-ancestor > a, .dhm_color_scheme_orange #top-menu li.current-menu-item > a,
			.dhm_color_scheme_green #top-menu li.current-menu-ancestor > a, .dhm_color_scheme_green #top-menu li.current-menu-item > a { color: <?php echo esc_html( $menu_link_active ); ?>; }
		<?php } ?>
		<?php if ( $footer_bg !== '#222222' ) { ?>
			#main-footer { background-color: <?php echo esc_html( $footer_bg ); ?>; }
		<?php } ?>
		<?php if ( $footer_widget_link_color !== '#fff' ) { ?>
			#footer-widgets .footer-widget li a { color: <?php echo esc_html( $footer_widget_link_color ); ?>; }
		<?php } ?>
		<?php if ( $footer_widget_text_color !== '#fff' ) { ?>
			.footer-widget { color: <?php echo esc_html( $footer_widget_text_color ); ?>; }
		<?php } ?>
		<?php if ( $footer_widget_header_color !== '#2ea3f2' ) { ?>
			#main-footer .footer-widget h4 { color: <?php echo esc_html( $footer_widget_header_color ); ?>; }
		<?php } ?>
		<?php if ( $footer_widget_bullet_color !== '#2ea3f2' ) { ?>
			.footer-widget li:before { border-color: <?php echo esc_html( $footer_widget_bullet_color ); ?>; }
		<?php } ?>
		<?php if ( $body_font_size !== $widget_body_font_size ) { ?>
			.footer-widget, .footer-widget li, .footer-widget li a, #footer-info { font-size: <?php echo esc_html( $widget_body_font_size ); ?>px; }
		<?php } ?>
		<?php
			/* Widget */
			dhm_pb_print_styles_css( array(
				array(
					'key' 		=> 'widget_header_font_style',
					'type' 		=> 'font-style',
					'default' 	=> '',
					'selector' 	=> '.footer-widget h4',
				),
				array(
					'key' 		=> 'widget_body_font_style',
					'type' 		=> 'font-style',
					'default' 	=> '',
					'selector' 	=> '.footer-widget .dhm_pb_widget div, .footer-widget .dhm_pb_widget ul, .footer-widget .dhm_pb_widget ol, .footer-widget .dhm_pb_widget label',
				),
				array(
					'key' 		=> 'widget_body_line_height',
					'type' 		=> 'line-height',
					'default' 	=> '',
					'selector' 	=> '.footer-widget .dhm_pb_widget div, .footer-widget .dhm_pb_widget ul, .footer-widget .dhm_pb_widget ol, .footer-widget .dhm_pb_widget label',
				),
			) );

			/* Footer widget bullet fix */
			if ( 1.7 !==  $widget_body_line_height || 14 !== $widget_body_font_size ) {
				// line_height (em) * font_size (px) = line height in px
				$widget_body_line_height_px 		= floatval( $widget_body_line_height ) * intval( $widget_body_font_size );

				// ( line height in px / 2 ) - half of bullet diameter
				$footer_widget_bullet_top 			= ( $widget_body_line_height_px / 2 ) - 3;

				printf( "#footer-widgets .footer-widget li:before { top: %spx; }", esc_html( $footer_widget_bullet_top ) );
			}

			/* Footer Menu */
			dhm_pb_print_styles_css( array(
				array(
					'key' 		=> 'footer_menu_background_color',
					'type' 		=> 'background-color',
					'default' 	=> 'rgba(255,255,255,0.05)',
					'selector' 	=> '#dhm-footer-nav'
 				),
				array(
					'key' 		=> 'footer_menu_text_color',
					'type' 		=> 'color',
					'default' 	=> '#bbbbbb',
					'selector' 	=> '.bottom-nav, .bottom-nav a, .bottom-nav li.current-menu-item a'
 				),
				array(
					'key' 		=> 'footer_menu_active_link_color',
					'type' 		=> 'color',
					'default' 	=> '#bbbbbb',
					'selector' 	=> '#dhm-footer-nav .bottom-nav li.current-menu-item a'
 				),
				array(
					'key' 		=> 'footer_menu_letter_spacing',
					'type' 		=> 'letter-spacing',
					'default' 	=> 0,
					'selector' 	=> '.bottom-nav'
 				),
				array(
					'key' 		=> 'footer_menu_font_style',
					'type' 		=> 'font-style',
					'default' 	=> '',
					'selector' 	=> '.bottom-nav a'
 				),
				array(
					'key' 		=> 'footer_menu_font_size',
					'type' 		=> 'font-size',
					'default' 	=> 14,
					'selector' 	=> '.bottom-nav, .bottom-nav a'
 				),
			) );

			/* Bottom Bar */
			dhm_pb_print_styles_css( array(
				array(
					'key' 		=> 'bottom_bar_background_color',
					'type' 		=> 'background-color',
					'default' 	=> 'rgba(0,0,0,0.32)',
					'selector' 	=> '#footer-bottom'
 				),
				array(
					'key' 		=> 'bottom_bar_text_color',
					'type' 		=> 'color',
					'default' 	=> '#666666',
					'selector' 	=> '#footer-info, #footer-info a'
 				),
				array(
					'key' 		=> 'bottom_bar_font_style',
					'type' 		=> 'font-style',
					'default' 	=> '',
					'selector' 	=> '#footer-info, #footer-info a'
 				),
				array(
					'key' 		=> 'bottom_bar_font_size',
					'type' 		=> 'font-size',
					'default' 	=> 14,
					'selector' 	=> '#footer-info'
 				),
				array(
					'key' 		=> 'bottom_bar_social_icon_size',
					'type' 		=> 'font-size',
					'default' 	=> 24,
					'selector' 	=> '#footer-bottom .dhm-social-icon a'
 				),
				array(
					'key' 		=> 'bottom_bar_social_icon_color',
					'type' 		=> 'color',
					'default' 	=> '#666666',
					'selector' 	=> '#footer-bottom .dhm-social-icon a'
 				),
			) );
		?>
		<?php if ( 'rgba' === substr( $primary_nav_bg, 0, 4 ) ) { ?>
			#main-header { box-shadow: none; }
		<?php } ?>
		<?php if ( 'rgba' === substr( $fixed_primary_nav_bg, 0, 4 ) || ( 'rgba' === substr( $primary_nav_bg, 0, 4 ) && '#ffffff' === $fixed_primary_nav_bg ) ) { ?>
			.dhm-fixed-header#main-header { box-shadow: none !important; }
		<?php } ?>
		<?php if ( 20 !== $button_text_size || '#ffffff' !== $button_text_color || 'rgba(0,0,0,0)' !== $button_bg_color || 2 !== $button_border_width || '#ffffff' !== $button_border_color || 3 !== $button_border_radius || '' !== $button_text_style || 0 !== $button_spacing ) { ?>
			body .dhm_pb_button,
			.woocommerce a.button.alt, .woocommerce-page a.button.alt, .woocommerce button.button.alt, .woocommerce-page button.button.alt, .woocommerce input.button.alt, .woocommerce-page input.button.alt, .woocommerce #respond input#submit.alt, .woocommerce-page #respond input#submit.alt, .woocommerce #content input.button.alt, .woocommerce-page #content input.button.alt,
			.woocommerce a.button, .woocommerce-page a.button, .woocommerce button.button, .woocommerce-page button.button, .woocommerce input.button, .woocommerce-page input.button, .woocommerce #respond input#submit, .woocommerce-page #respond input#submit, .woocommerce #content input.button, .woocommerce-page #content input.button
			{
				<?php if ( 20 !== $button_text_size ) { ?>
					 font-size: <?php echo esc_html( $button_text_size ); ?>px;
				<?php } ?>
				<?php if ( 'rgba(0,0,0,0)' !== $button_bg_color ) { ?>
					background: <?php echo esc_html( $button_bg_color ); ?>;
				<?php } ?>
				<?php if ( 2 !== $button_border_width ) { ?>
					border-width: <?php echo esc_html( $button_border_width ); ?>px !important;
				<?php } ?>
				<?php if ( '#ffffff' !== $button_border_color ) { ?>
					border-color: <?php echo esc_html( $button_border_color ); ?>;
				<?php } ?>
				<?php if ( 3 !== $button_border_radius ) { ?>
					border-radius: <?php echo esc_html( $button_border_radius ); ?>px;
				<?php } ?>
				<?php if ( '' !== $button_text_style ) { ?>
					<?php echo esc_html( dhm_pb_print_font_style( $button_text_style ) ); ?>;
				<?php } ?>
				<?php if ( 0 !== $button_spacing ) { ?>
					letter-spacing: <?php echo esc_html( $button_spacing ); ?>px;
				<?php } ?>
			}
			body.dhm_pb_button_helper_class .dhm_pb_button,
			.woocommerce.dhm_pb_button_helper_class a.button.alt, .woocommerce-page.dhm_pb_button_helper_class a.button.alt, .woocommerce.dhm_pb_button_helper_class button.button.alt, .woocommerce-page.dhm_pb_button_helper_class button.button.alt, .woocommerce.dhm_pb_button_helper_class input.button.alt, .woocommerce-page.dhm_pb_button_helper_class input.button.alt, .woocommerce.dhm_pb_button_helper_class #respond input#submit.alt, .woocommerce-page.dhm_pb_button_helper_class #respond input#submit.alt, .woocommerce.dhm_pb_button_helper_class #content input.button.alt, .woocommerce-page.dhm_pb_button_helper_class #content input.button.alt,
			.woocommerce.dhm_pb_button_helper_class a.button, .woocommerce-page.dhm_pb_button_helper_class a.button, .woocommerce.dhm_pb_button_helper_class button.button, .woocommerce-page.dhm_pb_button_helper_class button.button, .woocommerce.dhm_pb_button_helper_class input.button, .woocommerce-page.dhm_pb_button_helper_class input.button, .woocommerce.dhm_pb_button_helper_class #respond input#submit, .woocommerce-page.dhm_pb_button_helper_class #respond input#submit, .woocommerce.dhm_pb_button_helper_class #content input.button, .woocommerce-page.dhm_pb_button_helper_class #content input.button {
				<?php if ( '#ffffff' !== $button_text_color ) { ?>
					color: <?php echo esc_html( $button_text_color ); ?> !important;
				<?php } ?>
			}
		<?php } ?>
		<?php if ( '5' !== $button_icon || '#ffffff' !== $button_icon_color || 20 !== $button_text_size ) { ?>
			body .dhm_pb_button:after,
			.woocommerce a.button.alt:after, .woocommerce-page a.button.alt:after, .woocommerce button.button.alt:after, .woocommerce-page button.button.alt:after, .woocommerce input.button.alt:after, .woocommerce-page input.button.alt:after, .woocommerce #respond input#submit.alt:after, .woocommerce-page #respond input#submit.alt:after, .woocommerce #content input.button.alt:after, .woocommerce-page #content input.button.alt:after,
			.woocommerce a.button:after, .woocommerce-page a.button:after, .woocommerce button.button:after, .woocommerce-page button.button:after, .woocommerce input.button:after, .woocommerce-page input.button:after, .woocommerce #respond input#submit:after, .woocommerce-page #respond input#submit:after, .woocommerce #content input.button:after, .woocommerce-page #content input.button:after
			{
				<?php if ( '5' !== $button_icon ) { ?>
					<?php if ( "'" === $button_icon ) { ?>
						content: "<?php echo htmlspecialchars_decode( $button_icon ); ?>";
					<?php } else { ?>
						content: '<?php echo htmlspecialchars_decode( $button_icon ); ?>';
					<?php } ?>
					font-size: <?php echo esc_html( $button_text_size ); ?>px;
				<?php } else { ?>
					font-size: <?php echo esc_html( $button_icon_size ); ?>px;
				<?php } ?>
				<?php if ( '#ffffff' !== $button_icon_color ) { ?>
					color: <?php echo esc_html( $button_icon_color ); ?>;
				<?php } ?>
			}
		<?php } ?>
		<?php if ( '#ffffff' !== $button_text_color_hover || 'rgba(255,255,255,0.2)' !== $button_bg_color_hover || 'rgba(0,0,0,0)' !== $button_border_color_hover || 3 !== $button_border_radius_hover || 0 !== $button_spacing_hover ) { ?>
			body .dhm_pb_button:hover,
			.woocommerce a.button.alt:hover, .woocommerce-page a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce-page button.button.alt:hover, .woocommerce input.button.alt:hover, .woocommerce-page input.button.alt:hover, .woocommerce #respond input#submit.alt:hover, .woocommerce-page #respond input#submit.alt:hover, .woocommerce #content input.button.alt:hover, .woocommerce-page #content input.button.alt:hover,
			.woocommerce a.button:hover, .woocommerce-page a.button:hover, .woocommerce button.button, .woocommerce-page button.button:hover, .woocommerce input.button:hover, .woocommerce-page input.button:hover, .woocommerce #respond input#submit:hover, .woocommerce-page #respond input#submit:hover, .woocommerce #content input.button:hover, .woocommerce-page #content input.button:hover
			{
				<?php if ( '#ffffff' !== $button_text_color_hover ) { ?>
					 color: <?php echo esc_html( $button_text_color_hover ); ?> !important;
				<?php } ?>
				<?php if ( 'rgba(255,255,255,0.2)' !== $button_bg_color_hover ) { ?>
					background: <?php echo esc_html( $button_bg_color_hover ); ?> !important;
				<?php } ?>
				<?php if ( 'rgba(0,0,0,0)' !== $button_border_color_hover ) { ?>
					border-color: <?php echo esc_html( $button_border_color_hover ); ?> !important;
				<?php } ?>
				<?php if ( 3 !== $button_border_radius_hover ) { ?>
					border-radius: <?php echo esc_html( $button_border_radius_hover ); ?>px;
				<?php } ?>
				<?php if ( 0 !== $button_spacing_hover ) { ?>
					letter-spacing: <?php echo esc_html( $button_spacing_hover ); ?>px;
				<?php } ?>
			}
		<?php } ?>

		<?php if ( '' !== $body_header_style || 0 !== $body_header_spacing || 1.0 !== $body_header_height) { ?>
				h1, h2, h3, h4, h5, h6, .dhm_quote_content blockquote p, .dhm_pb_slide_description .dhm_pb_slide_title {
					<?php if ( $body_header_style !== '' ) { ?>
						<?php echo esc_html( dhm_pb_print_font_style( $body_header_style ) ); ?>
					<?php } ?>
					<?php if ( 0 !== $body_header_spacing ) { ?>
						letter-spacing: <?php echo esc_html( $body_header_spacing ); ?>px;
					<?php } ?>

					<?php if ( 1.0 !== $body_header_height ) { ?>
						line-height: <?php echo esc_html( $body_header_height ); ?>em;
					<?php } ?>
				}
		<?php } ?>

		<?php
			/* Blog Meta */
			$dhm_pb_print_selectors_post_meta = "body.home-posts #left-area .dhm_pb_post .post-meta, body.archive #left-area .dhm_pb_post .post-meta, body.search #left-area .dhm_pb_post .post-meta, body.single #left-area .dhm_pb_post .post-meta";

			dhm_pb_print_styles_css( array(
				array(
					'key'      => 'post_meta_height',
					'type'     => 'line-height',
					'default'  => 1,
					'selector' => $dhm_pb_print_selectors_post_meta,
 				),
				array(
					'key'      => 'post_meta_spacing',
					'type'     => 'letter-spacing',
					'default'  => 0,
					'selector' => $dhm_pb_print_selectors_post_meta,
 				),
				array(
					'key'      => 'post_meta_style',
					'type'     => 'font-style',
					'default'  => '',
					'selector' => $dhm_pb_print_selectors_post_meta,
 				),
			) );

			/* Blog Title */
			$dhm_pb_print_selectors_post_header = "body.home-posts #left-area .dhm_pb_post h2, body.archive #left-area .dhm_pb_post h2, body.search #left-area .dhm_pb_post h2, body.single .dhm_post_meta_wrapper h1";

			dhm_pb_print_styles_css( array(
				array(
					'key'      => 'post_header_height',
					'type'     => 'line-height',
					'default'  => 1,
					'selector' => $dhm_pb_print_selectors_post_header,
 				),
				array(
					'key'      => 'post_header_spacing',
					'type'     => 'letter-spacing',
					'default'  => 0,
					'selector' => $dhm_pb_print_selectors_post_header,
 				),
				array(
					'key'      => 'post_header_style',
					'type'     => 'font-style',
					'default'  => '',
					'selector' => $dhm_pb_print_selectors_post_header,
 				),
			) );
		?>

		@media only screen and ( min-width: 981px ) {
			<?php if ( 4 !== $section_padding ) { ?>
				.dhm_pb_section { padding: <?php echo esc_html( $section_padding ); ?>% 0; }
				.dhm_pb_section.dhm_pb_section_first { padding-top: inherit; }
				.dhm_pb_fullwidth_section { padding: 0; }
			<?php } ?>
			<?php if ( 2 !== $row_padding ) { ?>
				.dhm_pb_row { padding: <?php echo esc_html( $row_padding ); ?>% 0; }
			<?php } ?>
			<?php if ( 30 !== $body_header_size ) { ?>
				h1 { font-size: <?php echo esc_html( $body_header_size ); ?>px; }
				h2, .product .related h2, .dhm_pb_column_1_2 .dhm_quote_content blockquote p { font-size: <?php echo esc_html( intval( $body_header_size * .86 ) ) ; ?>px; }
				h3 { font-size: <?php echo esc_html( intval( $body_header_size * .73 ) ); ?>px; }
				h4, .dhm_pb_circle_counter h3, .dhm_pb_number_counter h3, .dhm_pb_column_1_3 .dhm_pb_post h2, .dhm_pb_column_1_4 .dhm_pb_post h2, .dhm_pb_blog_grid h2, .dhm_pb_column_1_3 .dhm_quote_content blockquote p, .dhm_pb_column_3_8 .dhm_quote_content blockquote p, .dhm_pb_column_1_4 .dhm_quote_content blockquote p, .dhm_pb_blog_grid .dhm_quote_content blockquote p, .dhm_pb_column_1_3 .dhm_link_content h2, .dhm_pb_column_3_8 .dhm_link_content h2, .dhm_pb_column_1_4 .dhm_link_content h2, .dhm_pb_blog_grid .dhm_link_content h2, .dhm_pb_column_1_3 .dhm_audio_content h2, .dhm_pb_column_3_8 .dhm_audio_content h2, .dhm_pb_column_1_4 .dhm_audio_content h2, .dhm_pb_blog_grid .dhm_audio_content h2, .dhm_pb_column_3_8 .dhm_pb_audio_module_content h2, .dhm_pb_column_1_3 .dhm_pb_audio_module_content h2, .dhm_pb_gallery_grid .dhm_pb_gallery_item h3, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item h2, .dhm_pb_filterable_portfolio_grid .dhm_pb_portfolio_item h2 { font-size: <?php echo esc_html( intval( $body_header_size * .6 ) ); ?>px; }
				h5 { font-size: <?php echo esc_html( intval( $body_header_size * .53 ) ); ?>px; }
				h6 { font-size: <?php echo esc_html( intval( $body_header_size * .47 ) ); ?>px; }
				.dhm_pb_slide_description .dhm_pb_slide_title { font-size: <?php echo esc_html( intval( $body_header_size * 1.53 ) ); ?>px; }
				.woocommerce ul.products li.product h3, .woocommerce-page ul.products li.product h3, .dhm_pb_gallery_grid .dhm_pb_gallery_item h3, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item h2, .dhm_pb_filterable_portfolio_grid .dhm_pb_portfolio_item h2, .dhm_pb_column_1_4 .dhm_pb_audio_module_content h2 { font-size: <?php echo esc_html( intval( $body_header_size * .53 ) ); ?>px; }
			<?php } ?>
			<?php if ( intval( $body_header_size * .6 ) !== $widget_header_font_size ) { ?>
				.footer-widget h4 { font-size: <?php echo esc_html( $widget_header_font_size ); ?>px; }
			<?php } ?>
			<?php if ( 66 !== $menu_height ) { ?>
				.dhm_header_style_left #dhm-top-navigation, .dhm_header_style_split #dhm-top-navigation  { padding: <?php echo esc_html( round( $menu_height / 2 ) ); ?>px 0 0 0; }
				.dhm_header_style_left #dhm-top-navigation nav > ul > li > a, .dhm_header_style_split #dhm-top-navigation nav > ul > li > a { padding-bottom: <?php echo esc_html( round ( $menu_height / 2 ) ); ?>px; }
				.dhm_header_style_split .centered-inline-logo-wrap { width: <?php echo esc_html( $menu_height ); ?>px; margin: -<?php echo esc_html( $menu_height ); ?>px 0; }
				.dhm_header_style_split .centered-inline-logo-wrap #logo { max-height: <?php echo esc_html( $menu_height ); ?>px; }
				.dhm_pb_svg_logo.dhm_header_style_split .centered-inline-logo-wrap #logo { height: <?php echo esc_html( $menu_height ); ?>px; }
				.dhm_header_style_centered #top-menu > li > a { padding-bottom: <?php echo esc_html( round ( $menu_height * .18 ) ); ?>px; }
				<?php if ( ! $vertical_nav ) { ?>
					.dhm_header_style_centered #main-header .logo_container { height: <?php echo esc_html( $menu_height ); ?>px; }
				<?php } ?>
			<?php } ?>
			<?php if ( 54 !== $logo_height && 'left' === $header_style ) { ?>
				#logo { max-height: <?php echo esc_html( $logo_height . '%' ); ?>; }
				.dhm_pb_svg_logo #logo { height: <?php echo esc_html( $logo_height . '%' ); ?>; }
			<?php } ?>
			<?php if ( 64 !== $logo_height && 'centered' === $header_style ) { ?>
				.dhm_header_style_centered #logo { max-height: <?php echo esc_html( $logo_height . '%' ); ?>; }
				.dhm_pb_svg_logo.dhm_header_style_centered #logo { height: <?php echo esc_html( $logo_height . '%' ); ?>; }
			<?php } ?>
			<?php if ( $vertical_nav && dhm_get_option( 'logo_height' ) ) { ?>
				#main-header .logo_container { width: <?php echo esc_html( $logo_height . '%' ); ?>; }
				.dhm_header_style_centered #main-header .logo_container,
				.dhm_header_style_split #main-header .logo_container { margin: 0 auto; }
			<?php } ?>
			<?php if ( 'false' !== $hide_primary_logo || 'false' !== $hide_fixed_logo ) { ?>
				.dhm_header_style_centered.dhm_hide_primary_logo #main-header:not(.dhm-fixed-header) .logo_container, .dhm_header_style_centered.dhm_hide_fixed_logo #main-header.dhm-fixed-header .logo_container { height: <?php echo esc_html( $menu_height * .18 ); ?>px; }
			<?php } ?>
			<?php if ( 40 !== $fixed_menu_height ) { ?>
				.dhm_header_style_left .dhm-fixed-header #dhm-top-navigation, .dhm_header_style_split .dhm-fixed-header #dhm-top-navigation { padding: <?php echo esc_html( intval( round( $fixed_menu_height / 2 ) ) ); ?>px 0 0 0; }
				.dhm_header_style_left .dhm-fixed-header #dhm-top-navigation nav > ul > li > a, .dhm_header_style_split .dhm-fixed-header #dhm-top-navigation nav > ul > li > a  { padding-bottom: <?php echo esc_html( round( $fixed_menu_height / 2 ) ); ?>px; }
				.dhm_header_style_centered header#main-header.dhm-fixed-header .logo_container { height: <?php echo esc_html( $fixed_menu_height ); ?>px; }
				.dhm_header_style_split .dhm-fixed-header .centered-inline-logo-wrap { width: <?php echo esc_html( $fixed_menu_height ); ?>px; margin: -<?php echo esc_html( $fixed_menu_height ); ?>px 0;  }
				.dhm_header_style_split .dhm-fixed-header .centered-inline-logo-wrap #logo { max-height: <?php echo esc_html( $fixed_menu_height ); ?>px; }
				.dhm_pb_svg_logo.dhm_header_style_split .dhm-fixed-header .centered-inline-logo-wrap #logo { height: <?php echo esc_html( $fixed_menu_height ); ?>px; }
			<?php } ?>
			<?php if ( 54 !== $logo_height && 'split' === $header_style ) { ?>
				.dhm_header_style_split .centered-inline-logo-wrap { width: auto; height: <?php echo esc_html( ( ( intval( $menu_height ) / 100 ) * $logo_height ) + 14 ); ?>px; }
				.dhm_header_style_split .dhm-fixed-header .centered-inline-logo-wrap { width: auto; height: <?php echo esc_html( ( ( intval( $fixed_menu_height ) / 100 ) * $logo_height ) + 14 ); ?>px; }
				.dhm_header_style_split .centered-inline-logo-wrap #logo,
				.dhm_header_style_split .dhm-fixed-header .centered-inline-logo-wrap #logo { height: auto; max-height: 100%; }

			<?php } ?>
			<?php if ( $fixed_secondary_nav_bg !== '#2ea3f2' ) { ?>
				.dhm-fixed-header#top-header, .dhm-fixed-header#top-header #dhm-secondary-nav li ul { background-color: <?php echo esc_html( $fixed_secondary_nav_bg ); ?>; }
			<?php } ?>
			<?php if ( $fixed_primary_nav_bg !== $primary_nav_bg ) { ?>
				.dhm-fixed-header#main-header, .dhm-fixed-header#main-header .nav li ul, .dhm-fixed-header .dhm-search-form { background-color: <?php echo esc_html( $fixed_primary_nav_bg ); ?>; }
			<?php } ?>
			<?php if ( 14 !== $fixed_primary_nav_font_size ) { ?>
				.dhm-fixed-header #top-menu li a { font-size: <?php echo esc_html( $fixed_primary_nav_font_size ); ?>px; }
			<?php } ?>
			<?php if ( $fixed_menu_link !== 'rgba(0,0,0,0.6)' ) { ?>
				.dhm-fixed-header #top-menu a, .dhm-fixed-header #dhm_search_icon:before, .dhm-fixed-header #dhm_top_search .dhm-search-form input, .dhm-fixed-header .dhm_search_form_container input, .dhm-fixed-header .dhm_close_search_field:after, .dhm-fixed-header #dhm-top-navigation .dhm-cart-info { color: <?php echo esc_html( $fixed_menu_link ); ?> !important; }
				.dhm-fixed-header .dhm_search_form_container input::-moz-placeholder { color: <?php echo esc_html( $fixed_menu_link ); ?> !important; }
				.dhm-fixed-header .dhm_search_form_container input::-webkit-input-placeholder { color: <?php echo esc_html( $fixed_menu_link ); ?> !important; }
				.dhm-fixed-header .dhm_search_form_container input:-ms-input-placeholder { color: <?php echo esc_html( $fixed_menu_link ); ?> !important; }
			<?php } ?>
			<?php if ( $fixed_menu_link_active !== '#2ea3f2' ) { ?>
				.dhm-fixed-header #top-menu li.current-menu-ancestor > a,
				.dhm-fixed-header #top-menu li.current-menu-item > a { color: <?php echo esc_html( $fixed_menu_link_active ); ?> !important; }
			<?php } ?>
			<?php if ( '#ffffff' !== $fixed_secondary_menu_link ) { ?>
				.dhm-fixed-header#top-header a { color: <?php echo esc_html( $fixed_secondary_menu_link ); ?>; }
			<?php } ?>

			<?php
				/* Blog Meta & Title */
				dhm_pb_print_styles_css( array(
					array(
						'key'      => 'post_meta_font_size',
						'type'     => 'font-size',
						'default'  => 14,
						'selector' => $dhm_pb_print_selectors_post_meta,
	 				),
					array(
						'key'      => 'post_header_font_size',
						'type'     => 'font-size-post-header',
						'default'  => 30,
						'selector' => '',
	 				),
				) );
			?>
		}
		@media only screen and ( min-width: <?php echo esc_html( $large_content_width ); ?>px) {
			.dhm_pb_row { padding: <?php echo esc_html( intval( $large_content_width * $row_padding / 100 ) ); ?>px 0; }
			.dhm_pb_section { padding: <?php echo esc_html( intval( $large_content_width * $section_padding / 100 ) ); ?>px 0; }
			.single.dhm_pb_pagebuilder_layout.dhm_full_width_page .dhm_post_meta_wrapper { padding-top: <?php echo esc_html( intval( $large_content_width * $row_padding / 100 * 3 ) ); ?>px; }
			.dhm_pb_section.dhm_pb_section_first { padding-top: inherit; }
			.dhm_pb_fullwidth_section { padding: 0; }
		}
		@media only screen and ( max-width: 980px ) {
			<?php if ( $mobile_primary_nav_bg !== $primary_nav_bg ) { ?>
				#main-header, #main-header .nav li ul, .dhm-search-form, #main-header .dhm_mobile_menu { background-color: <?php echo esc_html( $mobile_primary_nav_bg ); ?>; }
			<?php } ?>
			<?php if ( $menu_link !== $mobile_menu_link ) { ?>
				.dhm_header_style_centered .mobile_nav .select_page, .dhm_header_style_split .mobile_nav .select_page, .dhm_mobile_menu li a, .mobile_menu_bar:before, .dhm_nav_text_color_light #top-menu > li > a, .dhm_nav_text_color_dark #top-menu > li > a, #top-menu a, .dhm_mobile_menu li a, #dhm_search_icon:before, #dhm_top_search .dhm-search-form input, .dhm_search_form_container input, #dhm-top-navigation .dhm-cart-info { color: <?php echo esc_html( $mobile_menu_link ); ?>; }
				.dhm_close_search_field:after { color: <?php echo esc_html( $mobile_menu_link ); ?> !important; }
				.dhm_search_form_container input::-moz-placeholder { color: <?php echo esc_html( $mobile_menu_link ); ?>; }
				.dhm_search_form_container input::-webkit-input-placeholder { color: <?php echo esc_html( $mobile_menu_link ); ?>; }
				.dhm_search_form_container input:-ms-input-placeholder { color: <?php echo esc_html( $mobile_menu_link ); ?>; }
			<?php } ?>
			<?php if ( 14 !== $tabldhm_body_font_size && $body_font_size !== $tablet_body_font_size ) { ?>
				body, .dhm_pb_column_1_2 .dhm_quote_content blockquote cite, .dhm_pb_column_1_2 .dhm_link_content a.dhm_link_main_url, .dhm_pb_column_1_3 .dhm_quote_content blockquote cite, .dhm_pb_column_3_8 .dhm_quote_content blockquote cite, .dhm_pb_column_1_4 .dhm_quote_content blockquote cite, .dhm_pb_blog_grid .dhm_quote_content blockquote cite, .dhm_pb_column_1_3 .dhm_link_content a.dhm_link_main_url, .dhm_pb_column_3_8 .dhm_link_content a.dhm_link_main_url, .dhm_pb_column_1_4 .dhm_link_content a.dhm_link_main_url, .dhm_pb_blog_grid .dhm_link_content a.dhm_link_main_url { font-size: <?php echo esc_html( $tablet_body_font_size ); ?>px; }
				.dhm_pb_slide_content, .dhm_pb_best_value { font-size: <?php echo esc_html( intval( $tablet_body_font_size * 1.14 ) ); ?>px; }
			<?php } ?>
			<?php if ( 30 !== $tablet_header_font_size && $tablet_header_font_size !== $body_header_size ) { ?>
				h1 { font-size: <?php echo esc_html( $tablet_header_font_size ); ?>px; }
				h2, .product .related h2, .dhm_pb_column_1_2 .dhm_quote_content blockquote p { font-size: <?php echo esc_html( intval( $tablet_header_font_size * .86 ) ) ; ?>px; }
				h3 { font-size: <?php echo esc_html( intval( $tablet_header_font_size * .73 ) ); ?>px; }
				h4, .dhm_pb_circle_counter h3, .dhm_pb_number_counter h3, .dhm_pb_column_1_3 .dhm_pb_post h2, .dhm_pb_column_1_4 .dhm_pb_post h2, .dhm_pb_blog_grid h2, .dhm_pb_column_1_3 .dhm_quote_content blockquote p, .dhm_pb_column_3_8 .dhm_quote_content blockquote p, .dhm_pb_column_1_4 .dhm_quote_content blockquote p, .dhm_pb_blog_grid .dhm_quote_content blockquote p, .dhm_pb_column_1_3 .dhm_link_content h2, .dhm_pb_column_3_8 .dhm_link_content h2, .dhm_pb_column_1_4 .dhm_link_content h2, .dhm_pb_blog_grid .dhm_link_content h2, .dhm_pb_column_1_3 .dhm_audio_content h2, .dhm_pb_column_3_8 .dhm_audio_content h2, .dhm_pb_column_1_4 .dhm_audio_content h2, .dhm_pb_blog_grid .dhm_audio_content h2, .dhm_pb_column_3_8 .dhm_pb_audio_module_content h2, .dhm_pb_column_1_3 .dhm_pb_audio_module_content h2, .dhm_pb_gallery_grid .dhm_pb_gallery_item h3, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item h2, .dhm_pb_filterable_portfolio_grid .dhm_pb_portfolio_item h2 { font-size: <?php echo esc_html( intval( $tablet_header_font_size * .6 ) ); ?>px; }
				.dhm_pb_slide_description .dhm_pb_slide_title { font-size: <?php echo esc_html( intval( $tablet_header_font_size * 1.53 ) ); ?>px; }
				.woocommerce ul.products li.product h3, .woocommerce-page ul.products li.product h3, .dhm_pb_gallery_grid .dhm_pb_gallery_item h3, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item h2, .dhm_pb_filterable_portfolio_grid .dhm_pb_portfolio_item h2, .dhm_pb_column_1_4 .dhm_pb_audio_module_content h2 { font-size: <?php echo esc_html( intval( $tablet_header_font_size * .53 ) ); ?>px; }
			<?php } ?>
			<?php if ( 50 !== $tablet_section_height ) { ?>
				.dhm_pb_section { padding: <?php echo esc_html( $tablet_section_height ); ?>px 0; }
				.dhm_pb_section.dhm_pb_section_first { padding-top: inherit; }
				.dhm_pb_section.dhm_pb_fullwidth_section { padding: 0; }
			<?php } ?>
			<?php if ( 30 !== $tablet_row_height ) { ?>
				.dhm_pb_row, .dhm_pb_column .dhm_pb_row_inner { padding: <?php echo esc_html( $tablet_row_height ); ?>px 0 !important; }
			<?php } ?>
		}
		@media only screen and ( max-width: 767px ) {
			<?php if ( 14 !== $phone_body_font_size && $phone_body_font_size !== $tablet_body_font_size ) { ?>
				body, .dhm_pb_column_1_2 .dhm_quote_content blockquote cite, .dhm_pb_column_1_2 .dhm_link_content a.dhm_link_main_url, .dhm_pb_column_1_3 .dhm_quote_content blockquote cite, .dhm_pb_column_3_8 .dhm_quote_content blockquote cite, .dhm_pb_column_1_4 .dhm_quote_content blockquote cite, .dhm_pb_blog_grid .dhm_quote_content blockquote cite, .dhm_pb_column_1_3 .dhm_link_content a.dhm_link_main_url, .dhm_pb_column_3_8 .dhm_link_content a.dhm_link_main_url, .dhm_pb_column_1_4 .dhm_link_content a.dhm_link_main_url, .dhm_pb_blog_grid .dhm_link_content a.dhm_link_main_url { font-size: <?php echo esc_html( $phone_body_font_size ); ?>px; }
				.dhm_pb_slide_content, .dhm_pb_best_value { font-size: <?php echo esc_html( intval( $phone_body_font_size * 1.14 ) ); ?>px; }
			<?php } ?>
			<?php if ( 30 !== $phone_header_font_size && $tablet_header_font_size !== $phone_header_font_size ) { ?>
				h1 { font-size: <?php echo esc_html( $phone_header_font_size ); ?>px; }
				h2, .product .related h2, .dhm_pb_column_1_2 .dhm_quote_content blockquote p { font-size: <?php echo esc_html( intval( $phone_header_font_size * .86 ) ) ; ?>px; }
				h3 { font-size: <?php echo esc_html( intval( $phone_header_font_size * .73 ) ); ?>px; }
				h4, .dhm_pb_circle_counter h3, .dhm_pb_number_counter h3, .dhm_pb_column_1_3 .dhm_pb_post h2, .dhm_pb_column_1_4 .dhm_pb_post h2, .dhm_pb_blog_grid h2, .dhm_pb_column_1_3 .dhm_quote_content blockquote p, .dhm_pb_column_3_8 .dhm_quote_content blockquote p, .dhm_pb_column_1_4 .dhm_quote_content blockquote p, .dhm_pb_blog_grid .dhm_quote_content blockquote p, .dhm_pb_column_1_3 .dhm_link_content h2, .dhm_pb_column_3_8 .dhm_link_content h2, .dhm_pb_column_1_4 .dhm_link_content h2, .dhm_pb_blog_grid .dhm_link_content h2, .dhm_pb_column_1_3 .dhm_audio_content h2, .dhm_pb_column_3_8 .dhm_audio_content h2, .dhm_pb_column_1_4 .dhm_audio_content h2, .dhm_pb_blog_grid .dhm_audio_content h2, .dhm_pb_column_3_8 .dhm_pb_audio_module_content h2, .dhm_pb_column_1_3 .dhm_pb_audio_module_content h2, .dhm_pb_gallery_grid .dhm_pb_gallery_item h3, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item h2, .dhm_pb_filterable_portfolio_grid .dhm_pb_portfolio_item h2 { font-size: <?php echo esc_html( intval( $phone_header_font_size * .6 ) ); ?>px; }
				.dhm_pb_slide_description .dhm_pb_slide_title { font-size: <?php echo esc_html( intval( $phone_header_font_size * 1.53 ) ); ?>px; }
				.woocommerce ul.products li.product h3, .woocommerce-page ul.products li.product h3, .dhm_pb_gallery_grid .dhm_pb_gallery_item h3, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item h2, .dhm_pb_filterable_portfolio_grid .dhm_pb_portfolio_item h2, .dhm_pb_column_1_4 .dhm_pb_audio_module_content h2 { font-size: <?php echo esc_html( intval( $phone_header_font_size * .53 ) ); ?>px; }
			<?php } ?>
			<?php if ( 50 !== $phone_section_height && $tablet_section_height !== $phone_section_height ) { ?>
				.dhm_pb_section { padding: <?php echo esc_html( $phone_section_height ); ?>px 0; }
				.dhm_pb_section.dhm_pb_section_first { padding-top: inherit; }
				.dhm_pb_section.dhm_pb_fullwidth_section { padding: 0; }
			<?php } ?>
			<?php if ( 30 !== $phone_row_height && $tablet_row_height !== $phone_row_height ) { ?>
				.dhm_pb_row, .dhm_pb_column .dhm_pb_row_inner { padding: <?php echo esc_html( $phone_row_height ); ?>px 0 !important; }
			<?php } ?>
		}
	</style>

	<?php
		$dhm_gf_heading_font = sanitize_text_field( dhm_get_option( 'heading_font', 'none' ) );
		$dhm_gf_body_font = sanitize_text_field( dhm_get_option( 'body_font', 'none' ) );
		$dhm_gf_buttons_font = sanitize_text_field( dhm_get_option( 'all_buttons_font', 'none' ) );
		$dhm_gf_primary_nav_font = sanitize_text_field( dhm_get_option( 'primary_nav_font', 'none' ) );
		$dhm_gf_secondary_nav_font = sanitize_text_field( dhm_get_option( 'secondary_nav_font', 'none' ) );
		$site_domain = get_locale();

		$dhm_one_font_languages = dhm_get_one_font_languages();

		if ( isset( $dhm_one_font_languages[$site_domain] ) ) {
			printf( '<style class="dhm_one_font_languages">%s { font-family: %s; }</style>',
				'h1, h2, h3, h4, h5, h6, body, input, textarea, select',
				$dhm_one_font_languages[$site_domain]['font_family']
			);
		} else if ( ! in_array( $dhm_gf_heading_font, array( '', 'none' ) ) || ! in_array( $dhm_gf_body_font, array( '', 'none' ) ) || ! in_array( $dhm_gf_buttons_font, array( '', 'none' ) ) || ! in_array( $dhm_gf_primary_nav_font, array( '', 'none' ) ) || ! in_array( $dhm_gf_secondary_nav_font, array( '', 'none' ) ) ) {
			if ( ! in_array( $dhm_gf_heading_font, array( '', 'none' ) ) ) { ?>
				<style class="dhm_heading_font">
				h1, h2, h3, h4, h5, h6 {
					<?php echo dhm_builder_get_font_family( $dhm_gf_heading_font ); ?>
				}
				</style>
			<?php }

			if ( ! in_array( $dhm_gf_body_font, array( '', 'none' ) ) ) { ?>
				<style class="dhm_body_font">
				body, input, textarea, select {
					<?php echo dhm_builder_get_font_family( $dhm_gf_body_font ); ?>
				}
				</style>
			<?php }

			if ( ! in_array( $dhm_gf_buttons_font, array( '', 'none' ) ) ) { ?>
				<style class="dhm_all_buttons_font">
				.dhm_pb_button {
					<?php echo dhm_builder_get_font_family( $dhm_gf_buttons_font ); ?>
				}
				</style>
			<?php }

			if ( ! in_array( $dhm_gf_primary_nav_font, array( '', 'none' ) ) ) { ?>
				<style class="dhm_primary_nav_font">
				#main-header,
				#dhm-top-navigation {
					<?php echo dhm_builder_get_font_family( $dhm_gf_primary_nav_font ); ?>
				}
				</style>
			<?php }

			if ( ! in_array( $dhm_gf_secondary_nav_font, array( '', 'none' ) ) ) { ?>
				<style class="dhm_secondary_nav_font">
				#top-header .container{
					<?php echo dhm_builder_get_font_family( $dhm_gf_secondary_nav_font ); ?>
				}
				</style>
			<?php }
		}
	?>

	<?php
	/**
	 * use_sidebar_width might invalidate the use of sidebar_width.
	 * It is placed outside other customizer style so live preview
	 * can invalidate and revalidate it for smoother experience
	 */
	if ( $use_sidebar_width && 21 !== $sidebar_width && 19 <= $sidebar_width && 33 >= $sidebar_width ) { ?>
	<style id="theme-customizer-sidebar-width-css">
		<?php
			$content_width = 100 - $sidebar_width;
			$content_width_percentage = $content_width . '%';
			$sidebar_width_percentage = $sidebar_width . '%';
			printf(
				'body #page-container #sidebar { width:%2$s; }
				body #page-container #left-area { width:%1$s; }
				.dhm_right_sidebar #main-content .container:before { right:%2$s !important; }
				.dhm_left_sidebar #main-content .container:before { left:%2$s !important; }',
				esc_html( $content_width_percentage  ),
				esc_html( $sidebar_width_percentage )
			);
		?>
	</style>
	<?php } ?>

	<style id="module-customizer-css">
		<?php

			/* Gallery */
			dhm_pb_print_module_styles_css( 'dhm_pb_gallery', array(
				array(
					'type'		=> 'color',
					'key' 		=> 'zoom_icon_color',
					'selector' 	=> '.dhm_pb_gallery_image .dhm_overlay:before',
					'important'	=> true,
				),
				array(
					'type'		=> 'background-color',
					'key' 		=> 'hover_overlay_color',
					'selector' 	=> '.dhm_pb_gallery_image .dhm_overlay',
				),
				array(
					'type'		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_gallery_grid .dhm_pb_gallery_item .dhm_pb_gallery_title',
				),
				array(
					'type'		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_gallery_grid .dhm_pb_gallery_item .dhm_pb_gallery_title',
				),
				array(
					'type'		=> 'font-size',
					'key' 		=> 'caption_font_size',
					'selector' 	=> '.dhm_pb_gallery .dhm_pb_gallery_item .dhm_pb_gallery_caption',
				),
				array(
					'type'		=> 'font-style',
					'key' 		=> 'caption_font_style',
					'selector' 	=> '.dhm_pb_gallery .dhm_pb_gallery_item .dhm_pb_gallery_caption',
				),
			) );

			/* Blurb */
			dhm_pb_print_module_styles_css( 'dhm_pb_blurb', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_blurb h4',
				),
			) );

			/* Tabs */
			dhm_pb_print_module_styles_css( 'dhm_pb_tabs', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_tabs_controls li',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_tabs_controls li',
				),
				array(
					'type' 		=> 'padding-tabs',
					'key' 		=> 'padding',
					'selector' 	=> '',
				),
			) );

			/* Slider */
			dhm_pb_print_module_styles_css( 'dhm_pb_slider', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_slider_fullwidth_off .dhm_pb_slide_description .dhm_pb_slide_title',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_slider_fullwidth_off .dhm_pb_slide_description .dhm_pb_slide_title',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'body_font_size',
					'selector' 	=> '.dhm_pb_slider_fullwidth_off .dhm_pb_slide_content',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'body_font_style',
					'selector' 	=> '.dhm_pb_slider_fullwidth_off .dhm_pb_slide_content',
				),
				array(
					'type' 		=> 'padding-slider',
					'key' 		=> 'padding',
					'selector' 	=> '.dhm_pb_slider_fullwidth_off .dhm_pb_slide_description',
				),
			) );

			/* Testimonial */
			dhm_pb_print_module_styles_css( 'dhm_pb_testimonial', array(
				array(
					'type' 		=> 'border-radius',
					'key' 		=> 'portrait_border_radius',
					'selector' 	=> '.dhm_pb_testimonial_portrait, .dhm_pb_testimonial_portrait:before',
				),
				array(
					'type' 		=> 'width',
					'key' 		=> 'portrait_width',
					'selector' 	=> '.dhm_pb_testimonial_portrait',
				),
				array(
					'type' 		=> 'height',
					'key' 		=> 'portrait_height',
					'selector' 	=> '.dhm_pb_testimonial_portrait',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'author_name_font_style',
					'selector' 	=> '.dhm_pb_testimonial_author',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'author_details_font_style',
					'selector' 	=> 'p.dhm_pb_testimonial_meta',
				),
			) );

			/* Pricing Table */
			dhm_pb_print_module_styles_css( 'dhm_pb_pricing_tables', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_pricing_heading h2',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_pricing_heading h2',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'subheader_font_size',
					'selector' 	=> '.dhm_pb_best_value',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'subheader_font_style',
					'selector' 	=> '.dhm_pb_best_value',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'price_font_size',
					'selector' 	=> '.dhm_pb_sum',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'price_font_style',
					'selector' 	=> '.dhm_pb_sum',
				),
			) );

			/* Call to Action */
			dhm_pb_print_module_styles_css( 'dhm_pb_cta', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_promo h2',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_promo h2, .dhm_pb_promo h1',
				),
				array(
					'type' 		=> 'padding-call-to-action',
					'key' 		=> 'custom_padding',
					'selector' 	=> '',
					'important' => true,
				),
			) );

			/* Audio */
			dhm_pb_print_module_styles_css( 'dhm_pb_audio', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_audio_module_content h2',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_audio_module_content h2',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'caption_font_size',
					'selector' 	=> '.dhm_pb_audio_module p',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'caption_font_style',
					'selector' 	=> '.dhm_pb_audio_module p',
				),
			) );

			/* Email Optin */
			dhm_pb_print_module_styles_css( 'dhm_pb_signup', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_subscribe h2',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_subscribe h2',
				),
				array(
					'type' 		=> 'padding',
					'key' 		=> 'padding',
					'selector' 	=> '.dhm_pb_subscribe',
				),
			) );

			/* Login */
			dhm_pb_print_module_styles_css( 'dhm_pb_login', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_login h2',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_login h2',
				),
				array(
					'type' 		=> 'padding-top-bottom',
					'key' 		=> 'custom_padding',
					'selector' 	=> '.dhm_pb_login',
				),
			) );

			/* Portfolio */
			dhm_pb_print_module_styles_css( 'dhm_pb_portfolio', array(
				array(
					'type' 		=> 'color',
					'key' 		=> 'zoom_icon_color',
					'selector' 	=> '.dhm_pb_portfolio .dhm_overlay:before, .dhm_pb_fullwidth_portfolio .dhm_overlay:before, .dhm_pb_portfolio_grid .dhm_overlay:before',
					'important' => true,
				),
				array(
					'type' 		=> 'background-color',
					'key' 		=> 'hover_overlay_color',
					'selector' 	=> '.dhm_pb_portfolio .dhm_overlay, .dhm_pb_fullwidth_portfolio .dhm_overlay, .dhm_pb_portfolio_grid .dhm_overlay',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_portfolio .dhm_pb_portfolio_item h2, .dhm_pb_fullwidth_portfolio .dhm_pb_portfolio_item h3, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item h2',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_portfolio .dhm_pb_portfolio_item h2, .dhm_pb_fullwidth_portfolio .dhm_pb_portfolio_item h3, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item h2',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'caption_font_size',
					'selector' 	=> '.dhm_pb_portfolio .dhm_pb_portfolio_item .post-meta, .dhm_pb_fullwidth_portfolio .dhm_pb_portfolio_item .post-meta, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item .post-meta',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'caption_font_style',
					'selector' 	=> '.dhm_pb_portfolio .dhm_pb_portfolio_item .post-meta, .dhm_pb_fullwidth_portfolio .dhm_pb_portfolio_item .post-meta, .dhm_pb_portfolio_grid .dhm_pb_portfolio_item .post-meta',
				),
			) );

			/* Filterable Portfolio */
			dhm_pb_print_module_styles_css( 'dhm_pb_filterable_portfolio', array(
				array(
					'type' 		=> 'color',
					'key' 		=> 'zoom_icon_color',
					'selector' 	=> '.dhm_pb_filterable_portfolio .dhm_overlay:before',
				),
				array(
					'type' 		=> 'background-color',
					'key' 		=> 'hover_overlay_color',
					'selector' 	=> '.dhm_pb_filterable_portfolio .dhm_overlay',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_filterable_portfolio .dhm_pb_portfolio_item h2',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_filterable_portfolio .dhm_pb_portfolio_item h2',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'caption_font_size',
					'selector' 	=> '.dhm_pb_filterable_portfolio .dhm_pb_portfolio_item .post-meta',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'caption_font_style',
					'selector' 	=> '.dhm_pb_filterable_portfolio .dhm_pb_portfolio_item .post-meta',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'filter_font_size',
					'selector' 	=> '.dhm_pb_filterable_portfolio .dhm_pb_portfolio_filters li',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'filter_font_style',
					'selector' 	=> '.dhm_pb_filterable_portfolio .dhm_pb_portfolio_filters li',
				),
			) );

			/* Bar Counter */
			dhm_pb_print_module_styles_css( 'dhm_pb_counters', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_counters .dhm_pb_counter_title',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_counters .dhm_pb_counter_title',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'percent_font_size',
					'selector' 	=> '.dhm_pb_counters .dhm_pb_counter_amount',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'percent_font_style',
					'selector' 	=> '.dhm_pb_counters .dhm_pb_counter_amount',
				),
				array(
					'type' 		=> 'border-radius',
					'key' 		=> 'border_radius',
					'selector' 	=> '.dhm_pb_counters .dhm_pb_counter_amount, .dhm_pb_counters .dhm_pb_counter_container',
				),
				array(
					'type' 		=> 'padding',
					'key' 		=> 'padding',
					'selector' 	=> '.dhm_pb_counter_amount',
				),
			) );

			/* Circle Counter */
			dhm_pb_print_module_styles_css( 'dhm_pb_circle_counter', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'number_font_size',
					'selector' 	=> '.dhm_pb_circle_counter .percent p',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'number_font_style',
					'selector' 	=> '.dhm_pb_circle_counter .percent p',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_circle_counter h3',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_circle_counter h3',
				),
			) );

			/* Number Counter */
			dhm_pb_print_module_styles_css( 'dhm_pb_number_counter', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'number_font_size',
					'selector' 	=> '.dhm_pb_number_counter .percent p',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'number_font_style',
					'selector' 	=> '.dhm_pb_number_counter .percent p',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_number_counter h3',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_number_counter h3',
				),
			) );

			/* Accordion */
			dhm_pb_print_module_styles_css( 'dhm_pb_accordion', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'toggle_font_size',
					'selector' 	=> '.dhm_pb_accordion .dhm_pb_toggle_title',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'toggle_font_style',
					'selector' 	=> '.dhm_pb_accordion .dhm_pb_toggle.dhm_pb_toggle_open .dhm_pb_toggle_title',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'inactive_toggle_font_style',
					'selector' 	=> '.dhm_pb_accordion .dhm_pb_toggle.dhm_pb_toggle_close .dhm_pb_toggle_title',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'toggle_icon_size',
					'selector' 	=> '.dhm_pb_accordion .dhm_pb_toggle_title:before',
				),
				array(
					'type' 		=> 'padding',
					'key' 		=> 'custom_padding',
					'selector' 	=> '.dhm_pb_accordion .dhm_pb_toggle_open, .dhm_pb_accordion .dhm_pb_toggle_close',
				),
			) );

			/* Toggle */
			dhm_pb_print_module_styles_css( 'dhm_pb_toggle', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_toggle.dhm_pb_toggle_item h5',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_toggle.dhm_pb_toggle_item.dhm_pb_toggle_open h5',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'inactive_title_font_style',
					'selector' 	=> '.dhm_pb_toggle.dhm_pb_toggle_item.dhm_pb_toggle_close h5',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'toggle_icon_size',
					'selector' 	=> '.dhm_pb_toggle.dhm_pb_toggle_item .dhm_pb_toggle_title:before',
				),
				array(
					'type' 		=> 'padding',
					'key' 		=> 'custom_padding',
					'selector' 	=> '.dhm_pb_toggle.dhm_pb_toggle_item',
				),
			) );

			/* Contact Form */
			dhm_pb_print_module_styles_css( 'dhm_pb_contact_form', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.dhm_pb_contact_form_container .dhm_pb_contact_main_title',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.dhm_pb_contact_form_container .dhm_pb_contact_main_title',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'form_field_font_size',
					'selector' 	=> '.dhm_pb_contact_form_container .dhm_pb_contact p input, .dhm_pb_contact_form_container .dhm_pb_contact p textarea',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'form_field_font_style',
					'selector' 	=> '.dhm_pb_contact_form_container .dhm_pb_contact p input, .dhm_pb_contact_form_container .dhm_pb_contact p textarea',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'captcha_font_size',
					'selector' 	=> '.dhm_pb_contact_captcha_question',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'captcha_font_style',
					'selector' 	=> '.dhm_pb_contact_captcha_question',
				),
				array(
					'type' 		=> 'padding',
					'key' 		=> 'padding',
					'selector' 	=> '.dhm_pb_contact p input, .dhm_pb_contact p textarea',
				),
			) );

			/* Sidebar */
			dhm_pb_print_module_styles_css( 'dhm_pb_sidebar', array(
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_widget_area h4',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_widget_area h4',
				),
			) );

			/* Divider */
			dhm_pb_print_module_styles_css( 'dhm_pb_divider', array(
				array(
					'type' 		=> 'border-top-style',
					'key' 		=> 'divider_style',
					'selector' 	=> '.dhm_pb_space:before',
				),
				array(
					'type' 		=> 'border-top-width',
					'key' 		=> 'divider_weight',
					'selector' 	=> '.dhm_pb_space:before',
				),
				array(
					'type' 		=> 'height',
					'key' 		=> 'height',
					'selector' 	=> '.dhm_pb_space',
				),
			) );

			/* Person */
			dhm_pb_print_module_styles_css( 'dhm_pb_team_member', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_team_member h4',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_team_member h4',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'subheader_font_size',
					'selector' 	=> '.dhm_pb_team_member .dhm_pb_member_position',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'subheader_font_style',
					'selector' 	=> '.dhm_pb_team_member .dhm_pb_member_position',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'social_network_icon_size',
					'selector' 	=> '.dhm_pb_member_social_links a',
				),
			) );

			/* Blog */
			dhm_pb_print_module_styles_css( 'dhm_pb_blog', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_posts .dhm_pb_post h2',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_posts .dhm_pb_post h2',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'meta_font_size',
					'selector' 	=> '.dhm_pb_posts .dhm_pb_post .post-meta',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'meta_font_style',
					'selector' 	=> '.dhm_pb_posts .dhm_pb_post .post-meta',
				),
			) );

			/* Blog Masonry */
			dhm_pb_print_module_styles_css( 'dhm_pb_blog_masonry', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_blog_grid .dhm_pb_post h2',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_blog_grid .dhm_pb_post h2',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'meta_font_size',
					'selector' 	=> '.dhm_pb_blog_grid .dhm_pb_post .post-meta',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'meta_font_style',
					'selector' 	=> '.dhm_pb_blog_grid .dhm_pb_post .post-meta',
				),
			) );

			/* Shop */
			dhm_pb_print_module_styles_css( 'dhm_pb_shop', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'title_font_size',
					'selector' 	=> '.woocommerce ul.products li.product h3, .woocommerce-page ul.products li.product h3',
					'important' => false,
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'title_font_style',
					'selector' 	=> '.woocommerce ul.products li.product h3, .woocommerce-page ul.products li.product h3',
					'important' => false,
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'sale_badge_font_size',
					'selector' 	=> '.woocommerce span.onsale, .woocommerce-page span.onsale',
					'important' => false,
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'sale_badge_font_style',
					'selector' 	=> '.woocommerce span.onsale, .woocommerce-page span.onsale',
					'important' => true,
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'price_font_size',
					'selector' 	=> '.woocommerce ul.products li.product .price .amount, .woocommerce-page ul.products li.product .price .amount',
					'important' => false,
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'price_font_style',
					'selector' 	=> '.woocommerce ul.products li.product .price .amount, .woocommerce-page ul.products li.product .price .amount',
					'important' => true,
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'sale_price_font_size',
					'selector' 	=> '.woocommerce ul.products li.product .price ins .amount, .woocommerce-page ul.products li.product .price ins .amount',
					'important' => false,
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'sale_price_font_style',
					'selector' 	=> '.woocommerce ul.products li.product .price ins .amount, .woocommerce-page ul.products li.product .price ins .amount',
					'important' => true,
				),
			) );

			/* Countdown */
			dhm_pb_print_module_styles_css( 'dhm_pb_countdown_timer', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_countdown_timer .title',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_countdown_timer .title',
				),
			) );

			/* Social */
			dhm_pb_print_module_styles_css( 'dhm_pb_social_media_follow', array(
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'button_font_style',
					'selector' 	=> '.dhm_pb_social_media_follow li a.follow_button',
				),
				array(
					'type' 		=> 'social-icon-size',
					'key' 		=> 'icon_size',
					'selector' 	=> '',
				),
			) );

			/* Fullwidth Slider */
			dhm_pb_print_module_styles_css( 'dhm_pb_fullwidth_slider', array(
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'header_font_size',
					'selector' 	=> '.dhm_pb_fullwidth_section .dhm_pb_slide_description .dhm_pb_slide_title',
					'default' 	=> '46',
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'header_font_style',
					'selector' 	=> '.dhm_pb_fullwidth_section .dhm_pb_slide_description .dhm_pb_slide_title',
					'default' 	=> '',
				),
				array(
					'type' 		=> 'font-size',
					'key' 		=> 'body_font_size',
					'selector' 	=> '.dhm_pb_fullwidth_section .dhm_pb_slide_content',
					'default' 	=> 16,
				),
				array(
					'type' 		=> 'font-style',
					'key' 		=> 'body_font_style',
					'selector' 	=> '.dhm_pb_fullwidth_section .dhm_pb_slide_content',
					'default' 	=> '',
				),
				array(
					'type' 		=> 'padding-slider',
					'key' 		=> 'padding',
					'selector' 	=> '.dhm_pb_fullwidth_section .dhm_pb_slide_description',
					'default' 	=> '16',
				),
			) );
		?>
	</style>

	<?php
}
add_action( 'wp_head', 'dhm_rrcfest_add_customizer_css' );

/**
 * Outputting saved customizer style settings
 *
 * @return void
 */
function dhm_pb_print_css( $setting ) {

	// Defaults value
	$defaults = array(
		'key'       => false,
		'selector'  => false,
		'type'      => false,
		'default'   => false,
		'important' => false
	);

	// Parse given settings aginst defaults
	$setting = wp_parse_args( $setting, $defaults );

	if (
		$setting['key']      !== false ||
		$setting['selector'] !== false ||
		$setting['type']     !== false ||
		$setting['settings'] !== false
	) {

		// Some attribute requires !important tag
		if ( $setting['important'] ) {
			$important = "!important";
		} else {
			$important = "";
		}

		// get value
		$value = dhm_get_option( $setting['key'], $setting['default'] );

		// Output css based on its type
		if ( $value !== false && $value != $setting['default'] ) {
			switch ( $setting['type'] ) {
				case 'font-size':
					printf( '%1$s { font-size: %2$spx %3$s; }',
						esc_html( $setting['selector'] ),
						esc_html( $value ),
						$important );
					break;

				case 'font-size-post-header':
					$posts_font_size = intval( $value ) * ( 26 / 30 );
					printf( 'body.home-posts #left-area .dhm_pb_post h2, body.archive #left-area .dhm_pb_post h2, body.search #left-area .dhm_pb_post h2 { font-size:%1$spx }
						body.single .dhm_post_meta_wrapper h1 { font-size:%2$spx; }',
						esc_html( $posts_font_size ),
						esc_html( $value )
					);
					break;

				case 'font-style':
					printf( '%1$s { %2$s }',
						esc_html( $setting['selector'] ),
						dhm_pb_print_font_style( $value, $important )
					);
					break;

				case 'letter-spacing':
					printf( '%1$s { letter-spacing: %2$spx %3$s; }',
						esc_html( $setting['selector'] ),
						esc_html( $value ),
						$important
					);
					break;

				case 'line-height':
					printf( '%1$s { line-height: %2$sem %3$s; }',
						esc_html( $setting['selector'] ),
						esc_html( $value ),
						$important
					);
					break;

				case 'color':
					printf( '%1$s { color: %2$s; }',
						esc_html( $setting['selector'] ),
						esc_html( $value )
					);
					break;

				case 'background-color':
					printf( '%1$s { background-color: %2$s; }',
						esc_html( $setting['selector'] ),
						esc_html( $value )
					);
					break;

				case 'border-radius':
					printf( '%1$s { -moz-border-radius: %2$spx; -webkit-border-radius: %2$spx; border-radius: %2$spx; }',
						esc_html( $setting['selector'] ),
						esc_html( $value )
					);
					break;

				case 'width':
					printf( '%1$s { width: %2$spx; }',
						esc_html( $setting['selector'] ),
						esc_html( $value )
					);
					break;

				case 'height':
					printf( '%1$s { height: %2$spx; }',
						esc_html( $setting['selector'] ),
						esc_html( $value )
					);
					break;

				case 'padding':
					printf( '%1$s { padding: %2$spx; }',
						esc_html( $setting['selector'] ),
						esc_html( $value )
					);
					break;

				case 'padding-top-bottom':
					printf( '%1$s { padding: %2$spx 0; }',
						esc_html( $setting['selector'] ),
						esc_html( $value )
					);
					break;

				case 'padding-tabs':
					printf( '%1$s { padding: %2$spx %3$spx; }',
						esc_html( $setting['selector'] ),
						esc_html( ( intval( $value ) * 0.5 ) ),
						esc_html( $value )
					);
					break;

				case 'padding-fullwidth-slider':
					printf( '%1$s { padding: %2$s %3$s; }',
						esc_html( $setting['selector'] ),
						esc_html( $value ) . '%',
						'0'
					);
					break;

				case 'padding-slider':
					printf( '%1$s { padding: %2$s %3$s; }',
						esc_html( $setting['selector'] ),
						esc_html( $value ) . '%',
						esc_html( ( intval( $value ) / 2 ) ) . '%'
					);
					break;

				case 'social-icon-size':
					$icon_margin 	= intval( $value ) * 0.57;
					$icon_dimension = intval( $value ) * 2;
					?>
					.dhm_pb_social_media_follow li a.icon{
						margin-right: <?php echo esc_html( $icon_margin ); ?>px;
						width: <?php echo esc_html( $icon_dimension ); ?>px;
						height: <?php echo esc_html( $icon_dimension ); ?>px;
					}

					.dhm_pb_social_media_follow li a.icon::before{
						width: <?php echo esc_html( $icon_dimension ); ?>px;
						height: <?php echo esc_html( $icon_dimension ); ?>px;
						font-size: <?php echo esc_html( $value ); ?>px;
						line-height: <?php echo esc_html( $icon_dimension ); ?>px;
					}
					<?php
					break;
			}
		}
	}
}

/**
 * Outputting saved customizer style(s) settings
 */
function dhm_pb_print_styles_css( $settings = array() ) {

	// $settings should be in array
	if ( is_array( $settings ) && ! empty( $settings ) ) {

		// Loop settings
		foreach ( $settings as $setting ) {

			// Print css
			dhm_pb_print_css( $setting );

		}
	}
}

/**
 * Outputting saved module styles settings. DRY
 *
 * @return void
 */
function dhm_pb_print_module_styles_css( $section = '', $settings = array() ) {

	// Verify settings
	if ( is_array( $settings ) && ! empty( $settings ) ) {

		// Loop settings
		foreach ( $settings as $setting ) {

			// settings must have these elements: key, selector, default, and type
			if ( ! isset( $setting['key'] ) ||
				! isset( $setting['selector'] ) ||
				! isset( $setting['type'] ) ) {
				continue;
			}

			// Some attributes such as shop requires !important tag
			if ( isset( $setting['important'] ) && true === $setting['important'] ) {
				$important = ' !important';
			} else {
				$important = '';
			}

			// Prepare the setting key
			$key = "{$section}-{$setting['key']}";

			// Get the value
			$value = ET_Global_Settings::get_value( $key );
			$default_value = ET_Global_Settings::get_value( $key, 'default' );

			// Output CSS based on its type
			if ( false !== $value && $default_value !== $value ) {

				switch ( $setting['type'] ) {
					case 'font-size':

						printf( "%s { font-size: %spx%s; }\n", esc_html( $setting['selector'] ), esc_html( $value ), $important );

						// Option with specific adjustment for smaller columns
						$smaller_title_sections = array(
							'dhm_pb_audio-title_font_size',
							'dhm_pb_blog-header_font_size',
							'dhm_pb_cta-header_font_size',
							'dhm_pb_contact_form-title_font_size',
							'dhm_pb_login-header_font_size',
							'dhm_pb_signup-header_font_size',
							'dhm_pb_slider-header_font_size',
							'dhm_pb_slider-body_font_size',
							'dhm_pb_countdown_timer-header_font_size',
						);

						if ( in_array( $key, $smaller_title_sections ) ) {

							// font size coefficient
							switch ( $key ) {
								case 'dhm_pb_slider-header_font_size':
									$font_size_coefficient = .565217391; // 26/46
									break;

								case 'dhm_pb_slider-body_font_size':
									$font_size_coefficient = .777777778; // 14/16
									break;

								default:
									$font_size_coefficient = .846153846; // 22/26
									break;
							}

							printf( '.dhm_pb_column_1_3 %1$s, .dhm_pb_column_1_4 %1$s { font-size: %2$spx%3$s; }',
								esc_html( $setting['selector'] ),
								esc_html( $value * $font_size_coefficient ),
								$important
							);
						}

						break;

					case 'font-size':
						$value = intval( $value );

						printf( ".dhm_pb_countdown_timer .title { font-size: %spx; }", esc_html( $value ) );
						printf( ".dhm_pb_column_3_8 .dhm_pb_countdown_timer .title, .dhm_pb_column_1_3 .dhm_pb_countdown_timer .title, .dhm_pb_column_1_4 .dhm_pb_countdown_timer .title { font-size: %spx; }", esc_html( $value * ( 18 / 22 ) ) );
						break;

					case 'font-style':
						printf( "%s { %s }\n", esc_html( $setting['selector'] ), dhm_pb_print_font_style( $value, $important ) );
						break;

					case 'color':
						printf( "%s { color: %s%s; }\n", esc_html( $setting['selector'] ), esc_html( $value ), $important );
						break;

					case 'background-color':
						printf( "%s { background-color: %s%s; }\n", esc_html( $setting['selector'] ), esc_html( $value ), $important );
						break;

					case 'border-radius':
						printf( "%s { -moz-border-radius: %spx; -webkit-border-radius: %spx; border-radius: %spx; }\n", esc_html( $setting['selector'] ), esc_html( $value ), esc_html( $value ), esc_html( $value ) );
						break;

					case 'width':
						printf( "%s { width: %spx; }\n", esc_html( $setting['selector'] ), esc_html( $value ) );
						break;

					case 'height':
						printf( "%s { height: %spx; }\n", esc_html( $setting['selector'] ), esc_html( $value ) );
						break;

					case 'padding':
						printf( "%s { padding: %spx; }\n", esc_html( $setting['selector'] ), esc_html( $value ) );
						break;

					case 'padding-top-bottom':
						printf( "%s { padding: %spx 0; }\n", esc_html( $setting['selector'] ), esc_html( $value ) );
						break;

					case 'padding-tabs':
						$padding_tab_top_bottom 	= intval( $value ) * 0.133333333;
						$padding_tab_active_top 	= $padding_tab_top_bottom + 1;
						$padding_tab_active_bottom 	= $padding_tab_top_bottom - 1;
						$padding_tab_content 		= intval( $value ) * 0.8;

						// negative result will cause layout issue
						if ( $padding_tab_active_bottom < 0 ) {
							$padding_tab_active_bottom = 0;
						}

						printf(
							".dhm_pb_tabs_controls li{ padding: %spx %spx %spx; } .dhm_pb_tabs_controls li.dhm_pb_tab_active{ padding: %spx %spx; } .dhm_pb_all_tabs { padding: %spx %spx; }\n",
							esc_html( $padding_tab_active_top ),
							esc_html( $value ),
							esc_html( $padding_tab_active_bottom ),
							esc_html( $padding_tab_top_bottom ),
							esc_html( $value ),
							esc_html( $padding_tab_content ),
							esc_html( $value )
						);
						break;

					case 'padding-slider':
						printf( "%s { padding-top: %s; padding-bottom: %s }\n", esc_html( $setting['selector'] ), esc_html( $value ) . '%', esc_html( $value ) . '%' );

						if ( 'dhm_pagebuilder_slider_padding' === $key ) {
							printf( '@media only screen and ( max-width: 767px ) { %1$s { padding-top: %2$s; padding-bottom: %2$s; } }', esc_html( $setting['selector'] ), '16%' );
						}
						break;

					case 'padding-call-to-action':
						$value = intval( $value );

						printf( ".dhm_pb_promo { padding: %spx %spx !important; }", esc_html( $value ), esc_html( $value * ( 60 / 40 ) ) );
						printf( ".dhm_pb_column_1_2 .dhm_pb_promo, .dhm_pb_column_1_3 .dhm_pb_promo, .dhm_pb_column_1_4 .dhm_pb_promo { padding: %spx; }", esc_html( $value ) );
						break;

					case 'social-icon-size':
						$icon_margin 	= intval( $value ) * 0.57;
						$icon_dimension = intval( $value ) * 2;
						?>
						.dhm_pb_social_media_follow li a.icon{
							margin-right: <?php echo esc_html( $icon_margin ); ?>px;
							width: <?php echo esc_html( $icon_dimension ); ?>px;
							height: <?php echo esc_html( $icon_dimension ); ?>px;
						}

						.dhm_pb_social_media_follow li a.icon::before{
							width: <?php echo esc_html( $icon_dimension ); ?>px;
							height: <?php echo esc_html( $icon_dimension ); ?>px;
							font-size: <?php echo esc_html( $value ); ?>px;
							line-height: <?php echo esc_html( $icon_dimension ); ?>px;
						}

						.dhm_pb_social_media_follow li a.follow_button{
							font-size: <?php echo esc_html( $value ); ?>px;
						}
						<?php
						break;

					case 'border-top-style':
						printf( "%s { border-top-style: %s; }\n", esc_html( $setting['selector'] ), esc_html( $value ) );
						break;

					case 'border-top-width':
						printf( "%s { border-top-width: %spx; }\n", esc_html( $setting['selector'] ), esc_html( $value ) );
						break;
				}
			}
		}
	}
}

/**
 * Outputting font-style attributes & values saved by ET_Rrcfest_Font_Style_Option on customizer
 *
 * @return string
 */
function dhm_pb_print_font_style( $styles = '', $important = '' ) {

	// Prepare variable
	$font_styles = "";

	if ( '' !== $styles && false !== $styles ) {
		// Convert string into array
		$styles_array = explode( '|', $styles );

		// If $important is in use, give it a space
		if ( $important && '' !== $important ) {
			$important = " " . $important;
		}

		// Use in_array to find values in strings. Otherwise, display default text

		// Font weight
		if ( in_array( 'bold', $styles_array ) ) {
			$font_styles .= "font-weight: bold{$important}; ";
		} else {
			$font_styles .= "font-weight: normal{$important}; ";
		}

		// Font style
		if ( in_array( 'italic', $styles_array ) ) {
			$font_styles .= "font-style: italic{$important}; ";
		} else {
			$font_styles .= "font-style: normal{$important}; ";
		}

		// Text-transform
		if ( in_array( 'uppercase', $styles_array ) ) {
			$font_styles .= "text-transform: uppercase{$important}; ";
		} else {
			$font_styles .= "text-transform: none{$important}; ";
		}

		// Text-decoration
		if ( in_array( 'underline', $styles_array ) ) {
			$font_styles .= "text-decoration: underline{$important}; ";
		} else {
			$font_styles .= "text-decoration: none{$important}; ";
		}
	}

	return esc_html( $font_styles );
}

/*
 * Adds color scheme class to the body tag
 */
function dhm_customizer_color_scheme_class( $body_class ) {
	$color_scheme        = dhm_get_option( 'color_schemes', 'none' );
	$color_scheme_prefix = 'dhm_color_scheme_';

	if ( 'none' !== $color_scheme ) $body_class[] = $color_scheme_prefix . $color_scheme;

	return $body_class;
}
add_filter( 'body_class', 'dhm_customizer_color_scheme_class' );

/*
 * Adds button class to the body tag
 */
function dhm_customizer_button_class( $body_class ) {
	$button_icon_placement = dhm_get_option( 'all_buttons_icon_placement', 'right' );
	$button_icon_on_hover = dhm_get_option( 'all_buttons_icon_hover', 'yes' );
	$button_use_icon = dhm_get_option( 'all_buttons_icon', 'yes' );
	$button_icon = dhm_get_option( 'all_buttons_selected_icon', '5' );

	if ( 'left' === $button_icon_placement ) {
		$body_class[] = 'dhm_button_left';
	}

	if ( 'no' === $button_icon_on_hover ) {
		$body_class[] = 'dhm_button_icon_visible';
	}

	if ( 'no' === $button_use_icon ) {
		$body_class[] = 'dhm_button_no_icon';
	}

	if ( '5' !== $button_icon ) {
		$body_class[] = 'dhm_button_custom_icon';
	}

	$body_class[] = 'dhm_pb_button_helper_class';

	return $body_class;
}
add_filter( 'body_class', 'dhm_customizer_button_class' );

function dhm_load_google_fonts_scripts() {
	$theme_version = dhm_get_theme_version();

	wp_enqueue_script( 'dhm_google_fonts', get_template_directory_uri() . '/epanel/google-fonts/dhm_google_fonts.js', array( 'jquery' ), $theme_version, true );
}
add_action( 'customize_controls_print_footer_scripts', 'dhm_load_google_fonts_scripts' );

function dhm_load_google_fonts_styles() {
	$theme_version = dhm_get_theme_version();

	wp_enqueue_style( 'dhm_google_fonts_style', get_template_directory_uri() . '/epanel/google-fonts/dhm_google_fonts.css', array(), $theme_version );
}
add_action( 'customize_controls_print_styles', 'dhm_load_google_fonts_styles' );

if ( ! function_exists( 'dhm_rrcfest_post_meta' ) ) :
function dhm_rrcfest_post_meta() {
	$postinfo = is_single() ? dhm_get_option( 'rrcfest_postinfo2' ) : dhm_get_option( 'rrcfest_postinfo1' );

	if ( $postinfo ) :
		echo '<p class="post-meta">';
		echo dhm_pb_postinfo_meta( $postinfo, dhm_get_option( 'rrcfest_date_format', 'M j, Y' ), esc_html__( '0 comments', 'Rrcfest' ), esc_html__( '1 comment', 'Rrcfest' ), '% ' . esc_html__( 'comments', 'Rrcfest' ) );
		echo '</p>';
	endif;
}
endif;

function dhm_video_embed_html( $video ) {
	if ( is_single() && 'video' === dhm_pb_post_format() ) {
		static $post_video_num = 0;

		$post_video_num++;

		// Hide first video in the post content on single video post page
		if ( 1 === $post_video_num ) {
			return '';
		}
	}

	return "<div class='dhm_post_video'>{$video}</div>";
}
add_filter( 'embed_oembed_html', 'dhm_video_embed_html' );

/**
 * Removes galleries on single gallery posts, since we display images from all
 * galleries on top of the page
 */
function dhm_delete_post_gallery( $content ) {
	if ( ( is_single() || is_archive() ) && is_main_query() && has_post_format( 'gallery' ) ) :
		$regex = get_shortcode_regex();
		preg_match_all( "/{$regex}/s", $content, $matches );

		// $matches[2] holds an array of shortcodes names in the post
		foreach ( $matches[2] as $key => $shortcode_match ) {
			if ( 'gallery' === $shortcode_match ) {
				$content = str_replace( $matches[0][$key], '', $content );
				break;
			}
		}
	endif;

	return $content;
}
add_filter( 'the_content', 'dhm_delete_post_gallery' );

function dhm_rrcfest_post_admin_scripts_styles( $hook ) {
	global $typenow;

	$theme_version = dhm_get_theme_version();
	$current_screen = get_current_screen();

	if ( ! in_array( $hook, array( 'post-new.php', 'post.php' ) ) ) return;

	if ( ! isset( $typenow ) ) return;

	if ( in_array( $typenow, array( 'post' ) ) ) {
		wp_enqueue_style( 'wp-color-picker' );
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_script( 'dhm-admin-post-script', get_template_directory_uri() . '/js/admin_post_settings.js', array( 'jquery' ), $theme_version );
	}

	// Only enqueue on editing screen
	if ( isset( $current_screen->base ) && 'post' === $current_screen->base ) {
		wp_enqueue_style( 'dhm-meta-box-style', get_template_directory_uri() . '/css/meta-box-styles.css', array(), $theme_version );
	}
}
add_action( 'admin_enqueue_scripts', 'dhm_rrcfest_post_admin_scripts_styles' );

function dhm_password_form() {
	$pwbox_id = rand();

	$form_output = sprintf(
		'<div class="dhm_password_protected_form">
			<h1>%1$s</h1>
			<p>%2$s:</p>
			<form action="%3$s" method="post">
				<p><label for="%4$s">%5$s: </label><input name="post_password" id="%4$s" type="password" size="20" maxlength="20" /></p>
				<p><button type="submit" class="dhm_submit_button dhm_pb_button">%6$s</button></p>
			</form
		</div>',
		esc_html__( 'Password Protected', 'Rrcfest' ),
		esc_html__( 'To view this protected post, enter the password below', 'Rrcfest' ),
		esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ),
		esc_attr( 'pwbox-' . $pwbox_id ),
		esc_html__( 'Password', 'Rrcfest' ),
		esc_html__( 'Submit', 'Rrcfest' )
	);

	$output = sprintf(
		'<div class="dhm_pb_section dhm_section_regular">
			<div class="dhm_pb_row">
				<div class="dhm_pb_column dhm_pb_column_4_4">
					%1$s
				</div>
			</div>
		</div>',
		$form_output
	);

	return $output;
}
add_filter( 'the_password_form', 'dhm_password_form' );

function dhm_add_wp_version( $classes ) {
	global $wp_version;

	$is_admin_body_class = 'admin_body_class' === current_filter();

	// add 'dhm-wp-pre-3_8' class if the current WordPress version is less than 3.8
	if ( version_compare( $wp_version, '3.7.2', '<=' ) ) {
		if ( 'body_class' === current_filter() ) {
			$classes[] = 'dhm-wp-pre-3_8';
		} else {
			$classes .= ' dhm-wp-pre-3_8';
		}
	} else if ( $is_admin_body_class ) {
		$classes .= ' dhm-wp-after-3_8';
	}

	if ( $is_admin_body_class ) {
		$classes = ltrim( $classes );
	}

	return $classes;
}
add_filter( 'body_class', 'dhm_add_wp_version' );
add_filter( 'admin_body_class', 'dhm_add_wp_version' );

function dhm_layout_body_class( $classes ) {
	if ( 'rgba' == substr( dhm_get_option( 'primary_nav_bg', '#ffffff' ), 0, 4 ) && false === dhm_get_option( 'vertical_nav', false ) ) {
		$classes[] = 'dhm_transparent_nav';
	}

	// home-posts class is used by customizer > blog to work. It modifies post title and meta
	// of WP default layout (home, archive, single), but should not modify post title and meta of blog module (page as home)
	if ( in_array( 'home', $classes ) && ! in_array( 'page', $classes ) ) {
		$classes[] = 'home-posts';
	}

	if ( true === dhm_get_option( 'nav_fullwidth', false ) ) {
		if ( true === dhm_get_option( 'vertical_nav', false ) ) {
			$classes[] = 'dhm_fullwidth_nav_temp';
		} else {
			$classes[] = 'dhm_fullwidth_nav';
		}
	}

	if ( true === dhm_get_option( 'secondary_nav_fullwidth', false ) ) {
		$classes[] = 'dhm_fullwidth_secondary_nav';
	}

	if ( true === dhm_get_option( 'vertical_nav', false ) ) {
		$classes[] = 'dhm_vertical_nav';
	} else if ( 'on' === dhm_get_option( 'rrcfest_fixed_nav', 'on' ) ) {
		$classes[] = 'dhm_fixed_nav';
	} else if ( 'on' !== dhm_get_option( 'rrcfest_fixed_nav', 'on' ) ) {
		$classes[] = 'dhm_non_fixed_nav';
	}

	if ( true === dhm_get_option( 'vertical_nav', false ) && 'on' === dhm_get_option( 'rrcfest_fixed_nav', 'on' ) ) {
		$classes[] = 'dhm_vertical_fixed';
	}

	if ( true === dhm_get_option( 'boxed_layout', false ) ) {
		$classes[] = 'dhm_boxed_layout';
	}

	if ( true === dhm_get_option( 'hide_nav', false ) && ( ! is_singular() || is_singular() && 'no' !== get_post_meta( get_the_ID(), '_dhm_pb_post_hide_nav', true ) ) ) {
		$classes[] = 'dhm_hide_nav';
	} else {
		$classes[] = 'dhm_show_nav';
	}

	if ( true === dhm_get_option( 'hide_primary_logo', false ) ) {
		$classes[] = 'dhm_hide_primary_logo';
	}

	if ( true === dhm_get_option( 'hide_fixed_logo', false ) ) {
		$classes[] = 'dhm_hide_fixed_logo';
	}

	if ( true === dhm_get_option( 'hide_mobile_logo', false ) ) {
		$classes[] = 'dhm_hide_mobile_logo';
	}

	if ( true === dhm_get_option( 'cover_background', true ) ) {
		$classes[] = 'dhm_cover_background';
	}

	$dhm_secondary_nav_items = dhm_rrcfest_get_top_nav_items();

	if ( $dhm_secondary_nav_items->top_info_defined ) {
		$classes[] = 'dhm_secondary_nav_enabled';
	}

	if ( $dhm_secondary_nav_items->two_info_panels ) {
		$classes[] = 'dhm_secondary_nav_two_panels';
	}

	if ( $dhm_secondary_nav_items->secondary_nav && ! ( $dhm_secondary_nav_items->contact_info_defined || $dhm_secondary_nav_items->show_header_social_icons ) ) {
		$classes[] = 'dhm_secondary_nav_only_menu';
	}

	if ( 'on' === get_post_meta( get_the_ID(), '_dhm_pb_side_nav', true ) && dhm_pb_is_pagebuilder_used( get_the_ID() ) ) {
		$classes[] = 'dhm_pb_side_nav_page';
	}

	if ( true === dhm_get_option( 'dhm_pb_sidebar-remove_border', false ) ) {
		$classes[] = 'dhm_pb_no_sidebar_vertical_divider';
	}

	if ( is_singular( array( 'post', 'page', 'project', 'product' ) ) && 'on' == get_post_meta( get_the_ID(), '_dhm_pb_post_hide_nav', true ) ) {
		$classes[] = 'dhm_hide_nav';
	}

	if ( ! dhm_get_option( 'use_sidebar_width', false ) ) {
		$classes[] = 'dhm_pb_gutter';
	}

	if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
		$classes[] = 'osx';
	} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
		$classes[] = 'linux';
	} elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
		$classes[] = 'windows';
	}

	$gutter_width = dhm_get_option( 'gutter_width', '3' );
	$classes[] = esc_attr( "dhm_pb_gutters{$gutter_width}" );

	$primary_dropdown_animation = dhm_get_option( 'primary_nav_dropdown_animation', 'fade' );
	$classes[] = esc_attr( "dhm_primary_nav_dropdown_animation_{$primary_dropdown_animation}" );

	$secondary_dropdown_animation = dhm_get_option( 'secondary_nav_dropdown_animation', 'fade' );
	$classes[] = esc_attr( "dhm_secondary_nav_dropdown_animation_{$secondary_dropdown_animation}" );

	$footer_columns = dhm_get_option( 'footer_columns', '4' );
	$classes[] = esc_attr( "dhm_pb_footer_columns{$footer_columns}" );

	$header_style = dhm_get_option( 'header_style', 'left' );
	$classes[] = esc_attr( "dhm_header_style_{$header_style}" );

	$logo = dhm_get_option( 'rrcfest_logo', '' );
	if ( '.svg' === substr( $logo, -4, 4 ) ) {
		$classes[] = 'dhm_pb_svg_logo';
	}

	return $classes;
}
add_filter( 'body_class', 'dhm_layout_body_class' );

if ( ! function_exists( 'dhm_show_cart_total' ) ) {
	function dhm_show_cart_total( $args = array() ) {
		if ( ! class_exists( 'woocommerce' ) ) {
			return;
		}

		$defaults = array(
			'no_text' => false,
		);

		$args = wp_parse_args( $args, $defaults );

		printf(
			'<a href="%1$s" class="dhm-cart-info">
				<span>%2$s</span>
			</a>',
			esc_url( WC()->cart->get_cart_url() ),
			( ! $args['no_text']
				? sprintf(
				__( '%1$s %2$s', 'Rrcfest' ),
				esc_html( WC()->cart->get_cart_contents_count() ),
				( 1 === WC()->cart->get_cart_contents_count() ? __( 'Item', 'Rrcfest' ) : __( 'Items', 'Rrcfest' ) )
				)
				: ''
			)
		);
	}
}

if ( ! function_exists( 'dhm_rrcfest_get_top_nav_items' ) ) {
	function dhm_rrcfest_get_top_nav_items() {
		$items = new stdClass;

		$items->phone_number = dhm_get_option( 'phone_number' );

		$items->email = dhm_get_option( 'header_email' );

		$items->contact_info_defined = $items->phone_number || $items->email;

		$items->show_header_social_icons = dhm_get_option( 'show_header_social_icons', false );

		$items->secondary_nav = wp_nav_menu( array(
			'theme_location' => 'secondary-menu',
			'container'      => '',
			'fallback_cb'    => '',
			'menu_id'        => 'dhm-secondary-nav',
			'echo'           => false,
		) );

		$items->top_info_defined = $items->contact_info_defined || $items->show_header_social_icons || $items->secondary_nav;

		$items->two_info_panels = $items->contact_info_defined && ( $items->show_header_social_icons || $items->secondary_nav );

		return $items;
	}
}

function dhm_rrcfest_activate_features(){
	/* activate shortcodes */
	require_once( get_template_directory() . '/epanel/shortcodes/shortcodes.php' );
}
add_action( 'init', 'dhm_rrcfest_activate_features' );

require_once( get_template_directory() . '/dhm-pagebuilder/dhm-pagebuilder.php' );

function dhm_rrcfest_sidebar_class( $classes ) {
	if ( dhm_pb_is_pagebuilder_used( get_the_ID() ) )
		$classes[] = 'dhm_pb_pagebuilder_layout';

	if ( is_single() || is_page() || ( class_exists( 'woocommerce' ) && is_product() ) )
		$page_layout = '' !== ( $layout = get_post_meta( get_the_ID(), '_dhm_pb_page_layout', true ) )
			? $layout
			: 'dhm_right_sidebar';

	if ( class_exists( 'woocommerce' ) && ( is_shop() || is_product() || is_product_category() || is_product_tag() ) ) {
		if ( is_shop() || is_tax() )
			$classes[] = dhm_get_option( 'rrcfest_shop_page_sidebar', 'dhm_right_sidebar' );
		if ( is_product() )
			$classes[] = $page_layout;
	}

	else if ( is_archive() || is_home() || is_search() || is_404() ) {
		$classes[] = 'dhm_right_sidebar';
	}

	else if ( is_singular( 'project' ) ) {
		if ( 'dhm_full_width_page' === $page_layout )
			$page_layout = 'dhm_right_sidebar dhm_full_width_portfolio_page';

		$classes[] = $page_layout;
	}

	else if ( is_single() || is_page() ) {
		$classes[] = $page_layout;
	}

	return $classes;
}
add_filter( 'body_class', 'dhm_rrcfest_sidebar_class' );

/**
 * Custom body classes for handling customizer preview screen
 * @return array
 */
function dhm_rrcfest_customize_preview_class( $classes ) {
	if ( is_customize_preview() ) {
		// Search icon state
		if ( ! dhm_get_option( 'show_search_icon', true ) ) {
			$classes[] = 'dhm_hide_search_icon';
		}
	}

	return $classes;
}
add_filter( 'body_class', 'dhm_rrcfest_customize_preview_class' );

function dhm_modify_shop_page_columns_num( $columns_num ) {
	if ( class_exists( 'woocommerce' ) && is_shop() ) {
		$columns_num = 'dhm_full_width_page' !== dhm_get_option( 'rrcfest_shop_page_sidebar', 'dhm_right_sidebar' )
			? 3
			: 4;
	}

	return $columns_num;
}
add_filter( 'loop_shop_columns', 'dhm_modify_shop_page_columns_num' );

// WooCommerce

global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' )
	add_action( 'init', 'dhm_rrcfest_woocommerce_image_dimensions', 1 );

/**
 * Default values for WooCommerce images changed in version 1.3
 * Checks if WooCommerce image dimensions have been updated already.
 */
function dhm_rrcfest_check_woocommerce_images() {
	if ( 'checked' === dhm_get_option( 'rrcfest_1_3_images' ) ) return;

	dhm_rrcfest_woocommerce_image_dimensions();
	dhm_update_option( 'rrcfest_1_3_images', 'checked' );
}
add_action( 'admin_init', 'dhm_rrcfest_check_woocommerce_images' );

function dhm_rrcfest_woocommerce_image_dimensions() {
	$catalog = array(
		'width' 	=> '400',
		'height'	=> '400',
		'crop'		=> 1,
	);

	$single = array(
		'width' 	=> '510',
		'height'	=> '9999',
		'crop'		=> 0,
	);

	$thumbnail = array(
		'width' 	=> '157',
		'height'	=> '157',
		'crop'		=> 1,
	);

	update_option( 'shop_catalog_image_size', $catalog );
	update_option( 'shop_single_image_size', $single );
	update_option( 'shop_thumbnail_image_size', $thumbnail );
}

function woocommerce_template_loop_product_thumbnail() {
	printf( '<span class="dhm_shop_image">%1$s<span class="dhm_overlay"></span></span>',
		woocommerce_get_product_thumbnail()
	);
}

function dhm_review_gravatar_size( $size ) {
	return '80';
}
add_filter( 'woocommerce_review_gravatar_size', 'dhm_review_gravatar_size' );


function dhm_rrcfest_output_content_wrapper() {
	echo '
		<div id="main-content">
			<div class="container">
				<div id="content-area" class="clearfix">
					<div id="left-area">';
}

function dhm_rrcfest_output_content_wrapper_end() {
	echo '</div> <!-- #left-area -->';

	if (
		( is_product() && 'dhm_full_width_page' !== get_post_meta( get_the_ID(), '_dhm_pb_page_layout', true ) )
		||
		( ( is_shop() || is_product_category() || is_product_tag() ) && 'dhm_full_width_page' !== dhm_get_option( 'rrcfest_shop_page_sidebar', 'dhm_right_sidebar' ) )
	) {
		woocommerce_get_sidebar();
	}

	echo '
				</div> <!-- #content-area -->
			</div> <!-- .container -->
		</div> <!-- #main-content -->';
}

function dhm_add_rrcfest_menu() {
	$core_page = add_menu_page( 'Rrcfest', 'Rrcfest', 'switch_themes', 'dhm_rrcfest_options', 'dhm_build_epanel' );

	// Add Theme Options menu only if it's enabled for current user
	if ( dhm_pb_is_allowed( 'theme_options' ) ) {
		add_submenu_page( 'dhm_rrcfest_options', __( 'Theme Options', 'Rrcfest' ), __( 'Theme Options', 'Rrcfest' ), 'manage_options', 'dhm_rrcfest_options' );
	}
	// Add Theme Customizer menu only if it's enabled for current user
	if ( dhm_pb_is_allowed( 'theme_customizer' ) ) {
		add_submenu_page( 'dhm_rrcfest_options', __( 'Theme Customizer', 'Rrcfest' ), __( 'Theme Customizer', 'Rrcfest' ), 'manage_options', 'customize.php?dhm_customizer_option_set=theme' );
	}
	// Add Module Customizer menu only if it's enabled for current user
	if ( dhm_pb_is_allowed( 'module_customizer' ) ) {
		add_submenu_page( 'dhm_rrcfest_options', __( 'Module Customizer', 'Rrcfest' ), __( 'Module Customizer', 'Rrcfest' ), 'manage_options', 'customize.php?dhm_customizer_option_set=module' );
	}
	add_submenu_page( 'dhm_rrcfest_options', __( 'Role Editor', 'Rrcfest' ), __( 'Role Editor', 'Rrcfest' ), 'manage_options', 'dhm_rrcfest_role_editor', 'dhm_pb_display_role_editor' );
	// Add Rrcfest Library menu only if it's enabled for current user
	if ( dhm_pb_is_allowed( 'rrcfest_library' ) ) {
		add_submenu_page( 'dhm_rrcfest_options', __( 'Rrcfest Library', 'Rrcfest' ), __( 'Rrcfest Library', 'Rrcfest' ), 'manage_options', 'edit.php?post_type=dhm_pb_layout' );
	}

	add_action( "load-{$core_page}", 'dhm_pb_check_options_access' ); // load function to check the permissions of current user
	add_action( "admin_print_scripts-{$core_page}", 'dhm_epanel_admin_js' );
	add_action( "admin_head-{$core_page}", 'dhm_epanel_css_admin');
	add_action( "admin_print_scripts-{$core_page}", 'dhm_epanel_media_upload_scripts');
	add_action( "admin_head-{$core_page}", 'dhm_epanel_media_upload_styles');
}
add_action('admin_menu', 'dhm_add_rrcfest_menu');

function add_rrcfest_customizer_admin_menu() {
	if ( ! current_user_can( 'customize' ) ) {
		return;
	}

	global $wp_admin_bar;

	$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	$customize_url = add_query_arg( 'url', urlencode( $current_url ), wp_customize_url() );

	// add Theme Customizer admin menu only if it's enabled for current user
	if ( dhm_pb_is_allowed( 'theme_customizer' ) ) {
		$wp_admin_bar->add_menu( array(
			'parent' => 'appearance',
			'id'     => 'customize-rrcfest-theme',
			'title'  => __( 'Theme Customizer', 'Rrcfest' ),
			'href'   => $customize_url . '&dhm_customizer_option_set=theme',
			'meta'   => array(
				'class' => 'hide-if-no-customize',
			),
		) );
	}

	// add Module Customizer admin menu only if it's enabled for current user
	if ( dhm_pb_is_allowed( 'module_customizer' ) ) {
		$wp_admin_bar->add_menu( array(
			'parent' => 'appearance',
			'id'     => 'customize-rrcfest-module',
			'title'  => __( 'Module Customizer', 'Rrcfest' ),
			'href'   => $customize_url . '&dhm_customizer_option_set=module',
			'meta'   => array(
				'class' => 'hide-if-no-customize',
			),
		) );
	}
	$wp_admin_bar->remove_menu( 'customize' );
}
add_action( 'admin_bar_menu', 'add_rrcfest_customizer_admin_menu', 999 );

function dhm_pb_hide_options_menu() {
	// do nothing if theme options should be displayed in the menu
	if ( dhm_pb_is_allowed( 'theme_options' ) ) {
		return;
	}

	$theme_version = dhm_get_theme_version();

	wp_enqueue_script( 'rrcfest-custom-admin-menu', get_template_directory_uri() . '/js/menu_fix.js', array( 'jquery' ), $theme_version, true );
}
add_action( 'admin_enqueue_scripts', 'dhm_pb_hide_options_menu' );

function dhm_pb_check_options_access() {
	// display wp error screen if theme customizer disabled for current user
	if ( ! dhm_pb_is_allowed( 'theme_options' ) ) {
		wp_die( __( "you don't have sufficient permissions to access this page", 'Rrcfest' ) );
	}
}

/**
 * Allowing blog and portfolio module pagination to work in non-hierarchical singular page.
 * Normally, WP_Query based modules wouldn't work in non-hierarchical single post type page
 * due to canonical redirect to prevent page duplication which could lead to SEO penalty.
 *
 * @see redirect_canonical()
 *
 * @return mixed string|bool
 */
function dhm_modify_canonical_redirect( $redirect_url, $requested_url ) {
	global $post;

	$allowed_shortcodes              = array( 'dhm_pb_blog', 'dhm_pb_portfolio' );
	$is_overwrite_canonical_redirect = false;

	// Look for $allowed_shortcodes in content. Once detected, set $is_overwrite_canonical_redirect to true
	foreach ( $allowed_shortcodes as $shortcode ) {
		if ( has_shortcode( $post->post_content, $shortcode ) ) {
			$is_overwrite_canonical_redirect = true;
			break;
		}
	}

	// Only alter canonical redirect if current page is singular, has paged and $allowed_shortcodes
	if ( is_singular() & ! is_home() && get_query_var( 'paged' ) && $is_overwrite_canonical_redirect ) {
		return $requested_url;
	}

	return $redirect_url;
}
add_filter( 'redirect_canonical', 'dhm_modify_canonical_redirect', 10, 2 );

// Determines how many related products should be displayed on single product page
if ( class_exists( 'WooCommerce' ) ) {
	function woocommerce_output_related_products() {
		$related_posts = 4; // 4 is default number

		if ( is_singular( 'product' ) ) {
			$page_layout = get_post_meta( get_the_ID(), '_dhm_pb_page_layout', true );

			if ( 'dhm_full_width_page' !== $page_layout ) {
				$related_posts = 3; // set to 3 if page has sidebar
			}
		}

		$woocommerce_args = array(
			'posts_per_page' => $related_posts,
			'columns'        => $related_posts,
		);

		woocommerce_related_products( $woocommerce_args );
	}
}

function dhm_rrcfest_maybe_change_frontend_locale( $locale ) {
	$option_name   = 'rrcfest_disable_translations';
	$theme_options = get_option( 'dhm_rrcfest' );

	$disable_translations = isset ( $theme_options[ $option_name ] ) ? $theme_options[ $option_name ] : false;

	if ( 'on' === $disable_translations ) {
		return 'en_US';
	}

	return $locale;
}
add_filter( 'locale', 'dhm_rrcfest_maybe_change_frontend_locale' );

// add datetimepicker for artists plugin page
function add_datePicker() {

	wp_enqueue_script(
		'datePicker', get_template_directory_uri() . '/js/jquery-ui.min.js',
		array( 'jquery' )
	);
	 wp_register_style( 'datepicker_css', get_template_directory_uri() . '/css/jquery-ui.theme.min.css', false, '1.0.0' );
        wp_enqueue_style( 'datepicker_css' );

 
}

add_action( 'admin_enqueue_scripts', 'add_datePicker' );

// add datetimepicker for artists plugin page
function add_dateTimePicker() {

	wp_enqueue_script(
		'dateTimePicker', get_template_directory_uri() . '/js/jquery.timepicker.min.js',
		array( 'jquery' )
	);
	 wp_register_style( 'datetimepicker_css', get_template_directory_uri() . '/css/jquery.timepicker.css', false, '1.0.0' );
        wp_enqueue_style( 'datetimepicker_css' );

 
}
add_action( 'admin_enqueue_scripts', 'add_dateTimePicker' );

// add underscore and backbone
function add_backbone_underscore(){
	wp_enqueue_script( 'underscore' );

	wp_enqueue_script( 'backbone' );
}

add_action( 'wp_enqueue_scripts', 'add_backbone_underscore' );

// backstretch.js
function add_backstretch(){
	wp_enqueue_script( 'backstretch', get_template_directory_uri() . '/js/backstretch.js', array( 'jquery' ), true );
}

add_action( 'wp_enqueue_scripts', 'add_backstretch' );

// import bootstrap

function add_custom_rrcf_styles() {
    wp_register_style( 'rrcf-styles', get_template_directory_uri() . '/css/rrcf-styles.less.min.css');
     wp_enqueue_style( 'rrcf-styles' );
}
add_action( 'wp_enqueue_scripts', 'add_custom_rrcf_styles' );

/**
 * FAQ Page
 */

add_action('admin_menu', 'faq_menu');

function faq_menu() {
	add_dashboard_page('FAQ', 'FAQ', 'read', 'faq', 'faq_data');
}

function faq_data(){
	include_once(get_template_directory() . '/views/admin_faq.php');
}