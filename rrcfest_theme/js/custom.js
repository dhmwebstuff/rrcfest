(function($){
	var $dhm_pb_post_fullwidth = $( '.single.dhm_pb_pagebuilder_layout.dhm_full_width_page' ),
		dhm_is_mobile_device = navigator.userAgent.match( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/ ),
		dhm_is_ipad = navigator.userAgent.match( /iPad/ ),
		$dhm_container = $( '.container' ),
		dhm_container_width = $dhm_container.width(),
		dhm_is_fixed_nav = $( 'body' ).hasClass( 'dhm_fixed_nav' ),
		dhm_is_vertical_fixed_nav = $( 'body' ).hasClass( 'dhm_vertical_fixed' ),
		dhm_is_rtl = $( 'body' ).hasClass( 'rtl' ),
		dhm_hide_nav = $( 'body' ).hasClass( 'dhm_hide_nav' ),
		dhm_header_style_left = $( 'body' ).hasClass( 'dhm_header_style_left' ),
		dhm_vertical_navigation = $( 'body' ).hasClass( 'dhm_vertical_nav' ),
		$top_header = $('#top-header'),
		$main_header = $('#main-header'),
		$main_container_wrapper = $( '#page-container' ),
		$dhm_transparent_nav = $( '.dhm_transparent_nav' ),
		$dhm_main_content_first_row = $( '#main-content .container:first-child' ),
		$dhm_main_content_first_row_meta_wrapper = $dhm_main_content_first_row.find('.dhm_post_meta_wrapper:first'),
		$dhm_main_content_first_row_meta_wrapper_title = $dhm_main_content_first_row_meta_wrapper.find( 'h1.entry-title' ),
		$dhm_main_content_first_row_content = $dhm_main_content_first_row.find('.entry-content:first'),
		$dhm_single_post = $( 'body.single-post' ),
		$dhm_window = $(window),
		dhmRecalculateOffset = false,
		dhm_header_height,
		dhm_header_modifier,
		dhm_header_offset,
		dhm_primary_header_top,
		$dhm_vertical_nav = $('.dhm_vertical_nav'),
		$dhm_header_style_split = $('.dhm_header_style_split'),
		$dhm_top_navigation = $('#dhm-top-navigation'),
		$logo = $('#logo'),
		$dhm_pb_first_row = $( 'body.dhm_pb_pagebuilder_layout .dhm_pb_section:first-child' );

	$(document).ready( function(){
		var $dhm_top_menu = $( 'ul.nav' ),
			$dhm_search_icon = $( '#dhm_search_icon' ),
			dhm_parent_menu_longpress_limit = 300,
			dhm_parent_menu_longpress_start,
			dhm_parent_menu_click = true;

		$dhm_top_menu.find( 'li' ).hover( function() {
			if ( ! $(this).closest( 'li.mega-menu' ).length || $(this).hasClass( 'mega-menu' ) ) {
				$(this).addClass( 'dhm-show-dropdown' );
				$(this).removeClass( 'dhm-hover' ).addClass( 'dhm-hover' );
			}
		}, function() {
			var $this_el = $(this);

			$this_el.removeClass( 'dhm-show-dropdown' );

			setTimeout( function() {
				if ( ! $this_el.hasClass( 'dhm-show-dropdown' ) ) {
					$this_el.removeClass( 'dhm-hover' );
				}
			}, 200 );
		} );

		// Dropdown menu adjustment for touch screen
		$dhm_top_menu.find('.menu-item-has-children > a').on( 'touchstart', function(){
			dhm_parent_menu_longpress_start = new Date().getTime();
		} ).on( 'touchend', function(){
			var dhm_parent_menu_longpress_end = new Date().getTime()
			if ( dhm_parent_menu_longpress_end  >= dhm_parent_menu_longpress_start + dhm_parent_menu_longpress_limit ) {
				dhm_parent_menu_click = true;
			} else {
				dhm_parent_menu_click = false;

				// Close sub-menu if toggled
				var $dhm_parent_menu = $(this).parent('li');
				if ( $dhm_parent_menu.hasClass( 'dhm-hover') ) {
					$dhm_parent_menu.trigger( 'mouseleave' );
				} else {
					$dhm_parent_menu.trigger( 'mouseenter' );
				}
			}
			dhm_parent_menu_longpress_start = 0;
		} ).click(function() {
			if ( dhm_parent_menu_click ) {
				return true;
			}

			return false;
		} );

		$dhm_top_menu.find( 'li.mega-menu' ).each(function(){
			var $li_mega_menu           = $(this),
				$li_mega_menu_item      = $li_mega_menu.children( 'ul' ).children( 'li' ),
				li_mega_menu_item_count = $li_mega_menu_item.length;

			if ( li_mega_menu_item_count < 4 ) {
				$li_mega_menu.addClass( 'mega-menu-parent mega-menu-parent-' + li_mega_menu_item_count );
			}
		});

		if ( $dhm_header_style_split.length && $dhm_vertical_nav.length < 1 ) {
			function dhm_header_menu_split(){
				var $logo_container = $( '#main-header > .container > .logo_container' ),
					$logo_container_splitted = $('.centered-inline-logo-wrap > .logo_container'),
					dhm_top_navigation_li_size = $dhm_top_navigation.children('nav').children('ul').children('li').size(),
					dhm_top_navigation_li_break_index = Math.round( dhm_top_navigation_li_size / 2 ) - 1;

				if ( $dhm_window.width() > 980 && $logo_container.length ) {
					$('<li class="centered-inline-logo-wrap"></li>').insertAfter($dhm_top_navigation.find('nav > ul >li:nth('+dhm_top_navigation_li_break_index+')') );
					$logo_container.appendTo( $dhm_top_navigation.find('.centered-inline-logo-wrap') );
				}

				if ( $dhm_window.width() <= 980 && $logo_container_splitted.length ) {
					$logo_container_splitted.prependTo('#main-header > .container');
					$('#main-header .centered-inline-logo-wrap').remove();
				}
			}
			dhm_header_menu_split();

			$(window).resize(function(){
				dhm_header_menu_split();
			});
		}

		if ( $('ul.dhm_disable_top_tier').length ) {
			$("ul.dhm_disable_top_tier > li > ul").prev('a').attr('href','#');
		}

		if ( $( '.dhm_vertical_nav' ).length ) {
			if ( $( '#main-header' ).height() < $( '#dhm-top-navigation' ).height() ) {
				$( '#main-header' ).height( $( '#dhm-top-navigation' ).height() + $( '#logo' ).height() + 100 );
			}
		}

		window.dhm_calculate_header_values = function() {
			var $top_header = $( '#top-header' ),
				secondary_nav_height = $top_header.length && $top_header.is( ':visible' ) ? $top_header.innerHeight() : 0,
				admin_bar_height     = $( '#wpadminbar' ).length ? $( '#wpadminbar' ).innerHeight() : 0;

			dhm_header_height      = $( '#main-header' ).innerHeight() + secondary_nav_height,
			dhm_header_modifier    = dhm_header_height <= 90 ? dhm_header_height - 29 : dhm_header_height - 56,
			dhm_header_offset      = dhm_header_modifier + admin_bar_height;

			dhm_primary_header_top = secondary_nav_height + admin_bar_height;
		}

		var $comment_form = $('#commentform');

		dhm_pb_form_placeholders_init( $comment_form );

		$comment_form.submit(function(){
			dhm_pb_remove_placeholder_text( $comment_form );
		});

		dhm_duplicate_menu( $('#dhm-top-navigation ul.nav'), $('#dhm-top-navigation .mobile_nav'), 'mobile_menu', 'dhm_mobile_menu' );

		if ( $( '#dhm-secondary-nav' ).length ) {
			$('#dhm-top-navigation #mobile_menu').append( $( '#dhm-secondary-nav' ).clone().html() );
		}

		function dhm_change_primary_nav_position( delay ) {
			setTimeout( function() {
				var $body = $('body'),
					$wpadminbar = $( '#wpadminbar' ),
					$top_header = $( '#top-header' ),
					dhm_primary_header_top = 0;

				if ( $wpadminbar.length ) {
					dhm_primary_header_top += $wpadminbar.innerHeight();
				}

				if ( $top_header.length ) {
					dhm_primary_header_top += $top_header.innerHeight();
				}

				if ( ! $body.hasClass( 'dhm_vertical_nav' ) && ( $body.hasClass( 'dhm_fixed_nav' ) ) ) {
					$('#main-header').css( 'top', dhm_primary_header_top );
				}
			}, delay );
		}

		function dhm_hide_nav_transofrm( ) {
			var $body = $( 'body' ),
				$body_height = $( document ).height(),
				$viewport_height = $( window ).height() + dhm_header_height + 200;

			if ( $body.hasClass( 'dhm_hide_nav' ) ||  $body.hasClass( 'dhm_hide_nav_disabled' ) && ( $body.hasClass( 'dhm_fixed_nav' ) ) ) {
				if ( $body_height > $viewport_height ) {
					if ( $body.hasClass( 'dhm_hide_nav_disabled' ) ) {
						$body.addClass( 'dhm_hide_nav' );
						$body.removeClass( 'dhm_hide_nav_disabled' );
					}
					$('#main-header').css( 'transform', 'translateY(-' + dhm_header_height +'px)' );
					$('#top-header').css( 'transform', 'translateY(-' + dhm_header_height +'px)' );
				} else {
					$('#main-header').css( { 'transform': 'translateY(0)', 'opacity': '1' } );
					$('#top-header').css( { 'transform': 'translateY(0)', 'opacity': '1' } );
					$body.removeClass( 'dhm_hide_nav' );
					$body.addClass( 'dhm_hide_nav_disabled' );
				}
			}
		}

		function dhm_fix_page_container_position(){
			var dhm_window_width     = $dhm_window.width(),
				$top_header          = $( '#top-header' ),
				secondary_nav_height = $top_header.length && $top_header.is( ':visible' ) ? $top_header.innerHeight() : 0;

			// Set data-height-onload for header if the page is loaded on large screen
			// If the page is loaded from small screen, rely on data-height-onload printed on the markup,
			// prevent window resizing issue from small to large
			if ( dhm_window_width > 980 && ! $main_header.attr( 'data-height-loaded' ) ){
				$main_header.attr({ 'data-height-onload' : $main_header.height(), 'data-height-loaded' : true });
			}

			// Use on page load calculation for large screen. Use on the fly calculation for small screen (980px below)
			if ( dhm_window_width <= 980 ) {
				var header_height = $main_header.innerHeight() + secondary_nav_height - 1;

				// If transparent is detected, #main-content .container's padding-top needs to be added to header_height
				// And NOT a pagebuilder page
				if ( $dhm_transparent_nav.length && ! $dhm_pb_first_row.length ) {
					header_height += 58;
				}
			} else {

				// Get header height from header attribute
				var header_height = parseInt( $main_header.attr( 'data-height-onload' ) ) + secondary_nav_height;

				// Non page builder page needs to be added by #main-content .container's fixed height
				if ( $dhm_transparent_nav.length && ! $dhm_vertical_nav.length && $dhm_main_content_first_row.length ) {
					header_height += 58;
				}
			}

			// Specific adjustment required for transparent nav + not vertical nav
			if ( $dhm_transparent_nav.length && ! $dhm_vertical_nav.length ){

				// Add class for first row for custom section padding purpose
				$dhm_pb_first_row.addClass( 'dhm_pb_section_first' );

				// List of conditionals
				var is_pb                            = $dhm_pb_first_row.length,
					is_post_pb                       = is_pb && $dhm_single_post.length,
					is_post_pb_full_layout_has_title = $dhm_pb_post_fullwidth.length && $dhm_main_content_first_row_meta_wrapper_title.length,
					is_post_pb_full_layout_no_title  = $dhm_pb_post_fullwidth.length && 0 === $dhm_main_content_first_row_meta_wrapper_title.length,
					is_pb_fullwidth_section_first    = $dhm_pb_first_row.is( '.dhm_pb_fullwidth_section' ),
					is_no_pb_mobile                  = dhm_window_width <= 980 && $dhm_main_content_first_row.length;

				if ( is_post_pb && ! ( is_post_pb_full_layout_no_title && is_pb_fullwidth_section_first ) ) {

					/* Desktop / Mobile + Single Post */

					/*
					 * EXCEPT for fullwidth layout + fullwidth section ( at the first row ).
					 * It is basically the same as page + fullwidth section with few quirk.
					 * Instead of duplicating the conditional for each module, it'll be simpler to negate
					 * fullwidth layout + fullwidth section in is_post_pb and rely it to is_pb_fullwidth_section_first
					 */

					// Remove main content's inline padding to styling to prevent looping padding-top calculation
					$dhm_main_content_first_row.css({ 'paddingTop' : '' });

					if ( dhm_window_width < 980 ) {
						header_height += 40;
					}

					if ( is_pb_fullwidth_section_first ) {
						// If the first section is fullwidth, restore the padding-top modified area at first section
						$dhm_pb_first_row.css({
							'paddingTop' : '0',
						});
					}

					if ( is_post_pb_full_layout_has_title ) {

						// Add header height to post meta wrapper as padding top
						$dhm_main_content_first_row_meta_wrapper.css({
							'paddingTop' : header_height
						});

					} else if ( is_post_pb_full_layout_no_title ) {

						$dhm_pb_first_row.css({
							'paddingTop' : header_height
						});

					} else {

						// Add header height to first row content as padding top
						$dhm_main_content_first_row.css({
							'paddingTop' : header_height
						});

					}

				} else if ( is_pb_fullwidth_section_first ){

					/* Desktop / Mobile + Pagebuilder + Fullwidth Section */

					var $dhm_pb_first_row_first_module = $dhm_pb_first_row.children( '.dhm_pb_module:first' );

					// Quirks: If this is post with fullwidth layout + no title + fullwidth section at first row,
					// Remove the added height at line 2656
					if ( is_post_pb_full_layout_no_title && is_pb_fullwidth_section_first && dhm_window_width > 980 ) {
						header_height = header_height - 58;
					}

					if ( $dhm_pb_first_row_first_module.is( '.dhm_pb_slider' ) ) {

						/* Desktop / Mobile + Pagebuilder + Fullwidth slider */

						var $dhm_pb_first_row_first_module_slide_image 		= $dhm_pb_first_row_first_module.find( '.dhm_pb_slide_image' ),
							$dhm_pb_first_row_first_module_slide 				= $dhm_pb_first_row_first_module.find( '.dhm_pb_slide' ),
							$dhm_pb_first_row_first_module_slide_container 	= $dhm_pb_first_row_first_module.find( '.dhm_pb_slide .dhm_pb_container' ),
							dhm_pb_slide_image_margin_top 		= 0 - ( parseInt( $dhm_pb_first_row_first_module_slide_image.height() ) / 2 ),
							dhm_pb_slide_container_height 		= 0,
							$dhm_pb_first_row_first_module_slider_arrow 		= $dhm_pb_first_row_first_module.find( '.dhm-pb-slider-arrows a'),
							dhm_pb_first_row_slider_arrow_height = $dhm_pb_first_row_first_module_slider_arrow.height();

						// Adding padding top to each slide so the transparency become useful
						$dhm_pb_first_row_first_module_slide.css({
							'paddingTop' : header_height,
						});

						// delete container's min-height
						$dhm_pb_first_row_first_module_slide_container.css({
							'min-height' : ''
						});

						// Adjusting slider's image, considering additional top padding of slideshow
						$dhm_pb_first_row_first_module_slide_image.css({
							'marginTop' : dhm_pb_slide_image_margin_top
						});

						// Adjusting slider's arrow, considering additional top padding of slideshow
						$dhm_pb_first_row_first_module_slider_arrow.css({
							'marginTop' : ( ( header_height / 2 ) - ( dhm_pb_first_row_slider_arrow_height / 2 ) )
						});

						// Looping the slide and get the highest height of slide
						dhm_pb_first_row_slide_container_height_new = 0

						$dhm_pb_first_row_first_module.find( '.dhm_pb_slide' ).each( function(){
							var $dhm_pb_first_row_first_module_slide_item = $(this),
								$dhm_pb_first_row_first_module_slide_container = $dhm_pb_first_row_first_module_slide_item.find( '.dhm_pb_container' );

							// Make sure that the slide is visible to calculate correct height
							$dhm_pb_first_row_first_module_slide_item.show();

							// Remove existing inline css to make sure that it calculates the height
							$dhm_pb_first_row_first_module_slide_container.css({ 'min-height' : '' });

							var dhm_pb_first_row_slide_container_height = $dhm_pb_first_row_first_module_slide_container.innerHeight();

							if ( dhm_pb_first_row_slide_container_height_new < dhm_pb_first_row_slide_container_height ){
								dhm_pb_first_row_slide_container_height_new = dhm_pb_first_row_slide_container_height;
							}

							// Hide the slide back if it isn't active slide
							if ( $dhm_pb_first_row_first_module_slide_item.is( ':not(".dhm-pb-active-slide")' ) ){
								$dhm_pb_first_row_first_module_slide_item.hide();
							}
						});

						// Setting appropriate min-height, considering additional top padding of slideshow
						$dhm_pb_first_row_first_module_slide_container.css({
							'min-height' : dhm_pb_first_row_slide_container_height_new
						});

					} else if ( $dhm_pb_first_row_first_module.is( '.dhm_pb_fullwidth_header' ) ) {

						/* Desktop / Mobile + Pagebuilder + Fullwidth header */

						// Remove existing inline stylesheet to prevent looping padding
						$dhm_pb_first_row_first_module.removeAttr( 'style' );

						// Get paddingTop from stylesheet
						var dhm_pb_first_row_first_module_fullwidth_header_padding_top = parseInt( $dhm_pb_first_row_first_module.css( 'paddingTop' ) );

						// Implement stylesheet's padding-top + header_height
						$dhm_pb_first_row_first_module.css({
							'paddingTop' : ( header_height + dhm_pb_first_row_first_module_fullwidth_header_padding_top )
						} );

					} else if ( $dhm_pb_first_row_first_module.is( '.dhm_pb_fullwidth_portfolio' ) ) {

						/* Desktop / Mobile + Pagebuilder + Fullwidth Portfolio */

						$dhm_pb_first_row_first_module.css({ 'paddingTop' : header_height });

					} else if ( $dhm_pb_first_row_first_module.is( '.dhm_pb_map_container' ) ) {

						/* Desktop / Mobile + Pagebuilder + Fullwidth Map */

						var $dhm_pb_first_row_map = $dhm_pb_first_row_first_module.find( '.dhm_pb_map' );

						// Remove existing inline height to prevent looping height calculation
						$dhm_pb_first_row_map.css({ 'height' : '' });

						// Implement map height + header height
						$dhm_pb_first_row_first_module.find('.dhm_pb_map').css({
							'height' : header_height + parseInt( $dhm_pb_first_row_map.css( 'height' ) )
						});

						// Adding specific class to mark the map as first row section element
						$dhm_pb_first_row_first_module.addClass( 'dhm_beneath_transparent_nav' );

					} else if ( $dhm_pb_first_row_first_module.is( '.dhm_pb_fullwidth_menu' ) ) {

						/* Desktop / Mobile + Pagebuilder + Fullwidth Menu */
						$dhm_pb_first_row_first_module.css({ 'marginTop' : header_height });

					} else if ( $dhm_pb_first_row_first_module.is( '.dhm_pb_fullwidth_code' ) ) {

						/* Desktop / Mobile + Pagebuilder + Fullwidth code */

						var $dhm_pb_first_row_first_module_code = $dhm_pb_first_row_first_module;

						$dhm_pb_first_row_first_module_code.css({ 'paddingTop' : '' });

						var dhm_pb_first_row_first_module_code_padding_top = parseInt( $dhm_pb_first_row_first_module_code.css( 'paddingTop' ) );

						$dhm_pb_first_row_first_module_code.css({
							'paddingTop' : header_height + dhm_pb_first_row_first_module_code_padding_top
						});

					} else if ( $dhm_pb_first_row_first_module.is( '.dhm_pb_post_title' ) ) {

						/* Desktop / Mobile + Pagebuilder + Fullwidth Post Title */

						var $dhm_pb_first_row_first_module_title = $dhm_pb_first_row_first_module;

						$dhm_pb_first_row_first_module_title.css({
							'paddingTop' : header_height + 50
						});
					}

				} else if ( is_pb ) {

					/* Desktop / Mobile + Pagebuilder + Regular section */

					// Remove first row's inline padding top styling to prevent looping padding-top calculation
					$dhm_pb_first_row.css({ 'paddingTop' : '' });

					// Pagebuilder ignores #main-content .container's fixed height and uses its row's padding
					// Anticipate the use of custom section padding.
					dhm_pb_first_row_padding_top = header_height + parseInt( $dhm_pb_first_row.css( 'paddingBottom' ) );

					// Implementing padding-top + header_height
					$dhm_pb_first_row.css({
						'paddingTop' : dhm_pb_first_row_padding_top
					});

				} else if ( is_no_pb_mobile ) {

					// Mobile + not pagebuilder
					$dhm_main_content_first_row.css({
						'paddingTop' : header_height
					});

				} else {

					$('#main-content .container:first-child').css({
						'paddingTop' : header_height
					});

				}

				// Set #page-container's padding-top to zero after inline styling first row's content has been added
				if ( ! $('#dhm_fix_page_container_position').length ){
					$( '<style />', {
						'id' : 'dhm_fix_page_container_position',
						'text' : '#page-container{ padding-top: 0 !important;}'
					}).appendTo('head');
				}

			} else if( dhm_is_fixed_nav ) {

				$main_container_wrapper.css( 'paddingTop', header_height );

			}

			dhm_change_primary_nav_position( 0 );
		}

		$( window ).resize( function(){
			var window_width                = $dhm_window.width(),
				dhm_container_css_width      = $dhm_container.css( 'width' ),
				dhm_container_width_in_pixel = ( typeof dhm_container_css_width !== 'undefined' ) ? dhm_container_css_width.substr( -1, 1 ) !== '%' : '',
				dhm_container_actual_width   = ( dhm_container_width_in_pixel ) ? $dhm_container.width() : ( ( $dhm_container.width() / 100 ) * window_width ), // $dhm_container.width() doesn't recognize pixel or percentage unit. It's our duty to understand what it returns and convert it properly
				containerWidthChanged       = dhm_container_width !== dhm_container_actual_width;

			if ( dhm_is_fixed_nav && containerWidthChanged ) {
				if ( typeof update_page_container_position != 'undefined' ){
					clearTimeout( update_page_container_position );
				}

				var update_page_container_position = setTimeout( function() {
					dhm_fix_page_container_position();
					if ( typeof dhm_fix_fullscreen_section === 'function' ) {
						dhm_fix_fullscreen_section();
					}
				}, 200 );
			}

			if ( dhm_hide_nav ) {
				dhm_hide_nav_transofrm();
			}

			if ( $( '#wpadminbar' ).length && dhm_is_fixed_nav && window_width >= 740 && window_width <= 782 ) {
				dhm_calculate_header_values();

				dhm_change_primary_nav_position( 0 );
			}

			dhm_set_search_form_css();
		} );

		$( window ).ready( function(){
			if ( $.fn.fitVids ) {
				$( '#main-content' ).fitVids( { customSelector: "iframe[src^='http://www.hulu.com'], iframe[src^='http://www.dailymotion.com'], iframe[src^='http://www.funnyordie.com'], iframe[src^='https://embed-ssl.ted.com'], iframe[src^='http://embed.revision3.com'], iframe[src^='https://flickr.com'], iframe[src^='http://blip.tv'], iframe[src^='http://www.collegehumor.com']"} );
			}
		} );

		$( window ).load( function(){
			if ( dhm_is_fixed_nav ) {
				dhm_calculate_header_values();
			}

			dhm_fix_page_container_position();

			if ( dhm_header_style_left && !dhm_vertical_navigation) {
				$logo_width = $( '#logo' ).width();
				if ( dhm_is_rtl ) {
					$dhm_top_navigation.css( 'padding-right', $logo_width + 30 );
				} else {
					$dhm_top_navigation.css( 'padding-left', $logo_width + 30 );
				}
			}

			if ( $('p.demo_store').length ) {
				$('#footer-bottom').css('margin-bottom' , $('p.demo_store').innerHeight());
			}

			if ( $.fn.waypoint ) {
				if ( dhm_is_vertical_fixed_nav ) {
					var $waypoint_selector = $('#main-content');

					$waypoint_selector.waypoint( {
						handler : function( direction ) {
							dhm_fix_logo_transition();

							if ( direction === 'down' ) {
								$('#main-header').addClass( 'dhm-fixed-header' );
							} else {
								$('#main-header').removeClass( 'dhm-fixed-header' );
							}
						}
					} );
				}

				if ( dhm_is_fixed_nav ) {

					if ( $dhm_transparent_nav.length && ! $dhm_vertical_nav.length && $dhm_pb_first_row.length ){

						// Fullscreen section at the first row requires specific adjustment
						if ( $dhm_pb_first_row.is( '.dhm_pb_fullwidth_section' ) ){
							var $waypoint_selector = $dhm_pb_first_row.children('.dhm_pb_module');
						} else {
							var $waypoint_selector = $dhm_pb_first_row.find('.dhm_pb_row');
						}
					} else if ( $dhm_transparent_nav.length && ! $dhm_vertical_nav.length && $dhm_main_content_first_row.length ) {
						var $waypoint_selector = $('#content-area');
					} else {
						var $waypoint_selector = $('#main-content');
					}

					$waypoint_selector.waypoint( {
						offset: function() {
							if ( dhmRecalculateOffset ) {
								setTimeout( function() {
									dhm_calculate_header_values();
								}, 200 );

								dhmRecalculateOffset = false;
							}
							if ( dhm_hide_nav ) {
								return dhm_header_offset - dhm_header_height - 200;
							} else {

								// Transparent nav modification: #page-container's offset is set to 0. Modify dhm_header_offset's according to header height
								var waypoint_selector_offset = $waypoint_selector.offset();

								if ( waypoint_selector_offset.top < dhm_header_offset ) {
									dhm_header_offset = 0 - ( dhm_header_offset - waypoint_selector_offset.top );
								}

								return dhm_header_offset;
							}
						},
						handler : function( direction ) {
							dhm_fix_logo_transition();

							if ( direction === 'down' ) {
								$main_header.addClass( 'dhm-fixed-header' );
								$main_container_wrapper.addClass ( 'dhm-animated-content' );
								$top_header.addClass( 'dhm-fixed-header' );

								if ( ! dhm_hide_nav && ! $dhm_transparent_nav.length && ! $( '.mobile_menu_bar' ).is(':visible') ) {
									var secondary_nav_height = $top_header.height(),
										dhm_is_vertical_nav = $( 'body' ).hasClass( 'dhm_vertical_nav' ),
										$clone_header,
										clone_header_height,
										fix_padding;

									$clone_header = $main_header.clone().addClass( 'dhm-fixed-header, dhm_header_clone' ).css( { 'transition': 'none', 'display' : 'none' } );

									clone_header_height = $clone_header.prependTo( 'body' ).height();

									// Vertical nav doesn't need #page-container margin-top adjustment
									if ( ! dhm_is_vertical_nav ) {
										fix_padding = parseInt( $main_container_wrapper.css( 'padding-top' ) ) - clone_header_height - secondary_nav_height + 1 ;

										$main_container_wrapper.css( 'margin-top', -fix_padding );
									}

									$( '.dhm_header_clone' ).remove();
								}

							} else {
								$main_header.removeClass( 'dhm-fixed-header' );
								$top_header.removeClass( 'dhm-fixed-header' );
								$main_container_wrapper.css( 'margin-top', '-1px' );
							}
							setTimeout( function() {
								dhm_set_search_form_css();
							}, 400 );
						}
					} );
				}

				if ( dhm_hide_nav ) {
					dhm_hide_nav_transofrm();
				}
			}
		} );

		// $( 'a[href*=#]:not([href=#])' ).click( function() {
		// 	var $this_link = $( this ),
		// 		disable_scroll = ( $this_link.closest( '.woocommerce-tabs' ).length && $this_link.closest( '.tabs' ).length ) || $this_link.closest( '.eab-shortcode_calendar-navigation-link' ).length;

		// 	if ( ( location.pathname.replace( /^\//,'' ) == this.pathname.replace( /^\//,'' ) && location.hostname == this.hostname ) && ! disable_scroll ) {
		// 		var target = $( this.hash );
		// 		target = target.length ? target : $( '[name=' + this.hash.slice(1) +']' );
		// 		if ( target.length ) {

		// 			dhm_pb_smooth_scroll( target, false, 800 );

		// 			if ( ! $( '#main-header' ).hasClass( 'dhm-fixed-header' ) && $( 'body' ).hasClass( 'dhm_fixed_nav' ) && $( window ).width() > 980 ) {
		// 				setTimeout(function(){
		// 					dhm_pb_smooth_scroll( target, false, 40, 'linear' );
		// 				}, 780 );
		// 			}

		// 			return false;
		// 		}
		// 	}
		// });

		if ( $( '.dhm_pb_section' ).length > 1 && $( '.dhm_pb_side_nav_page' ).length ) {
			var $i=0;

			$( '#main-content' ).append( '<ul class="dhm_pb_side_nav"></ul>' );

			$( '.dhm_pb_section' ).each( function(){
				if ( $( this ).height() > 0 ) {
					$active_class = $i == 0 ? 'active' : '';
					$( '.dhm_pb_side_nav' ).append( '<li class="side_nav_item"><a href="#" id="side_nav_item_id_' + $i + '" class= "' + $active_class + '">' + $i + '</a></li>' );
					$( this ).addClass( 'dhm_pb_scroll_' + $i );
					$i++;
				}
			});

			$side_nav_offset = ( $i * 20 + 40 ) / 2;
			$( 'ul.dhm_pb_side_nav' ).css( 'marginTop', '-' + parseInt( $side_nav_offset) + 'px' );
			$( '.dhm_pb_side_nav' ).addClass( 'dhm-visible' );


			$( '.dhm_pb_side_nav a' ).click( function(){
				$top_section =  ( $( this ).text() == "0" ) ? true : false;
				$target = $( '.dhm_pb_scroll_' + $( this ).text() );

				dhm_pb_smooth_scroll( $target, $top_section, 800);

				if ( ! $( '#main-header' ).hasClass( 'dhm-fixed-header' ) && $( 'body' ).hasClass( 'dhm_fixed_nav' ) && $( window ).width() > 980 ) {
					setTimeout(function(){
						 dhm_pb_smooth_scroll( $target, $top_section, 200);
					},500);
				}

				return false;
			});

			$( window ).scroll( function(){

				$add_offset = ( $( 'body' ).hasClass( 'dhm_fixed_nav' ) ) ? 20 : -90;

				if ( $ ( '#wpadminbar' ).length && $( window ).width() > 600 ) {
					$add_offset += $( '#wpadminbar' ).outerHeight();
				}

				$side_offset = ( $( 'body' ).hasClass( 'dhm_vertical_nav' ) ) ? $( '#top-header' ).height() + $add_offset + 60 : $( '#top-header' ).height() + $( '#main-header' ).height() + $add_offset;

				for ( var $i = 0; $i <= $( '.side_nav_item a' ).length - 1; $i++ ) {
					 if ( $( window ).scrollTop() + $( window ).height() == $( document ).height() ) {
						$last = $( '.side_nav_item a' ).length - 1;
						$( '.side_nav_item a' ).removeClass( 'active' );
						$( 'a#side_nav_item_id_' + $last ).addClass( 'active' );
					 } else {
						if ( $( this ).scrollTop() >= $( '.dhm_pb_scroll_' + $i ).offset().top - $side_offset ) {
							$( '.side_nav_item a' ).removeClass( 'active' );
							$( 'a#side_nav_item_id_' + $i ).addClass( 'active' );
						}
					}
				}
			});
		}

		if ( $('.dhm_pb_scroll_top').length ) {
			$(window).scroll(function(){
				if ($(this).scrollTop() > 800) {
					$('.dhm_pb_scroll_top').show().removeClass( 'dhm-hidden' ).addClass( 'dhm-visible' );
				} else {
					$('.dhm_pb_scroll_top').removeClass( 'dhm-visible' ).addClass( 'dhm-hidden' );
				}
			});

			//Click event to scroll to top
			$('.dhm_pb_scroll_top').click(function(){
				$('html, body').animate({scrollTop : 0},800);
			});
		}

		if ( $( '.comment-reply-link' ).length ) {
			$( '.comment-reply-link' ).addClass( 'dhm_pb_button' );
		}

		$( '#dhm_top_search' ).click( function() {
			var $search_container = $( '.dhm_search_form_container' );

			$( '.dhm_menu_container' ).removeClass( 'dhm_pb_menu_visible' ).removeClass( 'dhm_pb_no_animation' ).addClass('dhm_pb_menu_hidden');
			$search_container.removeClass( 'dhm_pb_search_form_hidden' ).removeClass( 'dhm_pb_no_animation' ).addClass('dhm_pb_search_visible');
			setTimeout( function() {
				$( '.dhm_menu_container' ).addClass( 'dhm_pb_no_animation' );
				$search_container.addClass( 'dhm_pb_no_animation' );
			}, 1000);
			$search_container.find( 'input' ).focus();

			dhm_set_search_form_css();
		});

		function dhm_hide_search() {
			$( '.dhm_menu_container' ).removeClass('dhm_pb_menu_hidden').removeClass( 'dhm_pb_no_animation' ).addClass( 'dhm_pb_menu_visible' );
			$( '.dhm_search_form_container' ).removeClass('dhm_pb_search_visible').removeClass( 'dhm_pb_no_animation' ).addClass( 'dhm_pb_search_form_hidden' );
			setTimeout( function() {
				$( '.dhm_menu_container' ).addClass( 'dhm_pb_no_animation' );
				$( '.dhm_search_form_container' ).addClass( 'dhm_pb_no_animation' );
			}, 1000);
		}

		function dhm_set_search_form_css() {
			var $search_container = $( '.dhm_search_form_container' );
			var $body = $( 'body' );
			if ( $search_container.hasClass( 'dhm_pb_search_visible' ) ) {
				var header_height = $( '#main-header' ).innerHeight(),
					menu_width = $( '#top-menu' ).width(),
					font_size = $( '#top-menu li a' ).css( 'font-size' );
				$search_container.css( { 'height' : header_height + 'px' } );
				$search_container.find( 'input' ).css( 'font-size', font_size );
				if ( ! $body.hasClass( 'dhm_header_style_left' ) ) {
					$search_container.css( 'max-width', menu_width + 60 );
				} else {
					$search_container.find( 'form' ).css( 'max-width', menu_width + 60 );
				}
			}
		}

		$( '.dhm_close_search_field' ).click( function() {
			dhm_hide_search();
		});

		$( document ).mouseup( function(e) {
			var $header = $( '#main-header' );
			if ( $( '.dhm_menu_container' ).hasClass('dhm_pb_menu_hidden') ) {
				if ( ! $header.is( e.target ) && $header.has( e.target ).length === 0 )	{
					dhm_hide_search();
				}
			}
		});

		// Detect actual logo dimension, used for tricky fixed navigation transition
		function dhm_define_logo_dimension() {
			var $logo = $('#logo'),
				logo_src = $logo.attr( 'src' ),
				is_svg = logo_src.substr( -3, 3 ) === 'svg' ? true : false,
				$logo_wrap,
				logo_width,
				logo_height;

			// Append invisible wrapper at the bottom of the page
			$('body').append( $('<div />', {
				'id' : 'dhm-define-logo-wrap',
				'style' : 'position: fixed; bottom: 0; opacity: 0;'
			}));

			// Define logo wrap
			$logo_wrap = $('#dhm-define-logo-wrap');

			if( is_svg ) {
				$logo_wrap.addClass( 'svg-logo' );
			}

			// Clone logo to invisible wrapper
			$logo_wrap.html( $logo.clone().css({ 'display' : 'block' }).removeAttr( 'id' ) );

			// Get dimension
			logo_width = $logo_wrap.find('img').width();
			logo_height = $logo_wrap.find('img').height();

			// Add data attribute to $logo
			$logo.attr({
				'data-actual-width' : logo_width,
				'data-actual-height' : logo_height
			});

			// Destroy invisible wrapper
			$logo_wrap.remove();

			// Init logo transition onload
			dhm_fix_logo_transition( true );
		}

		if ( $('#logo').length ) {
			dhm_define_logo_dimension();
		}

		// Set width for adsense in footer widget
		$('.footer-widget').each(function(){
			var $footer_widget = $(this),
				footer_widget_width = $footer_widget.width(),
				$adsense_ins = $footer_widget.find( '.widget_adsensewidget ins' );

			if ( $adsense_ins.length ) {
				$adsense_ins.width( footer_widget_width );
			}
		});
	} );

	// Fixing logo size transition in tricky header style
	function dhm_fix_logo_transition( is_onload ) {
		var $body                = $( 'body' ),
			$logo                = $( '#logo' ),
			logo_actual_width    = parseInt( $logo.attr( 'data-actual-width' ) ),
			logo_actual_height   = parseInt( $logo.attr( 'data-actual-height' ) ),
			logo_height_percentage = parseInt( $logo.attr( 'data-height-percentage' ) ),
			$top_nav             = $( '#dhm-top-navigation' ),
			top_nav_height       = parseInt( $top_nav.attr( 'data-height' ) ),
			top_nav_fixed_height = parseInt( $top_nav.attr( 'data-fixed-height' ) ),
			$main_header          = $('#main-header'),
			is_header_split      = $body.hasClass( 'dhm_header_style_split' ),
			is_fixed_nav         = $main_header.hasClass( 'dhm-fixed-header' ),
			is_vertical_nav      = $body.hasClass( 'dhm_vertical_nav' ),
			is_hide_primary_logo = $body.hasClass( 'dhm_hide_primary_logo' ),
			is_hide_fixed_logo   = $body.hasClass( 'dhm_hide_fixed_logo' ),
			is_onload            = typeof is_onload === 'undefined' ? false : is_onload,
			logo_height_base     = is_fixed_nav ? top_nav_height : top_nav_fixed_height,
			logo_wrapper_width,
			logo_wrapper_height;

		// Fix for inline centered logo in horizontal nav
		if ( is_header_split && ! is_vertical_nav ) {
			// On page load, logo_height_base should be top_nav_height
			if ( is_onload ) {
				logo_height_base = top_nav_height;
			}

			// Calculate logo wrapper height
			logo_wrapper_height = ( logo_height_base * ( logo_height_percentage / 100 ) + 22 );

			// Calculate logo wrapper width
			logo_wrapper_width = logo_actual_width * (  logo_wrapper_height / logo_actual_height  );

			// Override logo wrapper width to 0 if it is hidden
			if ( is_hide_primary_logo && ( is_fixed_nav || is_onload ) ) {
				logo_wrapper_width = 0;
			}

			if ( is_hide_fixed_logo && ! is_fixed_nav && ! is_onload ) {
				logo_wrapper_width = 0;
			}

 			// Set fixed width for logo wrapper to force correct dimension
			$( '.dhm_header_style_split .centered-inline-logo-wrap' ).css( { 'width' : logo_wrapper_width } );
		}
	}
})(jQuery)