// EZForm.js
(function ( $ ) {
		
		$.fn.ezForm = function(target){

			var formReturnObject = this;
			var target = (target == "") ? $('body') : target;
			var ezForm = '<div class="ezForm"/>';
			var msgStatus = '<div class="msgStatus"/>';
			var formBlock = '<form id="jsForm" class="formBlock" action="sendform.php"/>';
			var nameBlock = '<div class="nameBlock form-group"/>';
			var nameLabel = '<label for="name">Name: </>';
			var nameInput = '<input type="text" id="name" name="name" class="form-control" required/>';
			var emailBlock = '<div class="emailBlock form-group"/>';
			var emailLabel = '<label for="email">Email: </>';
			var emailInput = '<input type="email" id="email" name="email" class="form-control" required/>';
			var interestBlock = '<div class="insterestBlock form-group"/>';
			var interestLabel = '<label for="interest">Interest: </>';
			var interestInput = '<select id="interest" class="form-control"><option value="Artist performance/booking">Artist performance/booking</option><option value="Sponsorship/investment">Sponsorship/investment</option><option value="Vending">Vending</option><option value="Employment">Employment</option><option value="General">General</option></select>';
			var msgBlock = '<div class="msgBlock form-group"/>';
			var msgLabel = '<label for="message">Message: </>';
			var msgInput = '<textarea name="msg" id="msg" class="form-control" required/>';
			var btnBlock = '<div class="btnBlock"/>';
			var sendBtn = '<button type="submit" class="send btn btn-success btn-block btn-lg">Send</button>'
			var header = 'Please Contact Us'
			var headerBlock = '<div class="headerBlock">' + '<h2>' + header + '</h2>' + '</div>';


			formReturnObject.append(ezForm);
			$('.ezForm').append(msgStatus);
			$('.ezForm').append(headerBlock);
			$('.ezForm').append(formBlock);
			$('.formBlock').append(nameBlock);
			$('.nameBlock').append(nameLabel);
			$('.nameBlock').append(nameInput);
			$('.formBlock').append(emailBlock);
			$('.emailBlock').append(emailLabel);
			$('.emailBlock').append(emailInput);
			$('.formBlock').append(msgBlock);
			$('.msgBlock').append(msgLabel);
			$('.msgBlock').append(msgInput);
			$('.formBlock').append(btnBlock);
			$('.btnBlock').append(sendBtn);

			// console.log($('.ezForm'))
			

			// if (header === undefined) {
			// 	$('.headerBlock').html("<h2>Please Contact Us</h2>");
			// };
			
			return formReturnObject;
		}

	}( jQuery ));
// send form
var $ = jQuery;
$(function() {
		    var form = $('#jsForm');

					// get status msg div
					var formMessages = $('.msgStatus');

					// rest of code

					$(form).submit(function(e){
						e.preventDefault();

						// Serialize the form data.
						var formData = $(form).serialize();

						// submit form via ajax
						$.ajax({
						    type: 'POST',
						    url: $(form).attr('action'),
						    data: formData
						}).done(function(response) {
						    // Make sure that the formMessages div has the 'success' class.
						    $(formMessages).removeClass('alert alert-danger');
						    $(formMessages).addClass('alert alert-success');

						    // Set the message text.
						    $(formMessages).text(response);

						    // Clear the form.
						    $('#name').val('');
						    $('#email').val('');
						    $('#msg').val('');


						    // fade out success message
						    setTimeout(function() {
						    	$('.msgStatus').fadeOut('slow');
						    }, 3000 );

						}).fail(function(data) {
						    // Make sure that the formMessages div has the 'error' class.
						    $(formMessages).removeClass('alert alert-success');
						    $(formMessages).addClass('alert alert-danger');


						    // Set the message text.
						    if (data.responseText !== '') {
						        $(formMessages).text(data.responseText);
						    } else {
						        $(formMessages).text('Oops! An error occured and your message could not be sent.');
						    }
						});
					})

			// if ($('.ezForm').size() > 1) {
			// 	$('.ezForm').eq(1).remove();
			// };
		});