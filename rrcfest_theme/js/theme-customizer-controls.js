(function($){
	$( document ).ready( function() {
		var ET_Select_Image = function(element, options){
			this.element = element;
			this.custom_select_link = null;
			this.custom_dropdown = null;
			this.frontend_customizer = $('body').hasClass( 'dhm_frontend_customizer' ) ? true : false;

			this.options = jQuery.extend({}, this.defaults, options);

			this.create_dropdown();
		}

		if ( typeof window.location.search !== 'undefined' && window.location.search.search( 'dhm_customizer_option_set=module' ) !== -1 ) {
			$( 'body' ).addClass( 'dhm_modules_customizer_option_set' );
		} else {
			$( 'body' ).addClass( 'dhm_theme_customizer_option_set' );
		}

		ET_Select_Image.prototype = {
			defaults: {
				apply_value_to	: 'body'
			},

			create_dropdown: function(){
				var $dhm_select_image_main_select = this.element,
					dhm_filter_options_html = '',
					$selected_option,
					$dropdown_selected_option,
					self = this;

				if ( $dhm_select_image_main_select.length ) {
					$dhm_select_image_main_select.hide().addClass( 'dhm_select_image_main_select' );

					$dhm_select_image_main_select.change( $.proxy( self.change_option, self ) );

					$dhm_select_image_main_select.find( 'option' ).each( function() {
						var $this_option = $(this),
							selected = $(this).is( ':selected' ) ? ' class="dhm_select_image_active"' : '',
							option_class = 0 === $this_option.attr( 'value' ).indexOf( '_' ) ? $this_option.attr( 'value' ) : '_' + $this_option.attr( 'value' );

						dhm_filter_options_html += '<li class="dhm_si' + option_class + '_column" data-value="' + $this_option.attr( 'value' ) + '"' + selected +'>' + $this_option.text() + '</li>';
					} );

					$dhm_select_image_main_select.after( '<a href="#" class="dhm_select_image_custom_select">' + '<span class="dhm_filter_text"></span>' + '</a>' + '<ul class="dhm_select_image_options '+ self.esc_classname( $dhm_select_image_main_select.attr('data-customize-setting-link') ) +'">' + dhm_filter_options_html + '</ul>' );
				}

				this.custom_select_link = $dhm_select_image_main_select.next( '.dhm_select_image_custom_select' );

				this.custom_dropdown = this.custom_select_link.next('.dhm_select_image_options');

				$selected_option = $dhm_select_image_main_select.find( ':selected' );

				if ( $selected_option.length ) {
					var selected_option_class = 0 === $selected_option.attr( 'value' ).indexOf( '_' ) ? $selected_option.attr( 'value' ) : '_' + $selected_option.attr( 'value' );
					this.custom_select_link.find('.dhm_filter_text').text( $selected_option.text() ).addClass( 'dhm_si' + selected_option_class + '_column' );

					$dropdown_selected_option = ( $selected_option.val() == 'none' ) ? this.custom_dropdown.find('li').eq(0) : this.custom_dropdown.find('li[data-value="' + $selected_option.text() + '"]');

					this.custom_select_link.find('.dhm_filter_text').addClass( $dropdown_selected_option.attr('class') ).attr( 'data-si-class', $dropdown_selected_option.attr('class') );

					$dropdown_selected_option.addClass( 'dhm_select_image_active' );
				}

				this.custom_select_link.click( $.proxy( self.open_dropdown, self ) );

				this.custom_dropdown.find('li').click( $.proxy( self.select_option, self ) );
			},

			open_dropdown: function(event) {
				var self = this,
					$this_link = $(event.target);

				if ( self.custom_dropdown.hasClass( 'dhm_select_image_open' ) ) return false;

				self.custom_dropdown.show().addClass( 'dhm_select_image_open' );

				$this_link.hide();

				return false;
			},

			select_option: function(event) {
				var self = this,
					$this_option = $(event.target),
					this_option_value = $this_option.attr('data-value'),
					$main_text = self.custom_select_link.find( '.dhm_filter_text' ),
					$main_select_active_element = self.element.find( 'option[value="' + this_option_value + '"]' );

				if ( $this_option.hasClass( 'dhm_select_image_active' ) ) {
					self.close_dropdown();

					return false;
				}

				$this_option.siblings().removeClass( 'dhm_select_image_active' );

				$main_text.removeClass(function(index, css){
					return (css.match(/\bdhm_si_\S+/g) || []).join(' ')
				});

				$main_text.addClass( $this_option.attr( 'class' ) ).attr( 'data-si-class', $this_option.attr( 'class' ) );

				$this_option.addClass('dhm_select_image_active');

				self.close_dropdown();

				if ( ! $main_select_active_element.length )
					self.element.val( 'none' ).trigger( 'change' );
				else
					self.element.val( this_option_value ).trigger( 'change' );

				return false;
			},

			close_dropdown: function() {
				this.custom_select_link.find( '.dhm_filter_text' ).show();
				this.custom_dropdown.hide().removeClass( 'dhm_select_image_open' );
			},

			change_option: function() {
				var self = this,
					$active_option = self.element.find('option:selected'),
					active_option_value = $active_option.val(),
					$this_option = this.custom_dropdown.find('li[data-value="' + active_option_value + '"]'),
					$main_text = self.custom_select_link.find( '.dhm_filter_text' ),
					main_text_si_class = $main_text.attr( 'data-si-class' );

				// set correct custom dropdown values on first load
				if ( this.custom_dropdown.find('li.dhm_select_image_active').data( 'value' ) !== active_option_value ) {
					this.custom_dropdown.find('li').removeClass( 'dhm_select_image_active' );
					$main_text.removeClass( main_text_si_class ).addClass( $this_option.attr( 'class' ) ).attr( 'data-si-class', $this_option.attr( 'class' ) );

					$this_option.addClass('dhm_select_image_active');
				}
			},

			esc_classname: function( option_value ) {
				return 'dhm_si_' + option_value.replace(/[ +\/\[\]]/g,'_').toLowerCase();
			}
		}

		$.fn.dhm_select_image = function(options){
			new ET_Select_Image(this, options)
			return this;
		};

		$('select[data-customize-setting-link="dhm_rrcfest[footer_columns]"]').dhm_select_image({ apply_value_to: 'body' });

		$( '.dhm_rrcfest_reset_slider' ).click( function () {
			var $this_input = $( this ).closest( 'label' ).find( 'input' ),
				input_name = $this_input.data( 'customize-setting-link' ),
				input_default = $this_input.data( 'reset_value' );

			$this_input.val( input_default );
			$this_input.change();
		});

		$( '#accordion-section-dhm_rrcfest_mobile_tablet h3, #accordion-panel-dhm_rrcfest_mobile h3' ).click( function () {
			var $iframe_preview = $( '#customize-preview' );
				$iframe_preview.removeClass( 'dhm_rrcfest_phone dhm_rrcfest_tablet' );
				$iframe_preview.addClass( 'dhm_rrcfest_tablet' );
		});

		$( '#accordion-section-dhm_rrcfest_mobile_phone h3, #accordion-section-dhm_rrcfest_mobile_menu h3' ).click( function () {
			var $iframe_preview = $( '#customize-preview' );
				$iframe_preview.removeClass( 'dhm_rrcfest_phone dhm_rrcfest_tablet' );
				$iframe_preview.addClass( 'dhm_rrcfest_phone' );
		});

		$( '.control-panel-back, .customize-panel-back' ).click( function () {
			var $iframe_preview = $( '#customize-preview' );
				$iframe_preview.removeClass( 'dhm_rrcfest_phone dhm_rrcfest_tablet' );
		});

		$( 'input[type=range]' ).on( 'mousedown', function() {
			var $range = $(this),
				$range_input = $range.parent().children( '.dhm-pb-range-input' );

			value = $( this ).attr( 'value' );
			$range_input.val( value );

			$( this ).mousemove(function() {
				value = $( this ).attr( 'value' );
				$range_input.val( value );
			});
		});

		var dhm_range_input_number_timeout;

		function dhm_autocorrect_range_input_number( input_number, timeout ) {
			$range_input = input_number,
			$range       = $range_input.parent().find('input[type="range"]'),
			value        = parseFloat( $range_input.val() ),
			reset        = parseFloat( $range.attr('data-reset_value') ),
			step         = parseFloat( $range_input.attr('step') ),
			min          = parseFloat( $range_input.attr('min') ),
			max          = parseFloat( $range_input.attr('max') );

			clearTimeout( dhm_range_input_number_timeout );

			dhm_range_input_number_timeout = setTimeout( function() {
				if ( isNaN( value ) ) {
					$range_input.val( reset );
					$range.val( reset ).trigger( 'change' );
					return;
				}

				if ( step >= 1 && value % 1 !== 0 ) {
					value = Math.round( value );
					$range_input.val( value );
					$range.val( value );
				}

				if ( value > max ) {
					$range_input.val( max );
					$range.val( max ).trigger( 'change' );
				}

				if ( value < min ) {
					$range_input.val( min );
					$range.val( min ).trigger( 'change' );
				}
			}, timeout );

			$range.val( value ).trigger( 'change' );
		}

		$('input.dhm-pb-range-input').on( 'change keyup', function() {
			dhm_autocorrect_range_input_number( $(this), 1000 );
		}).on( 'focusout', function() {
			dhm_autocorrect_range_input_number( $(this), 0 );
		});

		$('input.dhm_font_style_checkbox[type=checkbox]').live('change', function(){
			var $this_el      = $(this),
				$main_option  = $this_el.closest( 'span' ).siblings( 'input.dhm_font_styles' ),
				value         = $this_el.val(),
				current_value = $main_option.val(),
				values        = ( current_value != 'false' ) ? current_value.split( '|' ) : [],
				query         = $.inArray( value, values ),
				result        = '';

			if ( $this_el.prop('checked' ) == true ) {

				if ( current_value.length ) {

					if ( query < 0 ) {
						values.push( value );

						result = values.join( '|' );
					}
				} else {
					result = value;
				}
			} else {

				if ( current_value.length !== 0 ) {

					if ( query >= 0 ) {
						values.splice( query, 1 );

						result = values.join( '|' );
					} else {
						result = current_value;
					}
				}
			}

			$main_option.val( result ).trigger( 'change' );
		});

		$( 'span.dhm_font_style' ).click( function() {
			var style_checkbox = $( this ).find( 'input' );

			$( this ).toggleClass( 'dhm_font_style_checked' );

			if ( style_checkbox.is( ':checked' ) ) {
				style_checkbox.prop( 'checked', false );
			} else {
				style_checkbox.prop( 'checked', true );
			}

			style_checkbox.change();
		});

		var $vertical_nav_input                   = $( '#customize-control-dhm_rrcfest-vertical_nav input[type=checkbox]' ),
			$nav_fullwidth_control                = $( '#customize-control-dhm_rrcfest-nav_fullwidth' ),
			$hide_navigation_until_scroll_control = $('#customize-control-dhm_rrcfest-hide_nav');

		if ( $vertical_nav_input.is( ':checked') ) {
			$nav_fullwidth_control.hide();
			$hide_navigation_until_scroll_control.hide();
		} else {
			$nav_fullwidth_control.show();
			$hide_navigation_until_scroll_control.show();
		}

		$('#customize-theme-controls').on( 'change', '#customize-control-dhm_rrcfest-vertical_nav input[type=checkbox]', function(){
			$input = $(this);

			if ( $input.is(':checked') ) {
				$nav_fullwidth_control.hide();
				$hide_navigation_until_scroll_control.hide();
			} else {
				$nav_fullwidth_control.show();
				$hide_navigation_until_scroll_control.show();
			}
		});

		function toggle_sidebar_width_control() {
			$checkbox              = $('#customize-control-dhm_rrcfest-use_sidebar_width input[type="checkbox"]'),
			$sidebar_width_control = $( '#customize-control-dhm_rrcfest-sidebar_width' );

			if ( $checkbox.is( ':checked' ) ) {
				$sidebar_width_control.fadeIn();
			} else {
				$sidebar_width_control.fadeOut();
			}
		}

		toggle_sidebar_width_control();

		$('#customize-theme-controls').on( 'change', '#customize-control-dhm_rrcfest-use_sidebar_width input[type=checkbox]', function(){
			toggle_sidebar_width_control();
		});

	});

	var api = wp.customize;

	api.ET_ColorAlphaControl = api.Control.extend({
		ready: function() {
			var control = this,
				picker = control.container.find('.color-picker-hex');

			picker.val( control.setting() ).wpColorPicker({
				change: function() {
					control.setting.set( picker.wpColorPicker('color') );
				},
				clear: function() {
					control.setting.set( false );
				}
			});

			control.setting.bind( function( value ) {
				picker.val( value );
				picker.wpColorPicker( 'color', value );
			});

			/**
			* Adding following event whenever footer_menu_text_color is changed, due to its relationship with footer_menu_active_link_color.
			*/
			if ( 'dhm_rrcfest[footer_menu_text_color]' === this.id ) {

				// Whenever user change footer_menu_text_color, do the following
				this.setting.bind( 'change', function( value ){

					// Set footer_menu_active_link_color equal to the newly changed footer_menu_text_color
					api( 'dhm_rrcfest[footer_menu_active_link_color]' ).set( value );

					// Update default color of footer_menu_active_link_color equal to the newly changed footer_menu_text_color.
					// If afterward user change the color and not happy with it, they can click reset and back to footer_menu_text_color color
					api.control( 'dhm_rrcfest[footer_menu_active_link_color]' ).container.find( '.color-picker-hex' )
						.data( 'data-default-color', value )
						.wpColorPicker({ 'defaultColor' : value, 'color' : value });
				});
			}
		}
	});

	$( 'body' ).on( 'click', '.dhm_font_icon li', function() {
		var $this_el = $( this ),
			$this_input;

		if ( ! $this_el.hasClass( 'active' ) ) {
			$( '.dhm_font_icon li' ).removeClass( 'dhm_active' );
			$this_el.addClass( 'dhm_active' );

			$this_input = $this_el.closest( 'label' ).find( '.dhm_selected_icon' );
			$this_input.val( $this_el.data( 'icon' ) );
			$this_input.change();
		}
	});

	api.controlConstructor['dhm_coloralpha'] = api.ET_ColorAlphaControl;

	$( window ).load( function() {
		if ( $( '#accordion-section-dhm_rrcfest_buttons' ).length ) {
			var $icon_options_trigger = $( '#customize-control-dhm_rrcfest-all_buttons_icon select' ),
				icon_options_trigger_val = $icon_options_trigger.val();

			trigger_button_options( icon_options_trigger_val );

			$icon_options_trigger.change( function() {
				icon_options_trigger_val = $( this ).val();
				trigger_button_options( icon_options_trigger_val );
			});
		}

		function trigger_button_options( trigger_val ) {
			var icon_options_set = [ 'all_buttons_icon_color', 'all_buttons_icon_placement', 'all_buttons_icon_hover', 'all_buttons_selected_icon' ];

			$.each( icon_options_set, function( i, option_name ) {
				if ( 'yes' === trigger_val ) {
					$( '#customize-control-dhm_rrcfest-' + option_name ).show();
				} else {
					$( '#customize-control-dhm_rrcfest-' + option_name ).hide();
				}
			} );
		}

		if ( $( '.dhm_font_icon' ).length ) {
			$( '.dhm_font_icon' ).each( function(){
				var $this_el = $( this ),
					this_input_val = $this_el.closest( 'label' ).find( '.dhm_selected_icon' ).val();
				$this_el.find( 'li[data-icon="' + this_input_val + '"]').addClass( 'dhm_active' );
			});
		}

	} );

})(jQuery)
