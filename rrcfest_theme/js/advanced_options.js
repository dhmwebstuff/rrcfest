(function($){
	$(document).ready( function(){
		var $save_message = $("#epanel-ajax-saving"),
			$save_message_spinner = $save_message.children("img"),
			$save_message_description = $save_message.children("span");

		$("#dhm_aweber_connection .dhm_make_connection").on( "click", function( event ) {
			event.preventDefault();

			$.ajax({
				type: "POST",
				url: ajaxurl,
				data: {
					action : "dhm_aweber_submit_authorization_code",
					dhm_load_nonce : dhm_advanced_options.dhm_load_nonce,
					dhm_authorization_code : $("#dhm_aweber_authorization #dhm_aweber_authentication_code").val()
				},
				beforeSend: function ( xhr ){
					$( '#dhm_aweber_connection .dhm_result_error' ).remove();

					$save_message_spinner.css("display","block");
					$save_message_description.css("margin","6px 0px 0px").html( dhm_advanced_options.aweber_connecting );
					$save_message.fadeIn('fast');
				},
				success: function( response ){
					hide_ajax_popup( response );

					if ( response === 'success' ) {
						$( '#dhm_aweber_authorization' ).hide();

						$( '#dhm_aweber_remove_connection' ).show();
					} else {
						aweber_show_error_message( response );
					}
				}
			});
		});

		$("#dhm_aweber_connection .dhm_remove_connection").on( "click", function( event ) {
			event.preventDefault();

			$.ajax({
				type: "POST",
				url: ajaxurl,
				data: {
					action : "dhm_aweber_remove_connection",
					dhm_load_nonce : dhm_advanced_options.dhm_load_nonce
				},
				beforeSend: function ( xhr ){
					$save_message.css( { 'width' : '228px' } );
					$save_message_spinner.css("display","block");
					$save_message_description.css("margin","6px 0px 0px").html( dhm_advanced_options.aweber_remove_connection );
					$save_message.fadeIn('fast');
				},
				success: function( response ){
					hide_ajax_popup( response );

					if ( response === 'success' ) {
						$( '#dhm_aweber_remove_connection' ).hide();

						$("#dhm_aweber_authorization #dhm_aweber_authentication_code").val( '' );

						$( '#dhm_aweber_authorization' ).show();
					} else {
						aweber_show_error_message( response );
					}
				}
			});
		});

		function aweber_show_error_message( response ) {
			var error_html = '<div class="dhm_result_error">';

			error_html += '<p><strong>' + dhm_advanced_options.aweber_failed + '</strong>.</p>';
			error_html += '<p>' + response + '</p>';

			error_html += '</div> <!-- .dhm_result_error -->';

			$( '#dhm_aweber_authorization' ).after( error_html );
		}

		function hide_ajax_popup( response ) {
			var error_message = response !== 'success' ? ' with errors' : '';

			$save_message.addClass( 'dhm_aweber_connect_done' );
			$save_message_spinner.css("display","none");
			$save_message_description.css("margin","0px").html( dhm_advanced_options.aweber_done + error_message );

			setTimeout( function() {
				$save_message.fadeOut( "slow", function() {
					$(this).removeClass( 'dhm_aweber_connect_done' );

					$save_message.css( { 'width' : '142px' } );
				} );
			}, 500 );
		}
	});
})(jQuery)