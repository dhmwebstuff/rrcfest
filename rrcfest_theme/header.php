<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php dirtyhands_description(); ?>
	<?php dirtyhands_keywords(); ?>
	<?php dirtyhands_canonical(); ?>

	<?php do_action( 'dhm_head_meta' ); ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php $template_directory_uri = get_template_directory_uri(); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
	<![endif]-->

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>
	

	<?php wp_head(); ?>
	
	
</head>

<?php 
	$url = explode('/', $_SERVER['REQUEST_URI']);

	$dir = $url[1] ? $url[1] : 'home'; 
?>
<body id="<?php echo $dir ?>" <?php body_class(); ?>>

	<div id="page-container">
<?php
	if ( is_page_template( 'page-template-blank.php' ) ) {
		return;
	}

	$dhm_secondary_nav_items = dhm_rrcfest_get_top_nav_items();

	$dhm_phone_number = $dhm_secondary_nav_items->phone_number;

	$dhm_email = $dhm_secondary_nav_items->email;

	$dhm_contact_info_defined = $dhm_secondary_nav_items->contact_info_defined;

	$show_header_social_icons = $dhm_secondary_nav_items->show_header_social_icons;

	$dhm_secondary_nav = $dhm_secondary_nav_items->secondary_nav;

	$dhm_top_info_defined = $dhm_secondary_nav_items->top_info_defined;
?>

	<?php if ( $dhm_top_info_defined ) : ?>
		<div id="top-header">
			<div class="container clearfix">

			<?php if ( $dhm_contact_info_defined ) : ?>

				<div id="dhm-info">
				<?php if ( '' !== ( $dhm_phone_number = dhm_get_option( 'phone_number' ) ) ) : ?>
					<span id="dhm-info-phone"><?php echo dhm_sanitize_html_input_text( $dhm_phone_number ); ?></span>
				<?php endif; ?>

				<?php if ( '' !== ( $dhm_email = dhm_get_option( 'header_email' ) ) ) : ?>
					<a href="<?php echo esc_attr( 'mailto:' . $dhm_email ); ?>"><span id="dhm-info-email"><?php echo esc_html( $dhm_email ); ?></span></a>
				<?php endif; ?>

				<?php
				if ( true === $show_header_social_icons ) {
					get_template_part( 'includes/social_icons', 'header' );
				} ?>
				</div> <!-- #dhm-info -->

			<?php endif; // true === $dhm_contact_info_defined ?>

				<div id="dhm-secondary-menu">
				<?php
					if ( ! $dhm_contact_info_defined && true === $show_header_social_icons ) {
						get_template_part( 'includes/social_icons', 'header' );
					} else if ( $dhm_contact_info_defined && true === $show_header_social_icons ) {
						ob_start();

						get_template_part( 'includes/social_icons', 'header' );

						$duplicate_social_icons = ob_get_contents();

						ob_end_clean();

						printf(
							'<div class="dhm_duplicate_social_icons">
								%1$s
							</div>',
							$duplicate_social_icons
						);
					}

					if ( '' !== $dhm_secondary_nav ) {
						echo $dhm_secondary_nav;
					}

					dhm_show_cart_total();
				?>
				</div> <!-- #dhm-secondary-menu -->

			</div> <!-- .container -->
		</div> <!-- #top-header -->
	<?php endif; // true ==== $dhm_top_info_defined ?>

		<header id="main-header" data-height-onload="<?php echo esc_attr( dhm_get_option( 'menu_height', '66' ) ); ?>">
			<div class="container clearfix dhm_menu_container">
			<?php
				$logo = ( $user_logo = dhm_get_option( 'rrcfest_logo' ) ) && '' != $user_logo
					? $user_logo
					: $template_directory_uri . '/images/logo.png';
			?>
				<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
						<img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo" data-height-percentage="<?php echo esc_attr( dhm_get_option( 'logo_height', '54' ) ); ?>" />
					</a>
				</div>
				<div id="dhm-top-navigation" data-height="<?php echo esc_attr( dhm_get_option( 'menu_height', '66' ) ); ?>" data-fixed-height="<?php echo esc_attr( dhm_get_option( 'minimized_menu_height', '40' ) ); ?>">
					<nav id="top-menu-nav">
					<?php
						$menuClass = 'nav';
						if ( 'on' == dhm_get_option( 'rrcfest_disable_toptier' ) ) $menuClass .= ' dhm_disable_top_tier';
						$primaryNav = '';

						$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false ) );

						if ( '' == $primaryNav ) :
					?>
						<ul id="top-menu" class="<?php echo esc_attr( $menuClass ); ?>">
							<?php if ( 'on' == dhm_get_option( 'rrcfest_home_link' ) ) { ?>
								<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Rrcfest' ); ?></a></li>
							<?php }; ?>

							<?php show_page_menu( $menuClass, false, false ); ?>
							<?php show_categories_menu( $menuClass, false ); ?>
						</ul>
					<?php
						else :
							echo( $primaryNav );
						endif;
					?>
					</nav>

					<?php
					if ( ! $dhm_top_info_defined ) {
						dhm_show_cart_total( array(
							'no_text' => true,
						) );
					}
					?>

					<?php if ( false !== dhm_get_option( 'show_search_icon', true ) || is_customize_preview() ) : ?>
					<div id="dhm_top_search">
						<span id="dhm_search_icon"></span>
					</div>
					<?php endif; // true === dhm_get_option( 'show_search_icon', false ) ?>

					<?php do_action( 'dhm_header_top' ); ?>
				</div> <!-- #dhm-top-navigation -->
			</div> <!-- .container -->
			<div class="dhm_search_outer">
				<div class="container dhm_search_form_container">
					<form role="search" method="get" class="dhm-search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<?php
						printf( '<input type="search" class="dhm-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
							esc_attr__( 'Search &hellip;', 'Rrcfest' ),
							get_search_query(),
							esc_attr__( 'Search for:', 'Rrcfest' )
						);
					?>
					</form>
					<span class="dhm_close_search_field"></span>
				</div>
			</div>
		</header> <!-- #main-header -->
		<div class="pageBanner"><img src="http://rrcfest.com/wp-content/uploads/2016/05/pagebanner.jpg" alt=""></div>
		<script>
			var $ = jQuery;
			$(document).ready(function(){
				
				if ($('body').attr('id') == "home") {
					console.log("jome")
					$('.pageBanner').addClass("displayNone");
				};
			})	
		</script>
		<div id="dhm-main-area">