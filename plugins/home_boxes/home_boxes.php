<?php 

/*
Plugin Name: Home Boxes For Reggae Roots & Culture Festival
Plugin URI: *
Description: CPT for Reggae Roots & Culture Festival Home Boxes
Author: Dirty Hands Media
Version: 1.1.1
Author URI: *
Text Domain: cpt-plugin
Domain Path: /languages
License: GPLv2
*/

// Registers the new post type and taxonomy

// define('WP_DEBUG', true);

function custom_post_rrcf_home_boxes() {
    register_post_type( 'rrcf_homeboxes',
        array(
            'labels' => array(
                'name' => __( 'Home Boxes' ),
                'singular_name' => __( 'Home Box' ),
                'add_new' => __( 'Add New Home Box' ),
                'add_new_item' => __( 'Add New Home Box' ),
                'edit_item' => __( 'Edit Home Box' ),
                'new_item' => __( 'Add New Home Box' ),
                'view_item' => __( 'View Home Box' ),
                'search_items' => __( 'Search Home Box' ),
                'not_found' => __( 'No rrcf_homeboxes found' ),
                'not_found_in_trash' => __( 'No rrcf_homeboxes found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title', 'editor' ),
            'capability_type' => 'post',
            'rewrite' => array("slug" => "rrcf_homeboxes"), // Permalinks format
            'menu_position' => 5,
            // 'register_meta_box_cb' => 'add_rrcf_homeboxes_metaboxes'
        )
    );
    register_post_type( 'custom_post_rrcf_home_boxes', $args );
}


// // Add the Home Boxes Meta Boxes

// function add_rrcf_homeboxes_metaboxes() {
//     add_meta_box('cpt_rrcf_homeboxes_price', 'Home Box Price', 'cpt_rrcf_homeboxes_price', 'rrcf_homeboxes', 'side', 'low');
//     add_meta_box('cpt_rrcf_homeboxes_links', 'Home Box Link', 'cpt_rrcf_homeboxes_links', 'rrcf_homeboxes', 'side', 'low');
    
// }

// // The Home Box  Metaboxes

// function cpt_rrcf_homeboxes_price() {
//     global $post;
    
//     // Noncename needed to verify where the data originated
//     echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
//     wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
//     // Get the location data if its already been entered
//     $ticket_price = get_post_meta($post->ID, '_ticket_price', true);
    
//     // Echo out the field
//     echo '<input type="text" name="_ticket_price" value="' . $ticket_price  . '" class="widefat" />';


// }

// function cpt_rrcf_homeboxes_links() {
//     global $post;
    
//     // Noncename needed to verify where the data originated
//     echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
//     wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
//     // Get the location data if its already been entered
//     $ticket_links = get_post_meta($post->ID, '_ticket_links', true);
    
//     // Echo out the field
//     echo '<input type="text" name="_ticket_links" value="' . $ticket_links  . '" class="widefat" />';

// }

// // Save the Metabox Data

// function cpt_save_rrcf_homeboxes_meta($post_id, $post) {
    
//     // verify this came from the our screen and with proper authorization,
//     // because save_post can be triggered at other times
//     if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
//     return $post->ID;
//     }

//     // Is the user allowed to edit the post or page?
//     if ( !current_user_can( 'edit_post', $post->ID ))
//         return $post->ID;

//     // OK, we're authenticated: we need to find and save the data
//     // We'll put it into an array to make it easier to loop though.
    
//     $rrcf_homeboxes_meta['_ticket_price'] = $_POST['_ticket_price'];
//     $rrcf_homeboxes_meta['_ticket_links'] = $_POST['_ticket_links'];
    
//     // Add values of $rrcf_homeboxes_meta as custom fields
    
//     foreach ($rrcf_homeboxes_meta as $key => $value) { // Cycle through the $rrcf_homeboxes_meta array!
//         if( $post->post_type == 'revision' ) return; // Don't store custom data twice
//         $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
//         if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
//             update_post_meta($post->ID, $key, $value);
//         } else { // If the custom field doesn't have a value
//             add_post_meta($post->ID, $key, $value);
//         }
//         if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
//     }

// }

// add_action('save_post', 'cpt_save_rrcf_homeboxes_meta', 1, 2); // save the custom fields


// // filters for meta
// add_filter( 'rest_prepare_rrcf_homeboxes', 'add_ticket_meta_to_json', 10, 3 );
// function add_ticket_meta_to_json($data, $post, $request){

// $response_data = $data->get_data();

// if ( $request['context'] !== 'view' || is_wp_error( $data ) ) {
//     return $data;
// }


// $ticket_price = get_post_meta( $post->ID, '_ticket_price', true );
// if(empty($ticket_price)){
//     $ticket_price = '';
// }

// $ticket_links = get_post_meta( $post->ID, '_ticket_links', true );
// if(empty($ticket_links)){
//     $ticket_links = '';
// }

// if($post->post_type == 'rrcf_homeboxes'){
//     $response_data['rrcf_homeboxes_meta'] = array(
//         'ticketPrice' => $ticket_price,
//         'ticketLinks' => $ticket_links,
        
//     );  
// }

// $data->set_data( $response_data );

// return $data;
// }

// function my_rest_prepare_rrcf_homeboxes_post( $data, $post, $request ) {
//     $_image_data = $data->data;
//     $thumbnail_id = get_post_thumbnail_id( $post->ID );
//     $thumbnail = wp_get_attachment_image_src( $thumbnail_id );
//     $_image_data['featured_image_thumbnail_url'] = $thumbnail[0];
//     $data->data = $_image_data;
//     return $data;
// }
// add_filter( 'rest_prepare_rrcf_homeboxes_post', 'my_rest_prepare_rrcf_homeboxes_post', 10, 3 );

add_action( 'init', 'custom_post_rrcf_home_boxes' );

add_action( 'init', 'add_rrcf_homeboxes_to_json_api', 30 );
function add_rrcf_homeboxes_to_json_api(){

    global $wp_post_types;
    $wp_post_types['rrcf_homeboxes']->show_in_rest = true;
    $wp_post_types['rrcf_homeboxes']->rest_base = 'rrcf_homeboxes';
    $wp_post_types['rrcf_homeboxes']->rest_controller_class = 'WP_REST_Posts_Controller';
}

?>