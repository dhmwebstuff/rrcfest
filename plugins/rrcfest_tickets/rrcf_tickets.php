<?php 

/*
Plugin Name: Ticket CPT For Reggae Roots & Culture Festival
Plugin URI: *
Description: CPT for Reggae Roots & Culture Festival Tickets
Author: Dirty Hands Media
Version: 1.1.1
Author URI: *
Text Domain: cpt-plugin
Domain Path: /languages
License: GPLv2
*/

// Registers the new post type and taxonomy

// define('WP_DEBUG', true);

function custom_post_rrcf_tickets() {
    register_post_type( 'rrcf_tickets',
        array(
            'labels' => array(
                'name' => __( 'Tickets' ),
                'singular_name' => __( 'Ticket' ),
                'add_new' => __( 'Add New Ticket' ),
                'add_new_item' => __( 'Add New Ticket' ),
                'edit_item' => __( 'Edit Ticket' ),
                'new_item' => __( 'Add New Ticket' ),
                'view_item' => __( 'View Ticket' ),
                'search_items' => __( 'Search Ticket' ),
                'not_found' => __( 'No rrcf_tickets found' ),
                'not_found_in_trash' => __( 'No rrcf_tickets found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title', 'editor' ),
            'capability_type' => 'post',
            'rewrite' => array("slug" => "rrcf_tickets"), // Permalinks format
            'menu_position' => 5,
            'register_meta_box_cb' => 'add_rrcf_tickets_metaboxes'
        )
    );
    register_post_type( 'custom_post_rrcf_tickets', $args );
}


// Add the Tickets Meta Boxes

function add_rrcf_tickets_metaboxes() {
    add_meta_box('cpt_rrcf_tickets_price', 'Ticket Price', 'cpt_rrcf_tickets_price', 'rrcf_tickets', 'side', 'low');
    add_meta_box('cpt_rrcf_tickets_links', 'Ticket Link', 'cpt_rrcf_tickets_links', 'rrcf_tickets', 'side', 'low');
    
}

// The Ticket  Metaboxes

function cpt_rrcf_tickets_price() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $ticket_price = get_post_meta($post->ID, '_ticket_price', true);
    
    // Echo out the field
    echo '<input type="text" name="_ticket_price" value="' . $ticket_price  . '" class="widefat" />';


}

function cpt_rrcf_tickets_links() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $ticket_links = get_post_meta($post->ID, '_ticket_links', true);
    
    // Echo out the field
    echo '<input type="text" name="_ticket_links" value="' . $ticket_links  . '" class="widefat" />';

}

// Save the Metabox Data

function cpt_save_rrcf_tickets_meta($post_id, $post) {
    
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
    return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
    
    $rrcf_tickets_meta['_ticket_price'] = $_POST['_ticket_price'];
    $rrcf_tickets_meta['_ticket_links'] = $_POST['_ticket_links'];
    
    // Add values of $rrcf_tickets_meta as custom fields
    
    foreach ($rrcf_tickets_meta as $key => $value) { // Cycle through the $rrcf_tickets_meta array!
        if( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
        if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
            update_post_meta($post->ID, $key, $value);
        } else { // If the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
        }
        if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }

}

add_action('save_post', 'cpt_save_rrcf_tickets_meta', 1, 2); // save the custom fields


// filters for meta
add_filter( 'rest_prepare_rrcf_tickets', 'add_ticket_meta_to_json', 10, 3 );
function add_ticket_meta_to_json($data, $post, $request){

$response_data = $data->get_data();

if ( $request['context'] !== 'view' || is_wp_error( $data ) ) {
    return $data;
}


$ticket_price = get_post_meta( $post->ID, '_ticket_price', true );
if(empty($ticket_price)){
    $ticket_price = '';
}

$ticket_links = get_post_meta( $post->ID, '_ticket_links', true );
if(empty($ticket_links)){
    $ticket_links = '';
}

if($post->post_type == 'rrcf_tickets'){
    $response_data['rrcf_tickets_meta'] = array(
        'ticketPrice' => $ticket_price,
        'ticketLinks' => $ticket_links,
        
    );  
}

$data->set_data( $response_data );

return $data;
}

function my_rest_prepare_rrcf_tickets_post( $data, $post, $request ) {
    $_image_data = $data->data;
    $thumbnail_id = get_post_thumbnail_id( $post->ID );
    $thumbnail = wp_get_attachment_image_src( $thumbnail_id );
    $_image_data['featured_image_thumbnail_url'] = $thumbnail[0];
    $data->data = $_image_data;
    return $data;
}
add_filter( 'rest_prepare_rrcf_tickets_post', 'my_rest_prepare_rrcf_tickets_post', 10, 3 );

add_action( 'init', 'custom_post_rrcf_tickets' );

add_action( 'init', 'add_rrcf_tickets_to_json_api', 30 );
function add_rrcf_tickets_to_json_api(){

    global $wp_post_types;
    $wp_post_types['rrcf_tickets']->show_in_rest = true;
    $wp_post_types['rrcf_tickets']->rest_base = 'rrcf_tickets';
    $wp_post_types['rrcf_tickets']->rest_controller_class = 'WP_REST_Posts_Controller';
}

?>