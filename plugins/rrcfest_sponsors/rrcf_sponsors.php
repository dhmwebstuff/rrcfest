<?php 

/*
Plugin Name: Sponsor CPT For Reggae Roots & Culture Festival
Plugin URI: *
Description: CPT for Sponsors
Author: Dirty Hands Media
Version: 1.1.1
Author URI: *
Text Domain: cpt-plugin
Domain Path: /languages
License: GPLv2
*/

// Registers the new post type and taxonomy

define('WP_DEBUG', true);

function custom_post_rrcf_sponsors() {
    register_post_type( 'rrcf_sponsors',
        array(
            'labels' => array(
                'name' => __( 'Sponsors' ),
                'singular_name' => __( 'Sponsor' ),
                'add_new' => __( 'Add New Sponsor' ),
                'add_new_item' => __( 'Add New Sponsor' ),
                'edit_item' => __( 'Edit Sponsor' ),
                'new_item' => __( 'Add New Sponsor' ),
                'view_item' => __( 'View Sponsor' ),
                'search_items' => __( 'Search Sponsor' ),
                'not_found' => __( 'No rrcf_sponsors found' ),
                'not_found_in_trash' => __( 'No rrcf_sponsors found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title', 'thumbnail', 'editor' ),
            'capability_type' => 'post',
            'rewrite' => array("slug" => "rrcf_sponsors"), // Permalinks format
            'menu_position' => 5,
            'hierarchical' => false,
            'posts_per_page' => -1,
            'register_meta_box_cb' => 'add_rrcf_sponsors_metaboxes'
        )
    );
    register_post_type( 'custom_post_rrcf_sponsors', $args );
}


// Add the Sponsors Meta Boxes

function add_rrcf_sponsors_metaboxes() {
    
    add_meta_box('cpt_rrcf_sponsors_link', 'Sponsor Link', 'cpt_rrcf_sponsors_link', 'rrcf_sponsors', 'side', 'low');
    add_meta_box('cpt_rrcf_sponsors_imageURL', 'Image URL', 'cpt_rrcf_sponsors_imageURL', 'rrcf_sponsors', 'side', 'low');
    add_meta_box('cpt_rrcf_sponsors_sponsor_title', 'Title Sponsor', 'cpt_rrcf_sponsors_sponsor_title', 'rrcf_sponsors', 'side', 'low');
    add_meta_box('cpt_rrcf_sponsors_sponsor_media_partner', 'Media Partner', 'cpt_rrcf_sponsors_sponsor_media_partner', 'rrcf_sponsors', 'side', 'low');
}

// The Sponsor  Metaboxes


function cpt_rrcf_sponsors_link() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $sponsorLink = get_post_meta($post->ID, '_sponsorLink', true);
    
    // Echo out the field
    echo '<input type="text" name="_sponsorLink" value="' . $sponsorLink  . '" class="widefat" />';

}

function cpt_rrcf_sponsors_imageURL() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $sponsorImageURL = get_post_meta($post->ID, '_sponsorImageURL', true);
    
    // Echo out the field
    echo '<input type="text" name="_sponsorImageURL" value="' . $sponsorImageURL  . '" class="widefat" />';

}

function cpt_rrcf_sponsors_sponsor_title() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $sponsors_sponsor_title = get_post_meta($post->ID, '_sponsors_sponsor_title', true);
    
    // Echo out the field
    echo '<input type="checkbox" name="_sponsors_sponsor_title" class="widefat" ',$sponsors_sponsor_title ? ' checked="checked"' : ' /> Title Sponsor';

}

function cpt_rrcf_sponsors_sponsor_media_partner() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $sponsors_sponsor_media_partner = get_post_meta($post->ID, '_sponsors_sponsor_media_partner', true);
    
    // Echo out the field
    echo '<input type="checkbox" name="_sponsors_sponsor_media_partner" class="widefat" ',$sponsors_sponsor_media_partner ? ' checked="checked"' : ' /> Media Partner';

}

// Save the Metabox Data

function cpt_save_rrcf_sponsors_meta($post_id, $post) {
    
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
    return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
    
    
    $rrcf_sponsors_meta['_sponsorLink'] = $_POST['_sponsorLink'];
    $rrcf_sponsors_meta['_sponsorImageURL'] = $_POST['_sponsorImageURL'];
    $rrcf_sponsors_meta['_sponsors_sponsor_title'] = $_POST['_sponsors_sponsor_title'];
    $rrcf_sponsors_meta['_sponsors_sponsor_media_partner'] = $_POST['_sponsors_sponsor_media_partner'];
    
    
    // Add values of $rrcf_sponsors_meta as custom fields
    
    foreach ($rrcf_sponsors_meta as $key => $value) { // Cycle through the $rrcf_sponsors_meta array!
        if( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
        if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
            update_post_meta($post->ID, $key, $value);
        } else { // If the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
        }
        if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }

}

add_action('save_post', 'cpt_save_rrcf_sponsors_meta', 1, 2); // save the custom fields


// filters for meta
add_filter( 'rest_prepare_rrcf_sponsors', 'add_rrcf_sponsor_meta_to_json', 10, 3 );
function add_rrcf_sponsor_meta_to_json($data, $post, $request){

$response_data = $data->get_data();

if ( $request['context'] !== 'view' || is_wp_error( $data ) ) {
    return $data;
}

$sponsorLink = get_post_meta( $post->ID, '_sponsorLink', true );
if(empty($sponsorLink)){
    $sponsorLink = '';
}


$sponsorImageURL = get_post_meta( $post->ID, '_sponsorImageURL', true );
if(empty($sponsorImageURL)){
    $sponsorImageURL = '';
}
$sponsors_sponsor_title = get_post_meta( $post->ID, '_sponsors_sponsor_title', true );
if(empty($sponsors_sponsor_title)){
    $sponsors_sponsor_title = '';
}

$sponsors_sponsor_media_partner = get_post_meta( $post->ID, '_sponsors_sponsor_media_partner', true );
if(empty($sponsors_sponsor_media_partner)){
    $sponsors_sponsor_media_partner = '';
}

if($post->post_type == 'rrcf_sponsors'){
    $response_data['rrcf_sponsors_meta'] = array(
        
        'sponsorLink' => $sponsorLink,
        'sponsorImageURL' => $sponsorImageURL,
        'sponsors_sponsor_title' => $sponsors_sponsor_title,
        'sponsors_sponsor_media_partner' => $sponsors_sponsor_media_partner
    );  
}

$data->set_data( $response_data );

return $data;
}

function my_rest_prepare_rrcf_sponsors_post( $data, $post, $request ) {
    $_image_data = $data->data;
    $thumbnail_id = get_post_thumbnail_id( $post->ID );
    $thumbnail = wp_get_attachment_image_src( $thumbnail_id );
    $_image_data['featured_image_thumbnail_url'] = $thumbnail[0];
    $data->data = $_image_data;
    return $data;
}
add_filter( 'rest_prepare_rrcf_sponsors_post', 'my_rest_prepare_rrcf_sponsors_post', 10, 3 );

add_action( 'init', 'custom_post_rrcf_sponsors' );

add_action( 'init', 'add_rrcf_sponsors_to_json_api', 30 );
function add_rrcf_sponsors_to_json_api(){

    global $wp_post_types;
    $wp_post_types['rrcf_sponsors']->show_in_rest = true;
    $wp_post_types['rrcf_sponsors']->rest_base = 'rrcf_sponsors';
    $wp_post_types['rrcf_sponsors']->rest_controller_class = 'WP_REST_Posts_Controller';
}

?>