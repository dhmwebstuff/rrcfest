<?php 

/*
Plugin Name: Artist CPT For Reggae Roots & Culture Festival
Plugin URI: *
Description: CPT for Artists Lineup 
Author: Dirty Hands Media
Version: 1.1.1
Author URI: *
Text Domain: cpt-plugin
Domain Path: /languages
License: GPLv2
*/

// Registers the new post type and taxonomy

// define('WP_DEBUG', true);

function custom_post_rrcf_artists() {
    register_post_type( 'rrcf_artists',
        array(
            'labels' => array(
                'name' => __( 'Artists' ),
                'singular_name' => __( 'Artist' ),
                'add_new' => __( 'Add New Artist' ),
                'add_new_item' => __( 'Add New Artist' ),
                'edit_item' => __( 'Edit Artist' ),
                'new_item' => __( 'Add New Artist' ),
                'view_item' => __( 'View Artist' ),
                'search_items' => __( 'Search Artist' ),
                'not_found' => __( 'No rrcf_artists found' ),
                'not_found_in_trash' => __( 'No rrcf_artists found in trash' )
            ),
            'public' => true,
            'supports' => array( 'title', 'thumbnail', 'editor' ),
            'capability_type' => 'post',
            'rewrite' => array("slug" => "rrcf_artists"), // Permalinks format
            'menu_position' => 5,
            'hierarchical' => false,
            'post_per_page' => 20,
            'register_meta_box_cb' => 'add_rrcf_artists_metaboxes'
        )
    );
    register_post_type( 'custom_post_rrcf_artists', $args );
}


// Add the Artists Meta Boxes

function add_rrcf_artists_metaboxes() {
    add_meta_box('cpt_rrcf_artists_year', 'Festival Year', 'cpt_rrcf_artists_year', 'rrcf_artists', 'side', 'low');
    add_meta_box('cpt_rrcf_artists_links', 'Artist Website Link', 'cpt_rrcf_artists_links', 'rrcf_artists', 'side', 'low');
    add_meta_box('cpt_rrcf_artists_fb_link', 'Artist Facebook Link', 'cpt_rrcf_artists_fb_link', 'rrcf_artists', 'side', 'low');
    add_meta_box('cpt_rrcf_artists_tw_link', 'Artist Twitter Link', 'cpt_rrcf_artists_tw_link', 'rrcf_artists', 'side', 'low');
    add_meta_box('cpt_rrcf_artists_insta_link', 'Artist Instagram Link', 'cpt_rrcf_artists_insta_link', 'rrcf_artists', 'side', 'low');
    add_meta_box('cpt_rrcf_artists_youtube', 'Artist Youtube Link', 'cpt_rrcf_artists_youtube', 'rrcf_artists', 'side', 'low');
    add_meta_box('cpt_rrcf_artists_performanceDate', 'Performance Date', 'cpt_rrcf_artists_performanceDate', 'rrcf_artists', 'side', 'low');
    add_meta_box('cpt_rrcf_artists_performanceTime', 'Performance Time', 'cpt_rrcf_artists_performanceTime', 'rrcf_artists', 'side', 'low');
    add_meta_box('cpt_rrcf_artists_imageURL', 'Image URL', 'cpt_rrcf_artists_imageURL', 'rrcf_artists', 'side', 'low');
}

// The Artist  Metaboxes

function cpt_rrcf_artists_year() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $festival_year = get_post_meta($post->ID, '_festival_year', true);

    if(empty($festival_year)){
        $date = date("Y");
        $festival_year = $date;
    }
    
    // Echo out the field
    echo '<input type="text" name="_festival_year" value="' . $festival_year  . '" class="widefat" />';


}

function cpt_rrcf_artists_links() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $artistLinks = get_post_meta($post->ID, '_artistLinks', true);
    
    // Echo out the field
    echo '<input type="text" name="_artistLinks" value="' . $artistLinks  . '" class="widefat" />';

}

function cpt_rrcf_artists_fb_link() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $artistLinksFb = get_post_meta($post->ID, '_artistLinksFb', true);
    
    // Echo out the field
    echo '<input type="text" name="_artistLinksFb" value="' . $artistLinksFb  . '" class="widefat" />';

}

function cpt_rrcf_artists_tw_link() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $artistLinksTw = get_post_meta($post->ID, '_artistLinksTw', true);
    
    // Echo out the field
    echo '<input type="text" name="_artistLinksTw" value="' . $artistLinksTw  . '" class="widefat" />';

}

function cpt_rrcf_artists_insta_link() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $artistLinksInsta = get_post_meta($post->ID, '_artistLinksInsta', true);
    
    // Echo out the field
    echo '<input type="text" name="_artistLinksInsta" value="' . $artistLinksInsta  . '" class="widefat" />';

}

function cpt_rrcf_artists_youtube() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $artistLinksYt = get_post_meta($post->ID, '_artistLinksYt', true);
    
    // Echo out the field
    echo '<input type="text" name="_artistLinksYt" value="' . $artistLinksYt  . '" class="widefat" />';

}

function cpt_rrcf_artists_performanceDate() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $performanceDate = get_post_meta($post->ID, '_performanceDate', true);
    
    // Echo out the field
    echo '<input type="text" name="_performanceDate"  class="widefat artistsPD" value="' . $performanceDate . '" />';

    echo "<script>jQuery(function(){jQuery('.artistsPD').datepicker({ dateFormat: 'MM d' });});</script>";

}

function cpt_rrcf_artists_performanceTime() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $performanceTime = get_post_meta($post->ID, '_performanceTime', true);
    
    // Echo out the field
    echo '<input type="text" name="_performanceTime"  class="widefat artistsPT" value="' . $performanceTime . '" />';

    echo "<script>jQuery(function(){jQuery('.artistsPT').timepicker();});</script>";

}

function cpt_rrcf_artists_imageURL() {
    global $post;
    
    // Noncename needed to verify where the data originated
    echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
    wp_create_nonce( plugin_basename(__FILE__) ) . '" />';
    
    // Get the location data if its already been entered
    $imageURL = get_post_meta($post->ID, '_imageURL', true);
    
    // Echo out the field
    echo '<input type="text" name="_imageURL" value="' . $imageURL  . '" class="widefat" />';

}

// Save the Metabox Data

function cpt_save_rrcf_artists_meta($post_id, $post) {
    
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
    if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
    return $post->ID;
    }

    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;

    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though.
    
    $rrcf_artists_meta['_festival_year'] = $_POST['_festival_year'];
    $rrcf_artists_meta['_artistLinks'] = $_POST['_artistLinks'];
    $rrcf_artists_meta['_artistLinksFb'] = $_POST['_artistLinksFb'];
    $rrcf_artists_meta['_artistLinksTw'] = $_POST['_artistLinksTw'];
    $rrcf_artists_meta['_artistLinksInsta'] = $_POST['_artistLinksInsta'];
    $rrcf_artists_meta['_artistLinksYt'] = $_POST['_artistLinksYt'];
    $rrcf_artists_meta['_performanceDate'] = $_POST['_performanceDate'];
    $rrcf_artists_meta['_performanceTime'] = $_POST['_performanceTime'];
    $rrcf_artists_meta['_imageURL'] = $_POST['_imageURL'];
    
    // Add values of $rrcf_artists_meta as custom fields
    
    foreach ($rrcf_artists_meta as $key => $value) { // Cycle through the $rrcf_artists_meta array!
        if( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode(',', (array)$value); // If $value is an array, make it a CSV (unlikely)
        if(get_post_meta($post->ID, $key, FALSE)) { // If the custom field already has a value
            update_post_meta($post->ID, $key, $value);
        } else { // If the custom field doesn't have a value
            add_post_meta($post->ID, $key, $value);
        }
        if(!$value) delete_post_meta($post->ID, $key); // Delete if blank
    }

}

add_action('save_post', 'cpt_save_rrcf_artists_meta', 1, 2); // save the custom fields


// filters for meta
add_filter( 'rest_prepare_rrcf_artists', 'add_meta_to_json', 10, 3 );
function add_meta_to_json($data, $post, $request){

$response_data = $data->get_data();

if ( $request['context'] !== 'view' || is_wp_error( $data ) ) {
    return $data;
}


$festival_year = get_post_meta( $post->ID, '_festival_year', true );
if(empty($festival_year)){
    $festival_year = '';
}

$artistLinks = get_post_meta( $post->ID, '_artistLinks', true );
if(empty($artistLinks)){
    $artistLinks = '';
}

$artistLinksFb = get_post_meta( $post->ID, '_artistLinksFb', true );
if(empty($artistLinksFb)){
    $artistLinksFb = '';
}

$artistLinksTw = get_post_meta( $post->ID, '_artistLinksTw', true );
if(empty($artistLinksTw)){
    $artistLinksTw = '';
}

$artistLinksInsta = get_post_meta( $post->ID, '_artistLinksInsta', true );
if(empty($artistLinksInsta)){
    $artistLinksInsta = '';
}

$artistLinksYt = get_post_meta( $post->ID, '_artistLinksYt', true );
if(empty($artistLinksYt)){
    $artistLinksYt = '';
}

$performanceDate = get_post_meta( $post->ID, '_performanceDate', true );
if(empty($performanceDate)){
    $performanceDate = '';
}

$performanceTime = get_post_meta( $post->ID, '_performanceTime', true );
if(empty($performanceTime)){
    $performanceTime = '';
}

$imageURL = get_post_meta( $post->ID, '_imageURL', true );
if(empty($imageURL)){
    $imageURL = '';
}

if($post->post_type == 'rrcf_artists'){
    $response_data['rrcf_artists_meta'] = array(
        'festivalYear' => $festival_year,
        'artistLinks' => $artistLinks,
        'artistLinksFb' => $artistLinksFb,
        'artistLinksTw' => $artistLinksTw,
        'artistLinksInsta' => $artistLinksInsta,
        'artistLinksYt' => $artistLinksYt,
        'performanceDate' => $performanceDate,
        'performanceTime' => $performanceTime,
        'imageURL' => $imageURL,
    );  
}

$data->set_data( $response_data );

return $data;
}

function my_rest_prepare_rrcf_artists_post( $data, $post, $request ) {
    $_image_data = $data->data;
    $thumbnail_id = get_post_thumbnail_id( $post->ID );
    $thumbnail = wp_get_attachment_image_src( $thumbnail_id );
    $_image_data['featured_image_thumbnail_url'] = $thumbnail[0];
    $data->data = $_image_data;
    return $data;
}
add_filter( 'rest_prepare_rrcf_artists_post', 'my_rest_prepare_rrcf_artists_post', 10, 3 );

add_action( 'init', 'custom_post_rrcf_artists' );

add_action( 'init', 'add_rrcf_artists_to_json_api', 30 );
function add_rrcf_artists_to_json_api(){

    global $wp_post_types;
    $wp_post_types['rrcf_artists']->show_in_rest = true;
    $wp_post_types['rrcf_artists']->rest_base = 'rrcf_artists';
    $wp_post_types['rrcf_artists']->rest_controller_class = 'WP_REST_Posts_Controller';
}


?>