<?php
if( !class_exists( 'WP_Http' ) )
        include_once( ABSPATH . WPINC. '/class-http.php' );
function woo_update_product($pid){
 if(!version_compare(phpversion(),'5.3','<')){
    require_once( plugin_dir_path(__FILE__) . 'classes/WCAPI/Product.php' );

    $product =  WCAPI\Product::find($pid);
    $current_user = wp_get_current_user();
    $user_id=$current_user->ID;
    $helpers = new JSONAPIHelpers();
    $token= $key1 = $helpers->getPluginPrefix() . '_token';
    if(get_post_type($pid) =='product'){
       
        $pa_data=$product->asApiArray();
        $data = array("method"=>"update","data"=>$pa_data,'token'=>urlencode(get_option(   	$token)));                                                                    
        $data_string = array();
        $data_string = json_encode($data);  
$request = new WP_Http;
$result = $request->request( 'https://printaura.com/woocommerce-synchronize-api/' , array( 'method' => 'POST', 'timeout'=>500, 'body' => $data_string ) );
$result=json_decode( $result['body'],true);                                                                                   
      
        if($result['status']!='success')
            wp_mail('aladin@printaura.com','error update woocommerce product from store',var_export($data,true));
    }
}    
}
function woo_delete_poduct( $pid ) {
    global $wpdb;
    $current_user = wp_get_current_user();
    $user_id=$current_user->ID;
    $helpers = new JSONAPIHelpers();
    $token= $key1 = $helpers->getPluginPrefix() . '_token';
    if(get_post_type($pid) =='product'){
      
        $data = array("method"=>"delete","data"=>array("id" => $pid),'token'=>urlencode(get_option($token)));                                                                    
        $data_string = array();
        $data_string = json_encode($data);     
$request = new WP_Http;
$result = $request->request( 'https://printaura.com/woocommerce-synchronize-api/' , array( 'method' => 'POST', 'timeout'=>500, 'body' => $data_string ) );
$result=json_decode( $result['body'],true);                                                                                
           if($result['status']!='success')
            wp_mail('aladin@printaura.com','error removing woocommerce product from store',var_export($data,true));
    }
    return true;
}

function add_pa_order($order_id){
    $active_plugins = (array) get_option( 'active_plugins', array() );
  if(in_array('woocommerce/woocommerce.php', $active_plugins)){
      $order= new WC_Order();
      $order->get_order($order_id);
      $order_data =(array) $order;
      $order_data['billing_email']=$order->billing_email;
      $order_data['billing_phone']=$order->billing_phone;
      $order_data['shipping_company']=$order->shipping_company;
      $order_data['shipping_first_name']=$order->shipping_first_name;
      $order_data['shipping_last_name']=$order->shipping_last_name;
      $order_data['shipping_address_1']=$order->shipping_address_1;
      $order_data['shipping_address_2']=$order->shipping_address_2;
      $order_data['shipping_city']=$order->shipping_city;
      $order_data['shipping_state']=$order->shipping_state;
      $order_data['shipping_postcode']=$order->shipping_postcode;
      $order_data['shipping_country']=$order->shipping_country;
      
      //$order_data=array_merge($order_data,);
      $order_data['order_items']=$order->get_items();
      //wp_mail('aladin@printaura.com','order data',  var_export($order_data, true));
      $current_user = wp_get_current_user();
      $user_id=$current_user->ID;
      $helpers = new JSONAPIHelpers();
      $token= $key1 = $helpers->getPluginPrefix() . '_token';
      $data = array("method"=>"create","data"=>$order_data,'token'=>  urlencode(get_option($token)));                                                                    
      $data_string = array();
$data_string = json_encode($data);
$request = new WP_Http;
$result = $request->request( 'https://printaura.com/woocommerce-synchronize-api/' , array( 'method' => 'POST', 'timeout'=>500, 'body' => $data_string ) );
$result=json_decode( $result['body'],true);  
     
      if($result['status']!='success')
      wp_mail('aladin@printaura.com','error adding woocommerce order',var_export($result,true));

  }
}

?>
