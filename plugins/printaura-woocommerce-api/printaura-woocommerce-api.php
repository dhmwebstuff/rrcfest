<?php 
/*
  Plugin Name: PrintAura WooCommerce API
  Plugin URI: https://printaura.com
  Description: PrintAura  WooCommerce Integration API
  Author: Print Aura
  Version: 3.2.8
  Author URI: http://printaura.com
*/
  // Turn on debugging?
define('WC_JSON_API_DEBUG',false);
define( 'REDE_PLUGIN_BASE_PATH', plugin_dir_path(__FILE__) );
if (! defined('REDENOTSET')) {
  define( 'REDENOTSET','__RED_E_NOTSET__' ); // because sometimes false, 0 etc are
  // exspected but consistently dealing with these situations is tiresome.
}
    
 if(!version_compare(phpversion(),'5.3','<')){
require_once( plugin_dir_path(__FILE__) . 'classes/class-rede-helpers.php' );
require_once( plugin_dir_path(__FILE__) . 'classes/class-pa-updater-config.php' );
require_once( plugin_dir_path(__FILE__) . 'classes/class-pa-updater.php' );
require_once( REDE_PLUGIN_BASE_PATH . '/printaura-woocommerce-api-synchronize.php' );
require_once( plugin_dir_path(__FILE__) . 'printaura-woocommerce-api-core.php' );
}

function add_shipped_order_woocommerce_email( $email_classes ) {

	// include our custom email class
	require_once( plugin_dir_path(__FILE__) .'classes/class-wc-shipped-order-email.php' );

	// add the email class to the list of email classes that WooCommerce loads
	$email_classes['WC_Shipped_Order_Email'] = new WC_Shipped_Order_Email();

	return $email_classes;

}
add_filter( 'woocommerce_email_classes', 'add_shipped_order_woocommerce_email' );

function add_woo_email(){

}

function send_ship($order_id,$tracking_number="",$tracking_method=""){
  
    $active_plugins = (array) get_option( 'active_plugins', array() );
  if(in_array('woocommerce/woocommerce.php', $active_plugins)){
      //$order = new WC_Order( $order_id );
      $email = new WC_Emails();
      $emails = $email->get_emails();
      $sh_email= $emails['WC_Shipped_Order_Email'];
      $sh_email->trigger($order_id,$tracking_number,$tracking_method);
     
  }
    
    
}

function print_aura_initialisation(){
             // fix some server adding additionals params   
            if(isset($_GET['page']) && !isset($_GET['consumer_key']) && !isset($_GET['consumer_secret'])){
            $request_uri = explode("?",$_SERVER['REQUEST_URI']);
            $params_exp = explode("&",$request_uri[1]);
            foreach($params_exp as $param){
                $val = explode("=",$param);
                $_GET[$val[0]] = $val[1];
            }
            }
         if(isset($_GET['oauth_consumer_key']) && isset($_GET['oauth_signature']) && isset($_GET['oauth_signature_method'])){
	       unset($_GET['q']);
	       unset($_GET['all_ids']);
        }
        // plugin updater section
	$args = array(
            'plugin_name' => 'Print Aura API',                 
            'plugin_slug' => 'printaura-woocommerce-api',                
            'plugin_path' => plugin_basename( __FILE__ ),  
            'plugin_url'  => WP_PLUGIN_URL . '/printaura-woocommerce-api', 
            'version'     => '3.2.8',                    
            'remote_url'  => 'https://printaura.com/printaura-woocommerce-updater/',    
            'time'        => 4230                       
        );
        $config            = new TGM_Updater_Config( $args );
        $namespace_updater = new TGM_Updater( $config );
        $namespace_updater->update_plugins();
	
}
add_action('init', 'print_aura_initialisation');

function pa_disabled_notice(){
     echo '<div id="error" class="error"><br /><strong>Print Aura Woocommere API Plugin requires PHP version 5.3 or higher. Please click <a href="https://printaura.com/woocommerce-server-requirements/" target="_blank"> here</a> for more information about server requirements.</strong><br /><br /></div>';
}

function check_version(){
    if(version_compare(phpversion(),'5.3','<')){
         if ( is_plugin_active( plugin_basename( __FILE__ ) ) ) {
             deactivate_plugins( plugin_basename( __FILE__ ) ); 
             add_action( 'admin_notices','pa_disabled_notice'  );
              if ( isset( $_GET['activate'] ) ) {
	                    unset( $_GET['activate'] );
	                }
         }
      
	        
  }
}
add_action( 'admin_init', 'check_version'  );

function printaura_woocommerce_api_activate() {
  global $wpdb;
   if(version_compare(phpversion(),'5.3','<')){
      deactivate_plugins( plugin_basename( __FILE__ ) );  
      die( __( 'Print Aura Woocommere API Plugin requires PHP version 5.3 or higher. Please click <a href="https://printaura.com/woocommerce-server-requirements/" target="_blank"> here</a> for more information about server requirements.', 'printaura_api' ) );
	        
  }
  $helpers = new JSONAPIHelpers();
  $current_user 	= wp_get_current_user();
  $user_id	= $current_user->ID;
  wp_insert_term('shipped','shop_order_status');
  wp_insert_term('partially shipped','shop_order_status');
  wp_insert_term('T-shirts','product_shipping_class');
  wp_insert_term('Sweatshirts','product_shipping_class');
  wp_insert_term('Bags','product_shipping_class');
  wp_insert_term('Mugs','product_shipping_class');
  wp_insert_term('Pillows','product_shipping_class');
  wp_insert_term('Snapback Hats','product_shipping_class');
  wp_insert_term('Small Posters','product_shipping_class');
  wp_insert_term('Large Posters','product_shipping_class');
  wp_insert_term('Jumbo Posters','product_shipping_class');
  wp_insert_term('Cases','product_shipping_class');
  wp_insert_term('Sunglasses','product_shipping_class');
  $key = $helpers->getPluginPrefix() . '_enabled';
  $key1 = $helpers->getPluginPrefix() . '_token';
  $key2 = $helpers->getPluginPrefix() . '_ips_allowed';
  $from = $helpers->getPluginPrefix() . '_mail_from';
  $subject = $helpers->getPluginPrefix() . '_mail_subject';
  $body = $helpers->getPluginPrefix() . '_mail_body';
  $from_default="From: {Sender} <{EMAIL}>";
  $subject_default = "Ship Notification: #{ORDERNUMBER}";
  $body_default  = '<p>Dear {CustomerFirstName},</p>
<p>The following items have shipped from order #{ORDERNUMBER}</p>

{CLIENT PRODUCT TITLE} - {COLOR} / {SIZE} (Qty: {QTY})
</p>
<p>
The order has been shipped to:
</p>
<p>
{SHIP_ADDRESS}
</p>
<p>via {SHIP_METHOD} (Tracking #: {TRACKINGNUMBER} )</p>
<p>Please note that it may take until the next business day before tracking becomes available.</p>

Thanks for your business.';
  $apiKey = wp_hash_password(date("YmdHis",time()) . rand(1000,99999) . $_SERVER['REMOTE_ADDR'] . SECURE_AUTH_SALT . $user_id);

  update_option($key,'yes');
  if(!get_option($key1)){
    update_option($key1,$apiKey);
    update_user_meta($user_id,$key1,$apiKey);
  }
  update_option($key2,'162.209.60.177');
  update_option($from,$from_default);
  update_option($subject,$subject_default);
  update_option($body,$body_default);
  
  
}
register_activation_hook( __FILE__, 'printaura_woocommerce_api_activate' );

function printaura_woocommerce_api_deactivate() {
  global $wpdb;
  $status1=get_term_by('name','shipped','shop_order_status');
  $status2=get_term_by('name','partially shipped','shop_order_status');
  wp_delete_term(intval($status2->term_id),'shop_order_status');
  wp_delete_term(intval($status1->term_id),'shop_order_status');
}
register_deactivation_hook( __FILE__, 'printaura_woocommerce_api_deactivate' );

function printaura_woocommerce_api_initialize_plugin(){
  $helpers = new JSONAPIHelpers();
}
add_action( 'init', 'printaura_woocommerce_api_initialize_plugin',5000 );

add_action('admin_menu','print_aura_api_admin_menu',10);
add_action('admin_head','admin_css');

function add_shippingclass_route($endpoints){

    $endpoints['/product/shippingclass']=array(array( 'get_pa_product_shipping_class', WC_API_Server::READABLE ));
    $endpoints['/products/tags']=array(array( 'get_pa_product_tags', WC_API_Server::READABLE ));
    $endpoints['/products/tags']=array(array( 'create_pa_product_tags', WC_API_Server::EDITABLE | WC_API_Server::ACCEPT_DATA  ));
    $endpoints['/products/edit_pa']=array(array( 'edit_product_pa', WC_API_Server::EDITABLE | WC_API_Server::ACCEPT_DATA  ));
    $endpoints['/products/delete_pa']=array(array( 'delete_product_pa', WC_API_Server::EDITABLE | WC_API_Server::ACCEPT_DATA  ));
    $endpoints['/store/infos']=array(array( 'get_pa_store_info', WC_API_Server::READABLE ));
    $endpoints['/orders/(?P<order_id>\d+)/shipment']=array(array( 'update_order_item_shipment',  WC_API_Server::EDITABLE | WC_API_Server::ACCEPT_DATA ));
    
    return $endpoints;
    
}
add_filter('woocommerce_api_endpoints','add_shippingclass_route',100,1);

function edit_product_pa($data=array()){

    $server = new WC_API_Server( $wp->query_vars['wc-api-route'] );
    $product_id = $data['product']['id'];
    $product = new \WC_API_Products($server );
    $return =  $product->edit_product($product_id,$data);

return $return;
}

function delete_product_pa($data){
    $server = new WC_API_Server( $wp->query_vars['wc-api-route'] );
    $product = new \WC_API_Products($server );
    return $product->delete_product($data['product'],true);
}

function create_pa_product_tags($data = array()){
     try {
           // Permissions check
           if ( ! current_user_can( 'manage_product_terms' ) ) {
                   throw new WC_API_Exception( 'woocommerce_api_user_cannot_read_product_tags', __( 'You do not have permission to read product tags', 'woocommerce' ), 401 );
           }
           $data        = isset( $data['tags'] ) ? $data['tags'] : array();
           $product_tags = array();
           if(empty($data)){

           }
           else {
               foreach($data as $tag){
                   $term = term_exists($tag, 'product_tag');
                   //wp_mail('alaeddine.nawali@gmail.com','term exists',var_export($term,true));
                   if ($term !== 0 && $term !== null) {//term exists
                       $product_tags[] =  current( get_pa_product_tag( $term['term_id'] ) );
                   }
                   else{//term doesnt exists , create it
                      $term = wp_insert_term( $tag, 'product_tag'); 
                      if(!is_wp_error($term)){
                          $product_tags[] =  current( get_pa_product_tag( $term['term_id'] ) );
                      }
                   }
               }
               return array( 'product_tags' => apply_filters( 'woocommerce_api_product_tags_response', $product_tags ) );
           }
     } catch ( WC_API_Exception $e ) {
			return new WP_Error( $e->getErrorCode(), $e->getMessage(), array( 'status' => $e->getCode() ) );                                    
                        
     }
    
}

function get_pa_product_tags($fields = null){
    try {
			// Permissions check
			if ( ! current_user_can( 'manage_product_terms' ) ) {
				throw new WC_API_Exception( 'woocommerce_api_user_cannot_read_product_tags', __( 'You do not have permission to read product tags', 'woocommerce' ), 401 );
			}
			$product_tags = array();
			$terms = get_terms( 'product_tag', array( 'hide_empty' => false, 'fields' => 'ids' ) );
			foreach ( $terms as $term_id ) {
				$product_tags[] = current( get_pa_product_tag( $term_id, $fields ) );
			}
			return array( 'product_tags' => apply_filters( 'woocommerce_api_product_tags_response', $product_tags, $terms, $fields, $this ) );
		} catch ( WC_API_Exception $e ) {
			return new WP_Error( $e->getErrorCode(), $e->getMessage(), array( 'status' => $e->getCode() ) );
		}
}

function get_pa_product_tag( $id, $fields = null ) {
		try {
			$id = absint( $id );
			// Validate ID
			if ( empty( $id ) ) {
				throw new WC_API_Exception( 'woocommerce_api_invalid_product_tag_id', __( 'Invalid product tag ID', 'woocommerce' ), 400 );
			}
			// Permissions check
			if ( ! current_user_can( 'manage_product_terms' ) ) {
				throw new WC_API_Exception( 'woocommerce_api_user_cannot_read_product_tags', __( 'You do not have permission to read product tags', 'woocommerce' ), 401 );
			}
			$term = get_term( $id, 'product_tag' );
			if ( is_wp_error( $term ) || is_null( $term ) ) {
				throw new WC_API_Exception( 'woocommerce_api_invalid_product_tag_id', __( 'A product tag with the provided ID could not be found', 'woocommerce' ), 404 );
			}
			$term_id = intval( $term->term_id );
			// Get category display type
			$display_type = get_woocommerce_term_meta( $term_id, 'display_type' );
			// Get category image
			$image = '';
			if ( $image_id = get_woocommerce_term_meta( $term_id, 'thumbnail_id' ) ) {
				$image = wp_get_attachment_url( $image_id );
			}
			$product_category = array(
				'id'          => $term_id,
				'name'        => $term->name,
				'slug'        => $term->slug,
				'parent'      => $term->parent,
				'description' => $term->description,
				'display'     => $display_type ? $display_type : 'default',
				'image'       => $image ? esc_url( $image ) : '',
				'count'       => intval( $term->count )
			);
			return array( 'product_category' => apply_filters( 'woocommerce_api_product_category_response', $product_category, $id, $fields, $term, $this ) );
		} catch ( WC_API_Exception $e ) {
			return new WP_Error( $e->getErrorCode(), $e->getMessage(), array( 'status' => $e->getCode() ) );
		}
	}
        
function get_pa_store_info(){
    		// General site data
		$available = array( 'store' => array(
			'name'        => get_option( 'blogname' ),
			'description' => get_option( 'blogdescription' ),
			'URL'         => get_option( 'siteurl' ),
			'wc_version'  => WC()->version,
                        'pa_version'  => '3.2.8'));
                
                return $available;
}

function update_order_item_shipment($order_id,$data){
    		$data        = isset( $data['order'] ) ? $data['order'] : array();
                $send_mail   = ( isset($data['notify_customer']) && $data['notify_customer'] ==true) ? $data['notify_customer'] : false;

		try {
                    
			$id = validate_request_pa( $order_id, 'shop_order', 'edit' );

			
                        if ( is_wp_error( $id ) ) {
				return $id;
			}
                        
                        $order       = wc_get_order( $id );
                       
                        $line_items  = (!empty($data['line_items'])) ? $data['line_items'] : array();
                       
                        if(!empty($line_items )){
                            foreach($line_items as  $line_item){
                                
			        if ( ! array_key_exists( 'id', $line_item ) ) {
                                
					throw new WC_API_Exception( 'woocommerce_invalid_item_id', __( 'Order item ID is required', 'woocommerce' ), 400 );
				 }
                                $line_item_id    = $line_item['id'];
                                $tracking_number = $line_item['tracking'];
                                wc_update_order_item_meta($line_item_id,'tracking_number',$tracking_number);
                                
                            }
                        }
                        
                        $total_items     = count($order->get_items());
                        $total_shipped   = get_total_shipped_items($order_id);
                        $ship_method     = $order->get_shipping_methods() ;
                        foreach($ship_method as $shp_mtd){
                           
                            $shipping_method = $shp_mtd['name'];
                            break;
                        }

                        if($total_items == $total_shipped){
                           
                            $order->update_status('shipped','');
                            if($send_mail){
                                send_ship($order_id, $tracking_number,$shipping_method);
                            }
                            return array('line_items'=>$order->get_items());
                        }
                        else {
                           
                            $order->update_status('wc-partially-shipped','');
                            if($send_mail){
                                send_ship($order_id, $tracking_number,$shipping_method);
                            }
                            return array('line_items'=>$order->get_items());
                        }
   
      
                }
                 catch ( WC_API_Exception $e ) {

			return new WP_Error( $e->getErrorCode(), $e->getMessage(), array( 'status' => $e->getCode() ) );
		}
}

function get_total_shipped_items($order_id){
    
    $order          = wc_get_order( $order_id );
    $line_items          = $order->get_items();
    $total_shipped  = 0;
    foreach($line_items as $line_item){
       
        $tracking  = "";
        if(isset($line_item['tracking_number']) && strlen($line_item['tracking_number'])>0)
            $tracking = $line_item['tracking_number'];

        if($tracking !='' && strlen($tracking) > 0)
            $total_shipped++;
        
    }

    return $total_shipped;
}

function get_pa_product_shipping_class( $fields = null ) {

		// permissions check
		if ( ! current_user_can( 'manage_product_terms' ) ) {
			return new WP_Error( "woocommerce_api_user_cannot_read_product_categories", __( 'You do not have permission to read product categories', 'woocommerce' ), array( 'status' => 401 ) );
		}

		$product_categories = array();

		$terms = get_terms( 'product_shipping_class', array( 'hide_empty' => false, 'fields' => 'ids' ) );

		foreach ( $terms as $term_id ) {

			$product_categories[] = current( get_pa_product_shipping_class_item( $term_id, $fields ) );
		}
                 //return $product_categories;
		return array( 'product_shipping_class' => apply_filters( 'woocommerce_api_product_categories_response', $product_categories, $terms, $fields, $this ) );
                exit();
}

function get_pa_product_shipping_class_item( $id, $fields = null ) {

		$id = absint( $id );

		// validate ID
		if ( empty( $id ) ) {
			return new WP_Error( 'woocommerce_api_invalid_product_category_id', __( 'Invalid product category ID', 'woocommerce' ), array( 'status' => 400 ) );
		}

		// permissions check
		if ( ! current_user_can( 'manage_product_terms' ) ) {
			return new WP_Error( 'woocommerce_api_user_cannot_read_product_categories', __( 'You do not have permission to read product categories', 'woocommerce' ), array( 'status' => 401 ) );
		}

		$term = get_term( $id, 'product_shipping_class' );

		if ( is_wp_error( $term ) || is_null( $term ) ) {
			return new WP_Error( 'woocommerce_api_invalid_product_category_id', __( 'A product category with the provided ID could not be found', 'woocommerce' ), array( 'status' => 404 ) );
		}

		$product_category = array(
			'id'          => intval( $term->term_id ),
			'name'        => $term->name,
			'slug'        => $term->slug,
			'parent'      => $term->parent,
			'description' => $term->description,
			'count'       => intval( $term->count ),
		);

		return array( 'product_shipping_class' => apply_filters( 'woocommerce_api_product_category_response', $product_category, $id, $fields, $term, $this ) );
	}
         
function validate_request_pa( $id, $type, $context ) {
         if ( 'shop_order' === $type || 'shop_coupon' === $type || 'shop_webhook' === $type )
			$resource_name = str_replace( 'shop_', '', $type );
		else
			$resource_name = $type;

		$id = absint( $id );

		// validate ID
		if ( empty( $id ) )
			return new WP_Error( "woocommerce_api_invalid_{$resource_name}_id", sprintf( __( 'Invalid %s ID', 'woocommerce' ), $type ), array( 'status' => 404 ) );

		// only custom post types have per-post type/permission checks
		if ( 'customer' !== $type ) {

			$post = get_post( $id );

			// for checking permissions, product variations are the same as the product post type
			$post_type = ( 'product_variation' === $post->post_type ) ? 'product' : $post->post_type;

			// validate post type
			if ( $type !== $post_type )
				return new WP_Error( "woocommerce_api_invalid_{$resource_name}", sprintf( __( 'Invalid %s', 'woocommerce' ), $resource_name ), array( 'status' => 404 ) );

			// validate permissions
			switch ( $context ) {

				case 'read':
					if ( ! is_pa_readable( $post ) )
						return new WP_Error( "woocommerce_api_user_cannot_read_{$resource_name}", sprintf( __( 'You do not have permission to read this %s', 'woocommerce' ), $resource_name ), array( 'status' => 401 ) );
					break;

				case 'edit':
					if ( ! is_pa_editable( $post ) )
						return new WP_Error( "woocommerce_api_user_cannot_edit_{$resource_name}", sprintf( __( 'You do not have permission to edit this %s', 'woocommerce' ), $resource_name ), array( 'status' => 401 ) );
					break;

				case 'delete':
					if ( ! is_pa_deletable( $post ) )
						return new WP_Error( "woocommerce_api_user_cannot_delete_{$resource_name}", sprintf( __( 'You do not have permission to delete this %s', 'woocommerce' ), $resource_name ), array( 'status' => 401 ) );
					break;
			}
		}

		return $id;
	}  
        
function check_permission_pa( $post, $context ) {

		if ( ! is_a( $post, 'WP_Post' ) )
			$post = get_post( $post );

		if ( is_null( $post ) )
			return false;

		$post_type = get_post_type_object( $post->post_type );

		if ( 'read' === $context )
			return current_user_can( $post_type->cap->read_private_posts, $post->ID );

		elseif ( 'edit' === $context )
			return current_user_can( $post_type->cap->edit_post, $post->ID );

		elseif ( 'delete' === $context )
			return current_user_can( $post_type->cap->delete_post, $post->ID );

		else
			return false;
	}     
        
function is_pa_readable( $post ) {

		return check_permission_pa( $post, 'read' );
	}
        
function is_pa_editable( $post ) {

		return check_permission_pa( $post, 'edit' );

	}
        
function is_pa_deletable( $post ) {

		return check_permission_pa( $post, 'delete' );
	}

function add_pa_order_statuses($order_statuses){
    
    $order_statuses['wc-shipped'] = _x( 'Shipped', 'Order status', 'woocommerce' );
    $order_statuses['wc-parially-shipped'] = _x( 'Partially Shipped', 'Order status', 'woocommerce' );
    
    return $order_statuses;
}
add_filter( 'wc_order_statuses','add_pa_order_statuses' );

function pa_attach_tags($product_id,$terms,$type='cat'){
    
    if(!empty($terms)){
        $product_terms= array();
        foreach($terms as $ter){
            $term = term_exists($ter, 'product_'.$type);
                   
                   if ($term !== 0 && $term !== null) {//term exists
                       $product_terms[] = (int)  $term['term_id'] ;
                   }
                   else{//term doesnt exists , create it
                      $term = wp_insert_term( $ter, 'product_'.$type); 
                                        

                      if(!is_wp_error($term)){
                          
                          $product_terms[] = (int) $term['term_id'];
                      }
                   }
        }
        
        if(!empty($product_terms)){
          

            wp_set_object_terms( $product_id, $product_terms, 'product_'.$type );
        }
    }
}

function pa_after_add_product($id,$data){
    pa_attach_tags($id,$data['tags'],'tag');   
}
add_action( 'woocommerce_api_create_product','pa_after_add_product',2,2);

function pa_after_edit_product($id,$data){
    
     $product        = new WC_Product_Variable($id);
     $variations     = $product->get_available_variations();
     $all_attributes = array();
     foreach($data['attributes'] as $attributes){
	      if(is_array($attributes['options']))
	     $all_attributes = array_merge($all_attributes,$attributes['options']);  
	     else  $all_attributes = array_merge($all_attributes,explode( ' | ', $attributes['options']));                       
                
     }
         
     foreach($all_attributes as $key=>$value){
         $all_attributes[] = sanitize_title(strtolower($value));
     }

     foreach($variations as $variation){
         foreach($variation['attributes'] as $attr=>$attribute_value){
             
             if(in_array($attr,array('attribute_color','attribute_size','attribute_pa_color','attribute_pa_size','attribute_pa_color-g','attribute_pa_size-g'))){
                        //$attribute_value = implode(" ",explode('-',$attribute_value));
                       
                        if(!in_array($attribute_value,$all_attributes)){    

                           
                         wp_delete_post(  $variation['variation_id'], true ) ;
                        break;
                        }
                     
             }
         }
     }
     
     pa_attach_tags($id,$data['tags'],'tag');
    
}
add_action( 'woocommerce_api_edit_product','pa_after_edit_product',2,2);

function append_woocommerce_key_to_payload($payload,$resource = "",$resource_id ="",$webhook_id = ""){                                
    global $wpdb;
    $keys1 	      =  $wpdb->get_col(  "SELECT  consumer_key FROM {$wpdb->prefix}woocommerce_api_keys" );
    $consumer_key     =  $wpdb->get_col('select meta_value from '.$wpdb->prefix.'usermeta where meta_key="woocommerce_api_consumer_key"');
    $payload['key']   = array_merge($consumer_key,  $keys1);    
    return $payload;
}
add_filter( 'woocommerce_webhook_payload','append_woocommerce_key_to_payload');

function increase_max_webhook_failure($count){
    return $count+1000000000000;
}
add_filter( 'woocommerce_max_webhook_delivery_failures', 'increase_max_webhook_failure' );

function fix_woocommrce_description($id, $data){
    //global $wpdb;
    $description       = stripslashes(wp_filter_post_kses(addslashes($data['description']))); 
    $short_description = stripslashes(wp_filter_post_kses(addslashes($data['short_description'])));
      $my_post = array(
      'ID'           => $id,
      'post_content' => $description,
      'post_excerpt' => $short_description   
  );

// Update the post into the database
  wp_update_post( $my_post );
    
           
}
add_action('woocommerce_api_create_product','fix_woocommrce_description',10,2);
add_action('woocommerce_api_edit_product','fix_woocommrce_description',10,2);

function fix_order_hook($topic_hooks){
  
$topic_hooks['order.created'][]  = 'woocommerce_order_status_processed';
$topic_hooks['order.updated'][]  = 'woocommerce_order_status_processed';
  
return $topic_hooks;

}
add_filter('woocommerce_webhook_topic_hooks','fix_order_hook');

function send_woo_processed($id){

$query_args = array(
			'fields'      => 'ids',
			'post_type'   => 'shop_webhook',
		);
$webhooks   =  new WP_Query( $query_args );

if($webhooks){
foreach( $webhooks->posts as $webhook_id ) {
$webhook = new WC_Webhook( $webhook_id );
if($webhook->get_topic() == 'order.created' ){
$webhook->process($id);
break;
}
}
}
}
add_action('woocommerce_order_status_processing','send_woo_processed');

function register_pa_custom_order_status() {
@ini_set( 'upload_max_size' , '200M' );
@ini_set( 'post_max_size', '200M');
@ini_set( 'max_execution_time', '800' );
    register_post_status( 'wc-shipped', array(
        'label'                     => 'Shipped',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Shipped <span class="count">(%s)</span>', 'Shipped <span class="count">(%s)</span>' )
    ) );
    register_post_status( 'wc-partially-shipped', array(
        'label'                     => 'Partially Shipped',
        'public'                    => true,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Partially Shipped <span class="count">(%s)</span>', 'Partially Shipped <span class="count">(%s)</span>' )
    ) );
}
add_action( 'init', 'register_pa_custom_order_status' );

function add_pa_custom_order_statuses( $order_statuses ) {
 
    $new_order_statuses = array();
 
    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {
 
        $new_order_statuses[ $key ] = $status;
 
        if ( 'wc-processing' === $key ) {
            $new_order_statuses['wc-shipped'] = 'Shipped';
            $new_order_statuses['wc-partially-shipped'] = 'Patially Shipped';
        }
    }
 
    return $new_order_statuses;
}
add_filter( 'wc_order_statuses', 'add_pa_custom_order_statuses' );

function fix_password_length_webhook($webhook_data){
    $webhook_data['post_password'] = substr(   $webhook_data['post_password'], 0, 19 );
    return $webhook_data;
}
add_filter('woocommerce_new_webhook_data','fix_password_length_webhook');

function wooc_delete_variants($product_id){
    global $wpdb;
    $products_variants = $wpdb->get_results("select ID from `wp_posts` where post_parent=$product_id and post_type='product_variation'");
    $variants_ids = "";
    foreach($products_variants as $product){
        $variants_ids .= $product->ID.",";
    }
    $variants_ids = rtrim($variants_ids,",");
    $wpdb->query("delete from wp_postmeta where post_id in($variants_ids)");
    $wpdb->query("delete from wp_posts where post_parent=$product_id ");
}
add_action('woocommerce_api_delete_product','wooc_delete_variants');
